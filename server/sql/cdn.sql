/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.12-log : Database - cdn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cdn` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `cdn`;

/*Table structure for table `t_alarm` */

DROP TABLE IF EXISTS `t_alarm`;

CREATE TABLE `t_alarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `moduleid` char(64) DEFAULT NULL COMMENT '告警模块ID',
  `serviceid` char(64) DEFAULT NULL COMMENT '告警服务ID',
  `alarmid` int(20) DEFAULT '0' COMMENT '告警ID',
  `alarmlevel` int(11) DEFAULT '0' COMMENT '告警级别 1:一般 2:提示 3:警告  4:严重 5:致命',
  `alarmname` char(64) DEFAULT NULL COMMENT '告警名称',
  `starttime` bigint(20) DEFAULT '0' COMMENT '告警开始时间，UTC秒时间戳',
  `stoptime` bigint(20) DEFAULT '0' COMMENT '告警结束时间,UTC秒时间戳',
  `alarmdes` varchar(256) DEFAULT NULL COMMENT '告警描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_alarm` */

/*Table structure for table `t_channel` */

DROP TABLE IF EXISTS `t_channel`;

CREATE TABLE `t_channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `channelname` char(128) DEFAULT NULL COMMENT '频道名',
  `nodeid` char(64) DEFAULT NULL COMMENT '录制节点ID',
  `providerid` char(64) DEFAULT NULL COMMENT '提供商ID',
  `assetid` char(64) DEFAULT NULL COMMENT '媒资id',
  `sourceurl` varchar(512) DEFAULT NULL COMMENT '收流地址',
  `sourcetype` char(128) DEFAULT NULL COMMENT '源类型,默认:CDefaultRTITask',
  `biterate` int(11) DEFAULT '0' COMMENT '传输码率,可以为空',
  `slicedur` int(11) DEFAULT '0' COMMENT '切片时长，单位为s',
  `recordedur` int(11) DEFAULT '0' COMMENT '录制时长，单位为小时',
  `storepath` varchar(256) DEFAULT NULL COMMENT '存储路径',
  `isenable` int(11) DEFAULT '0' COMMENT '是否启动，0:不启用 1：启用',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_channel` */

/*Table structure for table `t_citask` */

DROP TABLE IF EXISTS `t_citask`;

CREATE TABLE `t_citask` (
  `taskid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `cmd` int(11) DEFAULT NULL COMMENT '0:注入 1: 取消 2：删除',
  `providerid` char(64) DEFAULT NULL COMMENT '提供商ID',
  `assetid` char(64) DEFAULT NULL COMMENT '媒资ID',
  `servicetype` int(11) DEFAULT '0' COMMENT '业务类型(可以支持的协议):0:普通文件类型 1:文本文件类型 2:图片文件类型 3:HLS协议，需要生成对应m3u8文件 4:MP4文件类型 5：NGOD协议，生成对应ngod索引 6：HLS+NGOD协议',
  `url` varchar(512) DEFAULT NULL COMMENT '注入下载文件url',
  `nodeid` char(32) DEFAULT NULL COMMENT '分配的节点ID',
  `filesize` bigint(20) DEFAULT '0' COMMENT '文件大小',
  `duration` bigint(20) DEFAULT '0' COMMENT '文件时长',
  `bitrate` int(11) DEFAULT '0' COMMENT '文件码率,非视频文件为0',
  `status` int(11) DEFAULT '0' COMMENT '注入状态 0:Waiting 等待注入 1：Transfer 内容正在注入 2：Complete完成',
  `progress` int(11) DEFAULT '0' COMMENT '注入进度',
  `createtime` bigint(20) DEFAULT '0' COMMENT '任务创建时间',
  `starttime` bigint(20) DEFAULT '0' COMMENT '任务开始时间',
  `endtime` bigint(20) DEFAULT '0' COMMENT '任务结束时间',
  `path` char(128) DEFAULT NULL COMMENT '存储路径',
  `errcode` int(11) DEFAULT '0' COMMENT '注入结果',
  `errdes` varchar(256) DEFAULT NULL COMMENT '注入结果描述',
  `reporturl` varchar(512) DEFAULT NULL COMMENT '上报状态url',
  `md5` char(64) DEFAULT NULL COMMENT '内容对应的md5值',
  PRIMARY KEY (`taskid`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_citask` */

/*Table structure for table `t_conf` */

DROP TABLE IF EXISTS `t_conf`;

CREATE TABLE `t_conf` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `paramname` char(32) NOT NULL COMMENT '参数名',
  `paramtype` int(11) DEFAULT '0' COMMENT '参数类型 1:int 2:string',
  `paramvalue` varchar(512) DEFAULT NULL COMMENT '参数值',
  `paramdec` varchar(128) DEFAULT NULL COMMENT '参数说明',
  `serviceid` char(64) DEFAULT NULL COMMENT '参数所属模块',
  `lasttime` bigint(20) DEFAULT '0' COMMENT '参数上次更新时间戳',
  `updatetime` bigint(20) DEFAULT '0' COMMENT '参数更新时间戳',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`paramname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_conf` */

/*Table structure for table `t_file_distribute` */

DROP TABLE IF EXISTS `t_file_distribute`;

CREATE TABLE `t_file_distribute` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `providerid` char(64) NOT NULL COMMENT '提供商ID',
  `assetid` char(64) NOT NULL COMMENT '媒资ID',
  `filename` char(128) NOT NULL COMMENT '文件名',
  `path` varchar(128) DEFAULT NULL COMMENT '存储路径',
  `nodeid` char(32) NOT NULL COMMENT '节点ID',
  `createtime` bigint(20) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`providerid`,`assetid`,`nodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_file_distribute` */

/*Table structure for table `t_file_info` */

DROP TABLE IF EXISTS `t_file_info`;

CREATE TABLE `t_file_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `providerid` char(255) NOT NULL COMMENT '提供商ID',
  `assetid` char(255) NOT NULL COMMENT '媒资ID',
  `servicetype` int(11) DEFAULT NULL COMMENT '业务类型(可以支持的协议):0:普通文件类型 1:文本文件类型 2:图片文件类型 3:HLS协议，需要生成对应m3u8文件 4:MP4文件类型 5：NGOD协议，生成对应ngod索引 6：HLS+NGOD协议',
  `filename` char(255) DEFAULT NULL COMMENT '文件名',
  `filesize` bigint(20) unsigned DEFAULT '0' COMMENT '文件大小',
  `duration` bigint(20) DEFAULT '0' COMMENT '文件时长',
  `bitrate` int(11) unsigned DEFAULT '0' COMMENT '文件码率,非视频文件为0',
  `updateTime` bigint(20) DEFAULT '0' COMMENT '更新时间',
  `md5` char(64) DEFAULT NULL COMMENT '内容对应的md5值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`providerid`,`assetid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_file_info` */

/*Table structure for table `t_node` */

DROP TABLE IF EXISTS `t_node`;

CREATE TABLE `t_node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `nodeid` char(64) NOT NULL COMMENT '节点ID',
  `nodetype` int(11) DEFAULT '0' COMMENT '节点类型  0:CI注入节点  1:RTI节点',
  `path` varchar(256) DEFAULT NULL COMMENT '节点存储路径',
  `maxsize` bigint(20) DEFAULT '0' COMMENT '最大空间',
  `usesize` bigint(20) DEFAULT '0' COMMENT '使用空间',
  `reservesize` bigint(20) DEFAULT '0' COMMENT '保留空间',
  `privatestreamaddr` char(128) DEFAULT NULL COMMENT '内网推流地址',
  `publicstreamaddr` char(128) DEFAULT NULL COMMENT '外网推流地址',
  `freebandwidth` bigint(20) DEFAULT '0' COMMENT '空闲带宽',
  `totalbandwidth` bigint(20) DEFAULT '0' COMMENT '总带宽',
  `hearttime` bigint(20) DEFAULT '0' COMMENT '心跳时间戳',
  `isonline` int(11) DEFAULT '0' COMMENT '节点是否在线，只针对CI类型节点。0:离线 1:在线',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`nodeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_node` */

/*Table structure for table `t_subnode` */

DROP TABLE IF EXISTS `t_subnode`;

CREATE TABLE `t_subnode` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `subareacode` char(64) NOT NULL COMMENT '子节点区域码',
  `subinaddress` char(128) NOT NULL COMMENT '子节点注入地址',
  `subvodaddress` char(128) NOT NULL COMMENT '子节点点播地址',
  `submanageurl` varchar(256) DEFAULT NULL COMMENT '子节点管理地址',
  `remarks` varchar(256) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`subareacode`,`subinaddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_subnode` */

/*Table structure for table `t_sysmonitor` */

DROP TABLE IF EXISTS `t_sysmonitor`;

CREATE TABLE `t_sysmonitor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `serviceid` char(64) NOT NULL COMMENT '服务器ID,监控服务ID',
  `servicename` char(64) DEFAULT NULL COMMENT '服务名称',
  `version` varchar(256) DEFAULT NULL COMMENT '服务版本号',
  `sysmonitorurl` varchar(256) DEFAULT NULL COMMENT '系统监控url地址',
  `modulename` char(64) DEFAULT NULL COMMENT '模块名称',
  `runstatus` int(11) DEFAULT '0' COMMENT '运行状态  0:停止 1:正在启动 2:运行',
  `laststarttime` bigint(20) DEFAULT '0' COMMENT '最新启动UTC时间戳',
  `iscontrol` int(11) DEFAULT '0' COMMENT '表示服务是否可以操作 0:表示不可以操作  1:表示可操作',
  `control` int(11) DEFAULT '0' COMMENT '服务控制 0:禁止 1:停止 2:启动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_sysmonitor` */

/*Table structure for table `t_task_distribute` */

DROP TABLE IF EXISTS `t_task_distribute`;

CREATE TABLE `t_task_distribute` (
  `taskid` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `providerid` char(64) NOT NULL COMMENT '提供商ID',
  `assetid` char(64) NOT NULL COMMENT '媒资ID',
  `servicetype` int(11) NOT NULL COMMENT '业务类型(可以支持的协议):0:普通文件类型 1:文本文件类型 2:图片文件类型 3:HLS协议，需要生成对应m3u8文件 4:MP4文件类型 5：NGOD协议，生成对应ngod索引 6：HLS+NGOD协议',
  `url` varchar(512) DEFAULT NULL COMMENT '文件下载url',
  `subnodeaisaddr` char(128) DEFAULT NULL COMMENT '子节点注入地址',
  `status` int(11) DEFAULT '0' COMMENT '注入状态 0:Waiting 等待分发 1：Transfer 内容正在分发 2：Complete 分发完成',
  `progress` int(11) DEFAULT '0' COMMENT '分发进度',
  `createtime` bigint(20) DEFAULT '0' COMMENT '开始时间',
  `endtime` bigint(20) DEFAULT '0' COMMENT '结束时间',
  `errcode` int(11) DEFAULT '0' COMMENT '执行结果',
  `errdes` varchar(256) DEFAULT NULL COMMENT '执行结果描述',
  PRIMARY KEY (`taskid`),
  KEY `taskid` (`taskid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_task_distribute` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `userID` varchar(128) NOT NULL,
  `userName` varchar(128) DEFAULT NULL,
  `userPasswd` varchar(128) DEFAULT NULL,
  `userRole` int(8) DEFAULT NULL,
  `userPhone` varchar(32) DEFAULT NULL,
  `userMailbox` varchar(32) DEFAULT NULL,
  `userSafeState` int(8) DEFAULT NULL,
  `userSafeAddress` varchar(32) DEFAULT NULL,
  `userLoginCount` int(16) DEFAULT '0',
  `userLoginTime` date DEFAULT NULL,
  `userAccountTime` date DEFAULT NULL,
  `userExpiryDateCount` int(16) DEFAULT NULL,
  `userExpiryDate` date DEFAULT NULL,
  `userPassExpiryDateCount` int(16) DEFAULT NULL,
  `userPassExpiryDate` date DEFAULT NULL,
  `userSessionTime` int(16) DEFAULT NULL,
  `userSort` int(16) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`userID`,`userName`,`userPasswd`,`userRole`,`userPhone`,`userMailbox`,`userSafeState`,`userSafeAddress`,`userLoginCount`,`userLoginTime`,`userAccountTime`,`userExpiryDateCount`,`userExpiryDate`,`userPassExpiryDateCount`,`userPassExpiryDate`,`userSessionTime`,`userSort`) values ('73d13858-3487-448c-861a-206370aebf98','root','go6xmIW/l7ZZlm8m3aj81Q==',0,NULL,NULL,NULL,NULL,22,'2016-08-15','2016-05-17',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
