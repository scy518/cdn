#!/bin/sh
#*************************************************************************
# Copyright (c) 2011-2030, 
#
# Author	: scy 907818
# 修改    : 更新linux sdk库
# Date		: 2014-07-08 #
#*************************************************************************
#清除本地sdk库
rm -rf sdk_*$1*.tar.gz

#创建临时目录
mkdir tmpsdk
svn checkout svn://120.24.253.78/svn/project/sdk/3.code/trunk/version/$1/ tmpsdk --username checksdk --password checksdk123456
mv ./tmpsdk/*.tar.gz ./

#删除临时目录
rm -rf tmpsdk








