/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udprecord.cpp
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : udp录制管理
       
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libvideo/hlsvideo.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/udpliverecord.h"
#include "libudprtistaticconf.h"
/*******************************************************************************
 Fuction name:CUDPLiveRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPLiveRecord::CUDPLiveRecord()
{
	m_UdpSource  = NULL;
	m_HLSVideo   = NULL;
}
/*******************************************************************************
 Fuction name:CHLSRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CUDPLiveRecord::~CUDPLiveRecord()
{
	//停止线程
	this->StopTask();

	if (m_UdpSource != NULL)
	{
		delete m_UdpSource;
		m_UdpSource = NULL;
	}

	if (m_HLSVideo != NULL)
	{
		delete m_HLSVideo;
		m_HLSVideo = NULL;
	}
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPLiveRecord::Init(const char *url, const char *localip, const char *storpath, Int32 splitdur)
{
	if (url == NULL || storpath == NULL || localip == NULL)
	{
		return -1;
	}

	m_HLSVideo = new CHLSVideo();
	Int32 ret = m_HLSVideo->Init(storpath, splitdur);
	if (ret != 0)
	{
		LOG_ERROR("hls record init fail.ret:"<<ret);
		return -2;
	}

	m_UdpSource = new CUDPSource();
	ret = m_UdpSource->Init(url, localip, m_HLSVideo, CLibUDPRTIStaticConf::m_RevBuffSize);
	if (ret != 0)
	{
		LOG_ERROR("udp source init fail.ret:"<<ret);
		return -3;
	}

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("run task fail")
		return -4;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :线程调用函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Bool CUDPLiveRecord::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//更新接收数据
		RefreshData();
	}

	return TRUE;
}
/*******************************************************************************
 Fuction name:RefreshData
 Description :更新接收数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CUDPLiveRecord::RefreshData()
{
	if (m_UdpSource == NULL || m_HLSVideo == NULL)
	{
		return -1;
	}

	//接收数据
	m_UdpSource->ReceiveDate();

	return 0;
}













