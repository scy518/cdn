/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsrtitask.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/udprtitask.h"
#include "libudprtistaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CUDPRTITask, CRTITaskInterface)
/*******************************************************************************
*  Function   : CUDPRTITask
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CUDPRTITask::CUDPRTITask()
{
	m_UDPLiveRecord = NULL;
}
/*******************************************************************************
*  Function   : ~CHLSRTITask()
*  Description: 析构函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CUDPRTITask::~CUDPRTITask()
{
	StopRecordTask();
}
/*******************************************************************************
*  Function   : StartRecordTask
*  Description: 开启直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CUDPRTITask::StartRecordTask()
{
	if (m_UDPLiveRecord != NULL)
	{
		LOG_ERROR("start recore is exist.m_UDPLiveRecord is no null")
		return -1;
	}

	//开启udp
	m_UDPLiveRecord = new CUDPLiveRecord();
	if (m_UDPLiveRecord == NULL)
	{
		LOG_DEBUG("new CUDPLiveRecord fail")
		return -2;
	}

	//初始化live hls下载
	SInt32 ret = m_UDPLiveRecord->Init(m_LiveChannel.m_SourceUrl.c_str(), 
		                               CLibUDPRTIStaticConf::m_LocalIP.c_str(),
		                               m_StoreFullPath.c_str(),
		                               m_LiveChannel.m_SliceDur);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("init udp live fail. ret:"<<ret)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
*  Function   : StopTask
*  Description: 停止直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32	CUDPRTITask::StopRecordTask()
{
	if (m_UDPLiveRecord != NULL)
	{
		delete m_UDPLiveRecord;
		m_UDPLiveRecord = NULL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : IsReStartRecordTask
*  Description: 检查是否需要重启,在录制返回TRUE, 否则返回FALSE
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CUDPRTITask::IsReStartRecordTask()
{
	return TRUE;
}








