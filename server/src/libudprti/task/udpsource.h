#ifndef UDP_SOURCE_H
#define UDP_SOURCE_H
/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: udpsource.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#include "libutil/UDPSocket.h"
#include "libvideo/hlsvideo.h"
#include "libudprti/libudprtimacros.h"

class CUDPSource
{
public:

	CUDPSource();

	virtual ~CUDPSource();

	Int32 Init(const char *url, const char *localip, CHLSVideo *hlsvideo, Int32 buffsize = DEFAULT_REVBUFF_SIZE);

	//接收数据
	Int32 ReceiveDate();

private:

	CSockAddr               m_UrlAddr;
	CSockAddr               m_LocaleIP;

	UDPSocket               *m_UDPSocket;

	CHLSVideo               *m_HLSVideo;

	UChar                   *m_RevBuff;    

	Int32                   m_RevBuffSize;             
};




#endif



