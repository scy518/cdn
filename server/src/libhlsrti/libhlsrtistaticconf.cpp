/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhlsrti/libhlsrtimacros.h"
#include "libhlsrtistaticconf.h"

//下载hls分片buff大小
SInt32	CLibHLSRTIStaticConf::m_HlsDataBuffSize = 0;

//下载m3u8空间大小
 SInt32	CLibHLSRTIStaticConf::m_Hlsm3u8BuffSize = 0;

 //刷新m3u8文件时间间隔,单位为秒
SInt32 CLibHLSRTIStaticConf::m_HlsRefreshTime = 10;

//一次下载m3u8分片个数
SInt32 CLibHLSRTIStaticConf::m_HlsOneceDownLoadNum = 2;

//下载子m3u8索引,从0开始
SInt32 CLibHLSRTIStaticConf::m_HlsSubM3U8Index = 0;

//下载超时时间
SInt32 CLibHLSRTIStaticConf::m_HlsDownLoadTimeOut = 30000;

IMPLEMENT_SINGLETON(CLibHLSRTIStaticConf)
/*******************************************************************************
*  Function   : CLibHLSRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibHLSRTIStaticConf::CLibHLSRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibHLSRTIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibHLSRTIStaticConf::~CLibHLSRTIStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibHLSRTIStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibHLSRTIStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibHLSRTIStaticConf::SynStaticParam()
{
	//下载hls分片buff大小
	CLibHLSRTIStaticConf::m_HlsDataBuffSize = GET_PARAM_INT(HLS_RTI_DATA_BUFF_SIZE, 15*1024*1024);

	//下载hls分片buff大小
	CLibHLSRTIStaticConf::m_Hlsm3u8BuffSize = GET_PARAM_INT(HLS_RTI_M3U8_BUFF_SIZE, 2*1024*1024);

	//刷新m3u8文件时间间隔,单位为秒
	CLibHLSRTIStaticConf::m_HlsRefreshTime = GET_PARAM_INT(HLS_RTI_REFRESH_TIME, 10);

	//一次下载m3u8分片个数
	CLibHLSRTIStaticConf::m_HlsOneceDownLoadNum = GET_PARAM_INT(HLS_ONECE_DOWNLOAD_NUM, 2);

	//下载子m3u8索引,从0开始
	CLibHLSRTIStaticConf::m_HlsSubM3U8Index = GET_PARAM_INT(HLS_SUB_M3U8_INDEX, 0);

	//下载超时时间
	CLibHLSRTIStaticConf::m_HlsDownLoadTimeOut = GET_PARAM_INT(HLS_DOWNLOAD_TIMEOUT, 30000);;

	return;
}









