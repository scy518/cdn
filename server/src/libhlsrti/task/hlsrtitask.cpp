/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsrtitask.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/hlsrtitask.h"

CONF_IMPLEMENT_DYNCREATE(CHLSRTITask, CRTITaskInterface)
/*******************************************************************************
*  Function   : CHLSRTITask
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHLSRTITask::CHLSRTITask()
{
	m_LiveHlsDownLoad = NULL;
}
/*******************************************************************************
*  Function   : ~CHLSRTITask()
*  Description: 析构函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHLSRTITask::~CHLSRTITask()
{
	StopRecordTask();
}
/*******************************************************************************
*  Function   : StartRecordTask
*  Description: 开启直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHLSRTITask::StartRecordTask()
{
	if (m_LiveHlsDownLoad != NULL)
	{
		LOG_ERROR("start recore is exist.m_LiveHlsDownLoad is null")
		return -1;
	}

	//开启hls下载
	m_LiveHlsDownLoad = new CLiveHLSDownload();
	if (m_LiveHlsDownLoad == NULL)
	{
		LOG_DEBUG("new CLiveHLSDownload fail")
		return -2;
	}

	//初始化live hls下载
	SInt32 ret = m_LiveHlsDownLoad->Init(m_LiveChannel.m_SourceUrl.c_str(), 
		                                 m_StoreFullPath.c_str(),
		                                 m_LiveChannel.m_SliceDur);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("init live hls down load fail. ret:"<<ret)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
*  Function   : StopTask
*  Description: 停止直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32	CHLSRTITask::StopRecordTask()
{
	if (m_LiveHlsDownLoad != NULL)
	{
		delete m_LiveHlsDownLoad;
		m_LiveHlsDownLoad = NULL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : IsReStartRecordTask
*  Description: 检查是否需要重启,在录制返回TRUE, 否则返回FALSE
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CHLSRTITask::IsReStartRecordTask()
{
	return TRUE;
}








