#ifndef HLS_RTI_TASK_H
#define HLS_RTI_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsrtitask.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "task/livehlsdownload.h"
#include "librtm/rtitaskinterface.h"

class CHLSRTITask : public CRTITaskInterface
{
	CONF_DECLARE_DYNCREATE(CHLSRTITask)
public:

	CHLSRTITask();

	virtual ~CHLSRTITask();

	//开启直播任务
	virtual SInt32 StartRecordTask();

	//停止直播任务
	virtual SInt32	StopRecordTask();

	//检查是否需要重启,在录制返回TRUE, 否则返回FALSE
	virtual Bool IsReStartRecordTask();

private:

	CLiveHLSDownload             *m_LiveHlsDownLoad;
};


#endif





