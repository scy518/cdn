#ifndef LIVE_HLS_DOWNLOAD_H
#define LIVE_HLS_DOWNLOAD_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    livehlsdownload.h
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libcdnutil/hlsdownload.h"

class CLiveHLSDownload : public CHLSDownload, public OSTask
{
public:
	CLiveHLSDownload();

	virtual ~CLiveHLSDownload();

	//初始化url
	virtual Int32 Init(const char *url, const char *storepath, SInt32 splitdur = 10);

	//线程调用函数
	virtual Bool Run();

protected:

	//保存下载数据
	virtual Int32 SaveMediaData(const char *buff, SInt32 len);

	//刷新m3u8数据
	void RefreshLive();

private:

	SInt64                      m_LastTime;

	SInt64                      m_CurSeq;  //本地保存序列号，也就是文件名

	SInt32                      m_SplitDur; //分片文件时长

	std::string                 m_StorePath; //录制文件存储路径
};


#endif
