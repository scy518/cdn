#ifndef LIB_HLS_RTI_STATIC_CONF_H
#define LIB_HLS_RTI_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libhlsrtistaicconf.h
*  Description:  rti静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>

class CLibHLSRTIStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibHLSRTIStaticConf)
public:

	//初始化线程
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//下载m3u8空间大小
	static SInt32						  m_Hlsm3u8BuffSize;

	//下载hls分片buff大小
	static SInt32						  m_HlsDataBuffSize;

	//刷新m3u8文件时间间隔,单位为秒
	static SInt32						  m_HlsRefreshTime;

	//一次下载m3u8分片个数
	static SInt32                         m_HlsOneceDownLoadNum;

	//下载子m3u8索引,从0开始
	static SInt32                         m_HlsSubM3U8Index;

	//下载超时时间
	static SInt32                         m_HlsDownLoadTimeOut;

protected:

	//同步参数
	void SynStaticParam();

};
#endif



