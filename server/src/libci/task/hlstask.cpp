/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsvideoingestimp.cpp
*  Description:  hls协议视频注入，只生成m3u8
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libvideo/libvideoapi.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/hlstask.h"
#include "mgr/diskmanage.h"
#include "libcistaticconf.h"
#include <fstream>

INGEST_TASK_REGISTER_DYNCREATE(SERVICE_TYPE_HLS, CHLSTask, CCITask)
/*******************************************************************************
*  Function   : CHLSTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHLSTask::CHLSTask()
{
}
/*******************************************************************************
*  Function   : ~CHLSTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHLSTask::~CHLSTask()
{
}
/*******************************************************************************
*  Function   : CHLSVideoIngestImp
*  Description: 处理注入消息,不同的注入类型对应不同的注入接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHLSTask::StartTask(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	//下载文件
	Int32 ret = DownLoadFile(tmptask);
	if (ret != 0)
	{
		return CDN_ERR_DOWN_LOAD_FAIL;
	}

	//创建m3u8
	std::string filename = GetFileName(tmptask);
	Int64 duration = 0;
	Int32 biteRate = CreateM3U8File(filename.c_str(), tmptask->m_SpliceDur, duration);
	if (biteRate <= 0)
	{
		return CDN_ERR_NO_SUPPORT_CONTENT;
	}

	//生成试看m3u8文件
	ret = CreateTryPlayM3U8File(filename.c_str(), tmptask->m_SpliceDur);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_WARN("create try file fail.ret:"<<ret);
	}

	//获取时长
	tmptask->m_Duration = duration;
	tmptask->m_BiteRate = biteRate/1024;;

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : DeleteContent
*  Description: 删除内容接口
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHLSTask::DeleteContent(void *taskitem)
{
	IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	std::string filename = tmptask->m_ContentKey.m_ProviderID + "_" + tmptask->m_ContentKey.m_AssetID;
	CDiskManage::Intstance()->DeleteFile(filename);
	CDiskManage::Intstance()->DeleteFile(filename+".m3u8");
	CDiskManage::Intstance()->DeleteFile(filename+"_T.m3u8");

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : CreateM3U8File
*  Description: 生成m3u8文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 源文件名
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHLSTask::CreateM3U8File(const char *filename, Int32 slicedur, Int64 &dur)
{
	return LibVideo::CreateM3U8AndBitRateDur(filename, slicedur, dur);
}
/*******************************************************************************
*  Function   : CreateTryPlayM3U8File
*  Description: 生成试看m3u8文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 源文件名
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHLSTask::CreateTryPlayM3U8File(const char *filename, Int32 slicedur)
{
	if (filename == NULL || slicedur <= 0)
	{
		return -1;
	}

	//获取分片序列号
	SInt32 seq = CLibCIStaticConf::m_TryPlayDur/slicedur;
	std::string srcm3u8file = std::string(filename) + ".m3u8";
	std::string tryplaym3u8 = std::string(filename) + "_T.m3u8";
	std::string findkey = "/" + STR::SInt32ToStr(seq);

	//打开文件
	ifstream in(srcm3u8file.c_str()); 
	ofstream out(tryplaym3u8.c_str()); 
	if (!in.is_open() || !out.is_open())
	{
		return -2;
	}

	//生成试看m3u8文件
	while (!in.eof() )  
	{  
		std::string tmpm3u8 = "";
		std::getline (in, tmpm3u8); 
		if (!tmpm3u8.empty())
		{
			//写文件到试看文件
			out<<tmpm3u8<<std::endl;
			if (tmpm3u8.find(findkey) != std::string::npos)
			{
				out<<"#EXT-X-ENDLIST" <<std::endl;  
				break;
			}
		}
	}  

	//关闭文件
	in.close();
	out.close();

	return CDN_ERR_SUCCESS;
}















