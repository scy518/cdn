/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestcommontask.h
*  Description:  通用注入任务处理，只注入文件不做任务文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libvideo/libvideoapi.h"
#include "libcore/errcodemacros.h"
#include "libcore/httpdownload.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libci/cicommon.h"
#include "mp4filetask.h"
#include "mgr/diskmanage.h"

INGEST_TASK_REGISTER_DYNCREATE(SERVICE_TYPE_MP4, CMP4FileTask, CCITask)
/*******************************************************************************
*  Function   : CMP4FileTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CMP4FileTask::CMP4FileTask()
{
}
/*******************************************************************************
*  Function   : CMP4FileTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
 CMP4FileTask::~CMP4FileTask()
 {
 }
 /*******************************************************************************
 *  Function   : CMP4FileTask
 *  Description: 处理注入消息,不同的注入类型对应不同的注入接口
 *  Calls      : 见函数实现
 *  Called By  : 
 *  Input      : 无
 *  Output     : 无
 *  Return     : 
 *******************************************************************************/
 Int32 CMP4FileTask::StartTask(void *taskitem)
 {
	 IngestTaskItem *tmptask = (IngestTaskItem*)taskitem;

	 //下载文件
	 Int32 ret = DownLoadFile(tmptask);
	 if (ret != 0)
	 {
		 return CDN_ERR_DOWN_LOAD_FAIL;
	 }

	 //创建m3u8
	 std::string filename = GetFileName(tmptask);

	 //获取时长
	 tmptask->m_BiteRate = LibVideo::GetVideoBitRateAndDur(filename.c_str(),tmptask->m_Duration, MP4_MEDIA_FIEL)/1024;;

	 return CDN_ERR_SUCCESS;
 }




