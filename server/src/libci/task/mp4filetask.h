#ifndef MP4_FILE_TASK_H
#define MP4_FILE_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    mp4filetask.h
*  Description:  mp4文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CMP4FileTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CMP4FileTask)
public:

	CMP4FileTask();

	virtual ~CMP4FileTask();

	//处理注入消息,不同的注入类型对应不同的注入接口
	virtual Int32 StartTask(void *taskitem);
};

#endif

