#ifndef FILE_TASK_H
#define FILE_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    filetask.h
*  Description:  通用注入任务处理，只注入文件不做任务文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CFileTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CFileTask)
public:

	CFileTask();

	virtual ~CFileTask();
};

#endif

