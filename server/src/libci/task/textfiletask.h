#ifndef TEXT_FILE_TASK_H
#define TEXT_FILE_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    textfiletask.h
*  Description:  处理文本文件
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CTextFileTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CTextFileTask)
public:

	CTextFileTask();

	virtual ~CTextFileTask();
};

#endif

