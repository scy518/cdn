#ifndef HLS_TASK_H
#define HLS_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlstask.h
*  Description:  hls协议视频注入，只生成m3u8
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CHLSTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CHLSTask)
public:

	CHLSTask();

	virtual ~CHLSTask();

	//处理注入消息,不同的注入类型对应不同的注入接口
	virtual Int32 StartTask(void *taskitem);

	//删除内容接口
	virtual Int32 DeleteContent(void *taskitem);

protected:

	//生成m3u8文件
	virtual Int32 CreateM3U8File(const char *filename, Int32 slicedur, Int64 &dur);

	//生成试看m3u8文件
	virtual Int32 CreateTryPlayM3U8File(const char *filename, Int32 slicedur);
};
#endif



