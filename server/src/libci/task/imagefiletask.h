#ifndef IMAGE_FILE_TASK_H
#define IMAGE_FILE_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    imagefiletask.h
*  Description:  ע��ͼƬ
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CImageFileTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CImageFileTask)
public:

	CImageFileTask();

	virtual ~CImageFileTask();
};

#endif

