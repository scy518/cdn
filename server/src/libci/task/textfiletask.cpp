/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestcommontask.h
*  Description:  通用注入任务处理，只注入文件不做任务文件处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/httpdownload.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libci/cicommon.h"
#include "textfiletask.h"
#include "mgr/diskmanage.h"

INGEST_TASK_REGISTER_DYNCREATE(SERVICE_TYPE_TXT, CTextFileTask, CCITask)
/*******************************************************************************
*  Function   : CTextFileTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CTextFileTask::CTextFileTask()
{
}
/*******************************************************************************
*  Function   : CTextFileTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
 CTextFileTask::~CTextFileTask()
 {
 }

