#ifndef NGOD_HLS_TASK_H
#define NGOD_HLS_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ngodhlstask.h
*  Description:  ngod + hls注入任务处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CNGODHLSTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CNGODHLSTask)
public:

	CNGODHLSTask();

	virtual ~CNGODHLSTask();

	//处理注入消息,不同的注入类型对应不同的注入接口
	virtual Int32 StartTask(void *taskitem);

	//删除内容接口
	virtual Int32 DeleteContent(void *taskitem);

protected:

	//生成nogd和m3u8索引
	virtual Int32 CreateNGODM3U8File(const char *filename, const char *pid, const char *aid, 
		                             Int32 slicedur, Int64 &dur);
};
#endif




