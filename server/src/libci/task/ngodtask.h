#ifndef NGOD_TASK_H
#define NGOD_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ngodtask.h
*  Description:  ngod协议视频注入，只生成ngod索引和倍速文件
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libci/citask.h"

class CNGODTask : public CCITask
{
	CONF_DECLARE_DYNCREATE(CNGODTask)
public:

	CNGODTask();

	virtual ~CNGODTask();

	//处理注入消息,不同的注入类型对应不同的注入接口
	virtual Int32 StartTask(void *taskitem);

	//删除内容接口
	virtual Int32 DeleteContent(void *taskitem);

protected:

	//生成m3u8文件
	virtual Int32 CreateNGODFile(const char *filename, const char *pid, const char *aid, Int64 &dur);
};
#endif



