/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libci/libcimacros.h"
#include "libcistaticconf.h"

//节点ID
std::string CLibCIStaticConf::m_NodeID = "CI_00";

//下载完文件需要执行的shell命令
std::string CLibCIStaticConf::m_ShellCmd = "./shellcmd.sh";

//默认pid
std::string CLibCIStaticConf::m_DefaultProviderID = "SCY";

//数据文件
std::string  CLibCIStaticConf::m_SqlFileName = "";

//下载任务线程数
SInt32  CLibCIStaticConf::m_DownLoadThreadNum = 1;

//是否缓存到redis 0:否;1:是
SInt32 CLibCIStaticConf::m_CIIsEnableRedis = DISENABLE_REDIS_CACHE;

//redis同步时间间隔
SInt32 CLibCIStaticConf::m_AsyRedisInterval = 86400000;

//hls分片时长
SInt32 CLibCIStaticConf::m_SliceDuration = 10;

//试看时长,单位为秒
SInt32 CLibCIStaticConf::m_TryPlayDur = 600;

//获取加载扫描路径时间间隔
SInt32  CLibCIStaticConf::m_LoadPathInterval = 30000;

//全量扫描时间间隔
SInt32 CLibCIStaticConf::m_AllScanInterval = DEFAULT_ALL_SCAN_INTERVAL*1000;

//扫描过滤文件扩展名串
std::string CLibCIStaticConf::m_ScanFilterFileExtName = "ts";

//扫描路径
std::string CLibCIStaticConf::m_ScanPathList = "";

//采用间隔扫描
SInt32 CLibCIStaticConf::m_ScanMode = SCAN_MODE_TIMING;

//推流内网地址
std::string CLibCIStaticConf::m_PrivateStreamAddr = "127.0.0.1:80";

//推流外网地址
std::string CLibCIStaticConf::m_PublicStreamAddr = "";

//存储磁盘配置文件
std::string CLibCIStaticConf::m_DiskFile = "../conf/private/disk.xml";

std::string	CLibCIStaticConf::m_NetworkCardFile= "../conf/private/networkcard.xml";
/*****************************ngod参数配置*********************************/
//idx文件的规范，1为I01，2为I03
UInt32 CLibCIStaticConf::m_ObjVersion = OBJ_VER_I01;

//快退文件的ending_byte是否减1
Bool CLibCIStaticConf::m_FrFilesizeMinus1 = TRUE;

//H.264 索引文件的I帧是否只打IDR帧
Bool CLibCIStaticConf::m_OnlyWriteIDR = FALSE;

//支持的倍速格式如：2,4,8
std::string CLibCIStaticConf::m_NgodIndexScale = "8,32";

IMPLEMENT_SINGLETON(CLibCIStaticConf)
/*******************************************************************************
*  Function   : CLibCIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCIStaticConf::CLibCIStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibCIStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibCIStaticConf::~CLibCIStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCIStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibCIStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibCIStaticConf::SynStaticParam()
{
	//节点ID
	CLibCIStaticConf::m_NodeID = std::string(GET_PARAM_CHAR(CI_NODE_ID, "CI_00"));

	//下载完文件需要执行的shell命令
	CLibCIStaticConf::m_ShellCmd = GET_PARAM_CHAR(CI_SHELL_CMD, "./shellcmd.sh");

	//默认pid
	CLibCIStaticConf::m_DefaultProviderID = GET_PARAM_CHAR(CI_DEFAULT_PROVIDERID, "SCY");

	//数据文件
	CLibCIStaticConf::m_SqlFileName = GET_PARAM_CHAR(CI_SQL_STATEMENT_FILE, "../conf/sql/sql.xml");

	//下载任务线程数
	CLibCIStaticConf::m_DownLoadThreadNum =  GET_PARAM_INT(CI_DOWN_THREAD_NUM, 1);

	//是否缓存到redis 0:否;1:是
	CLibCIStaticConf::m_CIIsEnableRedis = GET_PARAM_INT(CI_IS_ENABLE_REDIS, DISENABLE_REDIS_CACHE);

	//redis同步时间间隔
	CLibCIStaticConf::m_AsyRedisInterval = GET_PARAM_INT(CI_ASY_REDIS_INTERVAL, 86400)*1000;

	//hls分片时长
	CLibCIStaticConf::m_SliceDuration = GET_PARAM_INT(CI_HLS_SLICE_DURATION, 10);

	//试看时长,单位为秒
	CLibCIStaticConf::m_TryPlayDur = GET_PARAM_INT(CI_TRY_PLAY_DURATION, 600);
	
	//获取加载扫描路径时间间隔
	CLibCIStaticConf::m_LoadPathInterval = GET_PARAM_INT("", 30)*1000;

	//全量扫描时间间隔
	CLibCIStaticConf::m_AllScanInterval = GET_PARAM_INT(CI_ALL_SCAN_INTERVAL, DEFAULT_ALL_SCAN_INTERVAL)*1000;

	//扫描过滤文件扩展名串
	CLibCIStaticConf::m_ScanFilterFileExtName = GET_PARAM_CHAR(CI_SCAN_FILTER_FILE_EXTNAME, "ts");

	//扫描路径
	CLibCIStaticConf::m_ScanPathList = GET_PARAM_CHAR(CI_SCAN_PATH, "");

	//采用间隔扫描
	CLibCIStaticConf::m_ScanMode = GET_PARAM_INT(CI_SCAN_MODE, SCAN_MODE_TIMING);

	//推流内网地址
	CLibCIStaticConf::m_PrivateStreamAddr = GET_PARAM_CHAR(CI_PRIVATE_STREAM_ADDR, "127.0.0.1:80");

	//推流外网地址
	CLibCIStaticConf::m_PublicStreamAddr = GET_PARAM_CHAR(CI_PUBLIC_STREAM_ADDR, "127.0.0.1:80");

	//存储磁盘配置文件
	CLibCIStaticConf::m_DiskFile = GET_PARAM_CHAR(CI_DISK_CONF_FILE, "../conf/private/disk.xml");

	//网卡配置文件
	CLibCIStaticConf::m_NetworkCardFile = GET_PARAM_CHAR(CI_NETWORK_CARD_CONF_FILE,"../conf/private/networkcard.xml");

	/*****************************ngod参数配置*********************************/
	//idx文件的规范，1为I01，2为I03
	CLibCIStaticConf::m_ObjVersion = GET_PARAM_INT(CI_OBJVERSION, OBJ_VER_I01);

	//快退文件的ending_byte是否减1
	if (GET_PARAM_INT(CI_FRFILESIZEMINUS1, 1) > 0)
	{
		CLibCIStaticConf::m_FrFilesizeMinus1 = TRUE;
	}
	else
	{
		CLibCIStaticConf::m_FrFilesizeMinus1 = FALSE;
	}
	 
	//H.264 索引文件的I帧是否只打IDR帧
	if (GET_PARAM_INT(CI_ONLYWRITEIDR, 1) > 0)
	{
		CLibCIStaticConf::m_OnlyWriteIDR = TRUE;
	}
	else
	{
		CLibCIStaticConf::m_OnlyWriteIDR = FALSE;
	}

	//支持的倍速格式如：2,4,8
	CLibCIStaticConf::m_NgodIndexScale = GET_PARAM_CHAR(CI_NGODINDEXSCALE, "8,32");

	return;
}









