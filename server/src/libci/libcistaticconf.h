#ifndef LIB_CI_STATIC_CONF_H
#define LIB_CI_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcistaicconf.h
*  Description:  ci静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>
#include <list>

class CLibCIStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibCIStaticConf)
public:

	//初始化注入内容管理器
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//节点ID
	static std::string                    m_NodeID;

	//下载完文件需要执行的shell命令
	static std::string		             m_ShellCmd;

	//默认pid
	static std::string                   m_DefaultProviderID;

	//数据文件
	static std::string                    m_SqlFileName;

	//下载任务线程数
	static  SInt32                        m_DownLoadThreadNum;

	//是否缓存到redis 0:否;1:是
	static  SInt32                        m_CIIsEnableRedis;

	//redis同步时间间隔
	static  SInt32                       m_AsyRedisInterval;

	//hls分片时长
	static  SInt32                       m_SliceDuration;

	//试看时长,单位为秒
	static SInt32                        m_TryPlayDur;

	//获取加载扫描路径时间间隔
	static  SInt32                       m_LoadPathInterval;

	//全量扫描时间间隔
	static  SInt32                       m_AllScanInterval;

	//是否启用扫描
	static SInt32                        m_CIIsScanEnable;

	//扫描过滤文件扩展名串
	static std::string                   m_ScanFilterFileExtName;

	//扫描路径
	static std::string                   m_ScanPathList;

	//采用间隔扫描
	static  SInt32                       m_ScanMode;

	//推流内网地址
	static std::string                   m_PrivateStreamAddr;

	//推流外网地址
	static std::string                   m_PublicStreamAddr;

	//存储磁盘配置文件
	static std::string                    m_DiskFile;

	//网卡配置文件
	static std::string					  m_NetworkCardFile;

	/*****************************ngod参数配置*********************************/
	//idx文件的规范，1为I01，2为I03
	static UInt32                         m_ObjVersion;

	//快退文件的ending_byte是否减1
	static Bool                           m_FrFilesizeMinus1;

	//H.264 索引文件的I帧是否只打IDR帧
	static Bool                           m_OnlyWriteIDR;

	//支持的倍速格式如：2,4,8
	static std::string                    m_NgodIndexScale;

protected:

	//同步参数
	void SynStaticParam();

};
#endif



