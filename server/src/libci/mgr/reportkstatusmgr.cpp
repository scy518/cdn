/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    reporttaskstatusmgr.h
*  Description:  任务上报接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libutil/OSIpc.h"
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libcore/httprequestclient.h"
#include "libcdninterface/aisrtransferstatus.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/reporttaskstatusmgr.h"

IMPLEMENT_SINGLETON(CReportTaskStatusMgr)
/*******************************************************************************
*  Function   : CReportTaskStatusMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CReportTaskStatusMgr::CReportTaskStatusMgr()
{
	m_ReportTaskList.clear();
}
/*******************************************************************************
*  Function   : ~CReportTaskStatusMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CReportTaskStatusMgr::~CReportTaskStatusMgr()
{
	//停止线程
	this->StopTask();

	m_ReportTaskMutex.Lock();
	m_ReportTaskList.clear();
	m_ReportTaskMutex.Unlock();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
SInt32 CReportTaskStatusMgr::Init()
{
	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CReportTaskStatusMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CReportTaskStatusMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//处理上报任务
		DoReportTask();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddReportTask
*  Description: 添加上报任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CReportTaskStatusMgr::AddReportTask(IngestTaskItem &taskitem)
{
	m_ReportTaskMutex.Lock();
	m_ReportTaskList.push_back(taskitem);
	m_ReportTaskMutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : ReportTaskStatus
*  Description: 上报任务状态
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CReportTaskStatusMgr::DoReportTask()
{
	IngestTaskItemList tempTaskList;
	m_ReportTaskMutex.Lock();
	tempTaskList.insert(tempTaskList.end(), m_ReportTaskList.begin(), m_ReportTaskList.end());
	m_ReportTaskList.clear();
	m_ReportTaskMutex.Unlock();

	IngestTaskItemList::iterator it = tempTaskList.begin();
	for (; it != tempTaskList.end(); it++)
	{
		IngestTaskItem tempTask = *it;

		 if (it->m_ReportURL.empty())
		 {
			 continue;
		 }

		 //填充消息
		 Int32 ret = SendMessage(tempTask);
		 if (ret != CDN_ERR_SUCCESS)
		 {
			LOG_WARN("report task status fail.ret:"<<ret<<";reporturl:"<<it->m_ReportURL)
		 }
	}

	tempTaskList.clear();
	return;
}
/*******************************************************************************
*  Function   : FillMessage
*  Description: 填充上报信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CReportTaskStatusMgr::SendMessage(IngestTaskItem &taskitem)
{
	CAISRTransferStatusReq req;
	CAISRTransferStatusRsp  rsp;

	req.SetRequstType(HTTP_POST_TYPE);
	req.SetCmdCode(CMD_AIS_RTRANSFER_STATUS);

	CIGetStatusResult result;
	FillStatusResult(taskitem, result);
	req.Setresult(result);

	char *strMsg = NULL;
	Int32 ret = req.Encode(strMsg);
	if (ret != CDN_ERR_SUCCESS)
	{
		ERROR_LOG("Encode fail. report url:"<<taskitem.m_ReportURL)
		return -1;
	}

	CHttpRequestClient  tmphttpClient;
	Int32 responseLen = 0;
	char *pResBuf = NULL;
	Int32 iresult = tmphttpClient.Post(taskitem.m_ReportURL.c_str(), strMsg, pResBuf, responseLen);
	if (iresult != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("report fail.ret:"<<iresult<<".report url:"<<taskitem.m_ReportURL);
		return -2;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : FillStatusResult
*  Description: 填充返回结果
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CReportTaskStatusMgr::FillStatusResult(const IngestTaskItem &ingesttask, CIGetStatusResult &result)
{
	memcpy(result.m_ProviderID, ingesttask.m_ContentKey.m_ProviderID.c_str(), ingesttask.m_ContentKey.m_ProviderID.length());
	memcpy(result.m_AssetID, ingesttask.m_ContentKey.m_AssetID.c_str(), ingesttask.m_ContentKey.m_AssetID.length());
	result.m_ServiceType = ingesttask.m_ContentKey.m_ServiceType;
	result.m_Status = ingesttask.m_Status;
	result.m_Progress = ingesttask.m_Progress;

	std::string tmpcreatetime = OS::Localtime(ingesttask.m_CreateTime*1000);
	memcpy(result.m_CreateTime,  tmpcreatetime.c_str(), tmpcreatetime.length());

	if (ingesttask.m_StartTime > 0)
	{
		std::string tmpstarttime = OS::Localtime(ingesttask.m_StartTime*1000);
		memcpy(result.m_StartTime,  tmpstarttime.c_str(), tmpstarttime.length());
	}

	if (ingesttask.m_EndTime > 0)
	{
		std::string tmpendtime = OS::Localtime(ingesttask.m_EndTime*1000);
		memcpy(result.m_EndTime,  tmpendtime.c_str(), tmpendtime.length());
	}

	result.m_ContentSize = ingesttask.m_FileSize;
	result.m_Duration    = ingesttask.m_Duration;
	result.m_AvgBitRate  = ingesttask.m_BiteRate;
	memcpy(result.m_Md5CheckSum,  ingesttask.m_FileMD5.c_str(), ingesttask.m_FileMD5.length());

	return;
}
