/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citaskexecute.cpp
*  Description:  注入任务执行管理器，负责消息具体处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libcore/errcodemacros.h"
#include "libutil/OS.h"
#include "libcdnutil/citaskinterfacemgr.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/citaskexecute.h"
#include "mgr/citaskmgr.h"
#include "libcistaticconf.h"

IMPLEMENT_SINGLETON(CCITaskExecute)
/*******************************************************************************
 Fuction name:CCITaskExecute
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCITaskExecute::CCITaskExecute()
{
	m_ThreadManager = NULL;
}
/*******************************************************************************
 Fuction name:~CDoFileTask
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CCITaskExecute::~CCITaskExecute()
{
	if (m_ThreadManager != NULL)
	{
		delete m_ThreadManager;
		m_ThreadManager = NULL;
	}
}
/*******************************************************************************
Fuction name:	Init
Description :
Input   para:
Output  para:
Call fuction:      
Autor       :	zk
date  time  :	2012.9.1
*******************************************************************************/
Int32 CCITaskExecute::Init()
{
	if (m_ThreadManager != NULL)
	{
		return -1;
	}

	m_ThreadManager = new CThreadManager();
	if (m_ThreadManager == NULL || m_ThreadManager->Initialize(this, CLibCIStaticConf::m_DownLoadThreadNum) != 0)
	{
		LOG_ERROR("Init thread fail.")
		if (m_ThreadManager != NULL)
		{
			delete m_ThreadManager;
			m_ThreadManager = NULL;
		}

		return -2;
	}

	return 0;
}
/*******************************************************************************
 Fuction name:Execute
 Description :实现Execute函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CCITaskExecute::Run()
{ 
	IngestTaskItem taskitem;
    SInt32 ret = CCITasktmgr::Intstance()->GetTaskItem(taskitem);
	if (ret != 0)
	{
		return 0;
	}

	ret = PreTask(taskitem);
	if (ret != CDN_ERR_SUCCESS)
	{
		return 0;
	}

	//启动任务
	StartTask(taskitem);

	//完成任务
	FinishTask(taskitem);

	return 0;
}
/*******************************************************************************
 Fuction name:PreTaskItem
 Description :处理文件
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCITaskExecute::PreTask(IngestTaskItem &taskitem)
{
	if (taskitem.m_Status != TRANF_STATUS_TRANSFERRING)
	{
		return -1;
	}

	//设置进度
	taskitem.m_Progress = 30;

	//刷新进度
	CCITasktmgr::Intstance()->RefreshePercent(taskitem);

	return 0;
}
/*******************************************************************************
 Fuction name:PreTaskItem
 Description :开始任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCITaskExecute::StartTask(IngestTaskItem &taskitem)
{
	//创建实现任务
	CCITaskInterface *ciTask = CCITaskInterfaceMgr::Intstance()->CreateCITask(taskitem.m_ContentKey.m_ServiceType);
	if (ciTask == NULL)
	{
		LOG_ERROR("create ci task fail. "<<taskitem.m_ContentKey.m_ServiceType);
		CCITasktmgr::Intstance()->AddTaskItem(taskitem);
		return -1;
	}

	//下载文件
	SInt32 ret = ciTask->StartTask(&taskitem);
	if (ret != 0)
	{
		taskitem.m_Status = TRANF_STATUS_COMPLETE;
		taskitem.m_ErrCode = ret;
		taskitem.m_EndTime = OS::Milliseconds()/1000;
		SET_ERROR_DES(taskitem.m_ErrDes, ret);

		ciTask->DeleteContent(&taskitem);
		CCITaskInterfaceMgr::Intstance()->ReleaseCITask(taskitem.m_ContentKey.m_ServiceType, ciTask);
		LOG_ERROR("Start task fail. ret:"<<ret);

		return 0;
	}

	if (GetCancel() == CANCEL_TASK)
	{
		taskitem.m_Status = TRANF_STATUS_COMPLETE;
		taskitem.m_ErrCode = CDN_ERR_CI_TASK_CANCEL;
		taskitem.m_EndTime = OS::Milliseconds()/1000;
		SET_ERROR_DES(taskitem.m_ErrDes, CDN_ERR_CI_TASK_CANCEL);

		ciTask->DeleteContent(&taskitem);
	}

	taskitem.m_Status = TRANF_STATUS_COMPLETE;
	taskitem.m_EndTime = OS::Milliseconds()/1000;
	taskitem.m_Progress = 100;

	taskitem.m_ErrCode = CDN_ERR_SUCCESS;
	SET_ERROR_DES(taskitem.m_ErrDes, CDN_ERR_SUCCESS);
	CCITaskInterfaceMgr::Intstance()->ReleaseCITask(taskitem.m_ContentKey.m_ServiceType, ciTask);
	
	return ret;
}
/*******************************************************************************
 Fuction name:FinishTask
 Description :任务完成处理
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CCITaskExecute::FinishTask(IngestTaskItem &taskitem)
{

	//设置进度
	taskitem.m_Progress = 90;

	//刷新进度
	CCITasktmgr::Intstance()->RefreshePercent(taskitem);

	//回填数据
	CCITasktmgr::Intstance()->FinishTask(taskitem);

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
Fuction name:CancelTask
Description :删除任务
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
void CCITaskExecute::CancelTask()
{
	m_MutexCancle.Lock();
	m_IsCancel = 1;
	m_MutexCancle.Unlock();
}
/*******************************************************************************
Fuction name:GetCancel
Description :是否取消
Input   para:
Output  para:
Call fuction:
Autor       :
date  time  :2012.5.5
*******************************************************************************/
Int32 CCITaskExecute::GetCancel()
{
	Int32 tmp = 0;
	m_MutexCancle.Lock();
	tmp = m_IsCancel;
	m_MutexCancle.Unlock();

	return tmp;
}



