#ifndef DISK_MANAGE_H
#define DISK_MANAGE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    diskmanage.h
*  Description:  磁盘管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libci/cicommon.h"
#include <string>
#include <list>

class CDiskManage : public OSTask
{
	DECLARATION_SINGLETON(CDiskManage)
public:

	//初始化任务
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

	//获取磁盘磁盘路径
	std::string GetDiskPath();

	//获取磁盘列表
	std::string GetDiskList(std::vector<std::string > &disklist);

	//删除文件
	void DeleteFile(const std::string &filename);

	//删除目录
	void DeleteDir(const std::string &dir);

	//获取剩余空间最大路径
	std::string GetMaxPath();

	//获取剩余空间最大
	SInt64 GetMaxFree() ; 

	//获取总磁盘空间
	SInt64 GetAllDiskSize();

	//获取使用磁盘空间
	SInt64 GetUseDiskSize();

	//获取预留磁盘空间
	SInt64 GetReserDiskSize();

	//获取磁盘信息
	void GetNodeDiskInfo(CINode &node);

protected:

	//更新磁盘信息
	void RefereshDisk();

	//加载磁盘信息
	SInt32 LoadDiskInfo();

private:

	OSMutex					m_MaxMutex;
	std::string             m_MaxPath;
	SInt64                  m_MaxFree;

	std::string             m_DiskPath;
	SInt64                  m_AllDiskSize;
	SInt64                  m_UseDiskSize;
	SInt64                  m_ReserDiskSize;

	std::list<CIDiskPath >  m_DiskList;

	SInt64                  m_LastTime;
};
#endif



