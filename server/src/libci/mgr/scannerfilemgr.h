#ifndef SCANNER_FILE_MGR_H
#define SCANNER_FILE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    scannerfilemgr.h
*  Description:  扫描文件管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libci/cicommon.h"

class CSannerFileMgr : public OSTask
{
	DECLARATION_SINGLETON(CSannerFileMgr)
public:

	//初始化任务
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

	//添加文件
	SInt32 AddFile(const FileItem &file);

	//删除文件
	SInt32 DeleteFile(const FileItem &file);

protected:

	//刷新数据到db
	void RefreshFileToDB();

	//添加文件到数据
	void AddFileToDB();

	//删除数据库文件
	void DeleteFileFromDB();

	//文件过滤
	Bool FileFilter(const std::string &filename);

	//获取文件类型
	ServiceType GetFileType(const std::string &filename);

	//分离pid aid
	void SplitPidAndAid(const std::string &filename, std::string &pid, std::string &aid);

	//填充redis缓存结构
	void FillRedisFile(const IngestTaskItem &taskitem, RedisFileInfo &tmpfile);

private:

	OSMutex                m_AddMutex;
    FileItemMap            m_AddFileMap;

	OSMutex                m_DelMutex;
	FileItemMap            m_DelFileMap;
};

#endif





