#ifndef CI_DB_OPERATER_MGR_H
#define CI_DB_OPERATER_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cidbopreatermgr.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "libci/cicommon.h"

class CCIDBOperatermgr : public CDBOperate
{
	DECLARATION_SINGLETON(CCIDBOperatermgr)

public:

	//恢复失败任务
	Int32 RecoveryTaskFromDB(const std::string &nodeid, IngestTaskItemList &tasklist);

	//去数据库取任务
	Int32 LoadTaskFromDB(const std::string &nodeid, IngestTaskItemList &tasklist);

	//更新任务开始
	Int32 UpdateTask(const IngestTaskItem &taskitem);

	//根据文件名去更新百分比
	Int32 UpdatePercent(const IngestTaskItem &taskitem);

	//处理完成
	Int32 DoFinish(const IngestTaskItem &taskitem);

	//更新磁盘信息
	Int32 UpdateNodeInfo(const CINode &cinode);

	//删除磁盘
	Int32 DeleteNode(const CINode &cinode);

	//删除文件
	Int32 DeleteFileFromDB(const std::string &pid, const std::string &aid, const std::string &nodeid);

	//磁盘上文件信息写入到数据库中
	Int32 ScanFileInsertDB(const IngestTaskItem &taskitem);

	//更新文件节点表
	Int32 UpdateFileNode(const IngestTaskItem &taskitem);

	//获取节点上所有的文件分布信息
	Int32 GetFileDistribute(std::list<FileDistribute> &filedistributeList);

	//从文件分布表中删除文件信息
	Int32 DeleteFileFromDistribute(const FileDistribute &filedistibute);

	//文件分布表中文件总数
	Int32 GetCountFileDistribute(const FileDistribute &filedistibute);

	//从文件信息表中删除文件信息
	Int32 DeleteFileFromFileInfo(const FileDistribute &filedistibute);

protected:

	//更新任务结果
	Int32 UpDateTaskResult(const IngestTaskItem &taskitem);

	//更新文件信息表
	Int32 UpdateFileInfo(const IngestTaskItem &taskitem);
};

#endif
