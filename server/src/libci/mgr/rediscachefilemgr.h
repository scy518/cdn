#ifndef REDIS_CACHE_FILE_MGR_H
#define REDIS_CACHE_FILE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rediscachefilemgr.h
*  Description:  redis 缓存文件管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libcdnutil/redisfileinfdataobject.h"

class CRedisCacheFileMgr : public OSTask
{
	DECLARATION_SINGLETON(CRedisCacheFileMgr)
public:

	//初始化文件缓存管理器
	SInt32 Init();

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//添加文件
	Int32 AddFile(const RedisFileInfoList &filelist);

	//删除文件
	Int32 DeleteFile(const RedisFileInfoList &filelist);

	//更新文件
	Int32 UpdateFile(const RedisFileInfoList &filelist);

protected:

	//添加文件到redis
	void SynFileListToRedis();

	//同步数据库数据到redis
	void SynFromDBFileToRedis();

	//添加文件
	void DoAddFile();

	//删除文件
	void DoDelFile();

	//更新文件
	void DoUpdateFile();

	//添加文件到redis
	Int32 AddFileToRedis(const RedisFileInfo &file);

	//删除文件
	Int32 DelFileFromRedis(const char *filename);

	//添加节点到文件分布set
	Int32 AddFileDistributeToRedis(const RedisFileInfo &file);

	//删除节点到文件分布set
	Int32 DelFileDistributeFromRedis(const RedisFileInfo &file);

private:

	OSMutex                   m_AddMutex;
	RedisFileInfoList         m_TmpAddFileList;

	OSMutex                   m_DelMutex;
	RedisFileInfoList         m_TmpDelFileList;

	OSMutex                   m_UpdateMutex;
	RedisFileInfoList         m_TmpUpdateFileList;

	Int64                     m_LastTime;
};
#endif





