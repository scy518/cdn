#ifndef CI_TASK_MGR_H
#define CI_TASK_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    citaskmgr.h
*  Description:  注入内容管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "mgr/citaskexecute.h"
#include "libci/cicommon.h"

class CCITasktmgr : public OSTask
{
	DECLARATION_SINGLETON(CCITasktmgr)
public:

	//初始化注入内容管理器
	Int32 Init();

	//线程调用函数
	virtual Bool Run();

	//获取task任务
	Int32 GetTaskItem(IngestTaskItem &taskitem);

	//获取task任务
	Int32 AddTaskItem(IngestTaskItem &taskitem);

	//更新任务进度
	Int32 RefreshePercent(const IngestTaskItem &taskitem);

	//添加完成任务
    Int32 FinishTask(const IngestTaskItem &taskitem);

protected:

	//处理完成任务
	void DoFinishTask();

	//处理非下载任务（取消，删除）
	void DoNoDLoadTask();

	//加载数据
	void DoLoadTaskFromDB();

	//找到指定的文件删除
	Int32 DeleteFile(const IngestTaskItem &taskitem);

	//取下载的任务，目的取消
	void CancelTask(const IngestTaskItem &taskitem);

	//填充redis缓存结构
	void FillRedisFile(const IngestTaskItem &taskitem, RedisFileInfo &tmpfile);

	//上报任务完成
	void ReportTaskStatus(const IngestTaskItem &taskitem);

private:		
	std::list<IngestTaskItem>		    m_TaskList;

	//存放需要注入的任务
	OSMutex								m_TaskMutex;
	std::map<SInt64, IngestTaskItem >   m_TaskIDItemMap;

	//需要更新的数据列表
	OSMutex								m_FinishTaskMutex;
	std::list<IngestTaskItem>           m_FinishTaskList;
};

#endif



