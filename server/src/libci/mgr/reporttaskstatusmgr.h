#ifndef REPORT_TASK_STATUS_MGR_H
#define REPORT_TASK_STATUS_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    reporttaskstatusmgr.h
*  Description:  任务上报接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include <map>
#include <list>
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include "libutil/OSIpc.h"
#include "libcdninterface/cdnifcommon.h"
#include "libci/cicommon.h"

class CReportTaskStatusMgr : public OSTask
{
	DECLARATION_SINGLETON(CReportTaskStatusMgr)
public:

	//初始化任务
	SInt32 Init();

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//添加上报任务
	void AddReportTask(IngestTaskItem &taskitem);

protected:
	//上报任务状态
	void DoReportTask();

	//填充上报信息
	Int32 SendMessage(IngestTaskItem &taskitem);

	//填充状态数据
	void FillStatusResult(const IngestTaskItem &ingesttask, CIGetStatusResult &result);

private:
	
	//存储需要上报任务
	OSMutex								m_ReportTaskMutex;
	IngestTaskItemList                  m_ReportTaskList;
};

#endif

