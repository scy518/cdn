#ifndef LIB_HTTP_RTI_STATIC_CONF_H
#define LIB_HTTP_RTI_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libhttprtistaicconf.h
*  Description:  rti静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>

class CLibHTTPRTIStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibHTTPRTIStaticConf)
public:

	//初始化线程
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//http下载失败重新连接时间间隔
	static SInt32                    m_HttpRtiReconnectTime;


protected:

	//同步参数
	void SynStaticParam();

};
#endif



