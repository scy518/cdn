/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    livehlsdownload.h
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libutil/stringtool.h"
#include "libcore/errcodemacros.h"
#include "libcdnutil/libcdnutilmacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/livehttpdownload.h"
#include "libhttprtistaticconf.h"
/*******************************************************************************
*  Function   : CLiveHTTPDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CLiveHTTPDownload::CLiveHTTPDownload()
{
	m_SplitDur   = 10;
	m_HlsVideo   = NULL;
}
/*******************************************************************************
*  Function   : CLiveHTTPDownload
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CLiveHTTPDownload::~CLiveHTTPDownload()
{
	//停止下载任务
	this->StopDownLoad();

	//停止线程
	this->StopTask();

	if (m_HlsVideo != NULL)
	{
		delete m_HlsVideo;
		m_HlsVideo = NULL;
	}
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化url
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLiveHTTPDownload::Init(const char *url, const char *storepath, SInt32 splitdur)
{
	if (url == NULL || storepath == NULL)
	{
		return -1;
	}

	m_SplitDur  = splitdur;
	if (m_SplitDur <= 0)
	{
		m_SplitDur = 10;
	}
	
	//保存存储路径
	m_StorePath = std::string(storepath);

	//保存url
	m_Url = std::string(url);

	//初始化hls协议处理
	m_HlsVideo = new CHLSVideo();
	if (m_HlsVideo == NULL)
	{
		LOG_ERROR("new CHLSProcess fail")
		return -2;
	}

	SInt32 ret = m_HlsVideo->Init(m_StorePath.c_str(), m_SplitDur);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("init hls process fail. ret:"<<ret)
		return -3;
	}

	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("run task fail")
		return -4;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :线程调用函数
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Bool CLiveHTTPDownload::Run()
{
	//上次下载失败时间
	SInt64 lasttime = 0;

	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//判断是否下载失败,需要间隔一段时间再重试下载
		if (lasttime > 0 && (OS::Seconds() - lasttime < CLibHTTPRTIStaticConf::m_HttpRtiReconnectTime))
		{
			continue;
		}

		//如果已经到下次，清空上次记录时间
		if (lasttime > 0)
		{
			lasttime = 0;
		}

		//启动下载数据
		SInt32 ret = StartRevData();
		if (ret != CDN_ERR_SUCCESS)
		{
			lasttime = OS::Seconds();
			continue;
		}
	
		//判断用户是否自己停止
		if (this->IsStop())
		{
			break;
		}
	}

	LOG_INFO("http live down load exit.")
	return TRUE;
}
/*******************************************************************************
*  Function   : StartRevData
*  Description: 开始下载数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CLiveHTTPDownload::StartRevData()
{
	//初始化curl
	SInt32 ret = InitCurl(m_Url.c_str(), HTTP_DOWNLOAD_TIMEOUT);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("http live init curl fail. ret:"<<ret)
		return -1;
	}

	//开始下载文件
	Int32 biterate = 0;
	ret = StartDownLoad(biterate);
	if (ret != CDN_ERR_SUCCESS && !(this->IsStop()))
	{
		LOG_ERROR("http live init curl fail. ret:"<<ret)
		return -2;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Write
*  Description: 写回调函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLiveHTTPDownload::Write(char *buffer, Int32 size, Int32 nitems)
{
	Int32 len = size*nitems;
	if (m_HlsVideo == NULL)
	{
		return len;
	}
	
	SInt32 ret = m_HlsVideo->ProcessData((UChar*)buffer, len);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("http live process data fail.ret:"<<ret)
	}

	return len;
}



