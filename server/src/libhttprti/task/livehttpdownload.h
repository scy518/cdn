#ifndef LIVE_HLS_DOWNLOAD_H
#define LIVE_HLS_DOWNLOAD_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    livehlsdownload.h
*  Description:  hls客户端下载
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "curl/curl.h"
#include "libcore/httpdownload.h"
#include "libvideo/hlsvideo.h"

class CLiveHTTPDownload : public CHttpDownLoad
{
public:
	CLiveHTTPDownload();

	virtual ~CLiveHTTPDownload();

	//初始化url
	virtual Int32 Init(const char *url, const char *storepath, SInt32 splitdur = 10);

	//线程调用函数
	virtual Bool Run();

protected:

	//开始下载数据
	SInt32 StartRevData();

	//写回调函数
	virtual Int32 Write(char *buffer, Int32 size, Int32 nitems);

private:

	SInt32                      m_SplitDur; //分片文件时长

	std::string                 m_StorePath; //录制文件存储路径

	std::string                 m_Url;       //下载

	CHLSVideo                   *m_HlsVideo; //hls协议处理
};


#endif
