/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    hlsrtitask.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "task/httprtitask.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPRTITask, CRTITaskInterface)
/*******************************************************************************
*  Function   : CHLSRTITask
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPRTITask::CHTTPRTITask()
{
	m_LiveHttpDownLoad = NULL;
}
/*******************************************************************************
*  Function   : ~CHLSRTITask()
*  Description: 析构函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPRTITask::~CHTTPRTITask()
{
	StopRecordTask();
}
/*******************************************************************************
*  Function   : StartRecordTask
*  Description: 开启直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPRTITask::StartRecordTask()
{
	if (m_LiveHttpDownLoad != NULL)
	{
		LOG_ERROR("start recore is exist.m_LiveHttpDownLoad is null")
		return -1;
	}

	//开启http下载
	m_LiveHttpDownLoad = new CLiveHTTPDownload();
	if (m_LiveHttpDownLoad == NULL)
	{
		LOG_DEBUG("new CLiveHTTPDownload fail")
		return -2;
	}

	//初始化live hls下载
	SInt32 ret = m_LiveHttpDownLoad->Init(m_LiveChannel.m_SourceUrl.c_str(), 
		                                 m_StoreFullPath.c_str(),
		                                 m_LiveChannel.m_SliceDur);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("init live http down load fail. ret:"<<ret)
		ret = -3;
	}

	return ret;
}
/*******************************************************************************
*  Function   : StopTask
*  Description: 停止直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32	CHTTPRTITask::StopRecordTask()
{
	if (m_LiveHttpDownLoad != NULL)
	{
		delete m_LiveHttpDownLoad;
		m_LiveHttpDownLoad = NULL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : IsReStartRecordTask
*  Description: 检查是否需要重启,在录制返回TRUE, 否则返回FALSE
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CHTTPRTITask::IsReStartRecordTask()
{
	return TRUE;
}








