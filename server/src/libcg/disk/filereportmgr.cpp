/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    filereportmgr.cpp
*  Description:  文件管理,上报文件到cls
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libconf/ParamManager.h"
#include "libconf/httprequest.h"
#include "libconf/logmodule.h"
#include "libcg/libcgmacros.h"
#include "libutil/OS.h"
#include "filereportmgr.h"

IMPLEMENT_SINGLETON(CFileReportMgr)
/*******************************************************************************
*  Function   : CFileReportMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CFileReportMgr::CFileReportMgr()
{
	m_ClsListenAddress = "";
	m_NodeName         = "";
}
/*******************************************************************************
*  Function   : ~CFileReportMgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CFileReportMgr::~CFileReportMgr()
{
	//停止线程
	this->StopTask();

	m_FileMutex.Lock();
	m_TmpAddFileMap.clear();
	m_TmpUpdateFileMap.clear();
	m_TmpDelFileMap.clear();
	m_FileMutex.Unlock();
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化OSTask对象
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CFileReportMgr::Initialize()
{
	//获取cls地址
	m_ClsListenAddress = std::string(CParamManager::Intstance()->GetCharParam(CLS_ADDRESS, ""));
	if (m_ClsListenAddress.empty())
	{
		return FALSE;
	}

	//获取节点名称
	m_NodeName = std::string(CParamManager::Intstance()->GetCharParam(NODE_NAME, ""));
	if (m_NodeName.empty())
	{
		return FALSE;
	}
	
	//获取分包上报文件个数
	m_ReportFileNum = CParamManager::Intstance()->GetIntParam(REPORT_FILE_NUM, 10);

	return this->RunTask();
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CFileReportMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//上报文件
		ReportFileToCls();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : AddReportFileItem
*  Description: 添加上报文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::AddReportFileItem(const ReportFileItem &item)
{
	if (item.m_FileName.empty())
	{
		return;
	}

	m_TmpFileMutex.Lock();
	if (!IsReportFile(item))
	{
		m_TmpFileMutex.Unlock();
		return;
	}

	if (item.m_ReportType == REPORT_ADD)
	{
		m_TmpAddFileMap[item.m_FileName] = item;
	}
	else if (item.m_ReportType == REPORT_UPDATE)
	{
		m_TmpUpdateFileMap[item.m_FileName] = item;
	}
	else if (item.m_ReportType == REPORT_DELETE)
	{
		m_TmpDelFileMap[item.m_FileName] = item;
	}
	else
	{
		ERROR_LOG("no support report type. %d:", item.m_ReportType)
	}
	m_TmpFileMutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : ReportFileToCls
*  Description: 统一上报文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportFileToCls()
{
	std::list<ReportFileItem > addfilelist;
	std::list<ReportFileItem > delfilelist;
	std::list<ReportFileItem > updatefilelist;

	m_TmpFileMutex.Lock();

	//移动添加队列
	FileNameInfMap::iterator it = m_TmpAddFileMap.begin();
	for (; it != m_TmpAddFileMap.end(); it++)
	{
		addfilelist.push_back(it->second);
	}
	m_TmpAddFileMap.clear();

	//移动更新队列
	it = m_TmpUpdateFileMap.begin();
	for (; it != m_TmpUpdateFileMap.end(); it++)
	{
		updatefilelist.push_back(it->second);
	}
	m_TmpUpdateFileMap.clear();

	//移动删除队列
	it = m_TmpDelFileMap.begin();
	for (; it != m_TmpDelFileMap.end(); it++)
	{
		delfilelist.push_back(it->second);
	}
	m_TmpDelFileMap.clear();

	m_TmpFileMutex.Unlock();

	//上报添加文件
	ReportAddFile(addfilelist);

	//上报更新文件
	ReportUpdateFile(updatefilelist);

	//上报删除文件
	ReportDelFile(delfilelist);

	return;
}
/*******************************************************************************
*  Function   : ReportAddFileToCls
*  Description: 上报添加文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportAddFile(const std::list<ReportFileItem > &filelist)
{
	if (filelist.empty())
	{
		return;
	}
	
	std::list<AddFileInf >   tmpaddfilelist;
	std::list<ReportFileItem >::const_iterator it = filelist.begin();
	for (; it != filelist.end(); it++)
	{
		AddFileInf tmpfileinf;
		memcpy(tmpfileinf.m_FileName, it->m_FileName.c_str(), it->m_FileName.length());
		memcpy(tmpfileinf.m_FilePath, it->m_FilePath.c_str(), it->m_FilePath.length());
		tmpfileinf.m_FileSize = it->m_FileSize;
		tmpfileinf.m_IsOpen = it->m_IsOpen;

		tmpaddfilelist.push_back(tmpfileinf);

		if (tmpaddfilelist.size() >= m_ReportFileNum)
		{
			ReportAddFileToCls(tmpaddfilelist);
			tmpaddfilelist.clear();
		}
	}

	//不足上报最大条数的
	if (!tmpaddfilelist.empty())
	{
		ReportAddFileToCls(tmpaddfilelist);
	}

	return;
}
/*******************************************************************************
*  Function   : ReportUpdataFileToCls
*  Description: 上报更新文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportUpdateFile(const std::list<ReportFileItem > &filelist)
{
	if (filelist.empty())
	{
		return;
	}

	std::list<AddFileInf >   tmpupdatefilelist;
	std::list<ReportFileItem >::const_iterator it = filelist.begin();
	for (; it != filelist.end(); it++)
	{
		AddFileInf tmpfileinf;
		memcpy(tmpfileinf.m_FileName, it->m_FileName.c_str(), it->m_FileName.length());
		memcpy(tmpfileinf.m_FilePath, it->m_FilePath.c_str(), it->m_FilePath.length());
		tmpfileinf.m_FileSize = it->m_FileSize;
		tmpfileinf.m_IsOpen = it->m_IsOpen;

		tmpupdatefilelist.push_back(tmpfileinf);

		if (tmpupdatefilelist.size() >= m_ReportFileNum)
		{
			ReportUpdateFileToCls(tmpupdatefilelist);
			tmpupdatefilelist.clear();
		}
	}

	//不足上报最大条数的
	if (!tmpupdatefilelist.empty())
	{
		ReportUpdateFileToCls(tmpupdatefilelist);
	}

	return;
}
/*******************************************************************************
*  Function   : ReportDelFileToCls
*  Description: 上报删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportDelFile(const std::list<ReportFileItem > &filelist)
{
	if (filelist.empty())
	{
		return;
	}

	std::list<DelFileInf >   tmpdelfilelist;
	std::list<ReportFileItem >::const_iterator it = filelist.begin();
	for (; it != filelist.end(); it++)
	{
		DelFileInf tmpfileinf;
		memcpy(tmpfileinf.m_FileName, it->m_FileName.c_str(), it->m_FileName.length());
		memcpy(tmpfileinf.m_FilePath, it->m_FilePath.c_str(), it->m_FilePath.length());

		tmpdelfilelist.push_back(tmpfileinf);
		if (tmpdelfilelist.size() >= m_ReportFileNum)
		{
			ReportDelFileToCls(tmpdelfilelist);
			tmpdelfilelist.clear();
		}
	}

	//不足上报最大条数的
	if (!tmpdelfilelist.empty())
	{
		ReportDelFileToCls(tmpdelfilelist);
	}
}
/*******************************************************************************
*  Function   : ReportAddFileToCls
*  Description: 上报添加文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportAddFileToCls(const std::list<AddFileInf > &filelist)
{
	std::string url = "http://" + m_ClsListenAddress + CMD_CLS_ADD_FILE;

	CCLSAddFileReq req;
	req.SetNodeName(m_NodeName.c_str());
	req.SetFileList(filelist);

	char *buff = NULL;
	Int32 ret = req.Encode(buff);
	if (ret == 0)
	{
		HttpRequest  httprequeset;
		char *result = NULL;
		Int32 resultlen = 0;
		ret = httprequeset.Post(url.c_str(), buff, result, resultlen);
		if (ret == 0)
		{
			CCLSAddFileRsp rsp;
			rsp.Decode(result);
			
			DEBUG_LOG("Add File. ErrCode:%d-ErrDes:%s", rsp.GetErrCode(), rsp.GetErrDes())
		}
	}

	return;
}
/*******************************************************************************
*  Function   : ReportUpdateFileToCls
*  Description: 上报更新文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportUpdateFileToCls(const std::list<AddFileInf > &filelist)
{
	std::string url = "http://" + m_ClsListenAddress + CMD_CLS_UPDATE_FILE;

	CCLSUpdateFileReq req;
	req.SetNodeName(m_NodeName.c_str());
	req.SetFileList(filelist);

	char *buff = NULL;
	Int32 ret = req.Encode(buff);
	if (ret == 0)
	{
		HttpRequest  httprequeset;
		char *result = NULL;
		Int32 resultlen = 0;
		ret = httprequeset.Post(url.c_str(), buff, result, resultlen);
		if (ret == 0)
		{
			CCLSUpdateFileRsp rsp;
			rsp.Decode(result);

			DEBUG_LOG("Update File. ErrCode:%d-ErrDes:%s", rsp.GetErrCode(), rsp.GetErrDes())
		}
	}

	return;
}
/*******************************************************************************
*  Function   : ReportDelFileToCls
*  Description: 上报删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CFileReportMgr::ReportDelFileToCls(const std::list<DelFileInf > &filelist)
{
	std::string url = "http://" + m_ClsListenAddress + CMD_CLS_DELETE_FILE;

	CCLSDeleteFileReq req;
	req.SetNodeName(m_NodeName.c_str());
	req.SetFileList(filelist);

	char *buff = NULL;
	Int32 ret = req.Encode(buff);
	if (ret == 0)
	{
		HttpRequest  httprequeset;
		char *result = NULL;
		Int32 resultlen = 0;
		ret = httprequeset.Post(url.c_str(), buff, result, resultlen);
		if (ret == 0)
		{
			CCLSDeleteFileRsp rsp;
			rsp.Decode(result);

			DEBUG_LOG("Del File. ErrCode:%d-ErrDes:%s", rsp.GetErrCode(), rsp.GetErrDes())
		}
	}

	return;
}
/*******************************************************************************
*  Function   : ReportFileToCls
*  Description: 确认文件是否需要上报
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CFileReportMgr::IsReportFile(const ReportFileItem &item)
{
	Bool  ret = TRUE;
	if (item.m_ReportType == REPORT_DELETE)
	{
		//从添加目录删除
		FileNameInfMap::iterator it = m_TmpAddFileMap.find(item.m_FileName);
		if (it != m_TmpAddFileMap.end())
		{
			m_TmpAddFileMap.erase(it);
			ret = FALSE;
		}

		//从更新目录删除
		it = m_TmpUpdateFileMap.find(item.m_FileName);
		if (it != m_TmpUpdateFileMap.end())
		{
			m_TmpUpdateFileMap.erase(it);
		}
	}
	else if (item.m_ReportType == REPORT_UPDATE)//更新就不用上报
	{
		FileNameInfMap::iterator it = m_TmpAddFileMap.find(item.m_FileName);
		if (it != m_TmpAddFileMap.end())
		{
			it->second = item;
			ret = FALSE;
		}
	}

	return ret;
}







