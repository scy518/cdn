#ifndef DISK_SCANNER_H
#define DISK_SCANNER_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    diskfilescanner.h
*  Description:  磁盘文件扫描
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libconf/OSThread.h"
#include "libutil/OS.h"
#include "filereportmgr.h"

#include <map>
#include <list>
#include <string>

typedef std::map<std::string, Int64 >             StringIntMap;
typedef std::pair<std::string, Int64 >            StringIntPair;
typedef std::map<std::string, StringIntMap >      PathFileMap;

class CDiskFileScanner : public IFileHandler, public OSTask
{
	DECLARATION_SINGLETON(CDiskFileScanner)

public:

	//初始化磁盘扫描
	virtual Bool Initialize();

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//设置全量扫描
	virtual void SetScanAll();

	//扫描文件 IFileHandler回调函数
	virtual void HandleFile(const std::string &rootpath, const std::string &subpath, const std::string &filename);

protected:

	//检查文件改变通知
	void CheckChangeNotify();

	//扫描文件
	void ScanFileFromDisk();

	//按照目录修改时间扫描
	void DirModScan();

	//目录改变通知方式扫描
	void DirNotifyScan();

	//添加监听时间
	void InitChangeNotify(const std::string &strDir);

	//关闭文件改变通知
	void CloseChangeNotify(const std::string &strDir);

	//加载配置文件
	void LoadScanPathFromCfg();

private:

	//添加数据到缓存
	void AddFileToCache(const ReportFileItem &reportfile, UInt64 lasttime);

	//windows文件路径转换
	std::string WindowsPathToLinuxPath(const std::string &winpath);

private:
	OSMutex                          m_ScanMutex;
	PathFileMap                      m_PathFileTimeMap;    //保存文件路径下对应的文件
	StringIntMap                     m_ScanPathTimeMap;    //扫描路径和扫描时间

	std::map<std::string, Int32 >    m_MapNotify;

	Int32                            m_LoadPathInterval;    //加载配置文件路径间隔
	Int64                            m_LastLoadPathTime;    //最新加载扫描路径时间

	Int32                            m_AllScanInterval;
	Int64                            m_LastAllScanTime;     //最新全量扫描时间
	Int32                            m_ScanMode;           //扫描方式
};

#endif//__SOURCE_SCANNER__

