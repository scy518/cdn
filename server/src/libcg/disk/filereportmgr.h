#ifndef FILE_REPORT_MGR_H
#define FILE_REPORT_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    filereportmgr.h
*  Description:  文件管理,上报文件到cls
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clsdeletefile.h"
#include "libcdninterface/clsaddfile.h"
#include "libconf/OSThread.h"
#include "libutil/singleton.h"
#include "libcg/cgcommon.h"

#include <string>
#include <list>
#include <map>

typedef std::map<std::string, ReportFileItem >      FileNameInfMap;

class CFileReportMgr : OSTask
{
	DECLARATION_SINGLETON(CFileReportMgr)

public:

	//初始化OSTask对象
	virtual Bool Initialize();

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//添加上报文件
	void AddReportFileItem(const ReportFileItem &item);

protected:

	//统一上报文件
	void ReportFileToCls();

	//上报添加文件
	void ReportAddFile(const std::list<ReportFileItem > &filelist);

	//上报更新文件
	void ReportUpdateFile(const std::list<ReportFileItem > &filelist);

	//上报删除文件
	void ReportDelFile(const std::list<ReportFileItem > &filelist);

private:

	//确认文件是否需要上报
	Bool IsReportFile(const ReportFileItem &item);

	//上报添加文件
	void ReportAddFileToCls(const std::list<AddFileInf > &filelist);

	//上报更新文件
	void ReportUpdateFileToCls(const std::list<AddFileInf > &filelist);

	//上报删除文件
	void ReportDelFileToCls(const std::list<DelFileInf > &filelist);

private:

	OSMutex                       m_TmpFileMutex;
	FileNameInfMap                m_TmpAddFileMap;
	FileNameInfMap                m_TmpUpdateFileMap;
	FileNameInfMap                m_TmpDelFileMap;

	OSMutex                       m_FileMutex;
	std::list<ReportFileItem >    m_ReportAddFileList;

	
	std::string       m_ClsListenAddress;
	std::string       m_NodeName;
	Int32             m_ReportFileNum;
};


#endif








