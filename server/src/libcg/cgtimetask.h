#ifndef CG_TIME_TASK_H
#define CG_TIME_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgtimetask.h
*  Description:  cg定时任务，完成一些定时处理事件，心跳，定时全量同步
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libconf/OSThread.h"
#include "libutil/singleton.h"
#include "libutil/OSSocket.h"

#include <string>

class CCGTimeTask : public OSTask
{
	DECLARATION_SINGLETON(CCGTimeTask)
public:

	//初始化定时任务
	virtual Bool Initialize();

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

protected:
	
	//发送心跳
	void SendHeartbeat();

	//检查是否全量扫描，处理定事件进行全量扫描
	void CheckDiskAllScan();

	//发送注册消息
	void SendRegistToCLs();

	//发送心跳
	void SendHeartbeatToCls();

	//初始化参数
	Bool InitParam();

private:

	std::string        m_ClsListenAddress;

	Int32              m_ClsHeartbeatTime;
	Int64              m_LastHeartbeatTime;
	Bool               m_RegistState;          //注册是否成功

	std::string        m_NodeName;
	std::string        m_PrivateStreamAddress;
	std::string        m_PublicStreamAddress;
	std::string        m_AreaCode;
	Int32              m_Priority;

	Int32              m_ScanMode;          //扫描方式
	std::string        m_AllScanFixTime;    //全量扫描固定时间点，例如04:01 

	Int64              m_TotalBand;         //推流总带宽
	static Int64       m_FreeBand;          //空闲带宽
};
#endif

