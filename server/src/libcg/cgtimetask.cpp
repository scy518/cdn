/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgtimetask.cpp
*  Description:  cg定时任务，完成一些定时处理事件，心跳，定时全量同步
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clsheartbeat.h"
#include "libcdninterface/clsregister.h"
#include "libconf/ParamManager.h"
#include "libconf/httprequest.h"
#include "libconf/logmodule.h"
#include "libutil/OS.h"
#include "libcg/libcgmacros.h"
#include "disk/diskfilescanner.h"
#include "cgtimetask.h"

Int64 CCGTimeTask::m_FreeBand = 0;  

IMPLEMENT_SINGLETON(CCGTimeTask)
/*******************************************************************************
*  Function   : CCGTimeTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCGTimeTask::CCGTimeTask()
{
	m_RegistState  = FALSE;
	m_LastHeartbeatTime = 0;
	m_TotalBand = 100000;
	m_FreeBand  = 100000;
}
/*******************************************************************************
*  Function   : ~CCGTimeTask
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCGTimeTask::~CCGTimeTask()
{
	this->StopTask();
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化定时任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCGTimeTask::Initialize()
{
	if (!InitParam())
	{
		return FALSE;
	}

	//发送注册命令
	SendRegistToCLs();
	
	return this->RunTask();
}
/*******************************************************************************
*  Function   : Run
*  Description: OSTask的执行函数, 执行具体的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCGTimeTask::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		//检查是否需要全量扫描
		CheckDiskAllScan();

		//发送心跳
		SendHeartbeat();

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SendClsHeartbeat
*  Description: 发送心跳
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCGTimeTask::SendHeartbeat()
{
	//判断是否注册成功
	if (!m_RegistState)
	{
		SendRegistToCLs();
	}

    //判断是否心跳到时
	if (OS::Milliseconds() - m_LastHeartbeatTime < m_ClsHeartbeatTime)
	{
		return;
	}

	//发送心跳
	SendHeartbeatToCls();

	return;
}
/*******************************************************************************
*  Function   : CheckDiskAllScan
*  Description: 检查是否全量扫描，处理定事件进行全量扫描
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCGTimeTask::CheckDiskAllScan()
{
	if(m_ScanMode != SCAN_MODE_FIXED_TIME)
	{
		return;
	}

	std::string str = OS::Localtime();
	if(strstr(str.c_str(), m_AllScanFixTime.c_str()) != NULL)
	{
		CDiskFileScanner::Intstance()->SetScanAll();
	}

	return;
}
/*******************************************************************************
*  Function   : SendClsRegist
*  Description: 发送注册消息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCGTimeTask::SendRegistToCLs()
{
	std::string url = "http://" + m_ClsListenAddress + CMD_CLS_REGISTER;

	CCLSRegisterReq req;
	req.SetNodeName(m_NodeName.c_str());
	req.SetPrivateStreamAddress(m_PrivateStreamAddress.c_str());
	req.SetPublicStreamAddress(m_PublicStreamAddress.c_str());
	req.SetTotalBand(m_TotalBand);
	req.SetFreeBand(m_FreeBand);
	req.SetAreaCode(m_AreaCode.c_str());
	req.SetPriority(m_Priority);

	char *buff = NULL;
	Int32 ret = req.Encode(buff);
	if (ret == 0)
	{
		HttpRequest  httprequeset;
		char *result = NULL;
		Int32 resultlen = 0;
		ret = httprequeset.Post(url.c_str(), buff, result, resultlen);
		if (ret == 0)
		{
			CCLSRegisterRsp rsp;
			rsp.Decode(result);
			if (rsp.GetErrCode() != 0) //判断返回结果是否成功
			{
				m_RegistState = FALSE;
			}
			else
			{
				m_RegistState = TRUE;
			}
			DEBUG_LOG("Add File. ErrCode:%d-ErrDes:%s", rsp.GetErrCode(), rsp.GetErrDes())
		}
	}
	else
	{
		m_RegistState = FALSE;
	}

	return;
}
/*******************************************************************************
*  Function   : SendClsHeartbeat
*  Description: 发送心跳
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCGTimeTask::SendHeartbeatToCls()
{
	std::string url = "http://" + m_ClsListenAddress + CMD_CLS_HEART_BEAT;

	CCLSHeartbeatReq req;
	req.SetNodeName(m_NodeName.c_str());
	req.SetTotalBand(m_TotalBand);
	req.SetFreeBand(m_FreeBand);

	char *buff = NULL;
	Int32 ret = req.Encode(buff);
	if (ret == 0)
	{
		HttpRequest  httprequeset;
		char *result = NULL;
		Int32 resultlen = 0;
		ret = httprequeset.Post(url.c_str(), buff, result, resultlen);
		if (ret == 0)
		{
			CCLSHeartbeatRsp rsp;
			rsp.Decode(result);
			if (rsp.GetErrCode() != 0) //判断返回结果是否成功
			{
				m_RegistState = FALSE;
			}
			DEBUG_LOG("Add File. ErrCode:%d-ErrDes:%s", rsp.GetErrCode(), rsp.GetErrDes())
		}
	}
	else
	{
		m_RegistState = FALSE;
	}

	return;
}
/*******************************************************************************
*  Function   : InitParam
*  Description: 初始化参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CCGTimeTask::InitParam()
{
	//获取心跳时间
	m_ClsHeartbeatTime = CParamManager::Intstance()->GetIntParam(CLS_HEARTBEAT_TIME, 5)*1000;

	//获取节点名称
	m_NodeName = std::string(CParamManager::Intstance()->GetCharParam(NODE_NAME, ""));
	if (m_NodeName.empty())
	{
		ERROR_LOG("node name is empty.")
		return FALSE;
	}

	//获取cls地址
	m_ClsListenAddress = std::string(CParamManager::Intstance()->GetCharParam(CLS_ADDRESS, ""));
	if (m_ClsListenAddress.empty())
	{
		ERROR_LOG("cls ip is empty.")
		return FALSE;
	}

	//获取推流ip
	m_PublicStreamAddress = std::string(CParamManager::Intstance()->GetCharParam(PUBLIC_STREAM_ADDRESS, ""));
	if (m_PublicStreamAddress.empty())
	{
		ERROR_LOG("public stream address is empty.")
		return FALSE;
	}

	//获取推流ip
	m_PrivateStreamAddress = std::string(CParamManager::Intstance()->GetCharParam(PRIVATE_STREAM_ADDRESS, ""));
	if (m_PrivateStreamAddress.empty())
	{
		m_PrivateStreamAddress = m_PublicStreamAddress;
	}

	//获取区域码
	m_AreaCode = std::string(CParamManager::Intstance()->GetCharParam(AREA_CODE, "10")); 

	//获取优先级
	m_Priority = CParamManager::Intstance()->GetIntParam(PRIORITY, 1);

	//默认采用间隔扫描
	m_ScanMode = CParamManager::Intstance()->GetIntParam(SCAN_MODE, SCAN_MODE_TIMING);

	//获取固定时间
	m_AllScanFixTime = std::string(CParamManager::Intstance()->GetCharParam(ALL_SCAN_FIXTIME, "04:00")); 

	return TRUE;
}


