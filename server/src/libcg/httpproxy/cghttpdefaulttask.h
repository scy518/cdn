#ifndef CG_HTTP_DEFAULT_TASK_H
#define CG_HTTP_DEFAULT_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cghttpdefaulttask.h
*  Description:  cg默认http处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttp/httptaskbase.h"

class CCGHttpDefaultTask : public CHttpTaskBase
{
	CONF_DECLARE_DYNCREATE(CCGHttpDefaultTask)
public:

	CCGHttpDefaultTask();

	virtual ~CCGHttpDefaultTask();

protected:

	//应用实现函数，统一处理响应消息，应用只需要填充响应消息即可,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

	//填充消息头
	void FillHttpHead(CHTTPResponse *rsp);
};


#endif


