/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cghttpdefaulttask.h
*  Description:  cg默认http处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OS.h"
#include "libcg/libcgmacros.h"
#include "httpproxymgr.h"
#include "cghttpdefaulttask.h"

CONF_IMPLEMENT_DYNCREATE(CCGHttpDefaultTask, CHttpTaskBase)
/*******************************************************************************
*  Function   : COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCGHttpDefaultTask::CCGHttpDefaultTask()
{
}
/*******************************************************************************
*  Function   : ~COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCGHttpDefaultTask::~CCGHttpDefaultTask()
{
}
/*******************************************************************************
*  Function   : COTTCGHttpDownLoad
*  Description: 应用实现函数，统一处理响应消息，应用只需要填充响应消息即可,
			  : 成功返回0，非0标示失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCGHttpDefaultTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		return -1;
	}

	//填充
	FillHttpHead(rsp);

	//构造请求消息
	CHTTPRequest *tmpreq = new CHTTPRequest(*req);
	if (tmpreq != NULL)
	{
		if (CHttpProxyMgr::Intstance()->AddHttpProxyReq(tmpreq) == 0)
		{
			rsp->SetMsgType(MSG_RESPONSE);
		}
		else
		{
			rsp->SetEnumErrorCode(HTTP_ERROR_404);
		}
	}
	else
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_503);
	}

	return 0;
}
/*******************************************************************************
*  Function   : FillHttpHead
*  Description: 填充消息头
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CCGHttpDefaultTask::FillHttpHead(CHTTPResponse *rsp)
{
	if (rsp == NULL)
	{
		return;
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_200);
	rsp->SetAttrValue("Content-Type", "application/x-www-form-urlencoded");
	rsp->SetAttrValue("Transfer-Encoding", "chunked");
	rsp->SetAttrValue("Date", OS::Localtime().c_str());
	rsp->SetAttrValue("Server", "CDN CLS Server");
	if (CHttpProxyMgr::Intstance()->GetHttpKeepLive() == HTTP_NO_KEEP_LIVE)
	{
		rsp->SetAttrValue("Connection", "close");
	}

	return;
}



