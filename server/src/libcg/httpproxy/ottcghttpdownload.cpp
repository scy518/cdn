/*******************************************************************************
*  Copyright (c), 2010, songchuangye.
*  All rights reserved.
*
*  File Name:    ottcghttpdownload.cpp
*  Description:  http代理下载
*  Others:
*  Version: 1.0       Author:scy 907818       Date: 2013-09-16
*  History:
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttp/httpsessionmanage.h"
#include "libconf/logmodule.h"
#include "ottcghttpdownload.h"
/*******************************************************************************
*  Function   : COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COTTCGHttpDownLoad::COTTCGHttpDownLoad()
{
	m_Req             = NULL;
}
/*******************************************************************************
*  Function   : ~COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
COTTCGHttpDownLoad::~COTTCGHttpDownLoad()
{
}
/*******************************************************************************
*  Function   : DownLoadData
*  Description: 同步下载数据，单位为毫秒
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COTTCGHttpDownLoad::DownLoadData(const char *url, CHTTPRequest *req,  
									   const char *cachefilename, Int32 timeout)
{
	if (url == NULL || req == NULL)
	{
		return -1;
	}

	//移除之前curl
	RemoveCurlHandle();

	Int32 biterate = 0;

	// 初始化curl
	Int32 ret = InitCurl(url, timeout);
	if (ret == 0)
	{

		//初始化头buff
		ret = InitHeadBuff();
		if (ret != 0)
		{
			ERROR_LOG("Init head buff fail. ret:%d", ret)
			return -2;
		}

		//初始化缓存文件
		if (cachefilename != NULL)
		{
			ret = InitFileHandle(cachefilename);
			if (ret != 0)
			{
				ERROR_LOG("Init file handle fail. ret:%d", ret)
				return -3;
			}
		}

		//保存代理请求请求
		m_Req = req;

		//成功在开启下载
		ret = StartDownLoad(biterate);
	}


	INFO_LOG("[DownLoadData]Down load data finish. biterate:%d; ret=%d", biterate, ret);
	return ret;
}
/*******************************************************************************
*  Function   : Write
*  Description: 写回调函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COTTCGHttpDownLoad::Write(char *buffer, Int32 size, Int32 nitems)
{
	if (IsStop())
	{
		return -1;
	}

	Int32 len = size*nitems;

	//发送http包体
	SendHttpBody(buffer, len);

	//下载数据到文件
	Int32 ret = WriteDataToFile(buffer, len);
	if (ret != len)
	{
		ERROR_LOG("cache file fail. ret:%d", ret)
	}

	return len;
}
/*******************************************************************************
*  Function   : SendHttpBody
*  Description: 发送http 包体数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 COTTCGHttpDownLoad::SendHttpBody(char *buff, Int32 len)
{
	CHTTPResponse *rsp = new CHTTPResponse();
	if (rsp == NULL)
	{
		ERROR_LOG("new CHTTPResponse fail.")
		return -1;
	}

	//发送chunk 包头
	char szHeader[16] = {0};
	Int32 result = sprintf(szHeader, "%x\r\n", len);

	rsp->SetIndex(m_Req->GetIndex());
	rsp->SetMsgType(MSG_ONLY_BODY_RESPONSE);
	rsp->SetBody(szHeader, result);
	rsp->AppendBody(buff, len);

	//发送chunk 包结束符
	memset(szHeader,0,16);
	result = sprintf(szHeader, "\r\n");
	rsp->AppendBody(szHeader, result);

	//添加消息
	CHTTPSessionManage::Intstance()->AddSendHttpMessage(rsp);

	return 0;
}

