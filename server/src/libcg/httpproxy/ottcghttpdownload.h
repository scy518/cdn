#ifndef OTT_CG_HTTP_DOWN_LOAD
#define OTT_CG_HTTP_DOWN_LOAD
/*******************************************************************************
*  Copyright (c), 2010, songchuangye.
*  All rights reserved.
*
*  File Name:    ottcghttpdownload.h
*  Description:  http代理下载
*  Others:
*  Version: 1.0       Author:scy 907818       Date: 2013-09-16
*  History:
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libconf/httpdownload.h"
#include "libhttp/httpcommon.h"

class COTTCGHttpDownLoad : public CHttpDownLoad
{
public:

	COTTCGHttpDownLoad();

	virtual ~COTTCGHttpDownLoad();

	//同步下载数据，单位为毫秒
	Int32 DownLoadData(const char *url, CHTTPRequest *req,  const char *cachefilename, 
					   Int32 timeout = 0);
protected:

	//写回调函数
	virtual Int32 Write(char *buffer, Int32 size, Int32 nitems);

	//发送http 包体数据
	virtual Int32 SendHttpBody(char *buff, Int32 len);

private:

	CHTTPRequest    *m_Req;
};



#endif



