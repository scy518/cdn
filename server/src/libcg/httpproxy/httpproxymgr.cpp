/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    httpproxymgr.cpp
*  Description:  代理管理模块
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libconf/ParamManager.h"
#include "libconf/logmodule.h"
#include "libhttp/httpsessionmgr.h"
#include "libutil/md5.h"
#include "libcg/libcgmacros.h"
#include "disk/cachespacemgr.h"
#include "ottcghttpdownload.h"
#include "httpproxymgr.h"

IMPLEMENT_SINGLETON(CHttpProxyMgr)
/*******************************************************************************
*  Function   : COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHttpProxyMgr::CHttpProxyMgr()
{
	m_IsKeepLive = HTTP_NO_KEEP_LIVE;
}
/*******************************************************************************
*  Function   : COTTCGHttpDownLoad
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHttpProxyMgr::~CHttpProxyMgr()
{
	m_ThreadMgr.Destroy();
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHttpProxyMgr::Init()
{
	//获取是否长连接
	m_IsKeepLive = CParamManager::Intstance()->GetIntParam(IS_KEEP_LIVE, HTTP_NO_KEEP_LIVE);

	//获取父节点cls地址
	m_ParentClsAddress = std::string(CParamManager::Intstance()->GetCharParam(PARENT_CLS_ADDRESS, ""));

	//如果父节点为空，就不需要启动线程处理
	if (m_ParentClsAddress.empty())
	{
		return 0;
	}

	//获取线程参数
	Int32 threadnum = CParamManager::Intstance()->GetIntParam(PROXY_THREAD_NUM, 8);
	if (m_ThreadMgr.Initialize(this, threadnum) != 0)
	{
		return -1;
	}

	return 0;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHttpProxyMgr::Run()
{
	CHTTPRequest *req = GetHttpProxyReq();
	if (req == NULL)
	{
		return 0;
	}

	//下载数据
	COTTCGHttpDownLoad  ottdownload;
	std::string filename = BuildCacheFileName(req);

	//下载开始
	std::string url = "http://" + m_ParentClsAddress + std::string(req->GetUrl().c_str());
	Int32 ret = ottdownload.DownLoadData(url.c_str(), req, filename.c_str());
	if (ret != 0)
	{
		ERROR_LOG("Down load data fail. ret:%d", ret)
	}
	else
	{
		//发送结束
		SendHttpChunckEnd(req);
	}

	delete req;
	req = NULL;

	return 0;
}
/*******************************************************************************
*  Function   : AddHttpProxyReq
*  Description: 添加数据发送请求
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CHttpProxyMgr::AddHttpProxyReq(CHTTPRequest *req)
{
	if (req == NULL || !IsExistParentCls())
	{
		return -1;
	}

	m_Mutex.Lock();
	m_HttpReqList.push_back(req);
	m_Mutex.Unlock();

	return 0;
}
/*******************************************************************************
*  Function   : GetHttpProxyReq
*  Description: 获取HttpProxyReq
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CHTTPRequest* CHttpProxyMgr::GetHttpProxyReq()
{
	CHTTPRequest *tmpreq = NULL;

	m_Mutex.Lock();
	if (!m_HttpReqList.empty())
	{
		tmpreq = m_HttpReqList.front();
		m_HttpReqList.pop_front();
	}
	m_Mutex.Unlock();

	return tmpreq;
}
/*******************************************************************************
*  Function   : BuildCacheFileName
*  Description: 生成缓存文件名
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
std::string CHttpProxyMgr::BuildCacheFileName(CHTTPRequest *req)
{
	if (req == NULL)
	{
		return "";
	}

	LibConf::CString  url = req->GetUrl();
	if (url.empty())
	{
		return "";
	}

	//获取cache路径
	std::string cachepath = CCacheSpaceMgr::Intstance()->GetCachePath();
	if (cachepath.empty())
	{
		return "";
	}

	//md5 文件名 ,只取文件名进行md5运算，
	unsigned char tmpmd5[17] = {0};
	char filename[64] = {0};
	MD5_FromString((const unsigned char *)url.c_str(), url.length(), tmpmd5);
	MD5_String(tmpmd5, filename);

	return cachepath+std::string(filename);
}
/*******************************************************************************
*  Function   : SendHttpChunckEnd
*  Description: 发送chunck结束
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CHttpProxyMgr::SendHttpChunckEnd(CHTTPRequest *req)
{
	std::string chunckend = "0\r\n\r\n";
	CHTTPResponse *rsp = new CHTTPResponse();
	if (rsp == NULL)
	{
		ERROR_LOG("new CHTTPResponse fail.")
		return;
	}

	rsp->SetIndex(req->GetIndex());
	if (m_IsKeepLive == HTTP_NO_KEEP_LIVE)
	{
		rsp->SetMsgType(MSG_DELSESSION_ONLY_BODY_RESPONSE);
	}
	else
	{
		rsp->SetMsgType(MSG_ONLY_BODY_RESPONSE);
	}
	
	rsp->SetBody(chunckend.c_str(), chunckend.length());

	//添加消息
	CHTTPSessionMgr::Intstance()->AddSendHttpMessage(rsp);

	return;
}
/*******************************************************************************
*  Function   : IsExistParentCls
*  Description: 代理源是否存在
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CHttpProxyMgr::IsExistParentCls()
{
	if (m_ParentClsAddress.empty())
	{
		return FALSE;
	}

	return TRUE;
}













