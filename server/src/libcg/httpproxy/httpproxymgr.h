#ifndef HTTP_PROXY_MGR_H
#define HTTP_PROXY_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    httpproxymgr.h
*  Description:  代理管理模块
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libconf/threadmanager.h"
#include "libhttp/httpcommon.h"
#include "libconf/task.h"

#include <string>

class CHttpProxyMgr : CTask
{
	//声明任务类型
	DECLARE_TASK(CTask)

	//定义独体类
	DECLARATION_SINGLETON(CHttpProxyMgr)

public:

	//初始化
	virtual Int32 Init();

	//线程调用函数
	virtual Int32 Run();

	//添加数据发送请求
	Int32 AddHttpProxyReq(CHTTPRequest *req);

	//获取HttpProxyReq
	CHTTPRequest* GetHttpProxyReq();

	//获取连接类型
	Int32 GetHttpKeepLive() {return m_IsKeepLive;}

protected:

	//生成缓存文件名
	std::string BuildCacheFileName(CHTTPRequest *req);   

	//发送chunck结束
	void SendHttpChunckEnd(CHTTPRequest *req);

	//代理源是否存在
	Bool IsExistParentCls();

private:

	CThreadManager                 m_ThreadMgr;

	OSMutex                        m_Mutex;
	std::list<CHTTPRequest* >      m_HttpReqList;

	Int32                          m_IsKeepLive; //是否长连接

	std::string                    m_ParentClsAddress; //父cls地址
};

#endif


