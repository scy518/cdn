/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clgetresult.cpp
*  Description:  查询文件注入状态
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clgetresult.h"
/*******************************************************************************
*  Function   : CCLGetResultReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLGetResultReq::CCLGetResultReq()
{
	ResetFileList();
}
/*******************************************************************************
*  Function   : ~CCLGetResultReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLGetResultReq::~CCLGetResultReq()
{
	ResetFileList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLGetResultReq::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}
	cJSON_AddItemToObject(json, "FileList", jsonarray);

	std::list<LibCore::CString>  *tmpfilelist = GetFileList();
	std::list<LibCore::CString >::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->c_str());
		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLGetResultReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	cJSON *jsonArry = cJSON_GetObjectItem(json,"FileList");
	if (jsonArry == NULL)
	{
		return -2;
	}

	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		char tmpfilename[64] = {0};
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");
		SetFileList(LibCore::CString(tmpfilename));

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}
/******************************************************************************/
/*                  CCLGetResultRsp                                     */
/******************************************************************************/
/*******************************************************************************
*  Function   : CCLGetResultRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLGetResultRsp::CCLGetResultRsp()
{
	ResetResultList();
}
/*******************************************************************************
*  Function   : ~CCLGetResultRsp
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLGetResultRsp::~CCLGetResultRsp()
{
	ResetResultList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLGetResultRsp::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}

	cJSON_AddItemToObject(json, "ResultList", jsonarray);

	CLIngestResultList  *tmpfilelist = GetResultList();
	CLIngestResultList::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->m_FileName);
		cJSON_AddNumberToObject(tmparry, "Status", it->m_Status);
		cJSON_AddNumberToObject(tmparry, "Result", it->m_Result);
		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLGetResultRsp::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	cJSON *jsonArry = cJSON_GetObjectItem(json,"ResultList");
	if (jsonArry == NULL)
	{
		return -2;
	}

	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		CLIngestResult  result;
		char *tmpfilename = result.m_FileName;
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");

		Int32 tmpStatus = 0;
		HTTP_CJSON_GET_INT32(tmpStatus, jsonlist, "Status");
		result.m_Status = tmpStatus;

		Int32 tmpret = 0;
		HTTP_CJSON_GET_INT32(tmpret, jsonlist, "Result");
		result.m_Result = tmpret;

		m_ResultList.push_back(result);

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}




