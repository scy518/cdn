/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cpmingestfile.h
*  Description:  文件注入接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/cpmingestfile.h"
/*******************************************************************************
*  Function   : CCPMIngestFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMIngestFileReq::CCPMIngestFileReq()
{
	ResetproviderID();
	ResetassetID();
	SetcontentType(0);
	ResetFileList();
}
/*******************************************************************************
*  Function   : ~CCPMIngestFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMIngestFileReq::~CCPMIngestFileReq()
{
	ResetproviderID();
	ResetassetID();
	SetcontentType(0);
	ResetFileList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMIngestFileReq::EncodeJson(cJSON *json)
{
	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}

	cJSON_AddStringToObject(json, "providerID", GetproviderID());
	cJSON_AddStringToObject(json, "assetID", GetassetID());
	cJSON_AddNumberToObject(json, "contentType", GetcontentType());
	cJSON_AddItemToObject(json, "FileList", jsonarray);

	//构造json列表
	std::list<CPMIngestFileInfo>  *tmpfilelist = GetFileList();
	std::list<CPMIngestFileInfo>::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->m_FileName);
		cJSON_AddStringToObject(tmparry, "SourceUrl", it->m_SourceUrl);
		cJSON_AddNumberToObject(tmparry, "ContentSize", it->m_ContentSize);
		cJSON_AddNumberToObject(tmparry, "BitRate", it->m_BitRate);
		cJSON_AddNumberToObject(tmparry, "Duration", it->m_Duration);

		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMIngestFileReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(providerID, json, "providerID");
	HTTP_CJSON_GET_STRING_FUN(assetID, json, "assetID");
	HTTP_CJSON_GET_INT32_FUN(contentType, json, "contentType")

	cJSON *jsonArry = cJSON_GetObjectItem(json,"FileList");
	if (jsonArry == NULL)
	{
		return -2;
	}

	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		CPMIngestFileInfo fileinfo;
		char *tmpfilename= fileinfo.m_FileName;
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");

		char *tmpfilepath = fileinfo.m_SourceUrl;
		HTTP_CJSON_GET_STRING(tmpfilepath, jsonlist, "SourceUrl");

		Int64 tmpContentSize = 0;
		HTTP_CJSON_GET_INT64(tmpContentSize, jsonlist, "ContentSize");
		fileinfo.m_ContentSize = tmpContentSize;

		Int64 tmpBitRate = 0;
		HTTP_CJSON_GET_INT32(tmpBitRate, jsonlist, "BitRate");
		fileinfo.m_BitRate = tmpBitRate;

		Int64 tmpDuration = 0;
		HTTP_CJSON_GET_INT32(tmpDuration, jsonlist, "Duration");
		fileinfo.m_Duration = tmpDuration;

		SetFileList(fileinfo);

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}




