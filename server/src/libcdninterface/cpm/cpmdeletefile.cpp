/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cpmdeletefile.cpp
*  Description:  删除文件
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/cpmdeletefile.h"
/*******************************************************************************
*  Function   : CCPMDeleteFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMDeleteFileReq::CCPMDeleteFileReq()
{
	ResetproviderID();
	ResetassetID();
	SetcontentType(0);
}
/*******************************************************************************
*  Function   : ~CCPMDeleteFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCPMDeleteFileReq::~CCPMDeleteFileReq()
{
	ResetproviderID();
	ResetassetID();
	SetcontentType(0);
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMDeleteFileReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "providerID", GetproviderID());
	cJSON_AddStringToObject(json, "assetID", GetassetID());
	cJSON_AddNumberToObject(json, "contentType", GetcontentType());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCPMDeleteFileReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	//获取模糊查询
	HTTP_CJSON_GET_STRING_FUN(providerID, json, "providerID");
	HTTP_CJSON_GET_STRING_FUN(assetID, json, "assetID");
	HTTP_CJSON_GET_INT32_FUN(contentType, json, "contentType")

	return 0;
}




