/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsheartbeat.h
*  Description:  cls心跳接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clsheartbeat.h"
/*******************************************************************************
*  Function   : CCLSHeartbeatReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSHeartbeatReq::CCLSHeartbeatReq()
{
	ResetNodeName();
	SetTotalBand(0);
	SetFreeBand(0);
}
/*******************************************************************************
*  Function   : ~CCLSHeartbeatReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSHeartbeatReq::~CCLSHeartbeatReq()
{
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSHeartbeatReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "NodeName", GetNodeName());
	cJSON_AddNumberToObject(json, "TotalBand", GetTotalBand());
	cJSON_AddNumberToObject(json, "FreeBand", GetFreeBand());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSHeartbeatReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(NodeName, json, "NodeName");
	HTTP_CJSON_GET_INT64_FUN(TotalBand, json, "TotalBand");
	HTTP_CJSON_GET_INT64_FUN(FreeBand, json, "FreeBand");

	return 0;
}





