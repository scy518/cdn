/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsaddfile.cpp
*  Description:  cg添加文件上报接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/clsaddfile.h"
/*******************************************************************************
*  Function   : CCLSAddFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSAddFileReq::CCLSAddFileReq()
{
	ResetNodeName();
	ResetFileList();
}
/*******************************************************************************
*  Function   : ~CCLSAddFileReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CCLSAddFileReq::~CCLSAddFileReq()
{
	ResetNodeName();
	ResetFileList();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSAddFileReq::EncodeJson(cJSON *json)
{
	//添加节点名
	cJSON_AddStringToObject(json, "NodeName", GetNodeName());

	//构建json数组
	cJSON *jsonarray = cJSON_CreateArray();
	if (NULL == jsonarray)
	{
		return -2;
	}
	cJSON_AddItemToObject(json, "FileList", jsonarray);

	std::list<AddFileInf>  *tmpfilelist = GetFileList();
	std::list<AddFileInf >::iterator it = tmpfilelist->begin();
	for(; it != tmpfilelist->end(); it++)
	{
		cJSON *tmparry = cJSON_CreateObject();
		if (NULL == tmparry)
		{
			return -3;
		}

		cJSON_AddStringToObject(tmparry, "FileName", it->m_FileName);
		cJSON_AddStringToObject(tmparry, "FilePath", it->m_FilePath);
		cJSON_AddNumberToObject(tmparry, "FileSize", it->m_FileSize);	
		cJSON_AddNumberToObject(tmparry, "IsOpen", it->m_IsOpen);	

		cJSON_AddItemToArray(jsonarray, tmparry);
	}

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CCLSAddFileReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(NodeName, json, "NodeName");

	cJSON *jsonArry = cJSON_GetObjectItem(json,"FileList");
	cJSON *jsonlist=jsonArry->child;
	while(jsonlist!=NULL)
	{
		AddFileInf fileinfo;
		char *tmpfilename= fileinfo.m_FileName;
		HTTP_CJSON_GET_STRING(tmpfilename, jsonlist, "FileName");

		char *tmpfilepath = fileinfo.m_FilePath;
		HTTP_CJSON_GET_STRING(tmpfilepath, jsonlist, "FilePath");

		Int64 tmpFileSize = 0;
		HTTP_CJSON_GET_INT64(tmpFileSize, jsonlist, "FileSize");
		fileinfo.m_FileSize = tmpFileSize;

		Int32 tmpisopen = 0;
		HTTP_CJSON_GET_INT32(tmpisopen, jsonlist, "IsOpen");
		fileinfo.m_IsOpen = tmpisopen;

		SetFileList(fileinfo);

		jsonlist = jsonlist->next;
	}

	jsonArry = NULL;
	jsonlist     = NULL;

	return 0;
}



























