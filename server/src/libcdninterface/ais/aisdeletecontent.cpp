/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cideletecontent.cpp
*  Description:  删除内容接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/aisdeletecontent.h"
/*******************************************************************************
*  Function   : CAISDeleteContentReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDeleteContentReq::CAISDeleteContentReq()
{
	ResetproviderID();
	ResetassetID();
}
/*******************************************************************************
*  Function   : ~CAISDeleteContentReq
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDeleteContentReq::~CAISDeleteContentReq()
{
	ResetproviderID();
	ResetassetID();
}
/*******************************************************************************
*  Function   : EncodeJson
*  Description: 构造json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDeleteContentReq::EncodeJson(cJSON *json)
{
	//添加providerID
	cJSON_AddStringToObject(json, "providerID", GetproviderID());
	cJSON_AddStringToObject(json, "assetID", GetassetID());
	cJSON_AddNumberToObject(json, "serviceType", GetserviceType());

	return 0;
}
/*******************************************************************************
*  Function   : DecodeJson
*  Description: 解码json代码
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDeleteContentReq::DecodeJson(cJSON *json)
{
	if (json == NULL)
	{
		return -1;
	}

	HTTP_CJSON_GET_STRING_FUN(providerID, json, "providerID");
	HTTP_CJSON_GET_STRING_FUN(assetID, json, "assetID");
	HTTP_CJSON_GET_INT32_FUN(serviceType, json, "serviceType");

	return 0;
}

