#ifndef LIB_CLS_STATIC_CONF_H
#define LIB_CLS_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicconf.h
*  Description:  cls静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>

class CLibCLSStaticConf  : public OSTask
{
	DECLARATION_SINGLETON(CLibCLSStaticConf)
public:

	//初始化节点管理器
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//cls缓存类型
	static Int32                   m_ClsCacheType;

	//时移分片时长
	static Int32                    m_ShiftTimeDuration;

	//时移延时文件片数
	static Int32                    m_ShiftTimeDelayNum;

	//时移m3u8返回的片数
	static Int32                    m_ShiftTimeReserveNum;
	 
	//数据库文件
	static std::string              m_SqlFileName;

	//全量同步时间间隔，单位为s
	static Int32                    m_ClsIncSynInterval;

	//增量同步时间间隔
	static Int32                    m_ClsAllSynInterval;

	//节点同步时间间隔，单位为s
	static Int32                    m_ClsSynNodeInterval;

	//默认使用流地址类型
	static Int32                    m_ClsUseStreamAddrType;

	//校验token key值
	static std::string              m_ClsTokenKey;

	//是否开启token校验 0:标示不开启 1：标示开启
	static Int32                    m_ClsIsCheckToken;

	//使用token算法   0：只校验url合法性 1：带有试看功能算法
	static Int32                    m_ClsTokenAlgorithmic;

protected:

	//同步参数
	void SynStaticParam();

};
#endif



