#ifndef CLS_HTTP_DEFAULT_TASK_H
#define CLS_HTTP_DEFAULT_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clshttpdefaulttask.h
*  Description:  cls默认http处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpserver/httptaskbase.h"

class CCLSHttpDefaultTask : public CHttpTaskBase
{
	CONF_DECLARE_DYNCREATE(CCLSHttpDefaultTask)
public:

	CCLSHttpDefaultTask();

	virtual ~CCLSHttpDefaultTask();

	//处理任务,优先调用应用自己处理返回函数，如果失败调用响应统一返回函数
	virtual void OnDealWith(CHTTPRequest *req); 

protected:

	//创建实际http对象
	CHttpTaskBase* CreateRealHttpTask(CHTTPRequest*req);
};


#endif


