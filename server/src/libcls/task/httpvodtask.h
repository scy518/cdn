#ifndef HTTP_VOD_TASK_H
#define HTTP_VOD_TASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:httpvodtask.h
version  :
created  : 2013-2-12   11:52
file path: 
modify   :
author   : songchuangye
purpose  : ott点播处理
*******************************************************************************/
#include "libhttpserver/httptaskbase.h"
#include "libcls/clscommon.h"
#include <string>

class CHTTPVodTask : public CHttpTaskBase
{
	CONF_DECLARE_DYNCREATE(CHTTPVodTask)
public:

	CHTTPVodTask();

	virtual ~CHTTPVodTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

protected:

	//返回最优cg节点
	Int32 DoDispatchBestNode(CHTTPRequest *req, CHTTPResponse *rsp);

	//填充http头
	void FillHttpHead(CHTTPResponse *rsp);

	//获取重定向URL
	std::string GetUrlAddress(const char *url, const std::string &nodeaddr, const std::string &path,
		                      const std::string &filename, const char *tpyevalue);

	//解析点播参数
	virtual SInt32 ParaVodParam(const char *url, VodParam &vodparam, const std::string key = "/vod/");

	//分割pid和aid
	virtual SInt32 SplitPidAndAid(const char *url, std::string &pid, std::string &aid, 
		                          const std::string key = "/vod/");

	//获取使用地址类型
	virtual SInt32 GetUseAddrTypeFromUrl(const char *url);

	//获取需要的节点地址
	std::string GetNeedNodeAddr(const Node &node, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

	//校验token
	virtual Bool CheckToken(CHTTPRequest *req);
};



#endif


