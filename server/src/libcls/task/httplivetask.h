#ifndef HTTP_LIVE_TASK_H
#define HTTP_LIVE_TASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name: httplivetask.h
version  :
created  : 2013-2-12   11:52
file path: 
modify   :
author   : songchuangye
purpose  : ott直播处理
*******************************************************************************/
#include "libcls/clscommon.h"
#include "task/httpvodtask.h"

class CHTTPLiveTask : public CHTTPVodTask
{
	CONF_DECLARE_DYNCREATE(CHTTPLiveTask)
public:

	CHTTPLiveTask();

	virtual ~CHTTPLiveTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

	//构造直播m3u8
	std::string BuildLiveM3u8(const LiveParam &liveparam, const std::string &url);

	//构造时移m3u8
	std::string BuildShiftTimeM3u8(const LiveParam &liveparam, const std::string &url);

	//构造回看m3u8
	std::string BuildLookBackM3u8(const LiveParam &liveparam, const std::string &url);

	//解析直播参数
	SInt32 ParaLiveParam(const char *url, LiveParam &liveparam);
};

#endif




