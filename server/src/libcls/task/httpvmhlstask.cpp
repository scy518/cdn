/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : ott点播vmhls处理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhttpserver/httperror.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/filedistributemgr.h"
#include "task/httpvmhlstask.h"
#include "libclsstaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPVmhlsTask, CHTTPVodTask)
/*******************************************************************************
*  Function   : CHTTPVmhlsTask
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPVmhlsTask::CHTTPVmhlsTask()
{
}
/*******************************************************************************
*  Function   : ~CHTTPVmhlsTask
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPVmhlsTask::~CHTTPVmhlsTask() 
{
}
/*******************************************************************************
*  Function   : DoRequst
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHTTPVmhlsTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp) 
{ 
	if (req == NULL || rsp == NULL)
	{
		LOG_TRACE("req or rsp is null")
		return -1;
	}

	//填充通用信息
	FillHttpHead(rsp);

	//解析vod参数
	VodParam vodparam;
	SInt32 ret = ParaVodParam(req->GetUrl().c_str(), vodparam);
	if (ret != CDN_ERR_SUCCESS)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("para vod param is fail.ret:"<<ret);
		return CDN_ERR_SUCCESS;
	}

	//从内存中获取数据
	Node node;
	std::string path;
	std::string url = CFileDistributeMgr::Intstance()->GetNodeStreamAddress(vodparam.m_ProviderID, 
																			vodparam.m_AssetID, 
																			node, path,
																			vodparam.m_UseStreamAddrType);
	if (url.empty())
	{
		//分配最优节点
		return DoDispatchBestNode(req, rsp);
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_302);

	//组装url
	std::string surl = "http://" + GetNeedNodeAddr(node, vodparam.m_UseStreamAddrType) + req->GetUrl().c_str();
	rsp->SetAttrValue("Location", surl.c_str());

	//nginx转发302转发使用
	LibCore::CString nginxredir = "/inter/" + surl;
	rsp->SetAttrValue("X-Accel-Redirect", nginxredir.c_str());

	return 0;
}
/*******************************************************************************
*  Function   : SplitVmHlsPidAndAid
*  Description: 分离PID和AID
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPVmhlsTask::SplitPidAndAid(const char *url, std::string &pid, std::string &aid, 
	                                  const std::string key )
{
	if (url == NULL)
	{
		return -1;
	}

	std::string tmpurl = std::string(url);
	std::string filename = STR::SubStr(tmpurl.c_str(), "filename=", "&");

	//分离PID和AID
	int startpost = filename.rfind("/");
	int endpost = filename.find("_");

	if (startpost != -1 || endpost == -1)
	{
		pid = filename.substr(startpost +1, endpost - startpost - 1);
		aid = filename.substr(endpost + 1);
	}

	if (pid.empty() || aid.empty())
	{
		return -2;
	}

	LOG_DEBUG("pid:" << pid << ";aid:"<<aid);
	return CDN_ERR_SUCCESS;
}