/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : ngod点播处理
*******************************************************************************/
#include <fcntl.h>
#include <sys/stat.h>
#include "libcore/errcodemacros.h"
#include "libcore/OSTool.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libutil/tinyxml.h"
#include "libhttpserver/httperror.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcls/libclsmacros.h"
#include "mgr/filedistributemgr.h"
#include "task/httpngodtask.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPNGODTask, CHttpTaskBase)
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPNGODTask::CHTTPNGODTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPNGODTask::~CHTTPNGODTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHTTPNGODTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		LOG_TRACE("req or rsp is null")
		return -1;
	}

	//填充通用信息
	FillHttpHead(rsp);

	LocateMessge locate;
	SInt32 result = ParseLocateMessage(req->GetBody(), locate);
	if (result != 0)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("HandleLocateMessage() is fail.result is "<<result);
		return -1;
	}

	//获取文件名
	std::string filename = GetFileName(locate);
	if (filename.empty())
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		return -1;
	}

	//从内存中获取数据
	Node node;
	std::string path = "";
	std::string url = CFileDistributeMgr::Intstance()->GetNodeStreamAddress(locate.m_ProviderID, 
																			locate.m_AssetID, 
																			node, path);
	if (url.empty())
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_404);
		LOG_ERROR("Content does not exists, or cannot be retrieved");
		return -1;	
	}
	
	std::string sTransferID =  path + filename;
	std::string sURL = "http://" + node.m_PrivateStreamAddr + sTransferID;

	//获取文件大小
	SInt64 filesize = LibCore::GetHttpFileSize(sURL.c_str());
	if (filesize == -1)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_404);
		LOG_ERROR("Content does not exists, or cannot be retrieved");
		return -1;
	}

	//对ngod做前缀处理
	sTransferID = "/ngod?&filename=" + sTransferID ;

	//对TransferID进行URL编码
	sTransferID = STR::EncodeURL(sTransferID);

	//组装消息体
	char szBody[1024] = {0};
	sprintf(szBody, Locate_Response, node.m_PrivateStreamAddr.c_str(), sTransferID.c_str(), filesize);

	int len = strlen(szBody);
	rsp->SetBody(szBody, len);
	rsp->SetAttrValue("Content-Length", STR::SInt32ToStr(len).c_str());

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :FillHttpHead
*  Description:填充http头
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CHTTPNGODTask::FillHttpHead(CHTTPResponse *rsp)
{
	if (rsp == NULL)
	{
		return;
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_200);
	rsp->SetAttrValue("Content-Type", "application/x-www-form-urlencoded");
	rsp->SetAttrValue("Date", OS::Localtime().c_str());
	rsp->SetAttrValue("Server", "CDN CLS Server");
	rsp->SetAttrValue("Connection", "close");

	return;
}
/*******************************************************************************
*  Function   :ParseLocateMessage
*  Description:解析Location消息
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPNGODTask::ParseLocateMessage(const char* pszBuf, LocateMessge &locateinfo)
{
	if(pszBuf == NULL)
	{
		LOG_ERROR("pszBuf is NULL");
		return -1;
	}

	TiXmlDocument xmlDoc;
	xmlDoc.Parse(pszBuf);

	TiXmlNode* root = xmlDoc.RootElement();
	if (root == NULL)
	{
		LOG_ERROR("invalid http body!");
		return -1;
	}

	TiXmlNode* node	= NULL;
	TiXmlElement* element = NULL;
	const char* pText = NULL;
	int iResult = -1;

	do 
	{
		TiXmlNode* obj = root->FirstChild("Object");
		if (obj == NULL)
		{
			LOG_ERROR("not found <Object>");
			break;
		}

		TiXmlNode* name = obj->FirstChild("Name");
		if (name == NULL)
		{
			LOG_ERROR("not found <Name>");
			break;
		}

		//AssetID
		node = name->FirstChild("AssetID");
		if (node == NULL)
		{
			LOG_ERROR("not found <AssetID>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_AssetID = pText;
		}

		// ProviderID
		node = name->FirstChild("ProviderID");
		if (node == NULL)
		{
			LOG_ERROR("not found <ProviderID>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_ProviderID = pText;
		}

		// SubType
		node = obj->FirstChild("SubType");
		if (node == NULL)
		{
			LOG_ERROR("not found <SubType>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_SubType = pText;
		}

		// TransferRate
		node = root->FirstChild("TransferRate");
		if (node == NULL)
		{
			LOG_ERROR("not found <TransferRate>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_TransferRate = pText;
		}

		// IngressCapacity
		node = root->FirstChild("IngressCapacity");
		if (node == NULL)
		{
			LOG_ERROR("not found <IngressCapacity>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_IngressCapacity = pText;
		}

		// Range
		node = root->FirstChild("Range");
		if (node == NULL)
		{
			LOG_ERROR("not found <Range>");
			break;
		}
		element = node->ToElement();
		pText = element->GetText();
		if (pText != NULL) 
		{
			locateinfo.m_Range = pText;
		}

		iResult = 0;
	} while (false);


	return iResult;
}
/*******************************************************************************
*  Function   :GetFileName
*  Description:获取文件名
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPNGODTask::GetFileName(LocateMessge &locateinfo)
{
	char filename[512] = {0};

	if (strcmp(locateinfo.m_SubType.c_str(), "index") == 0)
	{
		sprintf(filename, "%s_%s.idx",locateinfo.m_ProviderID.c_str(), locateinfo.m_AssetID.c_str());
	}
	else if (strcmp(locateinfo.m_SubType.c_str(), "forward/1") == 0)
	{
		sprintf(filename, "%s_%s",locateinfo.m_ProviderID.c_str(), locateinfo.m_AssetID.c_str());	
	}
	else	
	{
		SInt32 startpos = 0;
		startpos = locateinfo.m_SubType.find("forward/");
		if (startpos == -1)
		{
			LOG_ERROR("invalid subtype: "<<locateinfo.m_SubType);
			return "";
		}

		locateinfo.m_SubType = locateinfo.m_SubType.substr(startpos + strlen("forward/"));

		int iSpeed = strtol(locateinfo.m_SubType.c_str(), NULL, 10);
		if (iSpeed > 0) 
		{
			sprintf(filename, "%s_%s_%d_1.ts", locateinfo.m_ProviderID.c_str(), locateinfo.m_AssetID.c_str(), iSpeed);
		} 
		else if (iSpeed < 0) 
		{
			sprintf(filename,  "%s_%s_%d_2.ts",  locateinfo.m_ProviderID.c_str(), locateinfo.m_AssetID.c_str(), -iSpeed);
		} 
		else 
		{
			LOG_ERROR("invalid subtype: " << locateinfo.m_SubType);
			return "";
		}
	}

	return filename;
}



