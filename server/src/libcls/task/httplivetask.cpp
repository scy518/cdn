/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : ott直播处理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhttpserver/httperror.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/filedistributemgr.h"
#include "task/httplivetask.h"
#include "libcls/libclsmacros.h"
#include "libclsstaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPLiveTask, CHTTPVodTask)
/*******************************************************************************
*  Function   : CHTTPLiveTask
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPLiveTask::CHTTPLiveTask()
{
}
/*******************************************************************************
*  Function   : CHTTPLiveTask
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPLiveTask::~CHTTPLiveTask() 
{
}
/*******************************************************************************
*  Function   : DoRequst
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHTTPLiveTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp) 
{ 
	if (req == NULL || rsp == NULL)
	{
		LOG_TRACE("req or rsp is null")
		return -1;
	}

	//填充通用信息
	FillHttpHead(rsp);

	//校验token
	if (!CheckToken(req))
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_401);
		LOG_ERROR("token check fail.");
		return CDN_ERR_SUCCESS;
	}

	LiveParam  tmpparam;
	SInt32 ret = ParaLiveParam(req->GetUrl().c_str(), tmpparam);
	if (ret != CDN_ERR_SUCCESS)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("ParaLiveParam() is fail."<<ret);
		return CDN_ERR_SUCCESS;
	}

	//从内存中获取数据
	Node node;
	std::string path;
	std::string url = CFileDistributeMgr::Intstance()->GetNodeStreamAddress(tmpparam.m_ProviderID, 
																			tmpparam.m_AssetID, node,
																			path,
																			tmpparam.m_UseStreamAddrType);
	if (url.empty())
	{
		//分配最优节点
		return DoDispatchBestNode(req, rsp); 
	}

	std::string m3u8;
	if (tmpparam.m_LiveType == TYPE_SHIFT_TIME)
	{
		m3u8 = BuildShiftTimeM3u8(tmpparam, url);
	}
	else if (tmpparam.m_LiveType == TYPE_LOOK_BACK)
	{
		m3u8 = BuildLookBackM3u8(tmpparam, url);
	}
	else
	{
		m3u8 = BuildLiveM3u8(tmpparam, url);
	}

	if (m3u8.empty())
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("build m3u8 fail. m_LiveType:"<<tmpparam.m_LiveType);
		return 0;
	}

	rsp->SetBody(m3u8.c_str(), m3u8.length());

	return CDN_ERR_SUCCESS; 
}
/*******************************************************************************
*  Function   : BuildLiveM3u8
*  Description: 构造直播m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPLiveTask::BuildLiveM3u8(const LiveParam &liveparam, const std::string &url)
{
	SInt64 now = OS::Milliseconds()/1000;
	SInt64 startfile = (now -liveparam.m_DelayTime)/CLibCLSStaticConf::m_ShiftTimeDuration;
	SInt64 lastfile = now/CLibCLSStaticConf::m_ShiftTimeDuration;

	char m3u8buffer[512] = {0};
	sprintf(m3u8buffer, SUB_M3U8_HEAD, CLibCLSStaticConf::m_ShiftTimeDuration,startfile);

	std::string m3u8 = std::string(m3u8buffer);
	for (; startfile < lastfile; startfile++)
	{
		char filename[256] = {0};
		memset(filename,0,sizeof(filename));
		sprintf(filename, M3U8_FILE_SPLICE, CLibCLSStaticConf::m_ShiftTimeDuration, url.c_str(),
			    liveparam.m_ProviderID.c_str(), liveparam.m_AssetID.c_str(), startfile);
		m3u8 += filename;	
	}

	return m3u8;
}
/*******************************************************************************
*  Function   : BuildShiftTimeM3u8
*  Description: 构造时移m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPLiveTask::BuildShiftTimeM3u8(const LiveParam &liveparam, const std::string &url)
{
	SInt64 now = OS::Milliseconds()/1000;
	SInt64 startfile = (now - liveparam.m_DelayTime)/CLibCLSStaticConf::m_ShiftTimeDuration;
	SInt64 lastfile = now/CLibCLSStaticConf::m_ShiftTimeDuration;

	char m3u8buffer[512] = {0};
	sprintf(m3u8buffer, SUB_M3U8_HEAD, CLibCLSStaticConf::m_ShiftTimeDuration, startfile);

	std::string m3u8 = std::string(m3u8buffer);
	Int32 countnum = 0;
	for (;startfile < lastfile; startfile++)
	{
		char filename[256] = {0};
		memset(filename,0,sizeof(filename));
		sprintf(filename,"#EXTINF:%d,\n%s%s_%s/%lld.ts\n",CLibCLSStaticConf::m_ShiftTimeDuration, url.c_str(),
			    liveparam.m_ProviderID.c_str(),liveparam.m_AssetID.c_str(), startfile);
		m3u8 += filename;

		countnum++;	
		if (countnum >= CLibCLSStaticConf::m_ShiftTimeReserveNum)
		{
			break;
		}
	}

	return m3u8;
}
/*******************************************************************************
*  Function   : BuildLookBackM3u8
*  Description: 构造回看m3u8
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPLiveTask::BuildLookBackM3u8(const LiveParam &liveparam, const std::string &url)
{
	SInt64 istartfile = liveparam.m_StartTime/CLibCLSStaticConf::m_ShiftTimeDuration;
	SInt64 iendfile = liveparam.m_EndTime/CLibCLSStaticConf::m_ShiftTimeDuration;

	char m3u8buffer[512] = {0};
	sprintf(m3u8buffer, SUB_M3U8_HEAD,CLibCLSStaticConf::m_ShiftTimeDuration,istartfile);

	std::string m3u8 = m3u8buffer;
	for (;istartfile < iendfile; istartfile++)
	{
		char filename[512] = {0};
		sprintf(filename, M3U8_FILE_SPLICE, CLibCLSStaticConf::m_ShiftTimeDuration,url.c_str(),
			    liveparam.m_ProviderID.c_str(), liveparam.m_AssetID.c_str(), istartfile);
		m3u8 += filename;	
	}

	m3u8 += M3U8_END_LIST;
	return m3u8;
}
/*******************************************************************************
*  Function   : ParaLiveParam
*  Description: 解析直播参数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPLiveTask::ParaLiveParam(const char *url, LiveParam &liveparam)
{
	SInt32 ret = SplitPidAndAid(url, liveparam.m_ProviderID, liveparam.m_AssetID,"/live/");
	if (ret != 0)
	{
		return -1;
	}

	std::string delaytime = STR::SubStr(url, "delaytime=", "&");
	std::string starttime = STR::SubStr(url, "starttime=", "&");
	std::string endtime = STR::SubStr(url, "endtime=", "&");

	liveparam.m_DelayTime = CLibCLSStaticConf::m_ShiftTimeDuration*(CLibCLSStaticConf::m_ShiftTimeDelayNum+1);
	if (!delaytime.empty())
	{
		SInt32 tmpdelaytime = STR::StrToSInt32(delaytime.c_str());
		if (tmpdelaytime > liveparam.m_DelayTime)
		{
			liveparam.m_DelayTime = tmpdelaytime;
		}
		liveparam.m_LiveType = TYPE_SHIFT_TIME;
	}

	if (!starttime.empty() && !endtime.empty())
	{
		liveparam.m_LiveType = TYPE_LOOK_BACK;
		liveparam.m_StartTime = STR::StrToSInt64(starttime.c_str());
		liveparam.m_EndTime = STR::StrToSInt64(endtime.c_str());
		if (liveparam.m_StartTime >= liveparam.m_EndTime)
		{
			return -2;
		}
	}

	//获取流类型
	liveparam.m_UseStreamAddrType = CLibCLSStaticConf::m_ClsUseStreamAddrType;
	std::string tmpstreamtype = STR::SubStr(url, "streamaddrtype=", "&"); 
	if (!tmpstreamtype.empty())
	{
		liveparam.m_UseStreamAddrType = STR::Str2int(tmpstreamtype);
	}

	return CDN_ERR_SUCCESS;
}



