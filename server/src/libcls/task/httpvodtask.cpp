/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : ott点播处理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhttpserver/httperror.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/filedistributemgr.h"
#include "task/httpvodtask.h"
#include "libclsstaticconf.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPVodTask, CHttpTaskBase)
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPVodTask::CHTTPVodTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPVodTask::~CHTTPVodTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHTTPVodTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		LOG_TRACE("req or rsp is null")
		return -1;
	}

	//填充通用信息
	FillHttpHead(rsp);

	//校验token
	Bool istryplay = FALSE;
	if (!CheckToken(req))
	{
		if (CLibCLSStaticConf::m_ClsTokenAlgorithmic != TOKENALGORITHMIC_URL_TRYPLAY)
		{
			rsp->SetEnumErrorCode(HTTP_ERROR_401);
			LOG_ERROR("token check fail.");
			return CDN_ERR_SUCCESS;
		}
		else
		{
			istryplay = TRUE;
		}
	}

	//解析vod参数
	VodParam vodparam;
	SInt32 ret = ParaVodParam(req->GetUrl().c_str(), vodparam);
	if (ret != CDN_ERR_SUCCESS)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("param vod param is fail."<<ret);
		return CDN_ERR_SUCCESS;
	}

	//从内存中获取数据
	Node node;
	std::string path;
	std::string url = CFileDistributeMgr::Intstance()->GetNodeStreamAddress(vodparam.m_ProviderID, 
																			vodparam.m_AssetID, 
		                                                                    node, path, 
																			vodparam.m_UseStreamAddrType);
	if (url.empty())
	{
		//分配最优节点
		return DoDispatchBestNode(req, rsp);
	}
	else
	{
		std::string filename = vodparam.m_ProviderID + "_" + vodparam.m_AssetID;
		if (strstr(req->GetUrl().c_str(),".mp4") != NULL || strstr(req->GetUrl().c_str(),".MP4") != NULL)
		{
			//获取重定向地址
			url = GetUrlAddress(req->GetUrl().c_str(), GetNeedNodeAddr(node, vodparam.m_UseStreamAddrType), 
								path, filename, "vm.mp4");
		}
		else
		{
			if (istryplay)//返回试看m3u8
			{
				url = url + vodparam.m_ProviderID + "_" + vodparam.m_AssetID + "_T.m3u8";
			}
			else
			{
				url = url + vodparam.m_ProviderID + "_" + vodparam.m_AssetID + ".m3u8";
			}
		}
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_302);
	rsp->SetAttrValue("Location", url.c_str());

	//nginx转发302转发使用
	LibCore::CString nginxredir = "/inter/" + url;
	rsp->SetAttrValue("X-Accel-Redirect", nginxredir.c_str());

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :DoDispatchBestNode
*  Description:返回最优cg节点
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPVodTask::DoDispatchBestNode(CHTTPRequest*req, CHTTPResponse *rsp)
{
	Node node;
	SInt32 useaddrtype = GetUseAddrTypeFromUrl(req->GetUrl().c_str());
	std::string nodeaddr = CFileDistributeMgr::Intstance()->GetBestNodeStreamAddress(node, useaddrtype);
	if (nodeaddr.empty())
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_404);
	}
	else
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_302);

		//组装url
		std::string url = "http://" + nodeaddr + req->GetUrl().c_str();

		rsp->SetAttrValue("Location", url.c_str());

		//nginx转发302转发使用
		LibCore::CString nginxredir = "/inter/" + url;
		rsp->SetAttrValue("X-Accel-Redirect", nginxredir.c_str());
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :ParaVodParam
*  Description:解析点播参数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPVodTask::ParaVodParam(const char *url, VodParam &vodparam, const std::string key)
{
	SInt32 ret = SplitPidAndAid(url, vodparam.m_ProviderID, vodparam.m_AssetID, key);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("split pid and aid fail.ret:"<<ret)
		return -1;
	}

	//获取流类型
	vodparam.m_UseStreamAddrType = GetUseAddrTypeFromUrl(url);

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :SplitPidAndAid
*  Description:分割pid和aid
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPVodTask::SplitPidAndAid(const char *url, std::string &pid, std::string &aid, 
	                                const std::string key)
{
	if (url == NULL)
	{
		return -1;
	}

	std::string filename = STR::SubStr(url, key.c_str(), "?");
	pid = STR::SubStr(filename.c_str(), "", "_");
	aid = STR::SubStr(filename.c_str(), "_", ".");

	if (pid.empty() || aid.empty())
	{
		return -2;
	}

	LOG_DEBUG("[SplitPidAndAid]pid:"<<pid<<"; id:"<<aid);
	return CDN_ERR_SUCCESS;
}

/*******************************************************************************
*  Function   :FillHttpHead
*  Description:填充http头
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CHTTPVodTask::FillHttpHead(CHTTPResponse *rsp)
{
	if (rsp == NULL)
	{
		return;
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_200);
	rsp->SetAttrValue("Content-Type", "application/x-www-form-urlencoded");
	rsp->SetAttrValue("Date", OS::Localtime().c_str());
	rsp->SetAttrValue("Server", "CDN CLS Server");
	rsp->SetAttrValue("Connection", "close");

	return;
}
/*******************************************************************************
*  Function   :GetUrlAddress
*  Description:获取重定向URL
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPVodTask::GetUrlAddress(const char *url, const std::string &nodeaddr, 
	                                    const std::string &path,const std::string &filename, 
										const char *tpyevalue)
{
	std::string tmpurl = std::string(url);
	std::string starttime = STR::SubStr(tmpurl.c_str(), "start=", "&");

	char curl[512] = {0};
	if (!starttime.empty())
	{
		sprintf(curl, "http://%s/%s?&filename=%s%s&start=%s", nodeaddr.c_str(), 
			    tpyevalue, path.c_str(), filename.c_str(), starttime.c_str());
	}
	else
	{
		sprintf(curl, "http://%s/%s?&filename=%s%s", nodeaddr.c_str(), tpyevalue, 
			    path.c_str(), filename.c_str());
	}

	return std::string(curl);
}
/*******************************************************************************
*  Function   :FillHttpHead
*  Description:获取需要的节点地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CHTTPVodTask::GetNeedNodeAddr(const Node &node, SInt32 useaddrtype)
{
	if (useaddrtype != CLS_USE_PUBLICADDR_TYPE)
	{
		return node.m_PrivateStreamAddr;
	}

	return node.m_PublicStreamAddr;
}
/*******************************************************************************
*  Function   :GetUseAddrTypeFromUrl
*  Description:获取使用地址类型
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CHTTPVodTask::GetUseAddrTypeFromUrl(const char *url)
{
	SInt32 UseStreamAddrType = CLibCLSStaticConf::m_ClsUseStreamAddrType;
	if (url == NULL)
	{
		return UseStreamAddrType;
	}

	//获取流类型
	std::string tmpstreamtype = STR::SubStr(url, "streamaddrtype=", "&"); 
	if (!tmpstreamtype.empty())
	{
		UseStreamAddrType = STR::Str2int(tmpstreamtype);
	}

	return UseStreamAddrType;
}
/*******************************************************************************
*  Function   :CheckToken
*  Description:校验token
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CHTTPVodTask::CheckToken(CHTTPRequest *req)
{
	//判断是否开启token校验
	if (CLibCLSStaticConf::m_ClsIsCheckToken == CHECKTOKEN_DISABLE)
	{
		return TRUE;
	}

	if (req == NULL)
	{
		return FALSE;
	}

	//获取过期时间
	char *tmpexpiretime = req->GetUrlParamValue("expiretime");
	if (tmpexpiretime != NULL)
	{
		if ( OS::Milliseconds() > STR::Str2int64(tmpexpiretime))
		{
			return FALSE;
		}
	}

	//获取token
	char *token = req->GetUrlParamValue("token");
	if (token == NULL)
	{
		LOG_ERROR("token is null.url:"<<req->GetUrl())
		return FALSE;
	}

	//原始url需要去掉token参数
	std::string tmpurl = req->GetUrl().c_str();
	STR::Replace(tmpurl, "?token="+std::string(token), "?");
	STR::Replace(tmpurl, "&token="+std::string(token), "");
    
	//进行md5计算
	std::string md5src = tmpurl + "&key=" + CLibCLSStaticConf::m_ClsTokenKey;
	if (CLibCLSStaticConf::m_ClsTokenAlgorithmic == TOKENALGORITHMIC_URL_TRYPLAY)
	{
		md5src = md5src + "&tryplay=0";
	}

	//计算生成md5值
	std::string calctoken = OS::MD5String(md5src);
	if (calctoken == token)
	{
		return TRUE;
	}

	return FALSE;
}



