/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : ott点播处理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libhttpserver/httperror.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/filedistributemgr.h"
#include "task/httpfiletask.h"

CONF_IMPLEMENT_DYNCREATE(CHTTPFileTask, CHttpTaskBase)
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPFileTask::CHTTPFileTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CHTTPFileTask::~CHTTPFileTask()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CHTTPFileTask::DoTask(CHTTPRequest*req, CHTTPResponse *rsp)
{
	if (req == NULL || rsp == NULL)
	{
		LOG_TRACE("req or rsp is null")
		return -1;
	}

	//填充通用信息
	FillHttpHead(rsp);

	//校验token
	if (!CheckToken(req))
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_401);
		LOG_ERROR("token check fail.");
		return CDN_ERR_SUCCESS;
	}

	//解析vod参数
	VodParam vodparam;
	SInt32 ret = ParaVodParam(req->GetUrl().c_str(), vodparam, "/file/");
	if (ret != CDN_ERR_SUCCESS)
	{
		rsp->SetEnumErrorCode(HTTP_ERROR_400);
		LOG_ERROR("para vod param is fail."<<ret);
		return CDN_ERR_SUCCESS;
	}

	//从内存中获取数据
	Node node;
	std::string path;
	std::string url = CFileDistributeMgr::Intstance()->GetNodeStreamAddress(vodparam.m_ProviderID, 
																			vodparam.m_AssetID, 
																			node, path,
																			vodparam.m_UseStreamAddrType);
	if (url.empty())
	{
		return DoDispatchBestNode(req, rsp);
	}

	rsp->SetEnumErrorCode(HTTP_ERROR_302);

	url = url + vodparam.m_ProviderID + "_" + vodparam.m_AssetID ;
	rsp->SetAttrValue("Location", url.c_str());

	//nginx转发302转发使用
	LibCore::CString nginxredir = "/inter/" + url;
	rsp->SetAttrValue("X-Accel-Redirect", nginxredir.c_str());

	return CDN_ERR_SUCCESS;
}




