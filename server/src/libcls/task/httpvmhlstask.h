#ifndef HTTP_VMHLSTASK_H
#define HTTP_VMHLSTASK_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:httpvodtask.h
version  :
created  : 2013-2-12   11:52
file path: 
modify   :
author   : songchuangye
purpose  : ott点播vmhls处理
*******************************************************************************/
#include "task/httpvodtask.h"
#include <string>

class CHTTPVmhlsTask : public CHTTPVodTask
{
	CONF_DECLARE_DYNCREATE(CHTTPVmhlsTask)
public:

	CHTTPVmhlsTask();

	virtual ~CHTTPVmhlsTask();

protected:
	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

protected:

	//分割pid和aid
	virtual SInt32 SplitPidAndAid(const char *url, std::string &pid, std::string &aid,
		                          const std::string key = "/vod/");
};



#endif


