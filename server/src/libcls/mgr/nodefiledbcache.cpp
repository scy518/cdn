/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 文件分布管理
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/clsdbopreatermgr.h"
#include "mgr/nodefiledbcache.h"
#include "mgr/nodemgr.h"
/*******************************************************************************
*  Function   :CNodeFileDBCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileDBCache::CNodeFileDBCache()
{
}
/*******************************************************************************
*  Function   :~CNodeFileDBCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileDBCache::~CNodeFileDBCache()
{
	LOG_DEBUG("CNodeFileDBCache exit.");
}
/*******************************************************************************
*  Function   : Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CNodeFileDBCache::Init()
{
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetNodeStreamAddr
*  Description:获取流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CNodeFileDBCache::GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
	                                               Node &node, std::string &path, SInt32 useaddrtype)
{
	FileDistributeList filenodelist;
	NodeList nodelist;

	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryFileDistribute(pid, aid, filenodelist);
	if (ret == CDN_ERR_SUCCESS)
	{
		ret = CLSCDbOperatermgr::Intstance()->QueryNode(nodelist);
	}

	//判断数据是否获取成功
	if (ret != CDN_ERR_SUCCESS)
	{
		return "";
	}

	//查找最优节点
	Node  tmpnode;
	FileDistribute filedistr;
	FileDistributeList::iterator it = filenodelist.begin();
	for (; it != filenodelist.end(); it++)
	{
		std::list<Node>::iterator it1 = nodelist.begin();
		for (; it1 != nodelist.end(); it1++)
		{
			if (it1->m_NodeID == it->m_NodeID)
			{
				if (tmpnode.m_NodeID.empty() || (tmpnode.m_FreeBandWidth < it1->m_FreeBandWidth))
				{
					tmpnode = *it1;
					filedistr = *it;
				}
			}
		}
	}

	//获取需要的流地址
	std::string streamaddr = GetNeedStreamAddress(tmpnode, useaddrtype);
	if (streamaddr.empty() || filedistr.m_Path.empty())
	{
		return "";
	}

	node = tmpnode;
	path = filedistr.m_Path;
	std::string url = "http://" + streamaddr + filedistr.m_Path;
	return url;
}








