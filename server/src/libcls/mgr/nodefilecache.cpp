/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: nodefilemencache.h
modify   :
author   : songchuangye
purpose  : 文件分布基类
*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcls/libclsmacros.h"
#include "mgr/nodefilecache.h"
/*******************************************************************************
*  Function   : CNodeFileCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileCache::CNodeFileCache()
{
}
/*******************************************************************************
*  Function   : CNodeFileCache
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeFileCache::~CNodeFileCache()
{
}
/*******************************************************************************
*  Function   : Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CNodeFileCache::Init()
{
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetNodeStreamAddress
*  Description:获取流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string  CNodeFileCache::GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
	                                              Node &node, std::string &path, SInt32 useaddrtype)
{
	return "";
}
/*******************************************************************************
*  Function   : GetStreamAddress
*  Description:根据类型获取需要的流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string  CNodeFileCache::GetNeedStreamAddress(const Node &node, SInt32 useaddrtype)
{
	if (useaddrtype != CLS_USE_PUBLICADDR_TYPE)
	{
		return node.m_PrivateStreamAddr;
	}

	return node.m_PublicStreamAddr;
}














