#ifndef CLS_DB_OPERATER_MGR_H
#define CLS_DB_OPERATER_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsdbopreatermgr.h
*  Description:  cls数据库操作
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "libcls/clscommon.h"

class CLSCDbOperatermgr : public CDBOperate
{
	DECLARATION_SINGLETON(CLSCDbOperatermgr)
public:

	// 查询节点信息
	Int32 QueryNode(NodeList &nodelist);

	//查询文件分布信息
	Int32 QueryFileDistribute(const std::string &pid, const std::string &aid, 
		                      FileDistributeList &filenodelist);

	//查询处于激活状态并且空闲带宽最大的节点
	Int32 QueryBestNode(Node &node);

	//取出所有文件分布信息
	Int32 QueryAllFileDistribute(FileDistributeList &filedistributelist, SInt64 createtime = 0);

	//查看记录是否存在
	Int32 QueryCountFileDistribute(const std::string &pid, const std::string &aid,
		                           const std::string &path,const std::string &nodeid);
};

#endif
