#ifndef NODE_MGR_H
#define NODE_MGR_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 节点管理
*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include "libcdnutil/cdncommon.h"
#include "libcls/clscommon.h"

#include <list>
#include <map>
#include <string>

typedef std::map<std::string, Node >      NodeMap;

class CNodeMgr : public OSTask
{
	DECLARATION_SINGLETON(CNodeMgr)
public:

    //初始化
	SInt32 Init();

	//线程调用函数
	virtual Bool Run();

	//获取最优节点,获取负载最小CG,返回CG工作地址
	virtual std::string GetBestNodeStreamAddress(Node &node, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

	//从列表找出负载最小的节点地址
	Node GetNodeFromNodeList(const std::list<std::string> &nodelist);

	//从列表找出负载最小的节点及存储路径
	Node GetNodeFromNodeList(const FileDistributeList &filedistributelist, std::string &path);

	//从列表找出负载最小的节点及存储路径
	Node GetNodeFromNodeVector(const RedisFileDistributeVec &filedistributevec, std::string &path);

protected:

	//更新节点信息
	void RefreshNode();

	//根据类型获取需要的流地址
	std::string  GetNeedStreamAddress(const Node &node, SInt32 useaddrtype);

private:

	OSMutex               m_Mutex;
	NodeMap               m_NodeMap;

	SInt64                m_LastTime;
};



#endif




