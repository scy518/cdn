/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 节点管理
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcore/errcodemacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcls/libclsmacros.h"
#include "mgr/clsdbopreatermgr.h"
#include "libclsstaticconf.h"
#include "mgr/nodemgr.h"

IMPLEMENT_SINGLETON(CNodeMgr)
/*******************************************************************************
*  Function   : 
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeMgr::CNodeMgr()
{
	m_LastTime   = 0;
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CNodeMgr::~CNodeMgr()
{
	//退出线程
	this->StopTask();

	LOG_DEBUG("CNodeMgr exit.");
}
/*******************************************************************************
*  Function   : Init
*  Description: 初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CNodeMgr::Init()
{
	//启动线程
	if (!this->RunTask())
	{
		ERROR_LOG("CNodeMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : Execute
*  Description: 
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CNodeMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//更新文件分布
		RefreshNode();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : GetBestNodeStreamAddress
*  Description:获取负载最小CG,返回CG工作地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string CNodeMgr::GetBestNodeStreamAddress(Node &node, SInt32 useaddrtype)
{
	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryBestNode(node);
	if (ret != CDN_ERR_SUCCESS)
	{
		ERROR_LOG("Query best node fail. ret:"<<ret)
	}

	return GetNeedStreamAddress(node, useaddrtype);
}
/*******************************************************************************
*  Function   : GetNodeFromNodeList
*  Description:从列表找出负载最小的节点地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Node CNodeMgr::GetNodeFromNodeList(const std::list<std::string> &nodelist)
{
	Node tmpnode;
	m_Mutex.Lock();
	std::list<std::string>::const_iterator it = nodelist.begin();
	for (; it != nodelist.end(); it++)
	{
		NodeMap::iterator mapit = m_NodeMap.find(*it);
		if (mapit != m_NodeMap.end())
		{
			if (tmpnode.m_NodeID.empty() || tmpnode.m_FreeBandWidth <= mapit->second.m_FreeBandWidth)
			{
				tmpnode = mapit->second;
			}
		}
	}

	m_Mutex.Unlock();

	return tmpnode;
}
/*******************************************************************************
*  Function   : GetNodeFromNodeList
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Node CNodeMgr::GetNodeFromNodeList(const FileDistributeList &filedistributelist,std::string &path)
{
	Node tmpnode;

	m_Mutex.Lock();
	FileDistributeList::const_iterator it = filedistributelist.begin();
	for (; it != filedistributelist.end(); it++)
	{
		NodeMap::iterator mapit = m_NodeMap.find(it->m_NodeID);
		if (mapit != m_NodeMap.end())
		{
			if (tmpnode.m_NodeID.empty() || tmpnode.m_FreeBandWidth <= mapit->second.m_FreeBandWidth)
			{
				tmpnode = mapit->second;
				path = it->m_Path;
			}
		}
	}
	m_Mutex.Unlock();

	return tmpnode;
}
/*******************************************************************************
*  Function   : GetNodeFromNodeVector
*  Description:从列表找出负载最小的节点及存储路径
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Node CNodeMgr::GetNodeFromNodeVector(const RedisFileDistributeVec &filedistributevec, std::string &path)
{
	Node tmpnode;

	m_Mutex.Lock();
	RedisFileDistributeVec::const_iterator it = filedistributevec.begin();
	for (; it != filedistributevec.end(); it++)
	{
		NodeMap::iterator mapit = m_NodeMap.find(it->m_NodeID);
		if (mapit != m_NodeMap.end())
		{
			if (tmpnode.m_NodeID.empty() || tmpnode.m_FreeBandWidth <= mapit->second.m_FreeBandWidth)
			{
				tmpnode = mapit->second;
				path = it->m_Path;
			}
		}
	}
	m_Mutex.Unlock();

	return tmpnode;
}
/*******************************************************************************
*  Function   : RefreshNode
*  Description:更新节点信息
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CNodeMgr::RefreshNode()
{
	if (OS::Milliseconds() - m_LastTime < CLibCLSStaticConf::m_ClsSynNodeInterval)
	{
		return;
	}

	//同步数据库数据
	std::list<Node> nodelist;
	SInt32 ret = CLSCDbOperatermgr::Intstance()->QueryNode(nodelist); 
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("refresh node fail. ret:"<<ret)
		return;
	}

	//刷新缓存
	if(!nodelist.empty())
	{
		m_Mutex.Lock();
		m_NodeMap.clear();
		std::list<Node>::iterator it = nodelist.begin();
		for (; it != nodelist.end(); it++)
		{
			m_NodeMap[it->m_NodeID] = *it;
		}
		m_Mutex.Unlock();

		m_LastTime = OS::Milliseconds();
	}

	return;
}
/*******************************************************************************
*  Function   : GetNeedStreamAddress
*  Description:根据类型获取需要的流地址
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
std::string  CNodeMgr::GetNeedStreamAddress(const Node &node, SInt32 useaddrtype)
{
	if (useaddrtype != CLS_USE_PUBLICADDR_TYPE)
	{
		return node.m_PrivateStreamAddr;
	}

	return node.m_PublicStreamAddr;
}
