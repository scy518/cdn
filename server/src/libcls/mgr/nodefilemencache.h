#ifndef NODE_FILE_EMN_CACHE_H
#define NODE_FILE_EMN_CACHE_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: nodefilemencache.h
modify   :
author   : songchuangye
purpose  : 文件分布管理
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/OSThread.h"
#include "mgr/nodefilecache.h"
#include "libcls/clscommon.h"

class CNodeFileMenCache : public OSTask, public CNodeFileCache
{
public:

	CNodeFileMenCache();

	virtual ~CNodeFileMenCache();

	//初始化
	virtual SInt32 Init();

	//获取流地址
	virtual std::string  GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
		                                      Node &node, std::string &path, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

	//线程调用函数
	virtual Bool Run();

protected:

	//全量同步文件
	void RefreshAllAddFileDistribute();

	//增量同步文件
	void RefreshIncAddFileDistribute();

private:

	//全量添加数据到内存中
	void AllAddFileToMem(FileDistributeList &filedistributelist);

	//增量添加数据到内存中
	void IncAddFileToMem(FileDistributeList &filedistributelist);

	//从数据库查询
	void QueryFileFromDB(const std::string &pid, const std::string &aid, FileDistributeList &filedistrilist);

private:

	OSMutex						 m_Mutex;
	FileNodeMap					 m_FileNodeMap;

	//最后一个文件生成时间
	SInt64						 m_LastFileCreateTime;

	//最后一次增量同步的时间
	SInt64                       m_LastIncTime;

	//最后一次全量同步的时间
	SInt64                       m_LastAllTime;

	//存储所有的文件分布信息
	std::list<FileDistribute>	 m_FileDistributeList;
};



#endif



