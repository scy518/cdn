/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsdbopreater.cpp
*  Description:  cls数据库操作
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libdb/dbconnectmgr.h"
#include "libdb/dbexception.h"
#include "libdb/dbresultset.h"
#include "libdb/sqlstatementmgr.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/clsdbopreatermgr.h"
#include "libclsstaticconf.h"

IMPLEMENT_SINGLETON(CLSCDbOperatermgr)
/*******************************************************************************
 Fuction name:CLSCDbOperatermgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CLSCDbOperatermgr::CLSCDbOperatermgr()
{
}
/*******************************************************************************
 Fuction name:~CLSCDbOperatermgrmgr
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CLSCDbOperatermgr::~CLSCDbOperatermgr()
{
}
/*******************************************************************************
*  Function   : QueryNode
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CLSCDbOperatermgr::QueryNode(NodeList &nodelist)
{
	const char *sqlstatement = GET_SQL_STRING("QUERY_NODE_SQL", CLibCLSStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_NODE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);
		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			Node tmpnode;
			tmpnode.m_NodeID = pRecord->GetColString(0).c_str();
			tmpnode.m_PrivateStreamAddr = pRecord->GetColString(1).c_str();
			tmpnode.m_PublicStreamAddr = pRecord->GetColString(2).c_str();
			tmpnode.m_TotalBandWidth = pRecord->GetColSInt64(3);
			tmpnode.m_FreeBandWidth = pRecord->GetColSInt64(4);
			tmpnode.m_HeartTime = pRecord->GetColSInt64(5);

			nodelist.push_back(tmpnode);
			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_ERROR("Execute sql fail "<<e.what()<<". "<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}

/*******************************************************************************
*  Function   : QueryFileDistribute
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CLSCDbOperatermgr::QueryFileDistribute(const std::string &pid, const std::string &aid, 
	                                      FileDistributeList &filenodelist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_FILE_DISTRIBUTE", CLibCLSStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_FILE_DISTRIBUTE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);
		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			FileDistribute tmpfile;
			tmpfile.m_ProviderID = pRecord->GetColString(0).c_str();
			tmpfile.m_AssetID = pRecord->GetColString(1).c_str();
			tmpfile.m_Path = pRecord->GetColString(2).c_str();
			tmpfile.m_NodeID = pRecord->GetColString(3).c_str();
			tmpfile.m_CreateTime = pRecord->GetColSInt64(4);

			filenodelist.push_back(tmpfile);
			pRecord = resultset.NextRow();
		}

	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_ERROR("Execute sql fail "<<e.what()<<". "<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : QueryAllFileDistribute
*  Description: 查询所有文件分布
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLSCDbOperatermgr::QueryAllFileDistribute(FileDistributeList &filedistributelist, 
	                                         SInt64 createtime)
{
	const char *sqlstatement = GET_SQL_STRING("QUERY_ALL_FILE_DISTRIBUTE", CLibCLSStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_ALL_FILE_DISTRIBUTE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, createtime);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);
		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			FileDistribute tmpfile;
			tmpfile.m_ProviderID = pRecord->GetColString(0).c_str();
			tmpfile.m_AssetID = pRecord->GetColString(1).c_str();
			tmpfile.m_Path = pRecord->GetColString(2).c_str();
			tmpfile.m_NodeID = pRecord->GetColString(3).c_str();
			tmpfile.m_CreateTime = pRecord->GetColSInt64(4);

			filedistributelist.push_back(tmpfile);
			pRecord = resultset.NextRow();
		}

	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_ERROR("Execute sql fail "<<e.what()<<". "<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : QueryCountFileDistribute
*  Description:查看记录是否存在
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLSCDbOperatermgr::QueryCountFileDistribute(const std::string &pid, const std::string &aid,
	                                           const std::string &path,const std::string &nodeid)
{
	const char *sqlstatement = GET_SQL_STRING("QUERY_COUNT_FILE_DISTRIBUTE", CLibCLSStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_COUNT_FILE_DISTRIBUTE")
		return -1;
	}

	Int32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(),aid.c_str(),path.c_str(),nodeid.c_str());

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_ERROR("Execute sql fail "<<e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			countnum = -2;
		}
		else
		{
			countnum = 0;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return countnum;
}
/*******************************************************************************
*  Function   : QueryBestNode
*  Description: 查询处于激活状态并且空闲带宽最大的节点
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Int32 CLSCDbOperatermgr::QueryBestNode(Node &node)
{
	const char *sqlstatement = GET_SQL_STRING("QUERY_BEST_NODE_SQL", CLibCLSStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_BEST_NODE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		if (pRecord != NULL)
		{
			node.m_NodeID = pRecord->GetColString(0).c_str();
			node.m_PrivateStreamAddr = pRecord->GetColString(1).c_str();
			node.m_PublicStreamAddr = pRecord->GetColString(2).c_str();
			node.m_FreeBandWidth = pRecord->GetColSInt64(3);
			node.m_TotalBandWidth = pRecord->GetColSInt64(4);
			node.m_HeartTime = pRecord->GetColSInt64(5);
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_ERROR("Execute sql fail "<<e.what()<<". "<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}