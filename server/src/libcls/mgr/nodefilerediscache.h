#ifndef CG_CACHE_FILE_MGR_H
#define CG_CACHE_FILE_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cgcachefilemgr.h
*  Description:  cg 缓存文件管理器
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libcore/OSThread.h"
#include "libcdnutil/redisfileinfdataobject.h"
#include "libcdnutil/cdncommon.h"
#include "mgr/nodefilecache.h"

class CNodeFileRedisCache : public OSTask, public CNodeFileCache
{
public:

	CNodeFileRedisCache();

	virtual ~CNodeFileRedisCache();

	//初始化
	virtual SInt32 Init(); 

	//获取流地址
	virtual std::string  GetNodeStreamAddress(const std::string &pid, const std::string &aid, 
		                                      Node &node, std::string &path, SInt32 useaddrtype = CLS_USE_PRIVATEADDR_TYPE);

	//OSTask的执行函数, 执行具体的任务
	virtual Bool Run();

	//获取文件分布
	Int32 GetFileNodeDistribute(const char *filename, RedisFileDistributeVec &nodepathvec);

	//获取文件信息
	Int32 GetFileInfo(const char *filename, RedisFileInfo &fileinf);

	//获取文件信息和文件分布
	Int32 GetFileInfoAndNodeDistribute(const char *filename, RedisFileInfo &fileinf, 
									   RedisFileDistributeVec &nodepathvec);
protected:

	//清除缓存数据
	void CleanDataFromMem();

	//从缓存获取文件分布，以及文件信息
	Int32 GetFileInfoAndNodeFromMem(const char *filename, RedisFileInfo  &file, RedisFileDistributeVec &distnodelist);

	//从redis获取文件分布，以及文件信息
	Int32 GetFileInfoAndNodeFromRedis(const char *filename, RedisFileInfo  &file, RedisFileDistributeVec &distnodelist);

	//获取文件信息
	Int32 GetFileInfoFromRedis(const char *filename, RedisFileInfo  &file);

	//查询文件分布
	Int32 GetFileDistributeFromRedis(const char *filename, RedisFileDistributeVec &distnodelist);

	//添加文件到缓存
	Int32 AddFileInfoAndNodeToMem(const RedisFileInfo  &file, const RedisFileDistributeVec &distnodelist);

	//添加文件到缓存
	Int32 AddFileInfoToMem(const RedisFileInfo  &file);

	//添加节点路径到缓存
	Int32 AddFileNodePathToMem(const char *filename, const RedisFileDistributeVec &distnodelist);

private:

	OSMutex                   m_FileInfoMutex;
	RedisFileInfoMap          m_FileInfoMap;         //存放文件缓存

	OSMutex                   m_DistributeMutex;
	FileDistributeMap         m_FileDistributeMap;   //文件分布信息

	Int64                     m_LastTime;
};



#endif



