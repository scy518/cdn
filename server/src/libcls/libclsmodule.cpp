/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libclsmodule.cpp
*  Description:  libcls模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/errcodemacros.h"
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/log.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libcls/libcls_dll.h"
#include "mgr/filedistributemgr.h"
#include "mgr/clsdbopreatermgr.h"
#include "mgr/nodemgr.h"
#include "libclsstaticconf.h"

REGISTER_MODULE(libcls, LIBCLS_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libcg模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcls::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibCLSStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CCISqlStatemnetmgr fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	SInt32 ret = ADD_SQL_FILE_STRING(CLibCLSStaticConf::m_SqlFileName);
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -2;
	}

	//初始化数据库
	ret = CLSCDbOperatermgr::Intstance()->Initialize();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CLSCDbOperatermgr fail. ret:%d\n", ret);
		return -3;
	}

	//初始化文件分布管理器
	ret = CFileDistributeMgr::Intstance()->Init();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CFileDistributeMgr fail. ret:%d\n", ret);
		return -4;
	}

	//初始化节点管理器
	ret = CNodeMgr::Intstance()->Init();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CNodeMgr fail. ret:%d\n", ret);
		return -5;
	}

	return 0;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libcg模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libcls::Release()
{
	//退出cg文件管理器
	CNodeMgr::Intstance()->Destroy();

	//退出节点管理器
	CFileDistributeMgr::Intstance()->Destroy();

	//退出数据库
	CLSCDbOperatermgr::Intstance()->Destroy();
	REMOVE_SQL_FILE_STRING(CLibCLSStaticConf::m_SqlFileName);

	//退出参数管理
	CLibCLSStaticConf::Intstance()->Destroy();

	return 0;
}





