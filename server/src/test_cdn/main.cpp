/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    main.cpp
*  Description:  测试工程
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <list>
#include "libcore/cstring.h"
#include "libvideo/libvideoapi.h"

/*class TestList
{
public: 
	TestList() 
	{
		m_list.push_back(1);
		m_list.push_back(2);
		m_list.push_back(3);
		m_list.push_back(4);
	}
	~TestList() {}

	std::list<int> GetList()
	{
		return m_list;
	}

private:

	std::list<int>   m_list;

};*/
typedef struct  Test_struct
{
	int     x;
	int    y;
	Test_struct()
	{
		x = 0;
		y = 0;
	}
}TestStru;

void TestPoint(TestStru *test)
{
	test->x = 10;
	test->y = 20;
}
#include "libutil/stringtool.h"
#include <fstream>
void test_fstream()
{
	//获取分片序列号
	SInt32 seq = 200/10;
	std::string filename = "E:\\2016_svn\\scy\\project\\cdn\\3.code\\trunk\\cdn\\test\\Test00001_0000112233";
	std::string srcm3u8file = filename + ".m3u8";
	std::string tryplaym3u8 = filename + "_T.m3u8";
	std::string findkey = "/" + STR::SInt32ToStr(seq);

	ifstream in(srcm3u8file.c_str()); 
	ofstream out(tryplaym3u8.c_str()); 
	if (!in.is_open() || !out.is_open())
	{
		return;
	}
	char buffer[256] = {0};  
	while (!in.eof() )  
	{  
		std::string tmp = "";
		std::getline (in, tmp); 
		if (tmp.empty())
		{
			continue;
		}

		//写文件到试看文件
		out<<tmp<<std::endl;
		if (tmp.find(findkey) != std::string::npos)
		{
			out<<"#EXT-X-ENDLIST" <<std::endl;  
			break;
		}
	}  
}

//#include "librtm/rtmcommon.h"

#include "test.h"
#include "libutil/stringtool.h"

int main (int argc, const char * argv[])
{
	test_fstream();
	std::string tmpurl = "http://139.199.194.237:8080/live/JunTeng_123456.m3u8?test=1&token=123456";
	std::string token = STR::GetKeyValue(tmpurl, "token=", "&"); 
	STR::Replace(tmpurl, "?token="+token, "?");
	STR::Replace(tmpurl, "&token="+token, "");

	CTestTask *testtask = new CTestTask();
	delete testtask;

	std::string tmptest = STR::SubStr("http://139.199.194.237:8080/live/JunTeng_123456.m3u8", "streamaddrtype=", "&");
	SInt32 testret = STR::Str2int(STR::GetKeyValue("#EXTINF:5.24,", "#EXTINF:", ","));
	/*LiveChannelItem  tmplive;
	CRTITaskInterface *prtitask = (CRTITaskInterface*)CreateRuntimeObjectFromClassName("CTestCRTITaskInterface");
	prtitask->Init(tmplive);
	delete prtitask;*/


	std::string teststr = "Test fffff\n";
	std::string teststring1 = STR::LRTrimString(teststr.c_str());
	//LibCdnUtil::CreateM3U8("D:\\Tool\\nginx-1.5.2\\wwwroot\\CIBN_769272.ts", 10);

	LibVideo::CreateM3U8File("E:\\ts\\ngod\\scy_123456.ts", 10);


	LibVideo::CreateNGODFile("E:\\ts\\ngod\\tool\\ad.ts", "scy", "123456", "4,8", 1, TRUE, FALSE);

	/*TestList  test;
	std::list<IngestResult> tmplist = test.GetList();
	std::vector<IngestResult> tmplist1 = test.Getvector();
	*/

	TestStru test;
	TestPoint(&test);

	LibCore::CString  testcstring = "Testfsdfsf==";
	std::string       teststring = "+++fffffffffffff";
	testcstring += teststring;
	printf("LibConf::CString+=:%s", testcstring.c_str());

	testcstring = testcstring + teststring;
	printf("LibConf::CString+=:%s", testcstring.c_str());

	std::list<LibCore::CString >  testlist;
	testlist.push_back(teststring);


	return 0;
}