/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    librtimodule.cpp
*  Description:  librti模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcore/errcodemacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rtmdbopreatermgr.h"
#include "mgr/rtiheartbeatmgr.h"
#include "mgr/rtitaskmanage.h"
#include "librtmstaticconf.h"
#include "librtm/librtm_dll.h"

REGISTER_MODULE(librtm, LIBRTM_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 librtm::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibRTMStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CCIStaticConf fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	SInt32 ret = ADD_SQL_FILE_STRING(CLibRTMStaticConf::m_SqlFileName);
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -2;
	}

	//初始化数据库
	ret = CRTMDBOperatermgr::Intstance()->Initialize();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -3;
	}

	//初始化任务管理
	ret = CRTITaskManage::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CRTITaskManage fail. ret:%d\n", ret);
		return -4;
	}

	//启动心跳管理
	ret = CRTIHeartBeatMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CRTIHeartBeatMgr fail. ret:%d\n", ret);
		return -5;
	}

	return ret;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出librti模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 librtm::Release()
{
	//心跳管理
	CRTIHeartBeatMgr::Intstance()->Destroy();

	//录制管理
	CRTITaskManage::Intstance()->Destroy();

	//退出数据库
	CRTMDBOperatermgr::Intstance()->Destroy();

	//退出sql语句
	REMOVE_SQL_FILE_STRING(CLibRTMStaticConf::m_SqlFileName);

	CLibRTMStaticConf::Intstance()->Destroy();

	return 0;
}





