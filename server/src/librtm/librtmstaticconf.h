#ifndef LIB_RTM_STATIC_CONF_H
#define LIB_RTM_STATIC_CONF_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    librtmstaicconf.h
*  Description:  rti静态参数
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"
#include <string>

class CLibRTMStaticConf : public OSTask
{
	DECLARATION_SINGLETON(CLibRTMStaticConf)
public:

	//初始化线程
	virtual Bool Initialize();

	//线程调用函数
	virtual Bool Run();

public:

	//节点ID
	static std::string                    m_NodeID;

	//下载完文件需要执行的shell命令
	static std::string		              m_ShellCmd;

	//清理录制过期文件
	static Int32                          m_ClearClearExpireFileTime;

	//数据文件
	static std::string                    m_SqlFileName;

	//推流内网地址
	static std::string                   m_PrivateStreamAddr;

	//推流外网地址
	static std::string                   m_PublicStreamAddr;

	//网卡配置文件
	static std::string					  m_NetworkCardFile;

protected:

	//同步参数
	void SynStaticParam();

};
#endif



