/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:defalutrtitask.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : 
purpose  : 
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "librtmstaticconf.h"
#include "task/defaultrtitask.h"

CONF_IMPLEMENT_DYNCREATE(CDefaultRTITask, CRTITaskInterface)
/*******************************************************************************
*  Function   : CDefaultRTITask
*  Description: 构造函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDefaultRTITask::CDefaultRTITask()
{
}
/*******************************************************************************
*  Function   : ~CDefaultRTITask()
*  Description: 析构函数
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDefaultRTITask::~CDefaultRTITask()
{
	StopRecordTask();
}
/*******************************************************************************
*  Function   : StartRecordTask
*  Description: 开启直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32 CDefaultRTITask::StartRecordTask()
{
	char shellcmd[512] = {0};
	sprintf(shellcmd,"%s %s %d %s %d &", CLibRTMStaticConf::m_ShellCmd.c_str(), 
		    m_LiveChannel.m_SourceUrl.c_str(), m_LiveChannel.m_SliceDur, m_StoreFullPath.c_str(), 
			m_AllSliceCount);

	SInt32 ret = system(shellcmd);
	if (ret != 0)
	{
		LOG_DEBUG("system fail.ret:" << ret << "; " << shellcmd);
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : StopTask
*  Description: 停止直播录制任务
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32	CDefaultRTITask::StopRecordTask()
{
	char shellcmd[256] = {0};
	sprintf(shellcmd,"ps -ef|grep %s| grep -v grep |awk '{print $2}' | xargs kill -9 > /dev/null 2>&1",
		    m_LiveChannel.m_SourceUrl.c_str());
	SInt32 ret = system(shellcmd);
	if (ret == -1)
	{
		LOG_WARN("system fail.ret:"<<ret<<"; shellcmd:"<<shellcmd);
		return -1;
	}

	LOG_INFO("shellcmd is :"<<shellcmd);

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : IsReStartRecordTask
*  Description: 检查是否需要重启,在录制返回TRUE, 否则返回FALSE
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CDefaultRTITask::IsReStartRecordTask()
{
	char shellcmd[512] = {0};
	sprintf(shellcmd,"ps -ef|grep %s| grep -v grep |awk '{print $2}'", m_LiveChannel.m_SourceUrl.c_str());
#ifndef WIN32
	FILE *fp = NULL;
	if ((fp = popen(shellcmd,"r")) ==NULL)
	{
		LOG_ERROR("Fail to popen.shellcmd is :"<<shellcmd);
		return true;
	}

	char pbuffer[256] = {0};
	SInt64 ipid = 0;
	if (fgets(pbuffer, sizeof(pbuffer)-1, fp)!=NULL)
	{
		ipid = atoll(pbuffer);
	}
	pclose(fp);
	printf("ipid is %d\n",ipid);

	return ipid > 0 ?TRUE:FALSE;
#endif

	return TRUE;
}

