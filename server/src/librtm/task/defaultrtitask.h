#ifndef DEFAULT_RTI_TASK_H
#define DEFAULT_RTI_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    defalutrtitask.h
*  Description: 
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "librtm/rtmcommon.h"
#include "librtm/rtitaskinterface.h"

class CDefaultRTITask : public CRTITaskInterface
{
	CONF_DECLARE_DYNCREATE(CDefaultRTITask)
public:

	CDefaultRTITask();

	virtual ~CDefaultRTITask();

	//开启直播任务
	virtual SInt32 StartRecordTask();

	//停止直播任务
	virtual SInt32	StopRecordTask();

	//检查是否需要重启,在录制返回TRUE, 否则返回FALSE
	virtual Bool IsReStartRecordTask();
};

#endif