/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtitaskmanage.cpp
*  Description:  直播频道管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/rtmdbopreatermgr.h"
#include "mgr/rtitaskmanage.h"
#include "librtmstaticconf.h"
#include "librtm/rtmcommon.h"

IMPLEMENT_SINGLETON(CRTITaskManage)
/*******************************************************************************
 Fuction name:CRTITaskManage
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
CRTITaskManage::CRTITaskManage()
{
	m_LastLoadLiveTime  = 0;
	m_LastCheckLiveTime = OS::Milliseconds();
	m_LastClearLiveTime = OS::Milliseconds();
	m_LastClearExpireFileTime = OS::Milliseconds();
}
/*******************************************************************************
 Fuction name:~CRTITaskManage
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
CRTITaskManage::~CRTITaskManage()
{
	//停止线程
	this->StopTask();

	//释放录制对象
	LiveRecordTaskMap::iterator it = m_LiveRecordTaskMap.begin();
	for (; it != m_LiveRecordTaskMap.end();it++)
	{
		if (it->second != NULL)
		{
			delete (it->second);
			it->second = NULL;
		}
	}
	m_LiveRecordTaskMap.clear();

	LOG_DEBUG("CRTITaskManage exit");
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
SInt32 CRTITaskManage::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CRTITaskManage run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
 Fuction name:Run
 Description :线程开始执行
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
Bool CRTITaskManage::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//加载频道
		LoadLiveChannel();

		//开始频道录制任务
		StartLiveRecordTask();

		//清除停止的频道
		ClearStopLiveChannel();

		//检查录制状态
		CheckLiveRecordTask();

		//清除过期数据
		ClearExpireFile();
	}

	return TRUE;
}
/*******************************************************************************
 Fuction name:LoadLiveChannel
 Description :加载频道信息
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void CRTITaskManage::LoadLiveChannel()
{
	if (OS::Milliseconds() - m_LastLoadLiveTime < 30000)
	{
		return;
	}

	LiveChannelList channeltasklist;
	SInt32 result = CRTMDBOperatermgr::Intstance()->GetAvtiveLiveChannel(channeltasklist);
	if (result != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("load live channel fail.ret:"<<result)
		return;
	}


	if (channeltasklist.empty())
	{
		LiveChannelMap::iterator it = m_LiveChannelMap.begin();
		for (; it != m_LiveChannelMap.end(); it++)
		{
			it->second.m_IsEnable = 0;
		}
	}
	else
	{
		LiveChannelList::iterator it = channeltasklist.begin();
		for (; it != channeltasklist.end(); it++)
		{
			LiveChannelMap::iterator it1 = m_LiveChannelMap.find(it->m_ChannelID);
			if (it1 != m_LiveChannelMap.end())
			{
				continue;
			}
			else
			{
				m_LiveChannelMap[it->m_ChannelID] = *it;
			}
		}
	}

	m_LastLoadLiveTime = OS::Milliseconds();
	
	return ;
}
/*******************************************************************************
 Fuction name:StartLiveRecordTask
 Description :开启频道录制任务
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void CRTITaskManage::StartLiveRecordTask()
{
	LiveChannelMap::iterator it = m_LiveChannelMap.begin();
	for (; it != m_LiveChannelMap.end(); )
	{
		LiveRecordTaskMap::iterator it1 = m_LiveRecordTaskMap.find(it->first);
		if (it1 != m_LiveRecordTaskMap.end())
		{
			continue;
		}

		if (it->second.m_IsEnable)
		{
			CRTITaskInterface *prtitask = (CRTITaskInterface*)CreateRuntimeObjectFromClassName(it->second.m_SourceType.c_str());
			if (prtitask != NULL)
			{
				SInt32 ret = prtitask->Init(it->second);
				if (ret == CDN_ERR_SUCCESS)
				{
					ret = prtitask->StartRecordTask();
				}

				if (ret == CDN_ERR_SUCCESS)
				{
					m_LiveRecordTaskMap[it->first] = prtitask;
					LOG_DEBUG("start live task."<<it->second.m_SourceUrl)
				}
				else
				{
					LOG_ERROR("start live task fail.url:"<<it->second.m_SourceUrl<<",ret:"<<ret)
						delete prtitask;
					prtitask = NULL;
				}
			}

			it++;
		}
		else
		{
			m_LiveChannelMap.erase(it++);
		}
	}

	return;
}
/*******************************************************************************
 Fuction name:ClearStopLiveChannel
 Description :清除停止的频道
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void  CRTITaskManage::ClearStopLiveChannel()
{
	if (OS::Milliseconds() - m_LastClearLiveTime < 600000)
	{
		return;
	}

	LiveChannelMap::iterator it = m_LiveChannelMap.begin();
	for (; it != m_LiveChannelMap.end();)
	{
		//是否已经不再需要录制
		SInt32 countnum = CRTMDBOperatermgr::Intstance()->GetChannelCount(it->first);
		if (countnum == 0)
		{
			LiveRecordTaskMap::iterator it1 = m_LiveRecordTaskMap.find(it->first);
			if (it1 != m_LiveRecordTaskMap.end())
			{
				SInt32 ret = it1->second->StopRecordTask();
				if (ret == 0)
				{
					LOG_DEBUG("stop record taks. SourceUrl:"<<it->second.m_SourceUrl)
					delete (it1->second);
					m_LiveRecordTaskMap.erase(it1);
					m_LiveChannelMap.erase(it++);
					continue;
				}
			}
		}
		it++;
	}

	return ;
}
/*******************************************************************************
 Fuction name:CheckLiveTask
 Description :检查频道录制任务状态
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void CRTITaskManage::CheckLiveRecordTask()
{
	if (OS::Milliseconds() - m_LastCheckLiveTime < 10000)
	{
		return;
	}

	LiveRecordTaskMap::iterator it = m_LiveRecordTaskMap.begin();
	for (; it != m_LiveRecordTaskMap.end(); it++)
	{
		if (!(it->second->CheckRecordTaskStatus()))
		{
			if (!(it->second->IsReStartRecordTask()))
			{
				LOG_INFO("restart record task")
				SInt32 ret = it->second->StartRecordTask();
				if (ret != CDN_ERR_SUCCESS)
				{
					LiveChannelMap::iterator it1 = m_LiveChannelMap.find(it->first);
					ret = CRTMDBOperatermgr::Intstance()->DeleteFromFileDistribute(it1->second.m_ProviderID, 
														                           it1->second.m_AssetID,
						                                                          OS::AddEndSymbolToPath(it1->second.m_StorePath).c_str());
					if (ret != CDN_ERR_SUCCESS)
					{
						LOG_ERROR("delete file distribute fail.ret:"<<ret)
					}
					LOG_ERROR("stream interrupt."<<it->first)
				}
			}
			else
			{
				LiveChannelMap::iterator it1 = m_LiveChannelMap.find(it->first);
				SInt32 ret = CRTMDBOperatermgr::Intstance()->DeleteFromFileDistribute(it1->second.m_ProviderID, 
																			          it1->second.m_AssetID,
					                                                                  OS::AddEndSymbolToPath(it1->second.m_StorePath).c_str());
				if (ret != CDN_ERR_SUCCESS)
				{
					LOG_ERROR("delete file distribute fail.ret:"<<ret)
				}
				LOG_ERROR("stream interrupt."<<it->first)
			}
		}
		else
		{
			LiveChannelMap::iterator it1 = m_LiveChannelMap.find(it->first);
			std::string filename = it1->second.m_ProviderID + "_" + it1->second.m_AssetID;
			SInt32 ret = CRTMDBOperatermgr::Intstance()->InsertToFileDistribute(it1->second.m_ProviderID, it1->second.m_AssetID,
				                                                                 filename, OS::AddEndSymbolToPath(it1->second.m_StorePath).c_str());
			if (ret != CDN_ERR_SUCCESS)
			{
				LOG_ERROR("Insert file distribute fail.ret:"<<ret)
			}
		}
	}

	m_LastCheckLiveTime = OS::Milliseconds();

	return ;
}
/*******************************************************************************
 Fuction name:ClearExpireFile
 Description :清除过期数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2016.7.14
*******************************************************************************/
void CRTITaskManage::ClearExpireFile()
{
	if (OS::Milliseconds() - m_LastClearExpireFileTime < CLibRTMStaticConf::m_ClearClearExpireFileTime)
	{
		return;
	}

	LiveRecordTaskMap::iterator it = m_LiveRecordTaskMap.begin();
	for (; it != m_LiveRecordTaskMap.end(); it++)
	{
		it->second->ClearExpireFile();
	}

	m_LastClearExpireFileTime = OS::Milliseconds();

	return ;
}

