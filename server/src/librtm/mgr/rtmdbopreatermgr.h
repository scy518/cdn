#ifndef RTM_DB_OPERATER_MGR_H
#define RTM_DB_OPERATER_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rtmdbopreatermgr.h
*  Description:  数据库处理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/singleton.h"
#include "libdb/dboperate.h"
#include "librtm/rtmcommon.h"
#include <list>

class CRTMDBOperatermgr : public CDBOperate
{
	DECLARATION_SINGLETON(CRTMDBOperatermgr)
public:

	//获取频道信息
	Int32 GetAvtiveLiveChannel(LiveChannelList &channellist);

	//插入数据到文件分布表中
	Int32 InsertToFileDistribute(const std::string &pid, const std::string &aid,
		                         const std::string &filename, const char *path);

	//从文件分布表中删除对应数据
	Int32 DeleteFromFileDistribute(const std::string &pid, const std::string &aid, const char *path);

	//更新带宽信息
	Int32 UpdateBandWidthInfo(const SInt64 &totalbandwidth, const SInt64 &freebandwidth);

	//查询对应频道id对应频道是否存在
	Int32 GetChannelCount(const SInt64 &channelid);

	//获取所有频道信息
	Int32 GetAllLiveChannel(LiveChannelList &channellist);
};

#endif
