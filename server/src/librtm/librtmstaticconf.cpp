/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "librtm/librtmmacros.h"
#include "librtmstaticconf.h"

//节点ID
std::string CLibRTMStaticConf::m_NodeID = "RTI_00";

//下载完文件需要执行的shell命令
std::string CLibRTMStaticConf::m_ShellCmd = "./shellcmd.sh";

//清理录制过期文件
Int32 CLibRTMStaticConf::m_ClearClearExpireFileTime = 600000;

//数据文件
std::string  CLibRTMStaticConf::m_SqlFileName = "";

//推流内网地址
std::string CLibRTMStaticConf::m_PrivateStreamAddr = "127.0.0.1:80";

//推流外网地址
std::string CLibRTMStaticConf::m_PublicStreamAddr = "";

//网卡配置文件
std::string	CLibRTMStaticConf::m_NetworkCardFile= "../conf/private/networkcard.xml";

IMPLEMENT_SINGLETON(CLibRTMStaticConf)
/*******************************************************************************
*  Function   : CLibRTMStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibRTMStaticConf::CLibRTMStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibRTMStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibRTMStaticConf::~CLibRTMStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibRTMStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibRTMStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibRTMStaticConf::SynStaticParam()
{
	//节点ID
	CLibRTMStaticConf::m_NodeID = std::string(GET_PARAM_CHAR(RTM_NODE_ID, "RTI_00"));

	//shell命令
	CLibRTMStaticConf::m_ShellCmd = GET_PARAM_CHAR(RTM_SHELL_CMD, "./shellcmd.sh");

	//清理录制过期文件
	CLibRTMStaticConf::m_ClearClearExpireFileTime = GET_PARAM_INT(RTM_CLEAR_EXPIRE_FILE_TIME, 10)*60*1000;

	//数据文件
	CLibRTMStaticConf::m_SqlFileName = GET_PARAM_CHAR(RTM_SQL_STATEMENT_FILE, "../conf/sql/sql.xml");

	//推流内网地址
	CLibRTMStaticConf::m_PrivateStreamAddr = GET_PARAM_CHAR(RTM_PRIVATE_STREAM_ADDR, "127.0.0.1:80");

	//推流外网地址
	CLibRTMStaticConf::m_PublicStreamAddr = GET_PARAM_CHAR(RTM_PUBLIC_STREAM_ADDR, "");

	//网卡配置文件
	CLibRTMStaticConf::m_NetworkCardFile = GET_PARAM_CHAR(RTM_NETWORK_CARD_CONF_FILE,"../conf/private/networkcard.xml");

	return;
}









