#ifndef AIS_DELETE_CONTENT_TASK_H
#define AIS_DELETE_CONTENT_TASK_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisdeletecontenttask.h
*  Description:  删除内容处理任务
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/aisdeletecontent.h"
#include "libcdnutil/cdnhttptaskbase.h"
#include "libais/aiscommon.h"

class CAISDeleteContentTask : public CCDNHttpTaskBase
{
public:

	CAISDeleteContentTask();

	virtual ~CAISDeleteContentTask();

protected:

	//应用实现函数，需要应用自己处理响应消息,成功返回0，非0标示失败
	virtual Int32 DoTask(CHTTPRequest*req, CHTTPResponse *rsp);

	//填充结构
	void FillContentKey(CAISDeleteContentReq *req, ContentKey &conkey);
};

#endif


