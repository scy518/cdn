/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsstaicparammgr.h
*  Description:  cls静态参数管理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libutil/stringtool.h"
#include "libutil/OS.h"
#include "libais/libaismacros.h"
#include "libaisstaticconf.h"


//数据文件
std::string  CLibAISStaticConf::m_SqlFileName = "";

//CI超时时间
SInt32  CLibAISStaticConf::m_NodeTimeOut = 30;

//文件备份数
SInt32 CLibAISStaticConf::m_BackupNum = 2;

//是否开启预分发
SInt32 CLibAISStaticConf::m_DistributeIsEnable = DISTRIBUTE_DISENABLE;

//同时分发任务数
SInt32 CLibAISStaticConf::m_AisDistributeTaskNum = 5;

//注入任务长时间没有启动，经过多长时间后清理,单位小时
SInt32 CLibAISStaticConf::m_AisAfterCleanTime = 24;

//注入任务完成后，经过多长时间后进行清理, 单位分钟
SInt32 CLibAISStaticConf::m_AisFinishCleanTime = 30;

//任务分发完成后，经过多长时间后进行清理,单位小时
SInt32 CLibAISStaticConf::m_DistributeCleanTime = 24;


IMPLEMENT_SINGLETON(CLibAISStaticConf)
/*******************************************************************************
*  Function   : CLibAISStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibAISStaticConf::CLibAISStaticConf()
{
}
/*******************************************************************************
*  Function   : CLibAISStaticConf
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CLibAISStaticConf::~CLibAISStaticConf()
{
}
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化节点管理器
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibAISStaticConf::Initialize()
{
	SynStaticParam();

	return TRUE;
}
/*******************************************************************************
*  Function   : Run
*  Description: 线程调用函数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Bool CLibAISStaticConf::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : SynStaticParam
*  Description: 同步参数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CLibAISStaticConf::SynStaticParam()
{
	//数据文件
	CLibAISStaticConf::m_SqlFileName = GET_PARAM_CHAR(AIS_SQL_STATEMENT_FILE, "../conf/sql/sql.xml");

	//CI超时时间
	CLibAISStaticConf::m_NodeTimeOut = GET_PARAM_INT(AIS_NODE_TIME_OUT, 30);

	//文件备份数
	CLibAISStaticConf::m_BackupNum = GET_PARAM_INT(AIS_BACKUP_NUM, 2);

	//是否开启预分发
	CLibAISStaticConf::m_DistributeIsEnable = GET_PARAM_INT(AIS_DISTRIBUTE_IS_ENABLE, DISTRIBUTE_DISENABLE);

	//同时分发任务数
	CLibAISStaticConf::m_AisDistributeTaskNum = GET_PARAM_INT(AIS_DISTRIBUTE_TASK_NUM, 5);

	//注入任务长时间没有启动，经过多长时间后清理,单位小时
	CLibAISStaticConf::m_AisAfterCleanTime = GET_PARAM_INT(AIS_AFTER_CLEAN_TIME, 24);

	//注入任务完成后，经过多长时间后进行清理, 单位分钟
	CLibAISStaticConf::m_AisFinishCleanTime = GET_PARAM_INT(AIS_FINISH_CLEAN_TIME, 30);

	//任务分发完成后，经过多长时间后进行清理,单位小时
	CLibAISStaticConf::m_DistributeCleanTime = GET_PARAM_INT(AIS_DISTRIBUTE_CLEAN_TIME, 24);

	return;
}









