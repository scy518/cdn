/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libaismodule.cpp
*  Description:  libais模块初始化类
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libmodulemgr/modulemac.h"
#include "libdb/sqlstatementmgr.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/ingesttaskhandle.h"
#include "mgr/distributedatamgr.h"
#include "mgr/distributetaskmgr.h"
#include "mgr/aisdboperatemgr.h"
#include "mgr/dbclearmgr.h"
#include "libais/libais_dll.h"
#include "libaisstaticconf.h"

REGISTER_MODULE(libais, LIBAIS_EXPORT)
/*******************************************************************************
*  Function   : Initialize
*  Description: 初始化libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libais::Initialize(const char *argv)
{	
	//初始化配置参数
	if (CLibAISStaticConf::Intstance()->Initialize() != TRUE)
	{
		printf("Init CLibAISStaticConf fail.\n");
		return -1;
	}

	//初始化sql 语句管理器
	SInt32 ret = ADD_SQL_FILE_STRING(CLibAISStaticConf::m_SqlFileName);
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -2;
	}

	//初始化数据连接
	ret = CAISDBOperatemgr::Intstance()->Initialize();
	if (ret != CDN_ERR_SUCCESS)
	{
		printf("Init CSqlStatementmgr fail. ret:%d\n", ret);
		return -3;
	}

	//初始化注入任务
	ret = CIngestTaskHandle::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CIngestTaskHandle fail. ret:%d\n", ret);
		return -4;
	}

	//初始化分发数据管理
	ret = CDistributeDataMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CDistributeTaskMgr fail. ret:%d\n", ret);
		return -5;
	}

	//初始化分发管理
	ret = CDistributeTaskMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CDistributeTaskMgr fail. ret:%d\n", ret);
		return -6;
	}

	//数据库定时清理
	ret = CDBClearMgr::Intstance()->Init();
	if (ret != 0)
	{
		printf("Init CDBClearMgr fail. ret:%d\n", ret);
		return -7;
	}

	return ret;
}
/*******************************************************************************
*  Function   : Release
*  Description: 退出libci模块
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 libais::Release()
{
	CDBClearMgr::Intstance()->Destroy();

	CDistributeTaskMgr::Intstance()->Destroy();

	CDistributeDataMgr::Intstance()->Destroy();

	CIngestTaskHandle::Intstance()->Destroy();

	CAISDBOperatemgr::Intstance()->Destroy();

	REMOVE_SQL_FILE_STRING(CLibAISStaticConf::m_SqlFileName);

	CLibAISStaticConf::Intstance()->Destroy();


	return 0;
}





