/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name: distributedatamgr.cpp
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务管理器
*******************************************************************************/
#include "libcore/log.h"
#include "libutil/OS.h"
#include "libais/libaismacros.h"
#include "libcdnutil/cdnerrorcode.h"
#include "mgr/distributedatamgr.h"
#include "mgr/aisdboperatemgr.h"
#include "libaisstaticconf.h"

IMPLEMENT_SINGLETON(CDistributeDataMgr)
/*******************************************************************************
*  Function   : CDistributeDataMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeDataMgr::CDistributeDataMgr()
{
	m_LastAllTime = 0;
	m_LastIncTime = 0;
	m_LastAsynSubNodeTime = 0;
}
/*******************************************************************************
*  Function   :~CDistributeDataMgr
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
CDistributeDataMgr::~CDistributeDataMgr()
{
	this->StopTask();
}
/*******************************************************************************
*  Function   :Init
*  Description:初始化
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
SInt32  CDistributeDataMgr::Init()
{
	if (!this->RunTask())
	{
		ERROR_LOG("CDistributeDataMgr run task fail")
		return -1;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   :Execute
*  Description:
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
Bool CDistributeDataMgr::Run()
{
	while(TRUE)
	{
		if(IsNormalQuit())
		{
			break;
		}

		OS::Sleep(1000);

		//加载子节点
		LoadSubNode();

		//加载特定分发数据
		LoadNoSubNodeAddrDistributeData();

		//加载需要全量分发的数据
		LoadAllDistributeData();
	}

	return TRUE;
}
/*******************************************************************************
*  Function   : LoadSubNode
*  Description: 加载子节点
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeDataMgr::LoadSubNode()
{
	if (OS::Milliseconds() - m_LastAsynSubNodeTime < 120000)
	{
		return ;
	}


	//获取子节点信息
	SubNodeItemList  subnodelist;
	Int32 ret = CAISDBOperatemgr::Intstance()->GetSubNodeList(subnodelist);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("GetSubNodeList fail. ret:"<<ret);
		return;
	}
	else
	{
		if (!subnodelist.empty())
		{
			m_SubNodeList.clear();
			m_SubNodeList.insert(m_SubNodeList.end(), subnodelist.begin(), subnodelist.end());
		}
	}

	m_LastAsynSubNodeTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : LoadAllDistributeData
*  Description: 加载需要全量分发的数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeDataMgr::LoadAllDistributeData()
{
	if (CLibAISStaticConf::m_DistributeIsEnable == DISTRIBUTE_DISENABLE)
	{	
		return;
	}

	if (OS::Milliseconds() - m_LastAllTime < 120000)
	{
		return ;
	}

	//处理全量分发的数据
	DoAllDistributeData();

	m_LastAllTime = OS::Milliseconds();

	return;
}
/*******************************************************************************
*  Function   : LoadAddDistributeData
*  Description: 加载特定分发数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeDataMgr::LoadNoSubNodeAddrDistributeData()
{
	if (OS::Milliseconds() - m_LastIncTime < 30000)
	{
		return ;
	}

	//处理特定分发的数据
	DoNoSubNodeAddrDistributeData();

	m_LastIncTime = OS::Milliseconds();
	return;
}
/*******************************************************************************
*  Function   : DealDistributeData
*  Description: 处理全量分发的数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeDataMgr::DoAllDistributeData()
{
	if (m_SubNodeList.size() == 0)
	{
		return ;
	}

	std::list<DistributeTaskItem> distributeTaskItemlist;
	SInt32 ret = CAISDBOperatemgr::Intstance()->GetCompleteTask(distributeTaskItemlist);
	if (ret != CDN_ERR_SUCCESS)
	{
		LOG_ERROR("GetCompleteTask fail. ret:"<<ret);
		return;
	}

	std::list<DistributeTaskItem>::iterator it = distributeTaskItemlist.begin();
	for (; it != distributeTaskItemlist.end(); it++)
	{
		DistributeTaskItem tempdistributetaskitem = *it;

		SubNodeItemList::iterator it1 = m_SubNodeList.begin();
		for (; it1 != m_SubNodeList.end(); it1++)
		{
			tempdistributetaskitem.m_SubNodeAisAddr = it1->m_SubInAddress;

			//查看记录是否存在
			int result = CAISDBOperatemgr::Intstance()->GetDistributeCount(tempdistributetaskitem);
			if (result == 0)
			{
				//记录不存在，插入到数据库中
				result = CAISDBOperatemgr::Intstance()->InsertDataToTaskDistribute(tempdistributetaskitem);
				if (result != 0)
				{
					LOG_ERROR("InsertDataToTaskDistribute fail ");
				}
			}
		}
	}

	return ;
}
/*******************************************************************************
*  Function   : DoNoSubNodeAddrDistributeData
*  Description: 处理没有分发节点地址的数据
*  Calls      :
*  Called By  :
*  Input      :
*  Output     :
*  Return     :
*******************************************************************************/
void CDistributeDataMgr::DoNoSubNodeAddrDistributeData()
{
	if (m_SubNodeList.size() == 0)
	{
		return;
	}

	//数据查询需要分发的任务
	std::list<DistributeTaskItem> distributeTaskItemlist;
	int ret = CAISDBOperatemgr::Intstance()->GetNoSubNodeAddrDistributeData(distributeTaskItemlist);
	if (ret < 0)
	{
		LOG_ERROR("GetNoSubNodeAddrDistributeData fail. ret:"<<ret);
		return  ;
	}

	//将分发任务转换成分需要的节点信息
	std::list<DistributeTaskItem>::iterator it = distributeTaskItemlist.begin();
	for (; it != distributeTaskItemlist.end(); it++)
	{
		DistributeTaskItem tempdistributetaskitem = *it;
		SInt32 countnum = 0;

		SubNodeItemList::iterator it1 = m_SubNodeList.begin();
		for (; it1 != m_SubNodeList.end(); it1++)
		{
			tempdistributetaskitem.m_SubNodeAisAddr = it1->m_SubInAddress;

			//查看记录是否存在
			int result = CAISDBOperatemgr::Intstance()->GetDistributeCount(tempdistributetaskitem);
			if (result == 0)
			{
				//记录不存在，插入到数据库中
				result = CAISDBOperatemgr::Intstance()->InsertDataToTaskDistribute(tempdistributetaskitem);
				if (result != 0)
				{
					LOG_ERROR("InsertDataToTaskDistribute fail ");
					continue;
				}
				countnum++;
			}
		}

		//只要有一条记录操作成功，删除subnodeaisaddr为空对应记录
		if (countnum > 0)
		{
			 CAISDBOperatemgr::Intstance()->DeleteNoSubNodeAddrDistributeTask(tempdistributetaskitem);
		}
	}

	return ;
}