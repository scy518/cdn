#ifndef DISTRIBUTE_TASK_MGR_H
#define DISTRIBUTE_TASK_MGR_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:distributetaskmgr.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务管理器
*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/OSTypes.h"
#include "mgr/distributetask.h"

#include <string.h>
#include <string>
#include <list>
#include <map>

typedef std::list<CDistributeTask* >     CDistributeTaskList;

class CDistributeTaskMgr : public OSTask
{
	DECLARATION_SINGLETON(CDistributeTaskMgr)
public:

	//初始化
	SInt32  Init();

	//线程调用函数
	virtual Bool Run();

	//删除子节点文件
	void DeleteSubNodeFile(DistributeTaskItem &taskitem);

protected:

	//加载子节点
	void LoadSubNode();

	//刷新分发状态
	void RefreshDistributeStatus();

	//加载分发任务
	void LoadDistributeTask();

	//预分发需要删除的任务
	void DoDeleteSubNodeFile();

	//程序重启，加载需要获取状态的分发任务
	void RecoveryDistributeTask();

	//获取空闲分发任务
	CDistributeTask* GetFreeDistributeTask();

private:

	CDistributeTaskList                          m_DistributeTaskList;
	CDistributeTaskList                          m_FreeTaskList;

	OSMutex                                      m_DelMutex;
	DistTaskItemList                             m_DelFileList; //需要删除的媒资

	SubNodeItemList                              m_SubNodeList;  //子节点列表
	SInt64                                       m_LastAsynSubNodeTime;  //最新同步子节点时间

	SInt64                                       m_LastTime;
};


#endif




