#ifndef DB_CLEAR_MGR_H
#define DB_CLEAR_MGR_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    dbclearmgr.h
*  Description:  数据清理
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/singleton.h"

class CDBClearMgr : public OSTask
{
	DECLARATION_SINGLETON(CDBClearMgr)
public:

	//初始化注入内容管理器
	Int32 Init();

	//线程调用函数
	virtual Bool Run();

protected:

	//清理完成任务
	void CleanFinishiTask();

	//更新节点状态
	void RefeshNodeStatus();
};
#endif //DBMANAGE_H_
