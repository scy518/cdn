/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    dbperate.h
*  Description:  输出操作
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/ParamManager.h"
#include "libcore/errcodemacros.h"
#include "libutil/OS.h"
#include "libcore/log.h"
#include "libdb/dbconnectmgr.h"
#include "libdb/dbresultset.h"
#include "libdb/dbexception.h"
#include "libdb/sqlstatementmgr.h"
#include "libcdnutil/cdnerrorcode.h"
#include "libais/libaismacros.h"
#include "mgr/aisdboperatemgr.h"
#include "libaisstaticconf.h"

IMPLEMENT_SINGLETON(CAISDBOperatemgr)
/*******************************************************************************
*  Function   : CAISDBOperatemgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDBOperatemgr::CAISDBOperatemgr()
{
}
/*******************************************************************************
*  Function   : CAISDBOperatemgr
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CAISDBOperatemgr::~CAISDBOperatemgr()
{
}
/*******************************************************************************
*  Function   : InsertTask
*  Description: 插入注入任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::InsertTask(const IngestTaskItem &taskitem, const std::string &nodeid)
{
	if (nodeid.empty())
	{
		LOG_ERROR("nodeid is null")
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("DELETE_FAIL_TASK_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_FAIL_TASK_SQL")
		return -2;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, taskitem.m_ContentKey.m_ProviderID.c_str(), 
		    taskitem.m_ContentKey.m_AssetID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		return -3;
	}

	sqlstatement = GET_SQL_STRING("INSTERT_TRANSFER_TASK_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSTERT_TRANSFER_TASK_SQL")
		return -2;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, CMD_TRANSFER, taskitem.m_ContentKey.m_ProviderID.c_str(), 
		    taskitem.m_ContentKey.m_AssetID.c_str(), taskitem.m_ContentKey.m_ServiceType, 
			taskitem.m_SourceURL.c_str(), nodeid.c_str(), taskitem.m_Status, taskitem.m_Progress, 
			taskitem.m_CreateTime/1000ll, taskitem.m_StartTime, taskitem.m_EndTime,
			taskitem.m_Path.c_str(), taskitem.m_ErrCode, taskitem.m_ErrDes.c_str(),
			taskitem.m_ReportURL.c_str());
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		return -3;
	}
	
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : QueryTaskStatus
*  Description: 查询注入任务状态
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::QueryTaskStatus(const ContentKey &req, IngestTaskItem &rsp)
{
	rsp.m_ContentKey = req;
	const char *sqlstatement = GET_SQL_STRING("QUERY_TASK_STATUS_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_TASK_STATUS_SQL")
		rsp.m_ErrCode = SYS_ERR_SQL_NO_EXIST;
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, req.m_ProviderID.c_str(), req.m_AssetID.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		if (recordset != NULL)
		{
			rsp.m_ContentKey.m_ServiceType = (ServiceType)recordset->GetColSInt32(2);
			rsp.m_Status = recordset->GetColSInt32(3);
			rsp.m_Progress = recordset->GetColSInt32(4);
			rsp.m_CreateTime = recordset->GetColSInt64(5);
			rsp.m_StartTime = recordset->GetColSInt64(6);
			rsp.m_EndTime = recordset->GetColSInt64(7);
			rsp.m_ErrCode = recordset->GetColSInt32(8);
			rsp.m_Duration = recordset->GetColSInt64(9);
			rsp.m_BiteRate = recordset->GetColSInt64(10);
			rsp.m_FileSize = recordset->GetColSInt64(11);
			rsp.m_FileMD5 = recordset->GetColString(12).c_str();
		}
		else//任务表中没有数据
		{
			sqlstatement = GET_SQL_STRING("QUERY_FILE_INFO_SQL", CLibAISStaticConf::m_SqlFileName);
			if (sqlstatement == NULL)
			{
				LOG_ERROR("sql not find. QUERY_FILE_INFO_SQL")
				rsp.m_ErrCode = SYS_ERR_SQL_NO_EXIST;
				return SYS_ERR_SQL_NO_EXIST;
			}

			memset(sql, 0, sizeof(sql));
			sprintf(sql, sqlstatement, req.m_ProviderID.c_str(), req.m_AssetID.c_str());
			try
			{
				CDBResultset resultset;
				m_DBConnect->ExecuteResultset(sql, resultset);

				CDBRecordSet  *recordset = resultset.NextRow();
				if (recordset != NULL)
				{
					rsp.m_ContentKey.m_ServiceType = req.m_ServiceType;
					rsp.m_Status = 2;
					rsp.m_Progress = 100;
					rsp.m_Duration = recordset->GetColSInt64(1);
					rsp.m_BiteRate = recordset->GetColSInt64(2);
					rsp.m_FileSize = recordset->GetColSInt64(3);
					rsp.m_FileMD5 = recordset->GetColString(4).c_str();
				}
				else
				{
					rsp.m_ErrCode = CDN_ERR_NO_CONTENT;
				}
			}
			catch (CDBException &e)
			{
				LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);
				rsp.m_ErrCode = SYS_ERR_OPER_DB_FAIL;

				//检查连接
				CheckDBConnect();
				return SYS_ERR_OPER_DB_FAIL;
			}
		}
	}
	catch (CDBException &e)
	{
		if (m_DBConnect->IsResultEmpty(e))
		{
			rsp.m_ErrCode = SYS_ERR_RECORD_NO_EXIST;
			return SYS_ERR_RECORD_NO_EXIST;
		}
		else
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);
			rsp.m_ErrCode = SYS_ERR_OPER_DB_FAIL;

			//检查连接
			CheckDBConnect();
			return SYS_ERR_OPER_DB_FAIL;
		}
	}
	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetTaskCount
*  Description: 获取任务数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetTaskCount(std::string providerid, std::string assetID, Int32 cmd)
{
	const char *sqlstatement = GET_SQL_STRING("QUERY_CI_TASKCOUNT", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_CI_TASKCOUNT")
		return -1;
	}

	Int32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, providerid.c_str(), assetID.c_str(), cmd);

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	if (m_DBConnect == NULL)
	{
		return -2;
	}

	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (m_DBConnect->IsResultEmpty(e))
		{
			countnum = 0;
			
		}
		else
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			return -3;
		}
	}

	if (countnum > 0)
	{
		return countnum;
	}

	sqlstatement = GET_SQL_STRING("QUERY_FILEINFO_COUNT", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. QUERY_FILEINFO_COUNT")
		return SYS_ERR_SQL_NO_EXIST;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, providerid.c_str(), assetID.c_str());
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (m_DBConnect->IsResultEmpty(e))
		{
			countnum = 0;
		}
		else
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();

			return -4;
		}
	}

	return countnum;
}
/*******************************************************************************
*  Function   : DeleteContent
*  Description: 删除文件
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::DeleteContent(const ContentKey &req, const std::string &nodeid)
{
	if (nodeid.empty())
	{
		return -1;
	}

	const char *sqlstatement = GET_SQL_STRING("DELETE_COMPLETE_TASK", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_COMPLETE_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, req.m_ProviderID.c_str(), req.m_AssetID.c_str(), nodeid.c_str());

	//加锁
	OSMutexLocker lock(&m_Mutex);

	//检查数据库连接
	CHECK_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}
	
    sqlstatement = GET_SQL_STRING("INSTERT_TRANSFER_TASK_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSTERT_TRANSFER_TASK_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	memset(sql, 0, sizeof(sql));
	sprintf(sql, sqlstatement, CMD_DELETE, req.m_ProviderID.c_str(), req.m_AssetID.c_str(), 
		     req.m_ServiceType, "", nodeid.c_str(),TRANF_STATUS_WAITING, 0, 
		    OS::Milliseconds()/1000, 0ll, 0ll, "", 0, "", "");
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		return SYS_ERR_OPER_DB_FAIL;
	}

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetCINode
*  Description: 获取分配节点,按照剩余磁盘空间大小获取节点数
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32  CAISDBOperatemgr::GetCINode(std::list<std::string> &nodelist, int nodenum)
{
	const char *sqlstatement = GET_SQL_STRING("GET_SQL_CI_NODE", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. GET_SQL_CI_NODE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, nodenum);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			nodelist.push_back(pRecord->GetColString(0).c_str());
			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	if (nodelist.empty() && ret != SYS_ERR_OPER_DB_FAIL)
	{
		ret = SYS_ERR_RECORD_NO_EXIST;
	}

	return ret;
}
/*******************************************************************************
*  Function   : GetAllCINode
*  Description: 获取所有节点
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetAllCINode(std::list<std::string> &nodelist)
{
	const char *sqlstatement = GET_SQL_STRING("GET_ALL_SQL_CI_NODE", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. GET_ALL_SQL_CI_NODE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			nodelist.push_back(pRecord->GetColString(0).c_str());
			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : CleanExpiredFinishTask
*  Description: 清除过期完成任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISDBOperatemgr::CleanExpiredFinishTask(SInt32  expiredtime)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_EXPIRED_FINISH_TASK", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_EXPIRED_FINISH_TASK")
		return;
	}

	SInt64 currettime = OS::Seconds();
	char sql[512] = {0};
	sprintf(sql, sqlstatement, currettime, expiredtime);

	m_Mutex.Lock();
	if (m_DBConnect == NULL)
	{
		m_Mutex.Unlock();
		return;
	}

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql)

		//检查连接
		CheckDBConnect();
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : CleanExpiredUnFinishTask
*  Description: 清除过期没有开始的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISDBOperatemgr::CleanExpiredUnFinishTask(SInt32  expiredtime)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_UNEXPIRED_FINISH_TASK", 
		                                      CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_UNEXPIRED_FINISH_TASK")
		return;
	}

	SInt64 currettime = OS::Seconds();
	char sql[512] = {0};
	sprintf(sql, sqlstatement, currettime, expiredtime);

	m_Mutex.Lock();
	if (m_DBConnect == NULL)
	{
		m_Mutex.Unlock();
		return;
	}

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql)

		//检查连接
		CheckDBConnect();
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : CleanCompleteDistributeTask
*  Description: 清除已经分发完成的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CAISDBOperatemgr::CleanCompleteDistributeTask(SInt32  expiredtime)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_COMPLETE_DISTRIBUTE_TASK", 
											   CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_COMPLETE_DISTRIBUTE_TASK")
		return;
	}

	SInt64 currettime = OS::Seconds();
	char sql[512] = {0};
	sprintf(sql, sqlstatement, currettime, expiredtime);

	m_Mutex.Lock();
	if (m_DBConnect == NULL)
	{
		m_Mutex.Unlock();
		return;
	}

	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql)

		//检查连接
		CheckDBConnect();
	}
	m_Mutex.Unlock();

	return;
}
/*******************************************************************************
*  Function   : GetCompleteTask
*  Description: 获取已经完成的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetCompleteTask(DistTaskItemList &distributeTasklist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_COMPLETE_TASK", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_COMPLETE_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);

		CDBRecordSet  *pRecord = resultset.NextRow();
		while (pRecord != NULL)
		{
			DistributeTaskItem tempDistributeTaskItem;
			tempDistributeTaskItem.m_ProviderID = pRecord->GetColString(0).c_str();
			tempDistributeTaskItem.m_AssetID = pRecord->GetColString(1).c_str();
			tempDistributeTaskItem.m_ServiceType = pRecord->GetColSInt32(2);
			tempDistributeTaskItem.m_NodeID = pRecord->GetColString(3).c_str();
			tempDistributeTaskItem.m_Path = pRecord->GetColString(4).c_str();
			distributeTasklist.push_back(tempDistributeTaskItem);

			pRecord = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : InsertDataToTaskDistribute
*  Description: 将分发任务插入到任务分发表
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::InsertDataToTaskDistribute(const DistributeTaskItem &distributeTaskItem)
{
	const char *sqlstatement = GET_SQL_STRING("INSTERT_TASK_DISTRIBUTE_TASK_SQL", 
		                                       CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. INSTERT_TASK_DISTRIBUTE_TASK_SQL")
		return -1;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, distributeTaskItem.m_ProviderID.c_str(), distributeTaskItem.m_AssetID.c_str(), 
		distributeTaskItem.m_ServiceType, distributeTaskItem.m_Url.c_str(), distributeTaskItem.m_SubNodeAisAddr.c_str(), 
		DISTRIBUTE_WAITING, 0, OS::Milliseconds()/1000, 0ll, 0, "");

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = -2;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : UpdatePorcessTaskDistribute
*  Description: 更新任务分发表中的进度及状态值
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::UpdatePorcessTaskDistribute(const DistributeTaskItem &distributeTaskItem)
{
	const char *sqlstatement = GET_SQL_STRING("UPDATE_TASK_DISTRIBUTE_TASK_SQL",
		                                       CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_TASK_DISTRIBUTE_TASK_SQL")
		return -1;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement,distributeTaskItem.m_Url.c_str(), distributeTaskItem.m_Status, distributeTaskItem.m_Progress, 
		    distributeTaskItem.m_EndTime, distributeTaskItem.m_ErrCode, distributeTaskItem.m_ErrDes.c_str(), 
		    distributeTaskItem.m_ProviderID.c_str(), distributeTaskItem.m_AssetID.c_str(), 
			distributeTaskItem.m_SubNodeAisAddr.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = -2;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : GetSubNodeList
*  Description: 获取子节点信息
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetSubNodeList(SubNodeItemList  &subnodelist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_SUB_NODE", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_SUB_NODE")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		while (recordset != NULL)
		{
			SubNodeItem subnode;
			subnode.m_SubAreaCode  = recordset->GetColString(0).c_str();
			subnode.m_SubInAddress = recordset->GetColString(1).c_str();
			subnode.m_SubVodAddress = recordset->GetColString(2).c_str();
			subnode.m_SubManageUrl = recordset->GetColString(3).c_str();
			subnode.m_Remarks      = recordset->GetColString(4).c_str();
			subnodelist.push_back(subnode);

			recordset = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return CDN_ERR_SUCCESS;
}
/*******************************************************************************
*  Function   : GetDistributeCount
*  Description: 分发任务在分发表中是否存在
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetDistributeCount(const DistributeTaskItem &distributeTaskItem)
{
	const char *sqlstatement = GET_SQL_STRING("GET_DISTRIBUTE_COUNT", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. GET_DISTRIBUTE_COUNT")
		return -1;
	}

	Int32 countnum = 0;
	char sql[512] = {0};
	sprintf(sql, sqlstatement, distributeTaskItem.m_ProviderID.c_str(), distributeTaskItem.m_AssetID.c_str(), 
		    distributeTaskItem.m_ServiceType, distributeTaskItem.m_SubNodeAisAddr.c_str());

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		countnum = m_DBConnect->ExecuteSInt32(sql);
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<< e.what()<<". "<<sql);

			//检查连接
			CheckDBConnect();
			countnum = -2;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return countnum;
}
/*******************************************************************************
*  Function   : GetNoSubNodeAddrDistributeData
*  Description: 从任务分发表中获取subnodeaisaddr为空的数据
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetNoSubNodeAddrDistributeData(DistTaskItemList &distributeTasklist)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_NO_SUB_NODE_ADDR", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_NO_SUB_NODE_ADDR")
		return SYS_ERR_SQL_NO_EXIST;
	}

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sqlstatement, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		while (recordset != NULL)
		{
			DistributeTaskItem tempDistributeTaskItem;
			tempDistributeTaskItem.m_ProviderID = recordset->GetColString(0).c_str();
			tempDistributeTaskItem.m_AssetID    = recordset->GetColString(1).c_str();
			tempDistributeTaskItem.m_ServiceType = recordset->GetColSInt32(2);
			distributeTasklist.push_back(tempDistributeTaskItem);

			recordset = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sqlstatement);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : DeleteDistributeTask
*  Description: 删除分发任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::DeleteDistributeTask(const DistributeTaskItem &distributeTaskItem)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_DISTRIBUTE_TASK", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_DISTRIBUTE_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, distributeTaskItem.m_ProviderID.c_str(), 
		    distributeTaskItem.m_AssetID.c_str(), distributeTaskItem.m_ServiceType);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : DeleteNoSubNodeAddrDistributeTask
*  Description: 从任务分发表中删除subnodeaisaddr为空的记录
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::DeleteNoSubNodeAddrDistributeTask(const DistributeTaskItem &distributeTaskItem)
{
	const char *sqlstatement = GET_SQL_STRING("DELETE_NO_SUB_NODE_ADDR", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. DELETE_NO_SUB_NODE_ADDR")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, distributeTaskItem.m_ProviderID.c_str(), 
		    distributeTaskItem.m_AssetID.c_str(), distributeTaskItem.m_ServiceType);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : GetNeedDistributeTask
*  Description: 加载需要分发的任务
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::GetDistributeTask(DistTaskItemList &distributeTasklist, Int32 numbers, 
	                                   Int32 status)
{
	const char *sqlstatement = GET_SQL_STRING("SELECT_NEED_DISTRIBUTE_TASK", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. SELECT_NEED_DISTRIBUTE_TASK")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, status, numbers);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		while (recordset != NULL)
		{
			DistributeTaskItem tempDistributeTaskItem;
			tempDistributeTaskItem.m_ProviderID = recordset->GetColString(0).c_str();
			tempDistributeTaskItem.m_AssetID    = recordset->GetColString(1).c_str();
			tempDistributeTaskItem.m_ServiceType = recordset->GetColSInt32(2);
			tempDistributeTaskItem.m_SubNodeAisAddr = recordset->GetColString(3).c_str();
			distributeTasklist.push_back(tempDistributeTaskItem);

			recordset = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : UpdateNoOnline
*  Description: 时间超时，将在线节点更新为不在线
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::UpdateNoOnline(SInt32 timeout)
{
	const char *sqlstatement = GET_SQL_STRING("UPDATE_NODE_NO_ONLINE_SQL",
		                                       CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_NODE_NO_ONLINE_SQL")
		return -1;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, OS::Milliseconds()/1000ll, timeout);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : UpdateOnline
*  Description: 在超时时间范围内，将不在线节点更新为在线
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::UpdateOnline(SInt32 timeout)
{
	const char *sqlstatement = GET_SQL_STRING("UPDATE_NODE_ONLINE_SQL",
		                                       CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. UPDATE_NODE_ONLINE_SQL")
		return -1;
	}

	char sql[512] = {0};
	sprintf(sql, sqlstatement, timeout, OS::Milliseconds()/1000ll);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		m_DBConnect->Execute(sql);
	}
	catch (CDBException &e)
	{
		LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

		//检查连接
		CheckDBConnect();
		ret = SYS_ERR_OPER_DB_FAIL;
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : QueryActiveNode
*  Description: 获取所有在线的节点
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::QueryOnlineNode(NodeList &nodelist)
{
	const char *sqlstatement = GET_SQL_STRING("GET_ONLINE_NODE_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. GET_ONLINE_NODE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement);

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		while (recordset != NULL)
		{
			Node tempnode;
			tempnode.m_NodeID = recordset->GetColString(0).c_str();
			tempnode.m_PrivateStreamAddr = recordset->GetColString(1).c_str();
			tempnode.m_PublicStreamAddr = recordset->GetColString(2).c_str();
			tempnode.m_FreeBandWidth = recordset->GetColSInt64(3);
			tempnode.m_HeartTime = recordset->GetColSInt64(4);
			nodelist.push_back(tempnode);

			recordset = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : QueryFileDistribute
*  Description: 获取媒资分布的节点
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::QueryFileDistribute(const std::string &pid, const std::string &aid, 
										 FileDistributeList &filedistributelist)
{
	const char *sqlstatement = GET_SQL_STRING("GET_MEDIA_DISTRIBUTE_NODE_SQL", CLibAISStaticConf::m_SqlFileName);
	if (sqlstatement == NULL)
	{
		LOG_ERROR("sql not find. GET_MEDIA_DISTRIBUTE_NODE_SQL")
		return SYS_ERR_SQL_NO_EXIST;
	}

	char  sql[512] = {0};
	sprintf(sql, sqlstatement, pid.c_str(), aid.c_str());

	SInt32 ret = CDN_ERR_SUCCESS;

	//开始使用数据连接
	USE_DB_CONNECT();
	try
	{
		CDBResultset resultset;
		m_DBConnect->ExecuteResultset(sql, resultset);

		CDBRecordSet  *recordset = resultset.NextRow();
		while (recordset != NULL)
		{
			FileDistribute tempfiledistribute;
			tempfiledistribute.m_ProviderID = recordset->GetColString(0).c_str();
			tempfiledistribute.m_AssetID = recordset->GetColString(1).c_str();
			tempfiledistribute.m_Path = recordset->GetColString(2).c_str();
			tempfiledistribute.m_NodeID = recordset->GetColString(3).c_str();
			filedistributelist.push_back(tempfiledistribute);

			recordset = resultset.NextRow();
		}
	}
	catch (CDBException &e)
	{
		if (!m_DBConnect->IsResultEmpty(e))
		{
			LOG_WARN("Execute sql fail "<<e.what()<<"."<<sql);

			//检查连接
			CheckDBConnect();
			ret = SYS_ERR_OPER_DB_FAIL;
		}
	}
	//数据库使用完成
	USE_DB_CONNECT_FINISH();

	return ret;
}
/*******************************************************************************
*  Function   : QueryNodeAddr
*  Description: 获取媒资推流地址和路径
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CAISDBOperatemgr::QueryNodeAddr(const std::string &pid, const std::string &aid, 
	                               std::string &nodeaddr, std::string &path)
{
	FileDistributeList filenodelist;
	int ret = QueryFileDistribute(pid, aid, filenodelist);
	if (ret != 0)
	{
		LOG_ERROR("QueryFileDistribute fail")
		return -1;
	}

	NodeList nodelist;
	ret = QueryOnlineNode(nodelist);
	if (ret != 0)
	{
		LOG_ERROR("QueryOnlineNode fail");
		return -1;
	}

	Node  tmpnode;
	FileDistribute filedistr;
	FileDistributeList::iterator it = filenodelist.begin();
	for (; it != filenodelist.end(); it++)
	{
		NodeList::iterator it1 = nodelist.begin();
		for (; it1 != nodelist.end(); it1++)
		{
			if (it1->m_NodeID == it->m_NodeID)
			{
				if (tmpnode.m_NodeID.empty() || (tmpnode.m_FreeBandWidth<= it1->m_FreeBandWidth))
				{
					tmpnode = *it1;
					filedistr = *it;
				}
			}
		}
	}

	nodeaddr = tmpnode.m_PrivateStreamAddr;
	path = filedistr.m_Path;

	if (nodeaddr.empty() || path.empty())
	{
		return -1;
	}

	return 0;
}












