#ifndef DISTRIBUTE_DATA_MGR_H
#define DISTRIBUTE_DATA_MGR_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:distributedatamgr.h
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 分发任务管理器
*******************************************************************************/
#include "libcore/OSThread.h"
#include "libutil/OSTypes.h"
#include "libais/aiscommon.h"

class CDistributeDataMgr : public OSTask
{
	DECLARATION_SINGLETON(CDistributeDataMgr)
public:

	//初始化
	SInt32  Init();

	//线程调用函数
	virtual Bool Run();

protected:

	//加载子节点
	void LoadSubNode();

	//加载需要全量分发的数据
	void LoadAllDistributeData();

	//加载特定分发数据
	void LoadNoSubNodeAddrDistributeData();

	//处理全量分发的数据
	void DoAllDistributeData();

	//处理特定分发的数据
	void DoNoSubNodeAddrDistributeData();
	
private:

	SubNodeItemList                              m_SubNodeList;  //子节点列表

	SInt64                                       m_LastAllTime;

	SInt64                                       m_LastIncTime;

	SInt64                                       m_LastAsynSubNodeTime;  //最新同步子节点时间

};


#endif




