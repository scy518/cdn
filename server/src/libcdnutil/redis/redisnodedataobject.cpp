/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    redisnodedataobject.cpp
*  Description:  管理数据对象
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/redisnodedataobject.h"

CONF_IMPLEMENT_DYNCREATE(CRedisNodeDataObject, CRedisDataObject)
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisNodeDataObject::CRedisNodeDataObject()
{
}
/*******************************************************************************
*  Function   : 
*  Description: 
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CRedisNodeDataObject::~CRedisNodeDataObject()
{
}
/*******************************************************************************
*  Function   : Encode
*  Description: 将结构数据编码成连续数据，buff返回编码后数据指针，返回buff数据长度
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisNodeDataObject::Encode(char *&buff)
{
	if (strlen(m_NodeItem.m_NodeName) <= 0)
	{
		return -1;
	}

	Int32 nodesize = sizeof(CGNodeItem);

	//分配空间
	buff = this->EncodeMalloc(nodesize);
	if (buff == NULL)
	{
		return -2;
	}

	memcpy(buff , &m_NodeItem, nodesize);

	return nodesize;
}
/*******************************************************************************
*  Function   : Decode
*  Description: 将buff数据转码成结构数据,返回成功失败,0:成功，非零失败
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
Int32 CRedisNodeDataObject::Decode(const char *buff, Int32 len)
{
	if (buff == NULL || len <= 0)
	{
		return -1;
	}

	Int32 nodesize = sizeof(CGNodeItem);
	if (len != nodesize)
	{
		return -2;
	}

	memcpy(&m_NodeItem, buff, len);
	return 0;
}
/*******************************************************************************
*  Function   : AddNode
*  Description: 添加节点列表
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
void CRedisNodeDataObject::AddCGNode(CGNodeItem nodevec)
{
	if (strlen(nodevec.m_NodeName) <= 0)
	{
		return;
	}

	m_NodeItem = nodevec;
	return;
}
/*******************************************************************************
*  Function   : GetNodeList
*  Description: 获取节点列表
*  Calls      : 见函数实现
*  Called By  : 
*  Input      : 无
*  Output     : 无
*  Return     : 
*******************************************************************************/
CGNodeItem CRedisNodeDataObject::GetCGNode()
{
	return m_NodeItem;
}





