/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingesttaskinterface.h
*  Description:  注入任务处理接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/citaskinterface.h"

CONF_IMPLEMENT_NO_DYNCREATE(CCITaskInterface, RuntimeObject)

