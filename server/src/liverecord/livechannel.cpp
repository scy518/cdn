/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: livechannel.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#include "libutil/OS.h"
#include "livechannel.h"
/*******************************************************************************
 Fuction name:CHLSRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CLiveChannel::CLiveChannel()
{
	m_UdpSource = NULL;
	m_HLSRecord = NULL;
	m_IsRuning = false;
}
/*******************************************************************************
 Fuction name:CHLSRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CLiveChannel::~CLiveChannel()
{
	if (m_UdpSource != NULL)
	{
		delete m_UdpSource;
		m_UdpSource = NULL;
	}

	if (m_HLSRecord != NULL)
	{
		delete m_HLSRecord;
		m_HLSRecord = NULL;
	}
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CLiveChannel::Init(const char *url, const char *localip, const char *storpath, Int32 splitdur)
{
	if (url == NULL || storpath == NULL || localip == NULL)
	{
		return -1;
	}

	m_HLSRecord = new CHLSRecord();
	Int32 ret = m_HLSRecord->Init(storpath, splitdur);
	if (ret != 0)
	{
		printf("hls record init fail.ret:%d\n", ret);
		return -2;
	}

	m_UdpSource = new CUDPSource();
	ret = m_UdpSource->Init(url, localip, m_HLSRecord);
	if (ret != 0)
	{
		printf("udp source init fail.ret:%d\n", ret);
		return -3;
	}

	return 0;
}
/*******************************************************************************
 Fuction name:StartRecord
 Description :开始录制
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CLiveChannel::StartRecord()
{
	if (m_UdpSource == NULL || m_HLSRecord == NULL)
	{
		return -1;
	}

	m_IsRuning = true;
	while(m_IsRuning)
	{
		//OS::Sleep(50);

		//接收数据
		m_UdpSource->Receive();
	}

	return 0;
}
/*******************************************************************************
 Fuction name:StopRecord
 Description :停止录制
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
Int32 CLiveChannel::StopRecord()
{
	m_IsRuning = false;
	OS::Sleep(3000);

	return 0;
}













