#ifndef LIVE_CHANNEL_H
#define LIVE_CHANNEL_H
/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: livechannel.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 直播频道管理
       
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "hlsrecord.h"
#include "udpsource.h"

class CLiveChannel
{
public:

	CLiveChannel();

	virtual ~CLiveChannel();

	//初始化
	Int32 Init(const char *url, const char *localip, const char *storpath, Int32 splitdur =10);


	//开始录制
	Int32 StartRecord();

	//停止录制
	Int32 StopRecord();

private:

	CUDPSource           *m_UdpSource;

	CHLSRecord            *m_HLSRecord;

	bool                  m_IsRuning;
};

#endif












