#ifndef HLS_RECORD_H
#define HLS_RECORD_H
/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: livechannel.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : hls录制管理
       
*******************************************************************************/
#include "libutil/OSTypes.h"
#include <string>

#define  WRITE_SIZE               4096*188

class CHLSRecord
{
public:
	
	CHLSRecord();
	
	~CHLSRecord();

	//初始化
	SInt32 Init(const char *path, SInt32  splitdur = 10, SInt32 cachesize = WRITE_SIZE);
		
	//处理媒体数据
	SInt32 ProcessData(UChar *lpdata, SInt32 idatalen);

	//处理单个ts包数据
	SInt32 ProcessTsData(UChar *lpdata);

	//获取码率
	SInt32 GetTransferRate();	

protected:

	//写数据到缓存	
	SInt32 WriteFileData(const char *lpdata, SInt32 idatalen, bool iscache = true);

	//写数据到磁盘
	SInt32 WriteToDisk(const char *lpdata, SInt32 idatalen);

	//创建文件
	SInt32 CreateFile(SInt32 cccount, SInt64 needseq);

	//分配内存
	SInt32 MallocBuff();

private:

	UChar             m_PAT[188];
	UChar             m_PMT[188];

	SInt64            m_LastPcr;
	SInt32            m_CurDur;

	short             m_sPmtPid;
	short             m_sVideoPid;
	short             m_sPcrPid;

	UChar             m_ucVideoType;

	UChar             *m_WriteBuff;
	SInt32            m_WriteDataLen;
	SInt32            m_WriteBuffSize;

	SInt32            m_BiteRate;

	std::string       m_StorePath;
	std::string       m_CurFileName;
    SInt64            m_CurSeq;    // 当前序号

	SInt32            m_CurFD;     //当前文件句柄

	SInt32            m_SplitDur;
};

#endif




