/*******************************************************************************
版权所有 (C), 2001-2013, 
********************************************************************************	
file name: cnormaltsfile.h
version  :
created  : 
file path: 
modify   :
author   : 
purpose  : 生成ngod标准索引文件，以及倍速文件
       
*******************************************************************************/
#include "libutil/OS.h"
#include "ts_packet.h"
#include "pat_pmt.h"
#include "demux.h"
#include "hlsrecord.h"
/*******************************************************************************
 Fuction name:CHLSRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CHLSRecord::CHLSRecord()
{
	memset(m_PAT,0x00,188);
	memset(m_PMT,0x00,188);

	m_LastPcr = -1;
	m_sPmtPid = -1;
	m_sVideoPid = -1;
	m_sPcrPid = -1;

	m_WriteBuff = NULL;
	m_WriteBuffSize = 0;
	m_WriteDataLen = 0;

	m_BiteRate     = 0;

	m_StorePath    = "";
	m_CurFileName  = "";
	m_CurSeq       = 0;
	m_CurDur       = 0;
	m_SplitDur     = 0;
	m_CurFD        = -1;
}
/*******************************************************************************
 Fuction name:CHLSRecord
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
CHLSRecord::~CHLSRecord()
{
	if (m_WriteBuff != NULL)
	{
		free(m_WriteBuff);
		m_WriteBuff = NULL;
	}
	
	//关闭文件
	if (m_CurFD > 0)
	{
		OS::Close(m_CurFD);
		m_CurFD = -1;
	}
}
/*******************************************************************************
 Fuction name:Init
 Description :初始化hls录制，path:录制存储路径
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::Init(const char *path, SInt32  splitdur, SInt32 cachesize)
{
	if (path == NULL)
	{
		return -1;
	}

	//初始化缓存
	if (cachesize < WRITE_SIZE)
	{
		cachesize = WRITE_SIZE;
	}

	m_WriteBuffSize = cachesize;
	if (MallocBuff() != 0)
	{
		return -2;
	}

	m_StorePath = OS::AddEndSymbolToPath(std::string(path));
	m_SplitDur = splitdur;

	return 0;
}
/*******************************************************************************
 Fuction name:GetTransferRate
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::GetTransferRate()
{
	return m_BiteRate;
}
/*******************************************************************************
 Fuction name:ProcessTsData
 Description :处理数据段
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::ProcessData(UChar *lpdata, SInt32 idatalen)
{
	if (lpdata == NULL || idatalen < 188)
	{
		return -1;
	}
	
	int ipackid = 0;
	for(ipackid = 0;ipackid < idatalen/188; ++ipackid)
	{
		UChar *lpchar = lpdata+188*ipackid;
		ProcessTsData(lpchar);
	}

	return 0;
}
/*******************************************************************************
 Fuction name:ProcessTsData
 Description :处理数据按照188自己处理
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::ProcessTsData(UChar *lpdata)
{
	int idatalen = 188;
	if(lpdata[0] != 0x47)
	{
		return -1;
	}

	short pid = TS_Get_Pid(lpdata);
	long long llpcrvalue = -1;
	long long llpts = -1;
	long long lldts = -1;
	int icurframetype = 0;

	if(pid == 0x00 && m_sPmtPid < 0)
	{
		pat_list tmppat = patparse(lpdata);
		for(int iindex = 0;iindex < tmppat.total;++iindex)
		{
			short tmppid = tmppat.program[iindex].pmt_pid;
			m_sPmtPid = tmppid;
		}
		memcpy(m_PAT,lpdata,188);

	}

	if(m_sPmtPid <= 0)
	{
		return -2;
	}

	//注意lpdate指的是一个包的数据
	if(pid == m_sPmtPid && m_sVideoPid < 0)
	{
		//printf("pid:%d,pid:%d\n",m_sPmtPid,m_sVideoPid);
		pmt_list tmppmt = pmtparse(lpdata,pid);
		int iindex = 0;
		for(iindex = 0;iindex < tmppmt.total_es;++iindex)
		{
			if(tmppmt.alles[iindex].stream_type == 0x1B || tmppmt.alles[iindex].stream_type == 0x02 || tmppmt.alles[iindex].stream_type == 0x24)
			{
				m_ucVideoType = tmppmt.alles[iindex].stream_type;
				m_sVideoPid = tmppmt.alles[iindex].es_pid;
			}
		}
		m_sPcrPid = tmppmt.pcr_pid;
		memcpy(m_PMT,lpdata,188);
	}

	if(m_sVideoPid <= 0)
	{
		return -3;
	}

	//pcr处理
	if(pid == m_sPcrPid && TS_Get_Pcr_flag(lpdata))
	{
		//获取当前数据包的pcr的值
		llpcrvalue = TS_Get_Pcr(lpdata);
		if(m_LastPcr < 0 || (m_LastPcr > llpcrvalue))
		{
			m_LastPcr = llpcrvalue;
		}
		else
		{
			//计算出来的是毫秒
			m_CurDur += ((27000*1000)/(llpcrvalue-m_LastPcr));
			m_LastPcr = llpcrvalue;
		}
	}

	//pts,dts,frame_type
	if(TS_Get_payload_unit_start_indicator(lpdata) || 
	  (TS_Get_adaptation_field_control(lpdata) && m_ucVideoType == 0x24))//h.256处理
	{
		if(pid == m_sVideoPid)
		{
			int m_ilog2_frame_num;
			icurframetype = TS_Get_FrameType(lpdata,m_ucVideoType, m_ilog2_frame_num);
			if (icurframetype != 2 && icurframetype != 3 && icurframetype != 1)
			{
				printf("packet is unknow, %d\n", icurframetype);
			}
		}
	}

	//第一次进入,过滤不是I帧开始的数据
	if (m_CurFD <= 0 && icurframetype != 1)
	{
		return 0;
	}
	
	//判断写文件是否结束
	SInt64 tmpseq = OS::Seconds()/m_SplitDur;
	if ((m_CurDur > (m_SplitDur*1000) || tmpseq > m_CurSeq) && icurframetype == 1)
	{
		//创建文件
		CreateFile(TSPACKET_GETCOUNT(*(TSPacket_t*)lpdata), tmpseq);

		m_CurDur = 0;
	}

	//写数据
	WriteFileData((const char*)lpdata,idatalen);
	
	return 0;
}
/*******************************************************************************
 Fuction name:WriteFileData
 Description :
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::WriteFileData(const char *lpdata, SInt32 idatalen, bool iscache)
{
	if (m_WriteBuff == NULL || m_WriteBuffSize < idatalen || !iscache)
	{
		int ret = WriteToDisk(lpdata,idatalen);
		if (ret != 0)
		{
			return ret;
		}
		
		return idatalen;
	}
	
	SInt32 freelen = m_WriteBuffSize - m_WriteDataLen;
	if (freelen < idatalen)
	{
		WriteToDisk((const char*)m_WriteBuff, m_WriteDataLen);
		m_WriteDataLen = 0;
	}

	memcpy(m_WriteBuff+m_WriteDataLen, lpdata, idatalen);
	m_WriteDataLen += idatalen;

	return idatalen;
}
/*******************************************************************************
 Fuction name:SyncWriteToDiskFromFile
 Description :同步写数据
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::WriteToDisk(const char *lpdata, SInt32 idatalen)
{
	int ret = OS::Write(m_CurFD, lpdata, idatalen);
	if (ret < 0)
	{
		printf("=====================fail %d===%d\n", ret, OS::GetErrno());
	}

	return (ret>0)?0:ret;
}
/*******************************************************************************
 Fuction name:CreateFile
 Description :创建文件
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::CreateFile(SInt32 cccount, SInt64 needseq)
{
	if (m_CurSeq == 0)
	{
		m_CurSeq = OS::Seconds()/m_SplitDur;
	}
	else
	{
		if (needseq - m_CurSeq > 3)
		{
			m_CurSeq = needseq;
		}
		else
		{
			m_CurSeq++;
		}
	}

	//写剩余数据
	if (m_CurFD > 0)
	{
		if (m_WriteDataLen > 0)
		{
			WriteToDisk((const char*)m_WriteBuff, m_WriteDataLen);
			m_WriteDataLen = 0;
		}
		
		if (m_BiteRate <= 0)
		{
			m_BiteRate = OS::Seek(m_CurFD, 0, SEEK_END)*8*1000/m_CurDur;
		}

		OS::Close(m_CurFD);
	}

	char buff[128] = {0};
	sprintf(buff, "%s%lld.ts", m_StorePath.c_str(), m_CurSeq);
	m_CurFileName = buff;

#ifdef WIN32
	m_CurFD = _open(m_CurFileName.c_str(), O_CREAT|O_TRUNC|O_WRONLY|O_BINARY, 0666);
#else
	m_CurFD = open(m_CurFileName.c_str(), O_CREAT|O_TRUNC|O_WRONLY|O_LARGEFILE, 0666);
#endif

/*
#ifdef WIN32
	m_CurFD = OS::Open(m_CurFileName.c_str(), URL_WRONLY, 0666);
#else
	m_CurFD = OS::Open(m_CurFileName.c_str(), URL_WRONLY|URL_DIRECT, 0666);
#endif
*/

	if (m_CurFD > 0)
	{
		int patccount = cccount-2;
		if (patccount < 0)
		{
			patccount = 15+patccount;
		}

		int pmtccount = patccount+1;
		if (pmtccount > 15)
		{
			pmtccount = 0;
		}

		char buff[188] = {0};
		memcpy(buff, m_PAT, 188);
		TSPACKET_SETCOUNT(*(TSPacket_t*)buff, patccount);
		WriteFileData(buff, 188);

		memcpy(buff, m_PMT, 188);
		TSPACKET_SETCOUNT(*(TSPacket_t*)buff, pmtccount);
		WriteFileData(buff, 188);
	}

	return m_CurFD > 0;
}
/*******************************************************************************
 Fuction name:MallocBuff
 Description :分配内存
 Input   para:
 Output  para:
 Call fuction:      
 Autor       :
 date  time  :2012.5.5
*******************************************************************************/
SInt32 CHLSRecord::MallocBuff()
{
	if (m_WriteBuff == NULL)
	{
#ifndef WIN32		
		int ret = posix_memalign((void**)&m_WriteBuff, 512, m_WriteBuffSize);
		if (ret != 0)
		{
			printf("posix_memalign() fail. result:%d\n ",ret);
		}
#else
		m_WriteBuff = (UChar*)malloc(m_WriteBuffSize);
#endif
	}

	return m_WriteBuff == NULL?-1:0;
}




