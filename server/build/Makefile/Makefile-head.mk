#定义目标文件
ifeq ($(DEBUG), -g)
	TARGET = ${MNAME}d${MEXTNAME}
else 
  TARGET = ${MNAME}${MEXTNAME}
endif

#定义编译器
C++PLUS=${CXX}
CC=${CC}
AR=${AR}

#定义编译宏
ifeq ($(DEBUG), -g)
	CFLAGS  += -DDEBUG
endif

#编译32还是64
CFLAGS += $(HOST_TAG)
CFLAGS += -fPIC

#定义头文件路径
INC += -I../../include/inc
INC += -I../../include/inc3
INC += -I.

#库存放路径
LIBPATH=../../lib

#定义编译依赖库路径
LINKS=-L${LIBPATH}

#编译可执行文件
BUILD_EXECUTABLE=Makefile-executable.mk

#定义编译动态库
BUILD_SHARED_LIBRARY=build-shared-library.mk

#定义编译静态库
BUILD_STATIC_LIBRARY=build-static-library.mk


