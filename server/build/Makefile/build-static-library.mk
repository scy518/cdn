##################################################################################
# 
# 
# 通用静态编译输出
#
# 
##################################################################################
# patsubst把$(SRC)中的符合后缀是.cpp的全部替换成.o
OBJS = $(patsubst %.cpp,%.o, $(patsubst %.c,%.o,$(SRCS)))

#连接
$(TARGET): $(OBJS)
	$(AR) -r $(TARGET) $(OBJS)
	mv $(TARGET) $(LIBPATH)

#编译
%.o : %.cpp
	$(C++PLUS) $(DEBUG) $(CFLAGS) -c $< $(INC) -o $@
	
%.o : %.c
	$(CC) $(DEBUG) $(CFLAGS) -c $< $(INC) -o $@
	
%.o : %.cxx
	$(CC) $(DEBUG) $(CFLAGS) -c $< $(INC) -o $@



