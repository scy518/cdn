#ifndef LIB_HLS_RTI_DLL_H
#define LIB_HLS_RTI_DLL_H

#ifdef  WIN32
	#ifdef LIBHLSRTI_STATIC
		#define LIBHLSRTI_EXPORT
	#else
		#ifdef LIBHLSRTI_EXPORTS
			#define LIBHLSRTI_EXPORT __declspec(dllexport)
		#else
			#define LIBHLSRTI_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBHLSRTI_EXPORT
#endif


#define  LIBHLSRTI_API  extern "C" LIBHLSRTI_EXPORT

#endif

