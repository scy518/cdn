#ifndef LIB_RTM_DLL_H
#define LIB_RTM_DLL_H

#ifdef  WIN32
	#ifdef LIBRTM_STATIC
		#define LIBRTM_EXPORT
	#else
		#ifdef LIBRTM_EXPORTS
			#define LIBRTM_EXPORT __declspec(dllexport)
		#else
			#define LIBRTM_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBRTM_EXPORT
#endif


#define  LIBRTM_API  extern "C" LIBRTM_EXPORT

#endif

