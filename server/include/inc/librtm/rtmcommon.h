#ifndef RTM_COMMON_H
#define RTM_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    rticommon.h
*  Description:  rti公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/cstring.h"
#include "libcdnutil/cdncommon.h"
#include "librtm/librtmmacros.h"
#include <list>
#include <map>

typedef struct LiveChannelItem_Struct
{
	SInt32          m_ChannelID;
	std::string     m_ChannelName;
	std::string     m_NodeID;
	std::string     m_ProviderID;
	std::string     m_AssetID;
	std::string     m_SourceUrl;
	std::string     m_SourceType;   //定义源类型,hls 类型源， udp型源
	SInt32          m_BiteRate;
	SInt32 		    m_SliceDur;
	SInt32          m_RecordeDur;
	std::string		m_StorePath;
	SInt32          m_IsEnable;

	LiveChannelItem_Struct()
	{
		m_ChannelID        = 0;
		m_ChannelName      = "";
		m_NodeID           = "";
		m_ProviderID       = "";
		m_AssetID          = "";
		m_SourceUrl        = "";
		m_SourceType       = "";
		m_BiteRate         = 0;
		m_SliceDur         = 0;
		m_RecordeDur       = 0;
		m_StorePath        = "";
		m_IsEnable         = 0;
	}

	LiveChannelItem_Struct(const LiveChannelItem_Struct &other)
	{
		m_ChannelID       = other.m_ChannelID;
		m_ChannelName     = other.m_ChannelName;
		m_NodeID          = other.m_NodeID;
		m_ProviderID      = other.m_ProviderID;
		m_AssetID         = other.m_AssetID;
		m_SourceUrl       = other.m_SourceUrl;
		m_SourceType      = other.m_SourceType;
		m_BiteRate        = other.m_BiteRate;
		m_SliceDur        = other.m_SliceDur;
		m_RecordeDur      = other.m_RecordeDur;
		m_StorePath       = other.m_StorePath;
		m_IsEnable        = other.m_IsEnable;
	}

	LiveChannelItem_Struct& operator=(const LiveChannelItem_Struct &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ChannelID       = other.m_ChannelID;
		m_ChannelName     = other.m_ChannelName;
		m_NodeID          = other.m_NodeID;
		m_ProviderID      = other.m_ProviderID;
		m_AssetID         = other.m_AssetID;
		m_SourceUrl       = other.m_SourceUrl;
		m_SourceType      = other.m_SourceType;
		m_BiteRate        = other.m_BiteRate;
		m_SliceDur        = other.m_SliceDur;
		m_RecordeDur      = other.m_RecordeDur;
		m_StorePath       = other.m_StorePath;
		m_IsEnable        = other.m_IsEnable;

		return *this;
	}
}LiveChannelItem;

typedef std::list<LiveChannelItem >           LiveChannelList;
typedef std::map<SInt64, LiveChannelItem >    LiveChannelMap;



#endif


