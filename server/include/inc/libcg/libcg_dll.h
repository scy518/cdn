#ifndef LIB_CG_DLL_H
#define LIB_CG_DLL_H

#ifdef  WIN32
	#ifdef LIBCG_STATIC
		#define LIBCG_EXPORT
	#else
		#ifdef LIBCG_EXPORTS
			#define LIBCG_EXPORT __declspec(dllexport)
		#else
			#define LIBCG_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCG_EXPORT
#endif


#define  LIBCG_API  extern "C" LIBCG_EXPORT

#endif

