#ifndef LIB_CG_MACROS_H
#define LIB_CG_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcgmacros.h
*  Description:   cg模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define PARENT_CLS_ADDRESS         "ParentClsAddress"
#define CLS_ADDRESS                "ClsAddress"
#define PRIVATE_STREAM_ADDRESS     "PrivateStreamAddress"
#define PUBLIC_STREAM_ADDRESS      "PublicStreamAddress"
#define NODE_NAME                  "NodeName"
#define AREA_CODE                  "AreaCode"
#define PRIORITY                   "Priority"
#define CG_SCAN_PATH               "CGScanPath"
#define LOAD_SCAN_PATH_INTERVAL    "LoadScanPathInterval"   
#define ALL_SCAN_INTERVAL          "AllScanInterval"
#define SCAN_MODE                  "ScanMode"
#define ALL_SCAN_FIXTIME           "AllScanFixTime"
#define REPORT_FILE_NUM            "ReportFileNum"

#define CLS_HEARTBEAT_TIME         "ClsHeartbeatTime"

#define PROXY_THREAD_NUM           "ProxyThreadNum"

//是否长连接
#define IS_KEEP_LIVE               "IsKeepLive"

//cg缓存目录
#define CG_CACHE_DIR               "CGCacheDir"

//磁盘剩余百分比
#define CG_CACHE_SPACE_SCALE       "CGCacheSpaceScale"

/*******************************************************************************
*                              定义常量
*******************************************************************************/
//长连接
#define HTTP_NO_KEEP_LIVE           0

//默认宏定义
#define  DEFAULT_ALL_SCAN_INTERVAL         90000    //s

//全量扫描方式
#define  SCAN_MODE_TIMING                     1 
#define  SCAN_MODE_FIXED_TIME                 2

#define CMD_DELETE_FILE                       "DeleteFile"
 

#endif



