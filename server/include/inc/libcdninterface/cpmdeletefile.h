#ifndef CPM_DELETE_FILE_H
#define CPM_DELETE_FILE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cpmdeletefile.h
*  Description:  cpm删除文件
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcore/cstring.h"

class LIBCDNINTERFACE_EXPORT CCPMDeleteFileReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCPMDeleteFileReq)

	//定义providerID
    HTTP_JSON_CHAR_DEFINE(providerID, 64)

	//定义assetID
	HTTP_JSON_CHAR_DEFINE(assetID, 64)

	//定义contentType
	HTTP_JSON_INT32_DEFINE(contentType)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCPMDeleteFileRsp;

#endif

