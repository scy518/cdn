#ifndef CDN_IF_COMMON_H
#define CDN_IF_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cdnifcommon.h
*  Description:  cdn 接口公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcdnutil/cdncommon.h"
#include <list>
/*******************************************************************************
*                              公共结构
*******************************************************************************/
//cpm注入数据结构
typedef struct CPMIngestResult_struct
{
	char              m_ProviderID[64];
	char              m_AssetID[64];
	Int32             m_ContentType;
	Int32             m_Status;       //注入状态

	CPMIngestResult_struct()
	{
		memset(m_ProviderID, 0, sizeof(m_ProviderID));
		memset(m_AssetID, 0, sizeof(m_AssetID));
		m_ContentType = 0;
		m_Status = TRANF_STATUS_WAITING;
	}

	CPMIngestResult_struct(const CPMIngestResult_struct &other)
	{
		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		m_ContentType = other.m_ContentType;
		m_Status = other.m_Status;
	}

	CPMIngestResult_struct &operator =(const CPMIngestResult_struct &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		m_ContentType = other.m_ContentType;
		m_Status = other.m_Status;

		return *this;
	}
}CPMIngestResult;
typedef std::list<CPMIngestResult>           CPMIngestResultList;

//cl注入数据结构
typedef struct CLIngestResult_struct
{
	char                   m_FileName[64]; //定义文件名
	Int32                  m_Status;       //注入状态
	Int32                  m_Result;       //注入结果

	CLIngestResult_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		m_Status = TRANF_STATUS_WAITING;
		m_Result = 0;
	}

	CLIngestResult_struct(const CLIngestResult_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		m_Status = other.m_Status;
		m_Result = other.m_Result;
	}

	CLIngestResult_struct &operator =(const CLIngestResult_struct &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		m_Status = other.m_Status;
		m_Result = other.m_Result;

		return *this;
	}
}CLIngestResult;
typedef std::list<CLIngestResult>           CLIngestResultList;
/*******************************************************************************
*                              CI结构
*******************************************************************************/
//查询状态结构
typedef struct CIGetStatusResult_Stru
{
	char              m_ProviderID[64];
	char              m_AssetID[64];
	Int32             m_ServiceType;
	Int32             m_Status;
	Int32             m_Progress;
	char              m_CreateTime[32];
	char              m_StartTime[32];
	char              m_EndTime[32];
	Int32             m_Duration ;
	Int32             m_AvgBitRate;
	Int32             m_ContentSize;
	char              m_Md5CheckSum[64];

	CIGetStatusResult_Stru()
	{
		memset(m_ProviderID, 0, sizeof(m_ProviderID));
		memset(m_AssetID, 0, sizeof(m_AssetID));
		memset(m_CreateTime, 0, sizeof(m_CreateTime));
		memset(m_StartTime, 0, sizeof(m_StartTime));
		memset(m_EndTime, 0, sizeof(m_EndTime));
		m_ServiceType = 0;
		m_Status      = 0;
		m_Progress    = 0;
		m_Duration    = 0;
		m_AvgBitRate  = 0;
		m_ContentSize = 0;
		memset(m_Md5CheckSum, 0, sizeof(m_Md5CheckSum));
	}

	CIGetStatusResult_Stru(const CIGetStatusResult_Stru &other)
	{
		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		memcpy(m_CreateTime, other.m_CreateTime, sizeof(other.m_CreateTime));
		memcpy(m_StartTime, other.m_StartTime, sizeof(other.m_StartTime));
		memcpy(m_EndTime, other.m_EndTime, sizeof(other.m_EndTime));

		m_ServiceType     = other.m_ServiceType;
		m_Status          = other.m_Status;
		m_Progress        = other.m_Progress;
		m_Duration        = other.m_Duration;
		m_AvgBitRate      = other.m_AvgBitRate;
		m_ContentSize     = other.m_ContentSize;
		memcpy(m_Md5CheckSum, other.m_Md5CheckSum, sizeof(other.m_Md5CheckSum));
	}

	CIGetStatusResult_Stru &operator =(const CIGetStatusResult_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		memcpy(m_CreateTime, other.m_CreateTime, sizeof(other.m_CreateTime));
		memcpy(m_StartTime, other.m_StartTime, sizeof(other.m_StartTime));
		memcpy(m_EndTime, other.m_EndTime, sizeof(other.m_EndTime));

		m_ServiceType     = other.m_ServiceType;
		m_Status          = other.m_Status;
		m_Progress        = other.m_Progress;
		m_Duration        = other.m_Duration;
		m_AvgBitRate      = other.m_AvgBitRate;
		m_ContentSize     = other.m_ContentSize;
		memcpy(m_Md5CheckSum, other.m_Md5CheckSum, sizeof(other.m_Md5CheckSum));

		return *this;
	}

}CIGetStatusResult;

//编码信息
typedef struct CodeInfo_Stru
{
	Int32        m_BitRate;           //编码码率
	char         m_Code[32];          //编码格式h.264等
	char         m_Format[32];        //封装格式

	CodeInfo_Stru()
	{
		memset(m_Code, 0, sizeof(m_Code));
		memset(m_Format, 0, sizeof(m_Format));
		m_BitRate = 0;
	}

	CodeInfo_Stru(const CodeInfo_Stru &other)
	{
		memcpy(m_Code, other.m_Code, sizeof(other.m_Code));
		memcpy(m_Format, other.m_Format, sizeof(other.m_Format));
		m_BitRate     = other.m_BitRate;
	}

	CodeInfo_Stru &operator =(const CodeInfo_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_Code, other.m_Code, sizeof(other.m_Code));
		memcpy(m_Format, other.m_Format, sizeof(other.m_Format));
		m_BitRate     = other.m_BitRate;

		return *this;
	}
}CodeInfo;
typedef std::list<CodeInfo>         CodeInfoList;
/*******************************************************************************
*                              CL结构
*******************************************************************************/
//注入数据结构
typedef struct IngestFileInf_struct
{
	char         m_FileName[64];      //定义文件名
	char         m_SourceUrl[512];      //定义文件路径

	IngestFileInf_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		memset(m_SourceUrl, 0, sizeof(m_SourceUrl));
	}

	IngestFileInf_struct(const IngestFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_SourceUrl, other.m_SourceUrl, sizeof(other.m_SourceUrl));
	}

	IngestFileInf_struct &operator =(const IngestFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_SourceUrl, other.m_SourceUrl, sizeof(other.m_SourceUrl));

		return *this;
	}
}IngestFileInf;

//CL信息结构
typedef struct CLInfoResult_struct
{
	char         m_NodeID[64];           //节点名
	Int64        m_StorageSpace;         //存储大小，单位为M
	Int64        m_FreeStorageSpace;     //空闲存储大小，单位为M

	CLInfoResult_struct()
	{
		memset(m_NodeID, 0, sizeof(m_NodeID));
		m_StorageSpace = 0;
		m_FreeStorageSpace = 0;
	}

	CLInfoResult_struct(const CLInfoResult_struct &other)
	{
		memcpy(m_NodeID, other.m_NodeID, sizeof(other.m_NodeID));
		m_StorageSpace = other.m_StorageSpace;
		m_FreeStorageSpace = other.m_FreeStorageSpace;
	}

	CLInfoResult_struct &operator =(const CLInfoResult_struct &other)
	{
		if (this == &other)
		{
			return *this;
		}

		memcpy(m_NodeID, other.m_NodeID, sizeof(other.m_NodeID));
		m_StorageSpace = other.m_StorageSpace;
		m_FreeStorageSpace = other.m_FreeStorageSpace;

		return *this;
	}
}CLInfoResult;
/*******************************************************************************
*                              CLS结构
*******************************************************************************/
//添加文件
typedef struct AddFileInf_struct
{
	char         m_FileName[64];       //定义文件名
	char         m_FilePath[128];      //定义文件路径
	Int64        m_FileSize;          //定义文件大小
	Int32        m_IsOpen;            //定义文件是否打开

	AddFileInf_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		memset(m_FilePath, 0, sizeof(m_FilePath));
		m_FileSize = 0;
		m_IsOpen   = 0;
	}

	AddFileInf_struct(const AddFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_FilePath, other.m_FilePath, sizeof(other.m_FilePath));
		m_FileSize     = other.m_FileSize;
		m_IsOpen       = other.m_IsOpen;
	}
	AddFileInf_struct &operator =(const AddFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_FilePath, other.m_FilePath, sizeof(other.m_FilePath));
		m_FileSize     = other.m_FileSize;
		m_IsOpen       = other.m_IsOpen;

		return *this;
	}
}AddFileInf;
typedef std::list<AddFileInf>         AddFileInfList;

//删除文件结构
typedef struct DelFileInf_struct
{
	char         m_FileName[64];      //定义文件名
	char         m_FilePath[128];      //定义文件路径

	DelFileInf_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		memset(m_FilePath, 0, sizeof(m_FilePath));
	}

	DelFileInf_struct(const DelFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_FilePath, other.m_FilePath, sizeof(other.m_FilePath));
	}
	DelFileInf_struct &operator =(const DelFileInf_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_FilePath, other.m_FilePath, sizeof(other.m_FilePath));

		return *this;
	}
}DelFileInf;
typedef std::list<DelFileInf>         DelFileInfList;
/*******************************************************************************
*                              CPM结构
*******************************************************************************/
//注入数据结构
typedef struct CPMIngestFileInfo_struct
{
	char         m_FileName[64];      //定义文件名
	char         m_SourceUrl[512];    //定义文件路径
	Int64        m_ContentSize;
	Int32        m_BitRate;
	Int64        m_Duration;

	CPMIngestFileInfo_struct()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		memset(m_SourceUrl, 0, sizeof(m_SourceUrl));
		m_ContentSize = 0;
		m_BitRate     = 0;
		m_Duration    = 0;
	}

	CPMIngestFileInfo_struct(const CPMIngestFileInfo_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_SourceUrl, other.m_SourceUrl, sizeof(other.m_SourceUrl));
		m_ContentSize = other.m_ContentSize;
		m_BitRate     = other.m_BitRate;
		m_Duration    = other.m_Duration;
	}

	CPMIngestFileInfo_struct &operator =(const CPMIngestFileInfo_struct &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_SourceUrl, other.m_SourceUrl, sizeof(other.m_SourceUrl));
		m_ContentSize = other.m_ContentSize;
		m_BitRate     = other.m_BitRate;
		m_Duration    = other.m_Duration;

		return *this;
	}
}CPMIngestFileInfo;
typedef std::list<CPMIngestFileInfo>         CPMIngestFileInfoList;

#endif











