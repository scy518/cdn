#ifndef CLS_DELETE_FILE_H
#define CLS_DELETE_FILE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clsdeletefile.h
*  Description:  cls删除文件上报接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libhttpinterface/httpjsonmessage.h"
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include "libcdninterface/libcdninterface_dll.h"

class LIBCDNINTERFACE_EXPORT CCLSDeleteFileReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLSDeleteFileReq)

	//节点名称
	HTTP_JSON_CHAR_DEFINE(NodeName, 64)

	//文件列表
	HTTP_JSON_LIST_DEFINE(DelFileInf,  FileList)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//构造json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCLSDeleteFileRsp;

#endif

