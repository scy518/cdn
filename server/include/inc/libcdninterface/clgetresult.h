#ifndef CL_GET_RESULT_H
#define CL_GET_RESULT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clgetresult.h
*  Description:  查询文件注入状态
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"
#include "libcore/cstring.h"

#include <list>

//请求消息
class LIBCDNINTERFACE_EXPORT CCLGetResultReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLGetResultReq)

	//文件列表
	HTTP_JSON_LIST_DEFINE(LibCore::CString,  FileList)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//响应消息
class LIBCDNINTERFACE_EXPORT CCLGetResultRsp : public CHttpJsonMessageRsp
{
	HTTP_JSON_CLASS_DEFINE(CCLGetResultRsp)

	//文件列表
	HTTP_JSON_LIST_DEFINE(CLIngestResult,  ResultList)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};


#endif


