#ifndef AIS_DELETE_CONTENT_H
#define AIS_DELETE_CONTENT_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    aisdeletecontent.h
*  Description:  删除内容接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libcdninterface/cdncmddefine.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include <string.h>

class LIBCDNINTERFACE_EXPORT CAISDeleteContentReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CAISDeleteContentReq)

	//定义providerID
    HTTP_JSON_CHAR_DEFINE(providerID, 64)

	//定义assetID
	HTTP_JSON_CHAR_DEFINE(assetID, 64)

	//定义内容类型
	HTTP_JSON_INT32_DEFINE(serviceType)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CAISDeleteContentRsp;

#endif

