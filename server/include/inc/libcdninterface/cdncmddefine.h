#ifndef CDN_CMD_DEFINE
#define CDN_CMD_DEFINE
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ccdncmddefine.h
*  Description:  定义接口对应的命名子宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
/*******************************************************************************
*                              CLS支持命令宏
*******************************************************************************/
#define CMD_CLS_ADD_FILE         "/CLSAddFile"
#define CMD_CLS_DELETE_FILE      "/CLSDeleteFile"
#define CMD_CLS_UPDATE_FILE      "/CLSUpdateFile"
#define CMD_CLS_HEART_BEAT       "/CLSHeartbeat"
#define CMD_CLS_REGISTER         "/CLSRegister"
/*******************************************************************************
*                              CL支持命令宏
*******************************************************************************/
#define CMD_CL_INGEST_FILE         "/CLIngestFile"
#define CMD_CL_DELETE_FILE         "/CLDeleteFile"
#define CMD_CL_GET_RESULT          "/CLGetResult"
#define CMD_CL_QUERY_CL_INFO       "/CLQueryCLInfo"
/*******************************************************************************
*                              CPM支持命令宏
*******************************************************************************/
#define CMD_CPM_INGEST_FILE         "/CPMIngestFile"
#define CMD_CPM_DELETE_FILE         "/CPMDeleteFile"
#define CMD_CPM_GET_RESULT          "/CPMGetResult"
/*******************************************************************************
*                              AIS支持命令宏
*******************************************************************************/
#define CMD_AIS_TRANSFER_CONTENT         "/JTransferContent"
#define CMD_AIS_DELETE_CONTENT           "/JDeleteContent"
#define CMD_AIS_GET_TRANSFER_STATUS      "/JGetTransferStatus"
#define CMD_AIS_RTRANSFER_STATUS         "/JRTransferStatus"
/*******************************************************************************
*                              AIS支持ngod命令宏
*******************************************************************************/
#define CMD_AIS_NGOD_TRANSFER_CONTENT        "/TransferContent"
#define CMD_AIS_NGOD_DELETE_CONTENT          "/DeleteContent"
#define CMD_AIS_NGOD_GET_TRANSFER_STATUS     "/GetTransferStatus"


#endif


