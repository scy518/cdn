#ifndef CL_INGEST_FILE_H
#define CL_INGEST_FILE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clingestfile.h
*  Description:  文件注入接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdninterface/libcdninterface_dll.h"
#include "libhttpinterface/httpjsonmessage.h" 
#include "libcdninterface/cdncmddefine.h"
#include "libcdninterface/cdnifcommon.h"

class LIBCDNINTERFACE_EXPORT CCLIngestFileReq : public CHttpJsonMessageReq
{
	HTTP_JSON_CLASS_DEFINE(CCLIngestFileReq)

	//文件列表
	HTTP_JSON_LIST_DEFINE(IngestFileInf,  FileList)

protected:

	//构造json代码
	virtual Int32 EncodeJson(cJSON *json);

	//解码json代码
	virtual Int32 DecodeJson(cJSON *json);
};

//使用默认返回
typedef CHttpJsonMessageRsp   CCLIngestFileRsp;

#endif


