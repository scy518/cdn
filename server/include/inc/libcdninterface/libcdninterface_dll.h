#ifndef LIB_CDN_INTERFACE_DLL_H
#define LIB_CDN_INTERFACE_DLL_H

#ifdef  WIN32
	#ifdef LIBCDNINTERFACE_STATIC
		#define LIBCDNINTERFACE_EXPORT
	#else
		#ifdef LIBCDNINTERFACE_EXPORTS
			#define LIBCDNINTERFACE_EXPORT __declspec(dllexport)
		#else
			#define LIBCDNINTERFACE_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCDNINTERFACE_EXPORT
#endif


#define  LIBCDNINTERFACE_API  extern "C" LIBCDNINTERFACE_EXPORT

#endif

