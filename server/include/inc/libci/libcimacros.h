#ifndef LIB_CI_MACROS_H
#define LIB_CI_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libcimacros.h
*  Description:   ci模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/libcdnutilmacros.h"

#define LIB_CI_ID        "libci"
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define CI_NODE_ID                                 "CINodeID"
#define CI_DOWN_THREAD_NUM                         "CIDownThreadNum"
#define CI_SHELL_CMD                               "CIShellCmd"
#define CI_HLS_SLICE_DURATION                      "CIHlsSliceDuration"
#define CI_TRY_PLAY_DURATION                       "CITryPlayDuration"
#define CI_SQL_STATEMENT_FILE                      "CISqlStatementFile"
#define CI_PRIVATE_STREAM_ADDR                     "CIPrivateStreamAddr"
#define CI_PUBLIC_STREAM_ADDR                      "CIPublicStreamAddr"

//文件扫描
#define CI_ALL_SCAN_INTERVAL                       "CIAllScanInterval"
#define CI_IS_SCAN_ENABLE                          "CIIsScanEnable"
#define CI_SCAN_FILTER_FILE_EXTNAME                "CIScanFilterFileExtName"
#define CI_SCAN_PATH                               "CIScanPath"
#define CI_SCAN_MODE                               "CIScanMode"

#define CI_DISK_CONF_FILE                          "CIDiskConfFile"
#define CI_NETWORK_CARD_CONF_FILE                  "CINetWorkCard"
#define CI_IS_ENABLE_REDIS                         "CIIsEnableRedis"
#define CI_ASY_REDIS_INTERVAL                      "CIAsyRedisInterval"

#define CI_DEFAULT_PROVIDERID                      "CIDefaultProviderID"

//ngod参数配置
#define CI_OBJVERSION                               "CIObjVersion"
#define CI_FRFILESIZEMINUS1                         "CIFrFilesizeMinus1"
#define CI_ONLYWRITEIDR                             "CIOnlyWriteIDR"
#define CI_NGODINDEXSCALE                           "CINgodIndexScale"
/*******************************************************************************
*                              定义常量
*******************************************************************************/
#define CI_HTTP_SERVER                   "CI Server"

//默认宏定义
#define  DEFAULT_ALL_SCAN_INTERVAL         90000    //s

//全量扫描方式
#define  SCAN_MODE_TIMING                     1     //全量扫描
#define  SCAN_MODE_FIXED_TIME                 2     //增量扫描

#define TASK_IDLE                             0     //任务空闲
#define TASK_BUSY                             1

#define CANCEL_TASK                           1     //取消任务

#define DISENABLE_REDIS_CACHE                 0     //禁止redis缓存
#define ENABLE_REDIS_CACHE                    1     //启动redis缓存

/*****************************ngod参数配置*********************************/
//ngod索引版本号
#define OBJ_VER_I01     1
#define OBJ_VER_I03     2

#endif



