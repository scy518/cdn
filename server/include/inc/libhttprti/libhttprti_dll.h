#ifndef LIB_HTTP_RTI_DLL_H
#define LIB_HTTP_RTI_DLL_H

#ifdef  WIN32
	#ifdef LIBHTTPRTI_STATIC
		#define LIBHTTPRTI_EXPORT
	#else
		#ifdef LIBHTTPRTI_EXPORTS
			#define LIBHTTPRTI_EXPORT __declspec(dllexport)
		#else
			#define LIBHTTPRTI_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBHTTPRTI_EXPORT
#endif


#define  LIBHTTPRTI_API  extern "C" LIBHTTPRTI_EXPORT

#endif

