#ifndef LIB_HTTP_RTI_MACROS_H
#define LIB_HTTP_RTI_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libhlsrtimacros.h
*  Description:   hlsrti模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/

/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define HTTP_RTI_RECONNECT_TIME                "HttpRtiReconnectTime"
/*******************************************************************************
*                              常量宏
*******************************************************************************/
#define DEFAULT_RECONNECT_TIME                  10


#endif
