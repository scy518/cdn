#ifndef CDN_ERROR_CODE_H
#define CDN_ERROR_CODE_H


/******************************************************************************/
/*********************************CDN_ERR_BASE*********************************/
/******************************************************************************/
#define CDN_ERR_BASE                                            1000
#define CDN_ERR_SUCCESS                                         0
#define CDN_ERR_NO_CONTENT                                      CDN_ERR_BASE+1
#define CDN_ERR_TIME_OUT                                        CDN_ERR_BASE+2
#define CDN_ERR_DOWN_LOAD_FAIL                                  CDN_ERR_BASE+3
#define CDN_ERR_NO_SUPPORT_CONTENT                              CDN_ERR_BASE+4
#define CDN_ERR_REQUEST_FAIL                                    CDN_ERR_BASE+5
#define CDN_ERR_FILE_EXIST                                      CDN_ERR_BASE+6
#define CDN_ERR_FILE_NO_EXIST                                   CDN_ERR_BASE+7
#define CDN_ERR_PARAME_ERROR                                    CDN_ERR_BASE+8
#define CDN_ERR_DEL_FILE_FAIL                                   CDN_ERR_BASE+9
#define CDN_ERR_SERVER_FAIL                                     CDN_ERR_BASE+10
#define CDN_ERR_CI_TASK_CANCEL                                  CDN_ERR_BASE+11

#endif
