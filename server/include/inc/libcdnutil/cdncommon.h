#ifndef CDN_COMMON_H
#define CDN_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    cdncommon.h
*  Description:  cdn公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include <string.h>

//注入内容类型
typedef enum ServiceType_enum
{
	SERVICE_TYPE_FILE        = 0,    //普通文件服务类型

	SERVICE_TYPE_TXT         = 1,    //文本文件类型

	SERVICE_TYPE_IMAGE       = 2,    //图片文件类型

	SERVICE_TYPE_HLS         = 3,     //hls协议视频,会生成m3u8

	SERVICE_TYPE_MP4         = 4,    //mp4文件类型

	SERVICE_TYPE_NGOD        = 5,      //ngod协议

	SERVICE_TYPE_NGOD_HLS    = 6       //ngod+hls协议
}ServiceType;

//注入状态
typedef enum TranfContentStatus_enum
{
	TRANF_STATUS_WAITING        = 0,    //注入等待状态

	TRANF_STATUS_TRANSFERRING   = 1,    //正在注入

	TRANF_STATUS_COMPLETE       = 2     //注入完成
}TranfContentStatus;

//内容基本参数
typedef struct ContentKey_Stru
{
	std::string         m_ProviderID;
	std::string         m_AssetID;
	ServiceType         m_ServiceType;

	ContentKey_Stru()
	{
		m_ProviderID     = "";
		m_AssetID        = "";
		m_ServiceType    = SERVICE_TYPE_FILE;
	}

	ContentKey_Stru(const ContentKey_Stru &other)
	{
		m_ProviderID     = other.m_ProviderID;
		m_AssetID        = other.m_AssetID;
		m_ServiceType    = other.m_ServiceType;
	}

	ContentKey_Stru &operator= (const ContentKey_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ProviderID     = other.m_ProviderID;
		m_AssetID        = other.m_AssetID;
		m_ServiceType    = other.m_ServiceType;

		return *this;
	}
}ContentKey;

//编码信息结构
typedef struct CodeInfoItem_Stru
{
	Int32               m_BitRate;
	std::string         m_Code;
	std::string         m_Formate;

	CodeInfoItem_Stru()
	{
		m_BitRate = 0;
		m_Code    = "";
		m_Formate = "";
	}

	CodeInfoItem_Stru(const CodeInfoItem_Stru &other)
	{
		m_BitRate = other.m_BitRate;
		m_Code    = other.m_Code;
		m_Formate = other.m_Formate;
	}

	CodeInfoItem_Stru &operator =(const CodeInfoItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_BitRate = other.m_BitRate;
		m_Code    = other.m_Code;
		m_Formate = other.m_Formate;

		return *this;
	}
}CodeInfoItem;
typedef std::list<CodeInfoItem >  CodeInfoItemList;

//注入消息结构
typedef struct IngestTaskItem_Stru
{
	SInt64              m_TaskID;
	std::string         m_NodeID;
	SInt32              m_Cmd;    //注入类型  0:注入 1: 取消 2：删除
	ContentKey          m_ContentKey;
	std::string         m_FileName;
	std::string         m_Path;
	std::string         m_SourceURL;
	Int64               m_FileSize;
	Int64               m_Duration;
	Int32               m_BiteRate;
	Int32               m_Status;
	Int32               m_Progress;
	Int64               m_CreateTime;
	Int64               m_StartTime;
	Int64               m_EndTime;
	CodeInfoItem        m_CodeInfoItem;
	Int32               m_ErrCode;
	std::string         m_ErrDes;
	std::string         m_ReportURL;
	std::string         m_FileMD5;
	Int32               m_SpliceDur;

	IngestTaskItem_Stru()
	{
		m_TaskID         = 0;
		m_Cmd            = 0;
		m_NodeID         = "";
		m_FileName       = "";
		m_Path           = "";
		m_SourceURL      = "";
		m_FileSize       = 0;    
		m_Duration       = 0;
		m_BiteRate       = 0;
		m_Status         = TRANF_STATUS_WAITING;
		m_Progress       = 0;
		m_CreateTime     = 0;
		m_StartTime      = 0;
		m_EndTime        = 0;
		m_ErrCode        = 0;
		m_ErrDes         = "Success";
		m_ReportURL      = "";
		m_FileMD5        = "";
		m_SpliceDur      = 10;
	}

	IngestTaskItem_Stru(const IngestTaskItem_Stru &other)
	{
		m_TaskID         = other.m_TaskID;
		m_NodeID         = other.m_NodeID;
		m_ContentKey     = other.m_ContentKey;
		m_FileName       = other.m_FileName;
		m_Path           = other.m_Path;
		m_Cmd            = other.m_Cmd;
		m_SourceURL      = other.m_SourceURL;
		m_FileSize       = other.m_FileSize;    
		m_Duration       = other.m_Duration;
		m_BiteRate       = other.m_BiteRate;
		m_Status         = other.m_Status;
		m_Progress       = other.m_Progress;
		m_CreateTime     = other.m_CreateTime;
		m_StartTime      = other.m_StartTime;
		m_EndTime        = other.m_EndTime;
		m_CodeInfoItem   = other.m_CodeInfoItem;
		m_ErrCode        = other.m_ErrCode;
		m_ErrDes         = other.m_ErrDes;
		m_ReportURL      = other.m_ReportURL;
		m_FileMD5        = other.m_FileMD5;
		m_SpliceDur      = other.m_SpliceDur;
	}

	IngestTaskItem_Stru &operator= (const IngestTaskItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_TaskID         = other.m_TaskID;
		m_NodeID         = other.m_NodeID;
		m_Cmd            = other.m_Cmd;
		m_ContentKey     = other.m_ContentKey;
		m_FileName       = other.m_FileName;
		m_Path           = other.m_Path;
		m_SourceURL      = other.m_SourceURL;
		m_FileSize       = other.m_FileSize;    
		m_Duration       = other.m_Duration;
		m_BiteRate       = other.m_BiteRate;
		m_Status         = other.m_Status;
		m_Progress       = other.m_Progress;
		m_CreateTime     = other.m_CreateTime;
		m_StartTime      = other.m_StartTime;
		m_EndTime        = other.m_EndTime;
		m_CodeInfoItem   = other.m_CodeInfoItem;
		m_ErrCode        = other.m_ErrCode;
		m_ErrDes         = other.m_ErrDes;
		m_ReportURL      = other.m_ReportURL;
		m_FileMD5        = other.m_FileMD5;
		m_SpliceDur      = other.m_SpliceDur;

		return *this;
	}
}IngestTaskItem;
typedef std::list<IngestTaskItem >  IngestTaskItemList;

//定义节点信息
typedef struct  CGNode_Itme_Stru
{
	char        m_NodeName[32];        //节点名称
	char        m_PrivateStreamAddress[128]; //私网推流地址
	char        m_PublicStreamAddress[128];  //公网推流地址
	UInt32      m_TotalBand;           //最大带宽,单位为Kb/s
	UInt32      m_FreeBand;            //剩余空闲带宽,单位为Kb/s
	char        m_AreaCode[32];        //区域码
	Int32       m_Priority;            //优先级
	UInt64      m_HeartbeatTime;       //心跳时间

	CGNode_Itme_Stru()
	{
		memset(m_NodeName, 0, sizeof(m_NodeName));
		memset(m_PrivateStreamAddress, 0, sizeof(m_PrivateStreamAddress));
		memset(m_PublicStreamAddress, 0, sizeof(m_PublicStreamAddress));
		m_TotalBand        = 0;
		m_FreeBand         = 0;
		memset(m_AreaCode, 0, sizeof(m_AreaCode));
		m_Priority         = 0;
		m_HeartbeatTime    = 0;
	}

	CGNode_Itme_Stru(const CGNode_Itme_Stru &other)
	{
		memcpy(m_NodeName, other.m_NodeName, sizeof(other.m_NodeName));
		memcpy(m_PrivateStreamAddress, other.m_PrivateStreamAddress, sizeof(other.m_PrivateStreamAddress));
		memcpy(m_PublicStreamAddress, other.m_PublicStreamAddress, sizeof(other.m_PublicStreamAddress));
		m_TotalBand        = other.m_TotalBand;
		m_FreeBand         = other.m_FreeBand;
		memcpy(m_AreaCode, other.m_AreaCode, sizeof(other.m_AreaCode));
		m_Priority         = other.m_Priority;
		m_HeartbeatTime    = other.m_HeartbeatTime;
	}

	CGNode_Itme_Stru& operator = (const CGNode_Itme_Stru &other)
	{
		memcpy(m_NodeName, other.m_NodeName, sizeof(other.m_NodeName));
		memcpy(m_PrivateStreamAddress, other.m_PrivateStreamAddress, sizeof(other.m_PrivateStreamAddress));
		memcpy(m_PublicStreamAddress, other.m_PublicStreamAddress, sizeof(other.m_PublicStreamAddress));
		m_TotalBand        = other.m_TotalBand;
		m_FreeBand         = other.m_FreeBand;
		memcpy(m_AreaCode, other.m_AreaCode, sizeof(other.m_AreaCode));
		m_Priority         = other.m_Priority;
		m_HeartbeatTime    = other.m_HeartbeatTime;

		return *this;
	}
}CGNodeItem;

// 文件信息结构
typedef struct RedisFileInfo_Stru
{
	char       m_FileName[128];      //文件名称
	char       m_ProviderID[64];     //提供商ID
	char       m_AssetID[64];        //媒资ID
	char       m_Path[128];          //文件路径
	Int32      m_ServiceType;        //内容服务类型
	Int64      m_FileSize;           //文件大小
	Int64      m_Duration;           //时长
	Int32      m_Bitrate;            //码率
	Int8       m_IsWrite;            //是否正在写，1：表示在写，0：表示文件写完成

	RedisFileInfo_Stru()
	{
		memset(m_FileName, 0, sizeof(m_FileName));
		memset(m_ProviderID, 0, sizeof(m_ProviderID));
		memset(m_AssetID, 0, sizeof(m_AssetID));
		memset(m_Path, 0, sizeof(m_Path));
		m_ServiceType = SERVICE_TYPE_FILE;
		m_FileSize  = 0;
		m_Duration  = 0;
		m_Bitrate   = 0;
		m_IsWrite   = 0;
	}

	RedisFileInfo_Stru(const RedisFileInfo_Stru &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		memcpy(m_Path, other.m_Path, sizeof(other.m_Path));
		m_ServiceType  = other.m_ServiceType;
		m_FileSize     = other.m_FileSize;
		m_Duration     = other.m_Duration;
		m_Bitrate      = other.m_Bitrate;
		m_IsWrite      = other.m_IsWrite;
	}

	RedisFileInfo_Stru& operator = (const RedisFileInfo_Stru &other)
	{
		memcpy(m_FileName, other.m_FileName, sizeof(other.m_FileName));
		memcpy(m_ProviderID, other.m_ProviderID, sizeof(other.m_ProviderID));
		memcpy(m_AssetID, other.m_AssetID, sizeof(other.m_AssetID));
		memcpy(m_Path, other.m_Path, sizeof(other.m_Path));
		m_ServiceType  = other.m_ServiceType;
		m_FileSize     = other.m_FileSize;
		m_Duration     = other.m_Duration;
		m_Bitrate      = other.m_Bitrate;
		m_IsWrite      = other.m_IsWrite;

		return *this;
	}
}RedisFileInfo;
typedef std::list<RedisFileInfo>                        RedisFileInfoList;  
typedef std::map<std::string, RedisFileInfo >           RedisFileInfoMap;

//cg节点路径结构
typedef struct RedisFileDistribute_Stru
{
	char        m_NodeID[64];        //节点名称
	char        m_Path[128];           //文件在节点路径

	RedisFileDistribute_Stru()
	{
		memset(m_NodeID, 0, sizeof(m_NodeID));
		memset(m_Path, 0, sizeof(m_Path));
	}

	RedisFileDistribute_Stru(const RedisFileDistribute_Stru &other)
	{
		memcpy(m_NodeID, other.m_NodeID, sizeof(other.m_NodeID));
		memcpy(m_Path, other.m_Path, sizeof(other.m_Path));
	}

	RedisFileDistribute_Stru& operator = (const RedisFileDistribute_Stru &other)
	{
		memcpy(m_NodeID, other.m_NodeID, sizeof(other.m_NodeID));
		memcpy(m_Path, other.m_Path, sizeof(other.m_Path));

		return *this;
	}

	Bool operator==(const RedisFileDistribute_Stru &other) const
	{
		if ((strcmp(m_NodeID, other.m_NodeID) == 0) && (strcmp(m_Path, other.m_Path) == 0))
		{
			return TRUE;
		}

		return FALSE;
	}
}RedisFileDistribute;

typedef std::vector<RedisFileDistribute>                   RedisFileDistributeVec;
typedef std::map<std::string, RedisFileDistributeVec >     FileDistributeMap;


//定义m3u8类型
typedef enum M3u8Type_ENUM 
{
	TYPE_UNKNOWN  = 0,

	//主m3u8
	MAIN_M3U8   = 1,

	//子m3u8
	SUB_M3U8    = 2
}M3u8Type;

//存储单个m3u8片的结构
typedef struct HLSSlipInfo_Stru
{
	Int32            m_Duration; 
	std::string      m_SplipUrl;      //内容中的url
	std::string      m_DomainUrl;     //源url域名

	HLSSlipInfo_Stru()
	{
		m_Duration    = 0;
		m_SplipUrl    = "";
		m_DomainUrl   = "";
	}

	HLSSlipInfo_Stru(const HLSSlipInfo_Stru &other)
	{
		m_Duration    = other.m_Duration;
		m_SplipUrl    = other.m_SplipUrl;  
		m_DomainUrl   = other.m_DomainUrl;
	}

	HLSSlipInfo_Stru& operator= (const HLSSlipInfo_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_Duration    = other.m_Duration; 
		m_SplipUrl    = other.m_SplipUrl;  
		m_DomainUrl   = other.m_DomainUrl;

		return *this;
	}
}HLSSlipInfo;
typedef std::list<HLSSlipInfo>    HLSSlipInfoList;

//子m3u8信息
typedef struct SubM3u8Info_Stru
{
	Int32                     m_TargetDuation;
	SInt64                    m_MediaSequence;
	Bool                      m_IsEndList;
	HLSSlipInfoList           m_HLSSlipList;

	SubM3u8Info_Stru()
	{
		m_TargetDuation   = 0;
		m_MediaSequence   = 0;
		m_IsEndList       = FALSE;
	}

	SubM3u8Info_Stru(const SubM3u8Info_Stru &other)
	{
		m_TargetDuation     = other.m_TargetDuation;
		m_MediaSequence     = other.m_MediaSequence;
		m_IsEndList         = other.m_IsEndList;
		m_HLSSlipList.clear();
		m_HLSSlipList.insert(m_HLSSlipList.end(), other.m_HLSSlipList.begin(), other.m_HLSSlipList.end());
	}

	SubM3u8Info_Stru& operator= (const SubM3u8Info_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_TargetDuation    = other.m_TargetDuation;
		m_MediaSequence    = other.m_MediaSequence;
		m_IsEndList        = other.m_IsEndList;
		m_HLSSlipList.clear();
		m_HLSSlipList.insert(m_HLSSlipList.end(), other.m_HLSSlipList.begin(), other.m_HLSSlipList.end());

		return *this;
	}
}SubM3u8Info;


//主m3u8播放列表中单个元素的媒体信息
typedef struct MediaStreamM3u8Info_Stru
{
	Int32         m_ProgramID;
	Int64         m_BandWidth;
	std::string   m_StreamUrl;   //主m3u8子m3u8 url地址
	std::string   m_DomainUrl;   //主m3u8访问域名地址
	SubM3u8Info   m_SubM3u8;     //子m3u8类型

	MediaStreamM3u8Info_Stru()
	{
		m_ProgramID     = 0;
		m_BandWidth     = 0;
		m_StreamUrl     = "";
		m_DomainUrl     = "";
	}

	MediaStreamM3u8Info_Stru(const MediaStreamM3u8Info_Stru &other)
	{
		m_ProgramID     = other.m_ProgramID;
		m_BandWidth     = other.m_BandWidth;
		m_StreamUrl     = other.m_StreamUrl;
		m_DomainUrl     = other.m_DomainUrl;
		m_SubM3u8       = other.m_SubM3u8;
	}

	MediaStreamM3u8Info_Stru& operator= (const MediaStreamM3u8Info_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ProgramID     = other.m_ProgramID;
		m_BandWidth     = other.m_BandWidth;
		m_StreamUrl     = other.m_StreamUrl;
		m_DomainUrl     = other.m_DomainUrl;
		m_SubM3u8       = other.m_SubM3u8;

		return *this;
	}

}MediaStreamM3u8Info;

//主m3u8的信息
typedef std::vector<MediaStreamM3u8Info >  MainMediaStreamVector;

#endif


