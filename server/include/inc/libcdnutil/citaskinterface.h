#ifndef CI_TASK_INTERFACE_H
#define CI_TASK_INTERFACE_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    ingestinterface.h
*  Description:  注入任务处理接口
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcore/RuntimeObj.h"
#include "libutil/OSTypes.h"
#include "libcdnutil/libcdnutil_dll.h"
#include "libcdnutil/cdncommon.h"

class LIBCDNUTIL_EXPORT CCITaskInterface : public RuntimeObject
{
	CONF_DECLARE_NO_DYNACREATE(CCITaskInterface)
public:
    /*
	*开启任务注入
	* 
	*
	*
	*返回0：标示成功处理消息;非0标示失败
    */
	virtual Int32 StartTask(void *taskitem) = 0;
	/*
	*删除内容
	*
	*
	*
	*返回0：标示成功处理消息;非0标示失败
    */
	virtual Int32 DeleteContent(void *taskitem) = 0;
};

#endif

