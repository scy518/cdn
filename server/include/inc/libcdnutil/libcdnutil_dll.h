#ifndef LIB_CDN_UTIL_DLL_H
#define LIB_CDN_UTIL_DLL_H

#ifdef  WIN32
	#ifdef LIBCDNUTIL_STATIC
		#define LIBCDNUTIL_EXPORT
	#else
		#ifdef LIBCDNUTIL_EXPORTS
			#define LIBCDNUTIL_EXPORT __declspec(dllexport)
		#else
			#define LIBCDNUTIL_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBCDNUTIL_EXPORT
#endif


#define  LIBCDNUTIL_API  extern "C" LIBCDNUTIL_EXPORT

#define  LIBCDNUTIL_BEGIN    namespace LibCdnUtil {
#define  LIBCDNUTIL_END      };

#endif

