#ifndef AIS_COMMON_H
#define AIS_COMMON_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    clcommon.h
*  Description:  cl公共结构
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcore/cstring.h"
#include "libcdnutil/cdncommon.h"
#include "libci/libcimacros.h"
#include <list>

//子节点定义
typedef struct SubNodeItem_Stru
{
	std::string       m_SubAreaCode;     //子节点区域码
	std::string       m_SubInAddress;    //子节点注入地址
	std::string       m_SubVodAddress;   //子节点点播地址
	std::string       m_SubManageUrl;    //子节点管理地址
	std::string       m_Remarks;         //备注

	SubNodeItem_Stru()
	{
		m_SubAreaCode    = "";
		m_SubInAddress   = "";
		m_SubVodAddress  = "";
		m_SubManageUrl   = "";
		m_Remarks        = "";
	}

	SubNodeItem_Stru(const SubNodeItem_Stru &other)
	{
		m_SubAreaCode    = other.m_SubAreaCode;
		m_SubInAddress   = other.m_SubInAddress;
		m_SubVodAddress  = other.m_SubVodAddress;
		m_SubManageUrl   = other.m_SubManageUrl;
		m_Remarks        = other.m_Remarks;
	}

	SubNodeItem_Stru& operator=(const SubNodeItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_SubAreaCode    = other.m_SubAreaCode;
		m_SubInAddress   = other.m_SubInAddress;
		m_SubVodAddress  = other.m_SubVodAddress;
		m_SubManageUrl   = other.m_SubManageUrl;
		m_Remarks        = other.m_Remarks;

		return *this;
	}
}SubNodeItem;
typedef std::list<SubNodeItem >      SubNodeItemList;

//分发状态接口
typedef struct DistributeTaskItem_Stru
{
	SInt64              m_TaskID;
	std::string         m_ProviderID;
	std::string         m_AssetID;
	SInt32              m_ServiceType;
	std::string         m_Url;              //文件下载url
	std::string			m_NodeID;           //文件存储节点ID
	std::string         m_SubNodeAisAddr;  //子节点地址
	SInt32              m_Status;           //分发状态
	SInt32              m_Progress;         //注入进度
	SInt64              m_CreateTime;       //创建时间
	SInt64              m_EndTime;       //结束时间
	std::string			m_Path;           //文件存储路径 
	SInt32              m_ErrCode;          //错误码
	std::string         m_ErrDes;           //错误码买描述

	DistributeTaskItem_Stru()
	{
		m_TaskID         = 0;
		m_ProviderID     = "";
		m_AssetID        = "";
		m_ServiceType    = 0;
		m_Url            = "";
		m_NodeID         = "";
		m_SubNodeAisAddr = "";
		m_Status         = 0;
		m_Progress       = 0;
		m_CreateTime     = 0;
		m_EndTime        = 0;
		m_Path           = "";
		m_ErrCode        = 0;
		m_ErrDes         = "success";
	}

	DistributeTaskItem_Stru(const DistributeTaskItem_Stru &other)
	{
		m_TaskID         = other.m_TaskID;
		m_ProviderID     = other.m_ProviderID;
		m_AssetID        = other.m_AssetID;
		m_ServiceType    = other.m_ServiceType;
		m_Url            = other.m_Url;
		m_NodeID         = other.m_NodeID;
		m_SubNodeAisAddr = other.m_SubNodeAisAddr;
		m_Status         = other.m_Status;
		m_Progress       = other.m_Progress;
		m_CreateTime     = other.m_CreateTime;
		m_EndTime        = other.m_EndTime;
		m_Path           = other.m_Path;
		m_ErrCode        = other.m_ErrCode;
		m_ErrDes         = other.m_ErrDes;
	}

	DistributeTaskItem_Stru& operator=(const DistributeTaskItem_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_TaskID         = other.m_TaskID;
		m_ProviderID     = other.m_ProviderID;
		m_AssetID        = other.m_AssetID;
		m_ServiceType    = other.m_ServiceType;
		m_Url            = other.m_Url;
		m_NodeID         = other.m_NodeID;
		m_SubNodeAisAddr = other.m_SubNodeAisAddr;
		m_Status         = other.m_Status;
		m_Progress       = other.m_Progress;
		m_CreateTime     = other.m_CreateTime;
		m_EndTime        = other.m_EndTime;
		m_Path           = other.m_Path;
		m_ErrCode        = other.m_ErrCode;
		m_ErrDes         = other.m_ErrDes;

		return *this;
	}
}DistributeTaskItem;
typedef std::list<DistributeTaskItem >       DistTaskItemList;

//定义文件分布
typedef struct FileDistribute_Stru
{
	std::string     m_ProviderID;
	std::string     m_AssetID;
	std::string     m_Path;
	std::string     m_NodeID;
	SInt64          m_createtime;

	FileDistribute_Stru()
	{
		m_ProviderID = "";
		m_AssetID = "";
		m_NodeID = "";
		m_Path = "";
		m_createtime = 0;
	}

	FileDistribute_Stru(const FileDistribute_Stru &other)
	{
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;
		m_createtime = other.m_createtime;
	}

	FileDistribute_Stru& operator=(const FileDistribute_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;
		m_createtime = other.m_createtime;

		return *this;
	}

}FileDistribute;
typedef std::list<FileDistribute > FileDistributeList;

//定义节点信息
typedef struct Node_Stru
{
	std::string    m_NodeID;
	std::string    m_PrivateStreamAddr;
	std::string    m_PublicStreamAddr;
	SInt64         m_FreeBandWidth;
	SInt64         m_HeartTime;

	Node_Stru()
	{
		m_NodeID = "";
		m_PrivateStreamAddr = "";
		m_HeartTime = 0;
		m_FreeBandWidth = 0;
	}

	Node_Stru(const Node_Stru &other)
	{
		m_NodeID = other.m_NodeID;
		m_PrivateStreamAddr = other.m_PrivateStreamAddr;
		m_PublicStreamAddr = other.m_PublicStreamAddr;
		m_FreeBandWidth = other.m_FreeBandWidth;
		m_HeartTime = other.m_HeartTime;
	}

	Node_Stru& operator=(const Node_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_NodeID = other.m_NodeID;
		m_PrivateStreamAddr = other.m_PrivateStreamAddr;
		m_PublicStreamAddr = other.m_PublicStreamAddr;
		m_FreeBandWidth = other.m_FreeBandWidth;
		m_HeartTime = other.m_HeartTime;
		return *this;
	}
}Node;
typedef std::list<Node > NodeList;

#endif


