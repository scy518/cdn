#ifndef LIB_AIS_MACROS_H
#define LIB_AIS_MACROS_H
/*******************************************************************************
*  Copyright (c), 2010, songchuangye. 
*  All rights reserved. 
*  
*  File Name:    libaismacros.h
*  Description:   ais模块宏
*  Others:
*  Version: 1.0       Author: songchuangye       Date: 2013-09-16
*  History: 
*           <Author>    <Date>        <Version >    <Desc>
*           scy       2013-09-16        1.0           Original

*******************************************************************************/
#include "libcdnutil/libcdnutilmacros.h"
/*******************************************************************************
*                              参数名宏定义
*******************************************************************************/
#define AIS_NODE_TIME_OUT                   "AisNodeTimeOut"
#define AIS_BACKUP_NUM                      "AisBackupNum"

#define AIS_AFTER_CLEAN_TIME                "AisAfterCleanTime"   
#define AIS_FINISH_CLEAN_TIME               "AisFinishCleanTime"    
#define AIS_DISTRIBUTE_CLEAN_TIME           "AisDistributeCleanTime"  

#define AIS_DISTRIBUTE_IS_ENABLE            "AisDistributeIsEnable"
#define AIS_DISTRIBUTE_TASK_NUM             "AisDistributeTaskNum"
#define AIS_SQL_STATEMENT_FILE              "AisSqlStatementFile"
  
/*******************************************************************************
*                              定义常量
*******************************************************************************/
#define AIS_HTTP_SERVER                   "AIS Server"  

//是否预分发
#define DISTRIBUTE_DISENABLE               0
#define DISTRIBUTE_ENABLE                  1

//分发状态
#define DISTRIBUTE_WAITING                 0
#define DISTRIBUTE_TRANSFER                1
#define DISTRIBUTE_COMPLETE                2

#endif



