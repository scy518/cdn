#ifndef LIB_AIS_DLL_H
#define LIB_AIS_DLL_H

#ifdef  WIN32
	#ifdef LIBAIS_STATIC
		#define LIBAIS_EXPORT
	#else
		#ifdef LIBAIS_EXPORTS
			#define LIBAIS_EXPORT __declspec(dllexport)
		#else
			#define LIBAIS_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBAIS_EXPORT
#endif


#define  LIBAIS_API  extern "C" LIBAIS_EXPORT

#endif

