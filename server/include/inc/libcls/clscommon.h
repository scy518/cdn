#ifndef CLS_COMMON_H
#define CLS_COMMON_H
/*******************************************************************************
版权所有 (C), 2001-2013, songchuangye
********************************************************************************
file name:
version  :
created  : 2013-2-12   11:52
file path: OSTool.h
modify   :
author   : songchuangye
purpose  : 公共结构
*******************************************************************************/
#include "libutil/OSTypes.h"
#include "libcls/libclsmacros.h"
#include <map>
#include <list>
#include <string>

//定义节点信息
typedef struct Node_Stru
{
	std::string    m_NodeID;
	std::string    m_PrivateStreamAddr;
	std::string    m_PublicStreamAddr;
	SInt64         m_FreeBandWidth;
	SInt64         m_TotalBandWidth;
	SInt64         m_HeartTime;

	Node_Stru()
	{
		m_NodeID = "";
		m_PrivateStreamAddr  = "";
		m_PublicStreamAddr   = "";
		m_HeartTime = 0;
		m_FreeBandWidth = 0;
		m_TotalBandWidth = 0;
	}

	Node_Stru(const Node_Stru &other)
	{
		m_NodeID = other.m_NodeID;
		m_PrivateStreamAddr  = other.m_PrivateStreamAddr;
		m_PublicStreamAddr   = other.m_PublicStreamAddr;
		m_FreeBandWidth = other.m_FreeBandWidth;
		m_TotalBandWidth = other.m_TotalBandWidth;
		m_HeartTime = other.m_HeartTime;
	}

	Node_Stru& operator=(const Node_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_NodeID = other.m_NodeID;
		m_PrivateStreamAddr  = other.m_PrivateStreamAddr;
		m_PublicStreamAddr   = other.m_PublicStreamAddr;
		m_FreeBandWidth = other.m_FreeBandWidth;
		m_TotalBandWidth = other.m_TotalBandWidth;
		m_HeartTime = other.m_HeartTime;

		return *this;
	}
}Node;
typedef std::list<Node>     NodeList;

//定义文件分布
typedef struct FileDistribute_Stru
{
	std::string     m_FileName;
	std::string     m_ProviderID;
	std::string     m_AssetID;
	std::string     m_Path;
	std::string     m_NodeID;
	SInt64          m_CreateTime;

	FileDistribute_Stru()
	{
		m_FileName   = "";
		m_ProviderID = "";
		m_AssetID    = "";
		m_NodeID     = "";
		m_Path       = "";
		m_CreateTime = 0;
	}

	FileDistribute_Stru(const FileDistribute_Stru &other)
	{
		m_FileName = other.m_FileName;
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;
		m_CreateTime = other.m_CreateTime;
	}

	FileDistribute_Stru& operator=(const FileDistribute_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_FileName = other.m_FileName;
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_Path = other.m_Path;
		m_NodeID = other.m_NodeID;
		m_CreateTime = other.m_CreateTime;

		return *this;
	}
}FileDistribute;

typedef std::list<FileDistribute >                    FileDistributeList;
typedef std::map<std::string, FileDistributeList >    FileNodeMap;

//定义Locate消息结构
typedef struct LocateMessge_Stru
{
	std::string m_AssetID;
	std::string m_ProviderID;
	std::string m_SubType;
	std::string m_TransferRate;
	std::string m_IngressCapacity;
	std::string m_Range;

	LocateMessge_Stru()
	{
		m_AssetID = "";
		m_ProviderID = "";
		m_SubType = "";
		m_TransferRate = "";
		m_IngressCapacity = "";
		m_Range = "";
	}

	LocateMessge_Stru(const LocateMessge_Stru &other)
	{
		m_AssetID = other.m_AssetID;
		m_ProviderID = other.m_ProviderID;
		m_SubType = other.m_SubType;
		m_TransferRate = other.m_TransferRate;
		m_IngressCapacity = other.m_IngressCapacity;
		m_Range = other.m_Range;
	}

	LocateMessge_Stru& operator=(const LocateMessge_Stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_AssetID = other.m_AssetID;
		m_ProviderID = other.m_ProviderID;
		m_SubType = other.m_SubType;
		m_TransferRate = other.m_TransferRate;
		m_IngressCapacity = other.m_IngressCapacity;
		m_Range = other.m_Range;

		return *this;
	}
}LocateMessge;

//直播url参数
typedef struct LiveParam_stru 
{
	std::string    m_ProviderID;
	std::string    m_AssetID;
	SInt32         m_LiveType;
	SInt32         m_DelayTime;   //时移延时
	SInt64         m_StartTime;   //回看开始时间
	SInt64         m_EndTime;     //回看结束时间
	SInt32         m_UseStreamAddrType;         //使用的流地址类型  

	LiveParam_stru()
	{
		m_ProviderID = "";
		m_AssetID = "";
		m_LiveType = TYPE_LIVE;
		m_DelayTime = 0;
		m_StartTime = 0;
		m_EndTime = 0;
		m_UseStreamAddrType = CLS_USE_PRIVATEADDR_TYPE;
	}

	LiveParam_stru(const LiveParam_stru &other)
	{
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_LiveType = other.m_LiveType;
		m_DelayTime = other.m_DelayTime;
		m_StartTime = other.m_StartTime;
		m_EndTime = other.m_EndTime;
		m_UseStreamAddrType = other.m_UseStreamAddrType;
	}

	LiveParam_stru& operator=(const LiveParam_stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_LiveType = other.m_LiveType;
		m_DelayTime = other.m_DelayTime;
		m_StartTime = other.m_StartTime;
		m_EndTime = other.m_EndTime;
		m_UseStreamAddrType = other.m_UseStreamAddrType;

		return *this;
	}
}LiveParam;

//点播url参数
typedef struct VodParam_stru 
{
	std::string    m_ProviderID;
	std::string    m_AssetID;
	SInt32         m_UseStreamAddrType;         //使用的流地址类型         

	VodParam_stru()
	{
		m_ProviderID             = "";
		m_AssetID                = "";
		m_UseStreamAddrType      = CLS_USE_PRIVATEADDR_TYPE;
	}

	VodParam_stru(const VodParam_stru &other)
	{
		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_UseStreamAddrType = other.m_UseStreamAddrType;
	}

	VodParam_stru& operator=(const VodParam_stru &other)
	{
		if (this == &other)
		{
			return *this;
		}

		m_ProviderID = other.m_ProviderID;
		m_AssetID = other.m_AssetID;
		m_UseStreamAddrType = other.m_UseStreamAddrType;

		return *this;
	}
}VodParam;

#endif



