#ifndef LIB_UDP_RTI_DLL_H
#define LIB_UDP_RTI_DLL_H

#ifdef  WIN32
	#ifdef LIBUDPRTI_STATIC
		#define LIBUDPRTI_EXPORT
	#else
		#ifdef LIBUDPRTI_EXPORTS
			#define LIBUDPRTI_EXPORT __declspec(dllexport)
		#else
			#define LIBUDPRTI_EXPORT __declspec(dllimport)
		#endif
	#endif
#else
	#define LIBUDPRTI_EXPORT
#endif


#define  LIBUDPRTI_API  extern "C" LIBUDPRTI_EXPORT

#endif

