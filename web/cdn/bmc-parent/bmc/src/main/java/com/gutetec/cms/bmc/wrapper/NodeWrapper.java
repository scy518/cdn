package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.NodeInfo;
import com.gutetec.cms.bmc.db.dao.NodeDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;
import com.gutetec.cms.bmc.util.DacUtil;

import net.sf.json.JSONObject;

public class NodeWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(NodeWrapper.class);
	
	/**
	 * add node
	 * @param node
	 * @return
	 */
	public static Response addNode(NodeInfo node)
	{
		LOGGER.info("Receive create bode request.node Bean:" + DacUtil.convertBeanToJson(node));
		JSONObject rt = new JSONObject();
		try {
			NodeDao dao = (NodeDao) FmDBUtil.getDaoInstance("NodeInfo");
			dao.save(node);
		} catch (Exception e) {
			LOGGER.error("create node fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return  Response.status(Response.Status.CREATED).entity(rt).build();
	}
	
	/**
	 * add node
	 * @param node
	 * @return
	 */
	public static Response updateNode(NodeInfo node)
	{
		LOGGER.info("Receive create bode request.node Bean:" + DacUtil.convertBeanToJson(node));
		JSONObject rt = new JSONObject();
		try {
			NodeDao dao = (NodeDao) FmDBUtil.getDaoInstance("NodeInfo");
			dao.update(node);
		} catch (Exception e) {
			LOGGER.error("update node fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return  Response.status(Response.Status.CREATED).entity(rt).build();
	}
	
	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		NodeDao dao = (NodeDao) FmDBUtil.getDaoInstance("NodeInfo");
		List<NodeInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum();
		} catch (HibernateException e) {
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}
	
	/**
	 * delete channle by id
	 * 
	 * @param id
	 * @return
	 */
	public static Response delNode(String id) {
		JSONObject rt = new JSONObject();
		try {
			NodeDao dao = (NodeDao) FmDBUtil.getDaoInstance("NodeInfo");
			dao.deleteById(id);
		} catch (Exception e) {
			LOGGER.error("del node fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return Response.status(Response.Status.OK).entity(rt).build();
	}
}
