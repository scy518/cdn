package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.FileDistributeInfo;
import com.gutetec.cms.bmc.db.dao.FileDisDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;

import net.sf.json.JSONObject;

public class FileDisWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileDisWrapper.class);
	
	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		FileDisDao dao = (FileDisDao) FmDBUtil.getDaoInstance("FileDistributeInfo");
		List<FileDistributeInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum(paramMap);
		} catch (HibernateException e) {
			LOGGER.error("error is" + e.getMessage());
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}
	
	public static Response addChannle(String  providerid, String assetid) {
		JSONObject result = new JSONObject();
		try {
			FileDisDao dao = (FileDisDao) FmDBUtil.getDaoInstance("FileDistributeInfo");
			FileDistributeInfo file = new FileDistributeInfo();
			file.setProviderid(providerid);
			file.setAssetid(assetid);
			dao.save(file);
		} catch (Exception e) {
			LOGGER.error("create chl fail.", e);
			result.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		result.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(result).build();
	}
}
