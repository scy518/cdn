package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 节点信息
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_node")
public class NodeInfo {
	
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;
	/**
	 * 节点ID
	 */
	@Column(name="nodeid", nullable= true)
	private String nodeid;
	
	/**
	 * 节点类型  0:CI注入节点  1:RTI节点
	 */
	@Column(name="nodetype")
	private int nodetype;
	
	/**
	 * 节点存储路径
	 */
	@Column(name="path")
	private String path;
	
	/**
	 * 最大空间
	 */
	@Column(name="maxsize")
	private long maxsize;
	
	/**
	 * 使用空间
	 */
	@Column(name="usesize")
	private long usesize;
	
	/**
	 * 保留空间
	 */
	@Column(name="reservesize")
	private long reservesize;
	
	/**
	 *私网推流地址
	 */
	@Column(name="privatestreamaddr")
	private String privatestreamaddr;
	
	/**
	 *公网推流地址
	 */
	@Column(name="publicstreamaddr")
	private String publicstreamaddr;
	
	/**
	 *空闲带宽
	 */
	@Column(name="freebandwidth")
	private long freebandwidth;
	
	/**
	 *总带宽
	 */
	@Column(name="totalbandwidth")
	private long totalbandwidth;
	
	/**
	 *心跳时间戳
	 */
	@Column(name="hearttime")
	private long hearttime;
	
	/**
	 *节点是否在线，只针对CI类型节点。0:离线 1:在线
	 */
	@Column(name="isonline")
	private int isonline;
}
