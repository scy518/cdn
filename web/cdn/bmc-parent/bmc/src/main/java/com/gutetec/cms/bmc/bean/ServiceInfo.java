package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 服务监控
 * @author asus
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_service")
public class ServiceInfo {
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;
	
	/**
	 * 服务名称
	 */
	@Column(name="serviceid")
	private String serviceid;
	
	/**
	 * 模块名称
	 */
	@Column(name="modulename")
	private String modulename;
	
	/**
	 * 服务安装地址
	 */
	@Column(name="installaddr")
	private String installaddr;
}
