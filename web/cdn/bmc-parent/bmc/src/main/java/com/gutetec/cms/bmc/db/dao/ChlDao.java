package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.ChannleInfo;

public class ChlDao extends FmDbDAO<ChannleInfo> {
	private static final Logger logger = LoggerFactory.getLogger(ChlDao.class);

	private final SessionFactory sessionFactory;

	public ChlDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    /**
     * 存储一条新的数据
     * <功能详细描述>
     * @param chl
     * @return
     * @see [类、类#方法、类#成员]
     */
    public boolean save(ChannleInfo chl)
    {
        try
        {
            beginTransaction();
            session.save(chl);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * update user info
     * <update user info>
     * @param curUser
     * @return
     * @see [class]
     */
    public boolean update(ChannleInfo chl)
    {
        try
        {
            beginTransaction();
            session.update(chl);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * delete
     * @param id
     * @return
     */
    public int deleteById(String id)
    {
        int result = -1;
        long ids = Long.parseLong(id);
        try
        {
            beginTransaction();
            //             session.createQuery("delete NatInfo info where info.id = :id")
            //                .setParameter("id", id)
            session.createQuery("delete from ChannleInfo info where info.id = :id")
                .setParameter("id", ids)
                .executeUpdate();
            
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        result = 0;
        return result;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<ChannleInfo> findByPage(HashMap<String, Object> paramMap)
    {
        List<ChannleInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from ChannleInfo info WHERE 1=1"); 
            //hql.append(" order by info.firstAccessTime asc ");
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<ChannleInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getDataNum()
    {
       int result =0;
    	try
        {
            beginTransaction();
            String hql = "select count(*) from ChannleInfo ";  
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
}
