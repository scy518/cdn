package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_alarm")
public class AlarmInfo {
	/**
	 *主键ID
	 */
	@Id
	@Column(name = "id",nullable = true, unique=true)
	private Long id;
	
	/**
	 * 模块ID
	 */
	@Column(name="moduleid", length=64)
	private String moduleid;
	
	/**
	 * 告警节点ID
	 */
	@Column(name="serviceid", length=64)
	private String serviceid;
	
	/**
	 * 告警id
	 */
	@Column(name="alarmid")
	private Long alarmid;
	
	/**
	 * 告警级别 1:一般 2:提示 3:警告  4:严重 5:致命
	 */
	@Column(name = "alarmlevel")
	private Integer alarmlevel;
	
	/**
	 * 告警名称
	 */
	@Column(name="alarmname", length=64)
	private String alarmname;
	
	/**
	 * 告警开始时间，UTC秒时间戳
	 */
	@Column(name="starttime")
	private Long starttime;
	
	/**
	 * 告警结束时间，UTC秒时间戳
	 */
	@Column(name="stoptime")
	private Long stoptime;
	
	/**
	 * 告警描述
	 */
	@Column(name="alarmdes")
	private String alarmdes;
}
