package com.gutetec.cms.bmc.resource;

import java.util.Calendar;
import java.util.HashMap;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.bean.ConfInfo;
import com.gutetec.cms.bmc.wrapper.ConfWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 配置管理接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " config Management " })
public class ConfigResource {
	@POST
	@Path("/conf")
	@ApiOperation(value = "create config.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addChannle(@ApiParam(value = "config", required = true) ConfInfo conf) {
		conf.setUpdatetime(Calendar.getInstance().getTimeInMillis()/1000);
		return ConfWrapper.addChannle(conf);
	}

	@GET
	@Path("/conf")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query conf by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit,
			@ApiParam(value = "serviceid") @QueryParam("serviceid") String serviceid,
			@ApiParam(value = "paraname") @QueryParam("paraname") String paraname) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		paramMap.put("serviceid", serviceid);
		paramMap.put("paraname", paraname);
		return ConfWrapper.findByPage(paramMap);
	}

	@DELETE
	@Path("/conf/{id}")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	@ApiOperation(value = "delete conf by id")
	@Timed
	public Response deleteChannle(
			@ApiParam(value = "config id", required = true) @PathParam("id") String channleId) {
		return ConfWrapper.delChannle(channleId);
	}

	@PUT
	@Path("/conf")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "update config")
	@Timed
	public Response updateCps(@ApiParam(value = "chl", required = true) ConfInfo chl) {
		chl.setUpdatetime(Calendar.getInstance().getTimeInMillis()/1000);
		return ConfWrapper.updateChannle(chl);
	}

}
