package com.gutetec.cms.bmc.db.dbUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.db.dao.FmDbDAO;



/**
 * <Function brief>
 * <Function details>
 * @author  Administrator
 * @version  [Version, 2016-2-17]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class FmDBUtil {
  private static final Logger logger = LoggerFactory.getLogger(FmDBUtil.class);

  private static SessionFactory sessionFactory = null;


  private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public static Date getDate(String dateStr) throws ParseException {
      return df.parse(dateStr);
  }

  public static String formatDate(Date date) {
      return df.format(date);
  }

  public static String formatArray(String[] sArray) {
      StringBuffer sb = new StringBuffer();
      for (String s : sArray) {
          sb.append(s).append(",");
      }

      return sb.deleteCharAt(sb.length() - 1).toString();
  }

  @SuppressWarnings("rawtypes")
  public static FmDbDAO getDaoInstance(String tableName) {

      String daoName = FmDbMapper.getDaoName(tableName);
      FmDbDAO fmDbDao = null;

      try {
          fmDbDao =
                  (FmDbDAO) Class.forName(daoName).getConstructor(SessionFactory.class)
                          .newInstance(sessionFactory);

      } catch (Exception ex) {
          ex.printStackTrace();
          logger.error("Get Dao Error, Please check tableName, tableName:" + tableName);
      }

      return fmDbDao;
  }

  public static SessionFactory getSessionFactory() {
      return sessionFactory;
  }

  public static void setSessionFactory(SessionFactory sessionFactory) {
      FmDBUtil.sessionFactory = sessionFactory;
  }
}
