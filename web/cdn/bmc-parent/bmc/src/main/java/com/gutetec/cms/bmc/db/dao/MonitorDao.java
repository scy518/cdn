package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.SysMonitorInfo;



public class MonitorDao extends FmDbDAO<SysMonitorInfo> {
	private static final Logger logger = LoggerFactory.getLogger(MonitorDao.class);

	private final SessionFactory sessionFactory;

	public MonitorDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
   
    
    /**
     * update user info
     * <update user info>
     * @param curUser
     * @return
     * @see [class]
     */
    public boolean update(SysMonitorInfo chl)
    {
        try
        {
            beginTransaction();
            session.update(chl);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**.*
     * 查询服务下的所有模块
     * @param serviceId
     * @return
     */
    public List<SysMonitorInfo> findModulesById(String serviceId)
    {
    	List<SysMonitorInfo> result = null;
    	 try
         {
             beginTransaction();
             
             result = list(session
                     .createQuery(
                         "SELECT info FROM SysMonitorInfo info where info.serviceid  =:serviceid")
                     .setParameter("serviceid", serviceId));
         }
         catch (HibernateException e)
         {
             logger.warn(e.getMessage(), e);
         }
         finally
         {
             closeTransaction();
         }
         return result;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<SysMonitorInfo> findByPage(HashMap<String, Object> paramMap)
    {
        List<SysMonitorInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            
            hql.append("from SysMonitorInfo group by serviceid");
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<SysMonitorInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<SysMonitorInfo> findmoudlesByPage(HashMap<String, Object> paramMap)
    {
        List<SysMonitorInfo> result = null;
        try
        {
            beginTransaction();
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from SysMonitorInfo info");
            
            if(null != paramMap.get("serviceid") && !"".equals(paramMap.get("serviceid").toString()))
            {
                hql.append(" where info.serviceid  =" + " '" + paramMap.get("serviceid").toString() + "'");
            }
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString()));  	// 从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); 	// 取出num条      
            result = (List<SysMonitorInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getDataNum()
    {
       int result =0;
    	try
        {
            beginTransaction();
            String hql = "select count(distinct serviceid) from SysMonitorInfo";  
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getMoudleDataNum(String serviceId)
    {
       int result =0;
    	try
        {
            beginTransaction();
            StringBuffer hql = new StringBuffer();
            hql.append("select count(*) from SysMonitorInfo info ");  
            hql.append("where info.serviceid  =" + " '" + serviceId + "'" +"");
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
}
