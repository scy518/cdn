package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.ConfInfo;


public class ConfDao extends FmDbDAO<ConfInfo> {
	private static final Logger logger = LoggerFactory.getLogger(ConfDao.class);

	private final SessionFactory sessionFactory;

	public ConfDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    /**
     * 存储一条新的数据
     * <功能详细描述>
     * @param chl
     * @return
     * @see [类、类#方法、类#成员]
     */
    public boolean save(ConfInfo chl)
    {
        try
        {
            beginTransaction();
            session.save(chl);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * update user info
     * <update user info>
     * @param curUser
     * @return
     * @see [class]
     */
    public boolean update(ConfInfo chl)
    {
        try
        {
            beginTransaction();
            session.update(chl);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * delete
     * @param id
     * @return
     */
    public int deleteById(String id)
    {
        int result = -1;
        long ids = Long.parseLong(id);
        try
        {
            beginTransaction();
            //             session.createQuery("delete NatInfo info where info.id = :id")
            //                .setParameter("id", id)
            session.createQuery("delete from ConfInfo info where info.id = :id")
                .setParameter("id", ids)
                .executeUpdate();
            
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        result = 0;
        return result;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<ConfInfo> findByPage(HashMap<String, Object> paramMap)
    {
        List<ConfInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from ConfInfo info WHERE 1=1"); 
            //hql.append(" order by info.firstAccessTime asc ");
            if(null != paramMap.get("paraname") && !"".equals(paramMap.get("paraname").toString()))
            {
                hql.append(" AND info.paramname like" + " '%" + paramMap.get("paraname").toString() + "%'");
            }
            
            if(null != paramMap.get("serviceid") && !"".equals(paramMap.get("serviceid").toString()))
            {
                hql.append(" AND info.serviceid like" + " '%" + paramMap.get("serviceid").toString() + "%'");
            }
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<ConfInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getDataNum(HashMap<String, Object> paramMap)
    {
       int result =0;
    	try
        {
            beginTransaction();
            StringBuffer hql = new StringBuffer();
            hql.append( "select count(*) from ConfInfo info  where 1 = 1  ");  
            
            if(null != paramMap.get("paraname") && !"".equals(paramMap.get("paraname").toString()))
            {
                hql.append(" AND info.paramname like" + " '%" + paramMap.get("paraname").toString() + "%'");
            }
            if(null != paramMap.get("serviceid") && !"".equals(paramMap.get("serviceid").toString()))
            {
                hql.append(" AND info.serviceid like" + " '%" + paramMap.get("serviceid").toString() + "%'");
            }
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
}
