package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 文件分布
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_file_distribute")
public class FileDistributeInfo {
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;
	
	/**
	 * 提供商ID
	 */
	@Column(name = "providerid")
	private String providerid;
	
	/**
	 * 媒资id
	 */
	@Column(name="assetid")
	private String assetid;
	
	/**
	 * 媒资id
	 */
	@Column(name="filename")
	private String filename;
	
	/**
	 * 存储路径
	 */
	@Column(name="path")
	private String path;
	
	/**
	 * 节点ID
	 */
	@Column(name="nodeid")
	private String nodeid;
	
	@Column(name="createtime")
	private long createtime;
}
