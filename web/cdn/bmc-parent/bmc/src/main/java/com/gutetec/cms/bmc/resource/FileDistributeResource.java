package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.wrapper.FileDisWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONObject;

/**
 * 分发监控接口
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " filedistributeInfo Management " })
public class FileDistributeResource {
	
	@GET
	@Path("/filedistributeInfo")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query filedistributeInfo by conditions")
	@Timed
	public Response queFileDistributes(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit,
			@ApiParam(value = "providerid") @QueryParam("providerid") String providerid,
			@ApiParam(value = "assetid") @QueryParam("assetid") String assetid) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		paramMap.put("providerid", providerid);
		paramMap.put("assetid", assetid);
		return FileDisWrapper.findByPage(paramMap);
	}
	
	@POST
	@Path("/filedistributeInfo")
	@ApiOperation(value = "create filedistributeInfo.")
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	public Response addChannle(@ApiParam(value = "providerid  assetid", required = true) String  body) {
		JSONObject obj = JSONObject.fromObject(body);
		String providerid = obj.getString("providerid");
		String assetid = obj.getString("assetid");
		return FileDisWrapper.addChannle(providerid, assetid);
	}
}
