package com.gutetec.cms.bmc.wrapper;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.ConfInfo;
import com.gutetec.cms.bmc.db.dao.ConfDao;
import com.gutetec.cms.bmc.db.dbUtil.FmDBUtil;
import com.gutetec.cms.bmc.util.DacUtil;

import net.sf.json.JSONObject;

/**
 * Channle rest interface processing class
 * 
 * @author macui
 *
 */
public class ConfWrapper {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfWrapper.class);

	/**
	 * Create data acquisition task
	 * 
	 * @param taskBean
	 *            task detail
	 * @return status code 201(success) or 500(fail)
	 */
	public static Response addChannle(ConfInfo chl) {
		LOGGER.info("Receive create chl request.taskBean:" + DacUtil.convertBeanToJson(chl));
		JSONObject rt = new JSONObject();
		try {
			ConfDao dao = (ConfDao) FmDBUtil.getDaoInstance("ConfInfo");
			dao.save(chl);
		} catch (Exception e) {
			LOGGER.error("create chl fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return Response.status(Response.Status.CREATED).entity(rt).build();
	}

	/**
	 * find by page
	 * 
	 * @param paramMap
	 * @return
	 */
	public static Response findByPage(HashMap<String, Object> paramMap) {
		ConfDao dao = (ConfDao) FmDBUtil.getDaoInstance("ConfInfo");
		List<ConfInfo> result = null;
		int totalRows = 0;
		try {
			result = dao.findByPage(paramMap);
			totalRows = dao.getDataNum(paramMap);
		} catch (HibernateException e) {
			return Response.serverError().build();
		}

		JSONObject object = new JSONObject();
		if (0 == totalRows) {
			object.put("total", totalRows);
			object.put("rows", "[]");
			return Response.ok(object).build();
		} else {
			object.put("total", totalRows);
			object.put("rows", result);
			return Response.ok(object).build();
		}
	}

	/**
	 * delete channle by id
	 * 
	 * @param id
	 * @return
	 */
	public static Response delChannle(String id) {
		JSONObject rt = new JSONObject();
		try {
			ConfDao dao = (ConfDao) FmDBUtil.getDaoInstance("ConfInfo");
			dao.deleteById(id);
		} catch (Exception e) {
			LOGGER.error("del chl fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return Response.status(Response.Status.OK).entity(rt).build();
	}

	/**
	 * update chl
	 * @param chl
	 * @return
	 */
	public static Response updateChannle(ConfInfo chl) {
		JSONObject rt = new JSONObject();
		try {
			ConfDao dao = (ConfDao) FmDBUtil.getDaoInstance("ConfInfo");
			dao.update(chl);
		} catch (Exception e) {
			LOGGER.error("update chl fail.", e);
			rt.put("result", "-1");
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(rt).build();
		}
		rt.put("result", "0");
		return Response.status(Response.Status.OK).entity(rt).build();
	}
}
