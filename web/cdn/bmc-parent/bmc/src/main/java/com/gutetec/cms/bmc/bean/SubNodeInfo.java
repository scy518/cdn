package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 节点信息
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_subnode")
public class SubNodeInfo {
	
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true,updatable=false)
	private long id;
	/**
	 * 子节点区域码
	 */
	@Column(name="subareacode", nullable= true,updatable=false)
	private String subareacode;
	
	/**
	 * 子节点注入地址
	 */
	@Column(name="subinaddress",updatable=true)
	private String subinaddress;

	/**
	 * 子节点点播地址
	 */
	@Column(name="subvodaddress",updatable=true)
	private String subvodaddress;

	/**
	 * 子节点管理地址
	 */
	@Column(name="submanageurl",updatable=true)
	private String submanageurl;
	
	/**
	 * 备注
	 */
	@Column(name="remarks",updatable=true)
	private String remarks;
}
