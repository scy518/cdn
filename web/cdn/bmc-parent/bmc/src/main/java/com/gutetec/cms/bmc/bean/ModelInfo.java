package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 文件信息
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_module")
public class ModelInfo {
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="anothername")
	private String anothername;
	
	@Column(name="describe")
	private String describe;
}
