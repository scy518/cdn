package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.wrapper.CitaskWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 注入监控接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " cotask Management " })
public class CiTaskResource {

	@GET
	@Path("/citask")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query citask by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return CitaskWrapper.findByPage(paramMap);
	}
}
