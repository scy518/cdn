package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 文件信息
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_file_info")
public class FileInfo {
	
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true)
	private long id;
	/**
	 * 提供商ID
	 */
	@Column(name = "providerid")
	private String providerid;
	
	/**
	 * 媒资id
	 */
	@Column(name="assetid")
	private String assetid;
	
	/**
	 *业务类型(可以支持的协议)：
	 * 0:未知 1：文件类型 2：HLS协议，需要生成对应m3u8文件 
	 * 3：NGOD协议，生成对应ngod索引 4：HLS+NGOD协议
	 */
	@Column(name = "servicetype")
	private int servicetype;
	
	/**
	 *文件名
	 */
	@Column(name="filename")
	private String filename;
	
	/**
	 *文件大小
	 */
	@Column(name="filesize")
	private long filesize;
	
	/**
	 *文件时长
	 */
	@Column(name="duration")
	private long duration;
	
	/**
	 *文件码率
	 */
	@Column(name="bitrate")
	private long bitrate;
	
	/**
	 *更新时间
	 */
	@Column(name="updateTime")
	private long updateTime;
	
	/**
	 *内容对应的md5值
	 */
	@Column(name="md5")
	private String md5;
}
