package com.gutetec.cms.bmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 *  系统监控
 * @author macui
 *
 */
@Entity
@Table(name = "t_sysmonitor")
public class SysMonitorInfo {
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true,updatable=false)
	private long id;
	
	/**
	 * 服务名称
	 */
	@Column(name="serviceid",updatable=false)
	private String serviceid;
	
	/**
	 * 服务名称
	 */
	@Column(name="servicename",updatable=false)
	private String servicename;
	
	/**
	 * 服务版本号
	 */
	@Column(name="version", updatable = false)
	private String version;
	
	/**
	 * 服务监控url
	 */
	@Column(name="sysmonitorurl", updatable = false)
	private String sysmonitorurl;
	
	/**
	 * 模块名称
	 */
	@Column(name="modulename",updatable=false)
	private String modulename;
	
	/**
	 * 运行状态  0:停止 1:正在启动 2:运行
	 */
	@Column(name="runstatus",updatable=false)
	private int runstatus;
	
	/**
	 * 第一次启动UTC时间戳
	 */
	@Column(name="laststarttime")
	private long laststarttime;
	
	/**
	 * 表示服务是否可以操作 0:表示不可以操作  1:表示可操作
	 */
	@Column(name="iscontrol",updatable=true)
	private int iscontrol;
	
	/**
	 * '服务控制 0:禁止 1:停止 2:启动
	 */
	@Column(name="control",updatable=true)
	private int control;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSysmonitorurl() {
		return sysmonitorurl;
	}

	public void setSysmonitorurl(String sysmonitorurl) {
		this.sysmonitorurl = sysmonitorurl;
	}

	public String getModulename() {
		return modulename;
	}

	public void setModulename(String modulename) {
		this.modulename = modulename;
	}

	public int getRunstatus() {
		return runstatus;
	}

	public void setRunstatus(int runstatus) {
		this.runstatus = runstatus;
	}

	public long getLaststarttime() {
		return laststarttime;
	}

	public void setLaststarttime(long laststarttime) {
		this.laststarttime = laststarttime;
	}

	public int getIscontrol() {
		return iscontrol;
	}

	public void setIscontrol(int iscontrol) {
		this.iscontrol = iscontrol;
	}

	public int getControl() {
		return control;
	}

	public void setControl(int control) {
		this.control = control;
	}
}
