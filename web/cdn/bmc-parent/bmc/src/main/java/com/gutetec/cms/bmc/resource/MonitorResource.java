package com.gutetec.cms.bmc.resource;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gutetec.cms.bmc.bean.SysMonitorInfo;
import com.gutetec.cms.bmc.wrapper.MonitorWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.sf.json.JSONObject;

/**
 * 配置管理接口类
 * 
 * @author macui
 *
 */
@Path("/v1/resource")
@Api(tags = { " sysmonitor Management " })
public class MonitorResource {

	@GET
	@Path("/sysmonitor")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query sysmonitor by conditions")
	@Timed
	public Response queryChannles(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		return MonitorWrapper.findByPage(paramMap);
	}

	@PUT
	@Path("/sysmonitor")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "update sysmonitor")
	@Timed
	public Response updateCps(@ApiParam(value = "chl", required = true) SysMonitorInfo chl) {
		return MonitorWrapper.updateChannle(chl);
	}
	
	@GET
	@Path("/modules/{serviceid}")
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "query sysmonitor by conditions")
	@Timed
	public Response queryModules(@ApiParam(value = "start") @QueryParam("start") String start,
			@ApiParam(value = "limit") @QueryParam("limit") String limit, @PathParam("serviceid") String serviceid) {
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		paramMap.put("serviceid", serviceid);
		return MonitorWrapper.findMoudlesByPage(paramMap);
	}
		
		@PUT
		@Path("/sysmonitor/runstatus")
		@Produces({ MediaType.APPLICATION_JSON })
		@ApiOperation(value = "query sysmonitor by conditions")
		@Timed
		public Response  updateService(@ApiParam(value = "serviceid & runstatus")  String body)
		{
			JSONObject obj = JSONObject.fromObject(body);
			String serviceid = obj.getString("serviceid");
			int control = obj.getInt("control");
			return MonitorWrapper.updateService(serviceid, control);
		}
}
