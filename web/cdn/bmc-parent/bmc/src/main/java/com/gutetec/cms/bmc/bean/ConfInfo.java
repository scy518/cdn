package com.gutetec.cms.bmc.bean;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * 配置参数实体类
 * @author macui
 *
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_conf")
public class ConfInfo {
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id", nullable = true, unique=true,updatable=false)
	private long id;
	
	/**
	 * 参数名
	 */
	@Column(name = "paramname", length=32,updatable=false)
	private String paramname;

	/**
	 * 参数类型 0:string 1:int
	 */
	@Column(name="paramtype", updatable=false)
	private int paramtype;
	 
	/**
	 * 参数值
	 */
	@Column(name="paramvalue",length=128,updatable=true)
	private String paramvalue;
	
	/**
	 * 参数描述
	 */
	@Column(name="paramdec",length=128,updatable=true)
	private String paramdec;
	
	/**
	 * '参数所属模块
	 */
	@Column(name="serviceid",length=64,updatable=false)
	private String serviceid;
	
	/**
	 * 参数上次更新时间戳，UTC秒时间戳
	 */
	@Column(name="lasttime",updatable=true)
	private long lasttime;
	
	/**
	 * 参数更新时间戳
	 */
	@Column(name="updatetime",updatable=true)
	private long updatetime;
}
