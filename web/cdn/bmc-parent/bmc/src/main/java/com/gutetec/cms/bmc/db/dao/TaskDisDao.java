package com.gutetec.cms.bmc.db.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gutetec.cms.bmc.bean.TaskDistributeInfo;

public class TaskDisDao extends FmDbDAO<TaskDistributeInfo> {
	private static final Logger logger = LoggerFactory.getLogger(TaskDisDao.class);

	private final SessionFactory sessionFactory;

	public TaskDisDao(SessionFactory sessionFactory) {
		super(sessionFactory);
		this.sessionFactory = sessionFactory;
	}

	/**
     * {@inheritDoc}
     */
    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }
    
    /**
     * 存储一条新的数据
     * <功能详细描述>
     * @param chl
     * @return
     * @see [类、类#方法、类#成员]
     */
    public boolean save(TaskDistributeInfo file)
    {
        try
        {
            beginTransaction();
            session.save(file);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            return false;
        }
        finally
        {
            closeTransaction();
        }
        return true;
    }
    
    /**
     * 分页查询
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    @SuppressWarnings("unchecked")
    public List<TaskDistributeInfo> findByPage(HashMap<String, Object> paramMap)
    {
        List<TaskDistributeInfo> result = null;
        try
        {
            beginTransaction();
            
            StringBuffer hql = new StringBuffer();
            
            // 拼接sql语句
            hql.append("from TaskDistributeInfo info WHERE 1=1"); 
            //hql.append(" order by info.firstAccessTime asc ");
            
            Query query = session.createQuery(hql.toString());
            query.setFirstResult(Integer.parseInt(paramMap.get("start").toString())); //从第0条开始      
            query.setMaxResults(Integer.parseInt(paramMap.get("limit").toString())); //取出num条      
            result = (List<TaskDistributeInfo>)query.list();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
    
    /**
     * 获取数据库条数
     * <功能详细描述>
     * @return
     * @see [类、类#方法、类#成员]
     */
    public int getDataNum()
    {
       int result =0;
    	try
        {
            beginTransaction();
            String hql = "select count(*) from TaskDistributeInfo ";  
            Query query = session.createQuery(hql.toString());
            result =  ((Number)query.uniqueResult()).intValue();
        }
        catch (HibernateException e)
        {
            logger.warn(e.getMessage(), e);
        }
        finally
        {
            closeTransaction();
        }
        return result;
    }
}
