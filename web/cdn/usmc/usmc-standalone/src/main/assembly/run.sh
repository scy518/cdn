#!/bin/bash

DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME

#JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
echo @JAVA_HOME@ $JAVA_HOME
JAVA="$JAVA_HOME/bin/java"
echo @JAVA@ $JAVA

JAVA_OPTS="-Xms50m -Xmx128m"
port=8304
JAVA_OPTS="$JAVA_OPTS -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=$port,server=y,suspend=n"
echo @JAVA_OPTS@ $JAVA_OPTS

class_path="$RUNHOME/:$RUNHOME/usmc.jar"
echo @class_path@ $class_path

"$JAVA" $JAVA_OPTS -classpath "$class_path"  com.gutetec.cms.usmc.UsmcApp server "$RUNHOME/conf/usmc.yml"
