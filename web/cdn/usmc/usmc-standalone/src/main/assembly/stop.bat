
@echo off
title stopping usmc-service

set HOME=%~dp0
set Main_Class="com.gutetec.cms.usmc.UsmcApp"

echo ================== usmc-service info  =============================================
echo HOME=$HOME
echo Main_Class=%Main_Class%
echo ===============================================================================

echo ### Stopping usmc-service
cd /d %HOME%

rem set JAVA_HOME=D:\WorkSoftware\jdk1.7.0_60
for /f "delims=" %%i in ('"%JAVA_HOME%\bin\jcmd"') do (
  call find_kill_process "%%i" %Main_Class%
)
exit
