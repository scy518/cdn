
/*Table structure for table `user` */
CREATE TABLE IF NOT EXISTS user (
  userID varchar(128) NOT NULL,
  userName varchar(128) DEFAULT NULL,
  userPasswd varchar(128) DEFAULT NULL,
  userRole int(8) DEFAULT NULL,
  userPhone varchar(32) DEFAULT NULL,
  userMailbox varchar(32) DEFAULT NULL,
  userSafeState int(8) DEFAULT NULL,
  userSafeAddress varchar(32) DEFAULT NULL,
  userLoginCount int(16) DEFAULT 0,
  userLoginTime date DEFAULT NULL,
  userAccountTime date DEFAULT NULL,
  userExpiryDateCount int(16) DEFAULT NULL,
  userExpiryDate date DEFAULT NULL,
  userPassExpiryDateCount int(16) DEFAULT NULL,
  userPassExpiryDate date DEFAULT NULL,
  userSessionTime int(16) DEFAULT NULL,
  userSort int(16) DEFAULT NULL,
  PRIMARY KEY (userID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

INSERT INTO `user` VALUES ('73d13858-3487-448c-861a-206370aebf98', 'root', 'go6xmIW/l7ZZlm8m3aj81Q==', '0', null, null, null, null, '3', '2016-07-17', '2016-05-17', null, null, null, null, null, null);