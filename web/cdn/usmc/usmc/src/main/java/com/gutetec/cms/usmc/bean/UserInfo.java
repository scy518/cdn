/*
 * Filename:  UserInfo.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * <user bean>
 * <>
 * @author  donghu
 * @version  [version, 2016年5月4日]
 * @see  [class/function]
 * @since  [product/version]
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user")
public class UserInfo
{
    @Id
    @Column(name = "userID", nullable = true)
    private String userID;
    
    @Column(name = "userName", nullable = true)
    private String userName;
    
    @Column(name = "userPasswd", nullable = true)
    private String userPasswd;
    
    @Column(name = "userRole")
    private String userRole;
    
    @Column(name = "userPhone")
    private String userPhone;
    
    @Column(name = "userMailbox")
    private String userMailbox;
    
    @Column(name = "userSafeState")
    private String userSafeState;
    
    @Column(name = "userSafeAddress")
    private String userSafeAddress;
    
    @Column(name = "userLoginCount")
    private String userLoginCount;
    
    @Column(name = "userLoginTime")
    private String userLoginTime;
    
    @Column(name = "userAccountTime")
    private String userAccountTime;
    
    @Column(name = "userExpiryDateCount")
    private String userExpiryDateCount;
    
    @Column(name = "userExpiryDate")
    private String userExpiryDate;
    
    @Column(name = "userPassExpiryDateCount")
    private String userPassExpiryDateCount;
    
    @Column(name = "userPassExpiryDate")
    private String userPassExpiryDate;
    
    @Column(name = "userSessionTime")
    private String userSessionTime;
    
    @Column(name = "userSort")
    private String userSort;
}
