/*
 * Filename:  UsmcUtil.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016年5月16日
 * Tracking odd Numbers:  <odd Numbers>
 * Modify the odd Numbers:  <odd Numbers>
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.util;

import java.util.HashMap;

/**
 * <usmc util>
 * <Function details>
 * @author  donghu
 * @version  [Version, 2016年5月16日]
 * @see  [Classs/Method]
 * @since  [Product/Model Version]
 */
public class UsmcUtil
{
    /**
     * user session timeout ,min
     */
    public static int USER_SESSION_TIME = 10;
    
    public static int USER_SESSION_TIMERS = 5;
    /**
     * user session period ,sec
     */
    public static int USER_SESSION_PERIOD = 3;
    
    public static int ONE_MIN = 60 * 1000;
    
    public static final String USMC_SUPER_MANAGER = "raisecom";
    
    public static final String USMC_DEFAULT_MANAGER = "root";
    
    public static final String USMC_ACCOUNT_EXPIRY = "180";
    
    public static final String USMC_PASS_EXPIRY = "90";
    
    public static final int LOGIN_MODIFY_PWD = 0;
    
    public static final int LOGIN_FORCES_PWD = 1;
    
    public static final HashMap<String, String> generateCodeMap = new HashMap<String, String>();
    
    /**
     * <string convert to int>
     * <string convert to int>
     * @param str
     * @return
     * @see [class]
     */
    public static int getInt(String str)
    {
        try
        {
            int intStr = Integer.parseInt(str);
            return intStr;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        return -1;
    }
    
    /**
     * <whether string is null>
     * <whether string is null>
     * @param str
     * @return return true if str is null
     * @see [class]
     */
    public static boolean isEmpty(String str)
    {
        if (null == str || str.isEmpty())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
