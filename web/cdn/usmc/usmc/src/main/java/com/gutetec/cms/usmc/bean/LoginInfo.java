/*
 * Filename:  LoginInfo.java
 * Copyright:  Raisecom Technologies Co., Ltd. Copyright YYYY-YYYY,  All rights reserved
 * Description:  <description>
 * Author:  donghu
 * Edit-time:  2016-5-4
 * Modify content:  <modify content>
 */
package com.gutetec.cms.usmc.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * <user login info>
 * <user login info>
 * @author  donghu
 * @version  [version, 2016-5-4]
 * @see  [class]
 * @since  [version]
 */
@Data
@Log4j
@NoArgsConstructor
@AllArgsConstructor
public class LoginInfo
{
    private String username;
    
    private String password;
    
    private String isEncypted = "false";
    
    private String vcode;
}
