/**
 * 处理登录页面的enter键盘事件
 */
$(document).ready(function()
{
	// 处理按下Enter键的事件
	$(window).keydown(function(event)
	{
		if (event.keyCode == 13)
		{
			submit();
		}
	});
});

function next()
{
	if (event.keyCode == 13)
	{
		document.login.password.focus();
	}
}
