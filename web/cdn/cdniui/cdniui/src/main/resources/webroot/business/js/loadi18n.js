﻿﻿var lang = getLanguage();
var i18n = null; //omui i18n constant
var cdn_i18n = null; //module i18n constant

jQuery.i18n.properties({
    language:lang,
    name:'lang-i18n',// properties file name
    path:'../../component/thirdparty/omui/i18n/',// properties file path
    mode:'map',
    cache : true,
    encoding : 'UTF-8',
    callback: function() {
    	i18n = $.i18n;
    }
});

jQuery.i18n.properties({
    language:lang,
    name:'business-i18n',// properties file name
    path:'../i18n/',// properties file path
    mode:'map',
    cache : true,
    encoding : 'UTF-8',
    callback: function() {
    	cdn_i18n = $.i18n;
    }
});

function loadLanguage(){
	var i18nItems = $("[name_i18n=com_raisecom_itms_ticket_i18n]");//name_i18n 不能包含“.”
	for(var i=0;i<i18nItems.length;i++){
		var $item = $(i18nItems.eq(i));
		var itemId = $item.attr('id');
		if(typeof($item.attr("title"))!="undefined"){
		    $item.attr("title", i18n.prop(itemId));
	    }else{
		    $item.text(i18n.prop(itemId));
	    }
	}		
}