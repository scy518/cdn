$(document).ready(function()
{
	//	parent.setIframeHeight('setParainit', 580);
	$("#networkTimeSetting").html(i18n.prop('syscfg_networkTimeSetting'));
	$("#systemTimeSetting").html(i18n.prop('syscfg_systemTimeSetting'));
	
	$("#tdTimeNow").html(i18n.prop('syscfg_tdTimeNow') + i18n.prop('colon'));
	$("#tdZone").html(i18n.prop('syscfg_tdZone') + i18n.prop('colon'));
	$("#lblNtpServer").html(i18n.prop('syscfg_lblNtpServer') + i18n.prop('colon'));
	$("#lblIpV6").html(i18n.prop('syscfg_lblIpV6') + i18n.prop('colon'));
	$("#lblSuggestServer").html(i18n.prop('syscfg_lblSuggestServer') + i18n.prop('colon'));
	$("#lblUpdateTime").html(i18n.prop('syscfg_lblUpdateTime') + i18n.prop('colon'));
	$("#lblManTime").html(i18n.prop('syscfg_lblManTime') + i18n.prop('colon'));
	$("#tdAutoUpdate").html(i18n.prop('syscfg_tdAutoUpdate'));
	$("#lbltxtIpv6").html(i18n.prop('syscfg_lbltxtIpv6'));
	$("#lbltxtupdatetime").html(i18n.prop('syscfg_lbltxtupdatetime'));
	$("#lblManSet").html(i18n.prop('syscfg_lblManSet'));
	
	
	var ntpId = "";
	var ntpServerType;
	
	// 开始时间
	$('input[name=time]').omCalendar(
	{
        date : new Date(),
        readOnly : true,
        showTime : true
    });
	
	var ucNtpSvrValueChange = function(newValue)
	{
		$('input[name=txtntpServer]').val(newValue);
	}
	
	$('input[name=suggestServer]').omCombo(
	{
		dataSource :
		[
		 	{
		 		'value' : 'pool.ntp.org',
		 		'text'  : 'pool.ntp.org'
			},
			{
				'value' : 'asia.pool.ntp.org',
				'text'  : 'asia.pool.ntp.org'
			},
			{
				'value' : 'europe.pool.ntp.org',
				'text'  : 'europe.pool.ntp.org'
			},
			{
				'value' : 'north-america.pool.ntp.org',
				'text'  : 'north-america.pool.ntp.org'
			},
			{
				'value' : 'oceania.pool.ntp.org',
				'text'  : 'oceania.pool.ntp.org'
			},
			{
				'value' : 'time.windows.com',
				'text'  : 'time.windows.com'
			},
			{
				'value' : 'time.nuri.net',
				'text'  : 'time.nuri.net'
			},
			{
				'value' : 'time.asia.apple.com',
				'text'  : 'time.asia.apple.com'
			},
			{
				'value' : 'clock.via.net',
				'text'  : 'clock.via.net'
			},
			{
				'value' : 'ntp.nasa.gov',
				'text'  : 'ntp.nasa.gov'
			},
			{
				'value' : '210.72.145.44',
				'text'  : '210.72.145.44(中国国家授时中心服务器)'
			},
			{
				'value' : 'ntp.sjtu.edu.cn',
				'text'  : 'ntp.sjtu.edu.cn(上海交通大学网络中心NTP服务器地址)'
			}
		],
		value : 'time.windows.com',
        width : '315px',
        listMaxHeight : '300',
        editable : false,
        onValueChange : function(target, newValue, oldValue, event)
		{ 
			ucNtpSvrValueChange(newValue);
		}
	});
	
	// 加密算法类型下拉框渲染
	$('input[name=zone]').omCombo(
	{
		dataSource : 
		[
		 	{'value' : '1', 'text' : i18n.prop('timeZone1')},
		 	{'value' : '2', 'text' : i18n.prop('timeZone2')},
		 	{'value' : '3', 'text' : i18n.prop('timeZone3')},
		 	{'value' : '4', 'text' : i18n.prop('timeZone4')},
		 	{'value' : '5', 'text' : i18n.prop('timeZone5')},
		 	{'value' : '6', 'text' : i18n.prop('timeZone6')},
		 	{'value' : '7', 'text' : i18n.prop('timeZone7')},
		 	{'value' : '8', 'text' : i18n.prop('timeZone8')},
		 	{'value' : '9', 'text' : i18n.prop('timeZone9')},
		 	{'value' : '10', 'text' : i18n.prop('timeZone10')},
		 	{'value' : '11', 'text' : i18n.prop('timeZone11')},
		 	{'value' : '12', 'text' : i18n.prop('timeZone12')},
		 	{'value' : '13', 'text' : i18n.prop('timeZone13')},
		 	{'value' : '14', 'text' : i18n.prop('timeZone14')},
		 	{'value' : '15', 'text' : i18n.prop('timeZone15')},
		 	{'value' : '16', 'text' : i18n.prop('timeZone16')},
		 	{'value' : '17', 'text' : i18n.prop('timeZone17')},
		 	{'value' : '18', 'text' : i18n.prop('timeZone18')},
		 	{'value' : '19', 'text' : i18n.prop('timeZone19')},
		 	{'value' : '20', 'text' : i18n.prop('timeZone20')},
		 	{'value' : '21', 'text' : i18n.prop('timeZone21')},
		 	{'value' : '22', 'text' : i18n.prop('timeZone22')},
		 	{'value' : '23', 'text' : i18n.prop('timeZone23')},
		 	{'value' : '24', 'text' : i18n.prop('timeZone24')},
		 	{'value' : '25', 'text' : i18n.prop('timeZone25')},
		 	{'value' : '26', 'text' : i18n.prop('timeZone26')},
		 	{'value' : '27', 'text' : i18n.prop('timeZone27')},
		 	{'value' : '28', 'text' : i18n.prop('timeZone28')},
		 	{'value' : '29', 'text' : i18n.prop('timeZone29')},
		 	{'value' : '30', 'text' : i18n.prop('timeZone30')},
		 	{'value' : '31', 'text' : i18n.prop('timeZone31')},
		 	{'value' : '32', 'text' : i18n.prop('timeZone32')},
		 	{'value' : '33', 'text' : i18n.prop('timeZone33')},
		 	{'value' : '34', 'text' : i18n.prop('timeZone34')},
		 	{'value' : '35', 'text' : i18n.prop('timeZone35')},
		 	{'value' : '36', 'text' : i18n.prop('timeZone36')},
		 	{'value' : '37', 'text' : i18n.prop('timeZone37')},
		 	{'value' : '38', 'text' : i18n.prop('timeZone38')},
		 	{'value' : '39', 'text' : i18n.prop('timeZone39')},
		 	{'value' : '40', 'text' : i18n.prop('timeZone40')},
		 	{'value' : '41', 'text' : i18n.prop('timeZone41')},
		 	{'value' : '42', 'text' : i18n.prop('timeZone42')},
		 	{'value' : '43', 'text' : i18n.prop('timeZone43')},
		 	{'value' : '44', 'text' : i18n.prop('timeZone44')},
		 	{'value' : '45', 'text' : i18n.prop('timeZone45')},
		 	{'value' : '46', 'text' : i18n.prop('timeZone46')},
		 	{'value' : '47', 'text' : i18n.prop('timeZone47')},
		 	{'value' : '48', 'text' : i18n.prop('timeZone48')},
		 	{'value' : '49', 'text' : i18n.prop('timeZone49')},
		 	{'value' : '50', 'text' : i18n.prop('timeZone50')},
		 	{'value' : '51', 'text' : i18n.prop('timeZone51')},
		 	{'value' : '52', 'text' : i18n.prop('timeZone52')},
		 	{'value' : '53', 'text' : i18n.prop('timeZone53')},
		 	{'value' : '54', 'text' : i18n.prop('timeZone54')},
		 	{'value' : '55', 'text' : i18n.prop('timeZone55')},
		 	{'value' : '56', 'text' : i18n.prop('timeZone56')},
		 	{'value' : '57', 'text' : i18n.prop('timeZone57')},
		 	{'value' : '58', 'text' : i18n.prop('timeZone58')},
		 	{'value' : '59', 'text' : i18n.prop('timeZone59')},
		 	{'value' : '60', 'text' : i18n.prop('timeZone60')},
		 	{'value' : '61', 'text' : i18n.prop('timeZone61')},
		 	{'value' : '62', 'text' : i18n.prop('timeZone62')},
		 	{'value' : '63', 'text' : i18n.prop('timeZone63')},
		 	{'value' : '64', 'text' : i18n.prop('timeZone64')},
		 	{'value' : '65', 'text' : i18n.prop('timeZone65')},
		 	{'value' : '66', 'text' : i18n.prop('timeZone66')},
		 	{'value' : '67', 'text' : i18n.prop('timeZone67')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone69')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone70')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone71')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone72')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone73')},
		 	{'value' : '68', 'text' : i18n.prop('timeZone74')},
		 	{'value' : '69', 'text' : i18n.prop('timeZone75')}
		],
		value : '1',
        width : '372px',
        listMaxHeight : '300',
        editable : false
    });
	
	$("#OK").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			var ntpType = '';
			
			var serverIpv4 = '';
			
			// 判断自动同步按钮是否选中
			if ($("#autoUpdate").attr('checked') == 'checked')
			{
				ntpType = 1; // 自动同步
				
				// 服务器按钮选中
				if($("#ntpserver").attr('checked') == 'checked')
				{
					ntpServerType = 1;
					serverIpv4 = $('input[name=txtntpServer]').val();
				}
				else
				{
					ntpServerType = 2;
					serverIpv4 = $('input[name=suggestServer]').omCombo('value');
				}
			}
			else
			{
				ntpType = 2;  // 手动设置
			}
			
			// 提交数据到后台
			var flag = true;
			if(flag)
			{
				if (checkFormInput())
				{
					$.omMessageBox.confirm(
					{
						title : i18n.prop('alert'),
						content : i18n.prop('applyUpdate'),
						onClose : function(value)
						{
							if (value)
							{
								var submitValue = 
								{
									id	: parseInt(ntpId),
									zoneId : parseInt($('input[name=zone]').omCombo('value')),
									ntpType : parseInt(ntpType),
									ntpServerType : parseInt(ntpServerType),
									serverIpv4 : serverIpv4,
									ntpInterval : parseInt($('input[name=txtupdatetime]').val()),
									serverIpv6 : $('input[name=txtIpv6]').val(),
									nowTime : $('input[name=textNow]').val(),
									sntp_time : $('input[name=time]').val()
								};
								
								$.ajax(
								{
									type : 'PUT',
									url : '/api/sm/v1/ntpInfo/{ntpInfoId}',
									contentType: "application/json",
									dataType : 'json',
									data : JSON.stringify(submitValue),
									success : function(msg)
									{
										if ("0" == msg.result)
										{
											alertSuccess();
										}
										else
										{
											alertFail();
										}
									}
								});
							}
						}
					});
				}
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});

	// 
	var getNtpInfo = function()
	{
		// 提交数据到后台
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/ntpInfo',
			dataType : 'json',
			success : function(msg)
			{
				ntpId = msg.id;
				$('input[name=textNow]').val(msg.nowTime);
				$('input[name=zone]').omCombo({'value' : msg.zoneId});
				if(msg.ntpType == 1)
				{
					$('input[name=txtntpServer]').val(msg.serverIpv4);
					
					$("#autoUpdate").attr("checked", "checked"); 
					$("#manSet").removeAttr("checked");
					ntpServerType = msg.ntpServerType;
					if(msg.ntpServerType = 1)
					{
						$('#ntpserver').attr("checked", "checked");
						$("#rdsuggestServer").removeAttr("checked");
					}
					else
					{
						$('#rdsuggestServer').attr("checked", "checked");
						$("#ntpserver").removeAttr("checked");
					}
				}
				else
				{
					$("#autoUpdate").removeAttr("checked");
					$("#manSet").attr("checked", "checked"); 
				}
				$('input[name=txtupdatetime]').val(msg.ntpInterval);
				$('input[name=txtIpv6]').val(msg.serverIpv6);
				$('input[name=time]').val(msg.ntpTime);
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 错误提示
				alertError();
			}
		});
	}
	
	// 渲染保存按钮
	$("#refDhcpServer").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons : 
		{
			left : '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 禁用刷新按钮
				$("#refDhcpServer").attr("disabled","disabled");
				
				// 执行单击函数
				getNtpInfo();
				
				// 按钮失去焦点
				this.blur();
				
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 自动同步按钮点击事件
	$("input[name=autoUpdate]").click(function(){
		$("#autoUpdate").attr("checked", "checked"); 
		$("#manSet").removeAttr("checked");
		
		$('#ntpserver').attr("disabled", false); 
		document.getElementById("txtntpServer").disabled =false;
		document.getElementById("txtIpv6").disabled =false;
		$("#rdsuggestServer").attr("disabled", false);
		
		$("#suggestServer").omCombo('enable');
		$("#suggestServer").removeAttr('disabled');
		$("#suggestServer").next().next().removeClass('def-om-state-disabled');
	});
	
	// 手动设置按钮点击事件
	$("input[name=manSet]").click(function()
	{
		$("#manSet").attr("checked", "checked"); 
		$("#autoUpdate").removeAttr("checked");
		$('#ntpserver').attr("disabled", true); 
		document.getElementById("txtntpServer").disabled =true;
		document.getElementById("txtIpv6").disabled =true;
		$("#rdsuggestServer").attr("disabled", true); 
		
		$("#suggestServer").omCombo('disable');
		$("#suggestServer").next().next().removeClass('om-state-disabled').addClass('def-om-state-disabled');
	});
	
	// 服务器按钮点击事件
	$("input[name=ntpserver]").click(function()
	{
		$("#ntpserver").attr("checked", "ntpserver"); 
		$("#rdsuggestServer").removeAttr("checked");
		document.getElementById("txtntpServer").disabled =false;
		document.getElementById("txtIpv6").disabled =false;
	});
	
	// 推荐服务器按钮点击事件
	$("input[name=rdsuggestServer]").click(function()
	{
		$("#rdsuggestServer").attr("checked", "checked"); 
		$("#ntpserver").removeAttr("checked");
		document.getElementById("txtntpServer").disabled =true;
		document.getElementById("txtIpv6").disabled =true;
	});
	
	// 查询ntp配置信息
	getNtpInfo();
	
	// 校验规则
	var rules =
	{
		txtupdatetime :
		{
			digits	:  true,
			range : [5, 65535]
		}
	};
	
	// 校验提示消息
	var messages =
	{
		txtupdatetime :
		{
			digits : i18n.prop('syscfg_ntp_txtupdatetime_err'),
			range : i18n.prop('syscfg_ntp_txtupdatetime_range_err')
		}
	};
	
	// 校验
	var validator = new form_validator(rules, messages, 'errorMsg', 'dhcpForm');

	// 
	var checkFormInput = function()
	{
		return $('#dhcpForm').valid();
	};
});
