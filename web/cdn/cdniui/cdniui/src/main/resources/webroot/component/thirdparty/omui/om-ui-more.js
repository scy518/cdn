(function($)
{
	$.omWidget.addInitListener('om.omGrid', function()
	{
		var gird = this;
		var op = this.options;
		var moreUrl = op.moreUrl;
		var title = op.moreTitle;
		if(null != moreUrl && undefined != moreUrl && "" != moreUrl)
		{
			// "更多"的显示区域
			var html = "<div class='btnseparator'></div>";
			html += "<div class='pGroup'>";
			html += "<div class='pLimit'>";
			html += "<span style='cursor:pointer;color:red;' onclick='addItemToTab(\"" + moreUrl + "\",this, \"" + title + "\");'>更多...</span>";
			html += "</div>";
			html += "</div>";
			var limitDiv = $(html);
			
			// 插到分页条中
			var allPDiv = $(".pDiv2 .pGroup", this.pDiv);
			$(allPDiv[allPDiv.length - 1]).after(limitDiv);
		}
	});
})(jQuery);