$(document).ready(function()
{	
	// 查询标识
	var backupData= "";
	
	$("#backupDataGrid").omGrid(
	{
		limit : Omui.TABLE_ROWS,
		showIndex : true,
		height : Omui.TABLE_HEIGHT,
		autoFit : true,
		dataSource : '/api/sm/v1/LogInfo/main',
		colModel :
		[
			{
				header : i18n.prop('syscfg_logFileName'),
				name : 'Name',
				width : 200,
				align : 'center'
			},
			{
				header : i18n.prop('syscfg_logFileSize'),
				name : 'Length',
				width : 200,
				align : 'center'
			},
			{
				header : i18n.prop('syscfg_logTime'),
				name : 'Time',
				width : 200,
				align : 'center'
			}
		]
	});

	// 初始化botton按钮
	$('#buttonbar').omButtonbar(
	{
		btns :
		[
			{
				label : i18n.prop('download'),
				id : "button-update",
				icons :
				{
					left : '../images/edit_hover.png'
				},
				onClick : function()
				{
					downloadData();
				}
			}
		]
	});

	

	
	
	// 下载数据
	var downloadData = function()
	{
		var selections = $('#backupDataGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		else
		{
		    $.omMessageBox.confirm(
		    {
			title : i18n.prop('alert'),
			content : i18n.prop('downloadContent'),
			onClose : function(value)
			{
				if (value)
				{
					// 随机数，防止页面缓存
					var ss = Math.random();
					
					// 下载数据
					window.location.href = '/api/sm/v1/LogInfo/download?path='+ selections[0].Path + "&ss=" + ss;
				}
			}
		    });
		}		
	};
});
