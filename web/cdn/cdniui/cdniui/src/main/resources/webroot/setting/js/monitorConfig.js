$(document).ready(function()
{
	// 系统监控配置主键ID
	var monitorId = "";
	
	// 设置界面的i18n
	$("#monitorConfig").html(i18n.prop('monitorConfig'));
	$("#tdMonitorConfig").html(i18n.prop('tdMonitorConfig'));
	$("#monitorSwitch").html(i18n.prop('monitorSwitch'));
	$("#tdMonitoringPeriod").html(i18n.prop('tdMonitoringPeriod'));
	$("#monitorSwitch").html(i18n.prop('monitoringPeriod'));
	$("#second").html(i18n.prop('second'));
	$("#lblsecond").html(i18n.prop('syscfg_monitorConfig'));
	
	
	var getMonitoringPeroid = function(value)
	{
		if (value == 1)
		{
			$('#trMonitoringPeriod').css('display','none');
        	$('input[name=monitoringPeriod]').attr('disabled','disabled');
		}
		else
		{
			$('#trMonitoringPeriod').css('display','');
        	$('input[name=monitoringPeriod]').removeAttr('disabled');
		}
	}
	
	
	// 系统监控开关 0：打开 1：关闭
	$('input[name=monitorSwitch]').omCombo(
	{
		dataSource : 
			[
				{
					'value' : '0',
					'text' :  i18n.prop('open')
				},
				{
					'value' : '1',
					'text' :  i18n.prop('close')
				}
			],
		width : '182px',
		listMaxHeight : 'auto',
		editable : false,
		onValueChange : function(target, newValue, oldValue, event)
		{
			getMonitoringPeroid(newValue);
		}
	});
	
	// 查询系统监控配置
	var getMonitorConfig = function()
	{
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/ConfSystemMonitor/main',
			dataType : 'json',
			success : function(msg)
			{
				if (msg != null)
				{
					// 查询到的JSON对象
					var json = msg;
					$('input[name=monitorSwitch]').omCombo({'value' : (json.flag).toString()});
					$('input[name=monitoringPeriod]').val(json.cycle);
					monitorId = json.id;
					
					if (json.flag == 1)
					{
						$('#trMonitoringPeriod').css('display','none');
			        	$('input[name=monitoringPeriod]').attr('disabled','disabled');
					}
					else
					{
						$('#trMonitoringPeriod').css('display','');
			        	$('input[name=monitoringPeriod]').removeAttr('disabled');
					}
				}
				else
				{
					$('input[name=monitorSwitch]').omCombo({'value' : ''});
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
			}
		});
	}

	// 初始化 
	getMonitorConfig();
	
	// 验证规则
	var monitorConfigRules =
	{
		monitoringPeriod :
		{
			required : true,
			isNum : true,
			range : [1, 3600]
		}
	};
	
	// 验证后的提示消息
	var monitorConfigMessages =
	{
		monitoringPeriod :
		{
			required : i18n.prop('inputRequired'),
			isNum : i18n.prop('inputIsDigits'),
			range : i18n.prop('inputRange3600')
		}
	};
	
	// 调用验证方法，创建验证对象
	var monitorConfigValidator = new form_validator(monitorConfigRules,monitorConfigMessages, 'errorMsg', 'monitorConfigForm');
	
	// 校验页面输入框内容
	var checkMonitorConfigFormInput = function()
	{
		return monitorConfigValidator.form();
	}
	
	// 系统监控设置
	$("#setConfig").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons :
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 如果校验通过则提交数据到后台
			if (checkMonitorConfigFormInput())
			{
				$.omMessageBox.confirm(
				{
					title : i18n.prop('alert'),
					content : i18n.prop('applyUpdate'),
					onClose : function(value)
					{
						// 如果确认设置并校验通过则提交数据到后台
						if (value)
						{
							// 按天选择
							var monitorSwitch = $('input[name=monitorSwitch]').val();
							var cycle = $('input[name=monitoringPeriod]').val();
							
							// 提交的数据对象
							var submitValue =
							{
								flag : monitorSwitch,
								id : monitorId,
								cycle : cycle
							};
							
							// 提交数据到后台
							$.ajax(
							{
								type : 'POST',
								url : '/api/sm/v1/ConfSystemMonitor/Update',
								contentType: "application/json",
								dataType : 'json',
								data : JSON.stringify(submitValue),
								success : function(msg)
								{
									if ("0" == msg.result)
									{
										getMonitorConfig();
										alertSuccess();
									}
									else
									{
										alertFail();
									}
								}
							});
						}
					}
				});
			}
		}
	});
	
	// 系统监控配置刷新
	$("#refConfig").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons :
		{
			left :  '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 禁用刷新按钮
			$('#refConfig').omButton('disable');
			
			// 执行单击函数
			getMonitorConfig();
			
			// 启用刷新按钮
			$('#refConfig').omButton('enable');
			
			// 按钮失去焦点
			this.blur();
		}
	});
});
