$(document).ready(function()
{	
	// 查询标识
	var backupData= "";
	
	$("#backupDataGrid").omGrid(
	{
		limit : Omui.TABLE_ROWS,
		showIndex : true,
		height : Omui.TABLE_HEIGHT,
		autoFit : true,
		dataSource : '/api/sm/v1/database/backupFile',
		colModel :
		[
			{
				header : i18n.prop('backupFileName'),
				name : 'fileName',
				width : 200,
				align : 'center'
			},
			{
				header : i18n.prop('backupFileSize'),
				name : 'fileSize',
				width : 200,
				align : 'center'
			},
			{
				header : i18n.prop('dataBackupTime'),
				name : 'fileBackupTime',
				width : 200,
				align : 'center'
			}
		]
	});

	// 初始化botton按钮
	$('#buttonbar').omButtonbar(
	{
		btns :
		[
			{
				label : i18n.prop('backupByHand'),
				id : "button-new",
				icons :
				{
					left : '../images/add_hover.png'
				},
				onClick : function()
				{
					backupByHand();
				}
			},
				
			{
				separtor : true
			},
				
			{
				label : i18n.prop('restore'),
				id : "button-update",
				icons :
				{
					left : '../images/edit_hover.png'
				},
				onClick : function()
				{
					reductionData();
				}
			}
			,
				
			{
				separtor : true
			},
				
			{
				label : i18n.prop('download'),
				id : "button-update",
				icons :
				{
					left : '../images/edit_hover.png'
				},
				onClick : function()
				{
					downloadData();
				}
			}
		]
	});

	// 备份数据
	var backupByHand = function()
	{
		$.omMessageBox.confirm(
		{
			title : i18n.prop('alert'),
			content : i18n.prop('applyBackup'),
			onClose : function(value)
			{
			    if(value)
			    {
				$.omMessageBox.waiting(
				{
					title : i18n.prop('alert'),
					content : i18n.prop('backuping')
				});
				
				var submitValue = 
				{
					type : 2
				};
					
				$.ajax(
				{
					type : 'PUT',
					url : '/api/sm/v1/database/backup',
					contentType: "application/json",
					dataType : 'json',
					data : JSON.stringify(submitValue),
					success : function(msg)
					{
						// 提示消息
						if ("0" == msg.result)
						{
							// 关闭等待框
							$.omMessageBox.waiting('close');
								
							alertSuccess();
							
							$("#backupDataGrid").omGrid('reload');
						}
						else
						{
							alertFail();
						}
					}
				});
			    }
			}
		});
	};

	// 还原数据
	var reductionData = function()
	{
	    var selections = $('#backupDataGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		else
		{
    		$.omMessageBox.confirm(
    		{
    			title : i18n.prop('alert'),
    			content : i18n.prop('applyReduction'),
    			onClose : function(value)
    			{
    			    if(value)
    			    {
        				var submitData =
						{
        					filePath : selections[0].filePath
						};
        				
        				$.ajax(
						{
							type : 'POST',
							url : '/api/sm/v1/database/resume',
							contentType: "application/json",
							dataType : 'json',
							data : JSON.stringify(submitData),
							success : function(msg)
							{
								// 提示消息
								if ("0" == msg.result)
								{
									// 关闭等待框
//									$.omMessageBox.waiting('close');
										
									alertSuccess();
								}
								else if ("1" == msg.result)
								{
								    	reductionFileNotExist();
								}
								else
								{
									alertFail();
								}
							}
						});
    			    }
    			}
    		});
		}
	};
	
	// 下载数据
	var downloadData = function()
	{
		var selections = $('#backupDataGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		else
		{
		    $.omMessageBox.confirm(
		    {
			title : i18n.prop('alert'),
			content : i18n.prop('downloadContent'),
			onClose : function(value)
			{
				if (value)
				{
					// 随机数，防止页面缓存
					var ss = Math.random();
					
					// 下载数据
					window.location.href = '/api/sm/v1/LogInfo/download?path='+ selections[0].filePath + "&ss=" + ss;
					
				}
			}
		    });
		}		
	};
});
