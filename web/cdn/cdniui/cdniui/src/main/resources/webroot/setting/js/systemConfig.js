$(document).ready(function()
{	
	// 国际化赋值
	$("#dhcpConfig").html(i18n.prop('database_config'));
	$("#tdProperIpAddr").html(i18n.prop('tdProperIpAddr') + i18n.prop('colon'));
	$("#tdMysqlPath").html(i18n.prop('tdMysqlPath') + i18n.prop('colon'));
	$("#tdProperPorts").html(i18n.prop('tdProperPorts') + i18n.prop('colon'));
	$("#tdProperPwd").html(i18n.prop('tdProperPwd') + i18n.prop('colon'));
	$("#lblMysqlPort").html(i18n.prop('lblRangeLength30'));
	$("#lbPrpPort").html(i18n.prop('lblRangeLength30'));
	$("#lblMysqlPath").html(i18n.prop('lblRangeLength50'));
	$("#lblSqlIpAddr").html(i18n.prop('ipFommat'));
	
	// 业务模板管理赋值
	/*$("#upload").html(i18n.prop('bussnissUpload'));
	$("#download").html(i18n.prop('bussnissDownload'));*/
	$("#bussniss_template_config").html(i18n.prop('Business_template_configuration'));
	
	
	// acs 国际化赋值
	$("#dhcpConfig").html(i18n.prop('acs_config'));
	$("#tdStunAddr").html(i18n.prop('syscfg_tdStunAddr') + i18n.prop('colon'));
	$("#tdStunPort").html(i18n.prop('syscfg_tdStunPort') + i18n.prop('colon'));
	$("#tdrtmsUserName").html(i18n.prop('syscfg_tdrtmsUserName') + i18n.prop('colon'));
	$("#tdRtmsPwd").html(i18n.prop('syscfg_tdRtmsPwd') + i18n.prop('colon'));
	$("#tdInterval").html(i18n.prop('syscfg_tdInterval') + i18n.prop('colon'));
	$("#tdFileServerAddr").html(i18n.prop('syscfg_FileServerAddr') + i18n.prop('colon'));
	$("#lblStunAddr").html(i18n.prop('syscfg_lblStunAddr'));
	$("#lblInterval").html(i18n.prop('syscfg_lblInterval'));
	$("#lblFileServerAddr").html(i18n.prop('syscfg_lblFileServerAddr'));
	
	// 渲染保存按钮
	$("#setStunServer").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 如果校验通过则提交数据到后台
				if (checkStunFormInput())
				{
					$.omMessageBox.confirm(
					{
						title : i18n.prop('alert'),
						content : i18n.prop('applyUpdate'),
						onClose : function(value)
						{
							// 如果确认设置并校验通过则提交数据到后台
							if (value)
							{
								// 提交的数据对象
								var submitValue = 
								{
									id : 1,
									stunIpAddr : $('input[name=stunAddr]').val(),
									stunPort : parseInt($('input[name=stunPort]').val()),
									userName : $('input[name=rtmsUserName]').val(),
									userPassWord : $('input[name=rtmsPwd]').val(),
									tr069HB : parseInt($('input[name=interval]').val()),
									fileServerIpAddr : $('input[name=fileServerAddr]').val()
								};
								
								var data  =
								{
										stunServerIP : $('input[name=stunAddr]').val(),
										stunServerPort : parseInt($('input[name=stunPort]').val())
								};
								
								var acsData =
								{
										heartbeat : parseInt($('input[name=interval]').val())
								};
								
								// 提交数据到后台 
								$.ajax(
								{
									type : 'POST',
									url : '/api/stun/v1/configStun/',
									contentType: "application/json",
									dataType : 'json',
									data : JSON.stringify(data),
									success : function(msg)
									{
										if (msg.result == "true")
										{
											// 提交数据到后台 ，修改acs状态更新心跳时间
											$.ajax(
											{
												type : 'POST',
												url : '/api/tr069/v1/heartbeatPeriod',
												contentType: "application/json",
												dataType : 'json',
												data : JSON.stringify(acsData),
												success : function(msg)
												{
													if (msg.result == "0")
													{
														// 提交数据到后台 ,更新数据库
														$.ajax(
														{
															type : 'POST',
															url : '/api/sm/v1/SysConfigInfo/',
															contentType: "application/json",
															dataType : 'json',
															data : JSON.stringify(submitValue),
															success : function(msg)
															{
																if (msg.result == "0")
																{
																	alertSuccess();
																}
																else
																{
																	alertFail();
																}
															}
														});
													}
													else
													{
														alertFail();
													}
												}
											});
										}
										else
										{
											$.omMessageBox.alert(
											{
										        type : 'error',
										        title : i18n.prop('alert'),
										        content : i18n.prop('syscfg_stun_error')
										    });
										}
									}
								});
							}
						}
					});
				}
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});

	// 渲染保存按钮
	$("#refStunServer").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons : 
		{
			left : '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 禁用刷新按钮
				$("#refDhcpServer").attr("disabled","disabled");
				
				// 执行单击函数
				getStunServerInfo();
				
				// 按钮失去焦点
				this.blur();
				
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 查询当前服务器IP信息函数
	var getStunServerInfo = function()
	{
		// 根据服务器类型查询对应的IP设置信息
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/SysConfigInfo/',
			dataType: 'json',
		    contentType: "application/json",
			success : function(msg)
			{
				// 赋值
				$('input[name=stunAddr]').val(msg.ipAddr);
				$('input[name=stunPort]').val(msg.port);
				$('input[name=rtmsUserName]').val(msg.userName);
				$('input[name=rtmsPwd]').val(msg.passWord);
				$('input[name=interval]').val(msg.Tr069HB);
				$('input[name=fileServerAddr]').val(msg.FileServerIpAddr);
				
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
				
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			}
		});
	};
	
	// 查询当前服务器IP配置信息
	getStunServerInfo();
	
	// 过滤字段
	var rules =
	{
		stunAddr :
		{
			required : true,
			isIp : true
		},
		stunPort : 
		{
			required : true,
			digits	:  true,
			range : [1, 65535]
		},
		rtmsUserName : 
		{
			required : true,
			isCorrectString : true,
			lengthrange : [1, 30]
		},
		rtmsPwd : 
		{
			required : true,
			isValidStr : true,
			lengthrange : [1, 30]
		},
		interval : 
		{
			required : true,
			digits	:  true,
			range : [1, 3600]
		},
		fileServerAddr : 
		{
			required : true,
			url	:  true
		}
	};
	
	// 提示消息
	var messages =
	{
		stunAddr : 
		{
			required : i18n.prop('inputRequired'),
			isIp : i18n.prop('inputIsIp')
		},	
		stunPort : 
		{
			required : i18n.prop('inputRequired'),
			digits : i18n.prop('syscfg_stun_interval_digits_err'),
			range : i18n.prop('syscfg_stun_port_err')
		},
		rtmsUserName : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		},
		rtmsPwd : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		},
		interval : 
		{
			required : i18n.prop('inputRequired'),
			digits : i18n.prop('syscfg_stun_interval_digits_err'),
			range : i18n.prop('syscfg_stun_interval_range_err')
			
		},	
		fileServerAddr : 
		{
			required : i18n.prop('inputRequired'),
			url : i18n.prop('isUrlMust')
		}
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, messages, 'errorMsg', 'stunForm');

	// 校验页面输入框内容
	var checkStunFormInput = function()
	{
		return $('#stunForm').valid();
	};
	
	// 渲染保存按钮
	$("#setDhcpServer").omButton(
	{
		label : i18n.prop('btnEdit'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 如果校验通过则提交数据到后台
				if (checkDhcpFormInput())
				{
					$.omMessageBox.confirm(
					{
						title : i18n.prop('alert'),
						content : i18n.prop('applyUpdate'),
						onClose : function(value)
						{
							// 如果确认设置并校验通过则提交数据到后台
							if (value)
							{
								// 提交的数据对象
								var submitValue = 
								{
									id  : 1,
									dbIpAddr : $('input[name=mySqlIpAddr]').val(),
									dbUser : $('input[name=mySqlUserName]').val(),
									dbPassword : $('input[name=mySqlPwd]').val(),
									dbPath : $('input[name=mySqlPath]').val()
								};
								
								// 提交数据到后台
								$.ajax(
								{
									type : 'POST',
									url : '/api/sm/v1/DbConfInfo',
									contentType: "application/json",
									dataType : 'json',
									data : JSON.stringify(submitValue),
									success : function(msg)
									{
										if ("0" == msg.result)
										{
											alertSuccess();
										}
										else
										{
											alertFail();
										}
									}
								});
							}
						}
					});
				}
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});

	// 渲染保存按钮
	$("#refDhcpServer").omButton(
	{
		label : i18n.prop('btRefresh'),
		icons : 
		{
			left : '../images/page_refresh.png'
		},
		width : 80,
		onClick : function(event)
		{
			// 检查session是否过期
			var flag = true;
			if(flag)
			{
				// 禁用刷新按钮
				$("#refDhcpServer").attr("disabled","disabled");
				
				// 执行单击函数
				getDhcpServerInfo();
				
				// 按钮失去焦点
				this.blur();
				
				return false;
			}
			
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 查询当前服务器IP信息函数
	var getDhcpServerInfo = function()
	{
		// 根据服务器类型查询对应的IP设置信息
		$.ajax(
		{
			type : 'GET',
			url : '/api/sm/v1/DbConfInfo',
			dataType : 'json',
			success : function(msg)
			{
				// 查询到的JSON对象
				var json = msg;
				
				// 赋值
				$('input[name=mySqlIpAddr]').val(json.dbIpAddr);
				$('input[name=mySqlPath]').val(json.dbPath);
				$('input[name=mySqlUserName]').val(json.dbUser);
				$('input[name=mySqlPwd]').val(json.dbPassword);
			
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
				
				// 启用刷新按钮
				$("#refDhcpServer").removeAttr("disabled","disabled");
			}
		});
	};
	
	// 查询当前服务器IP配置信息
	getDhcpServerInfo();
	
	// 过滤字段
	var rules =
	{
		mySqlIpAddr :
		{
			required : true,
			isIp : true
		},
		mySqlPath : 
		{
			required : true,
			isValidStr : true,
			lengthrange : [1, 50]
		},
		mySqlUserName : 
		{
			required : true,
			isCorrectString : true,
			lengthrange : [1, 30]
		},
		mySqlPwd : 
		{
			required : true,
			isValidStr : true,
			lengthrange : [1, 30]
		}
	};
	
	// 提示消息
	var messages =
	{
		mySqlIpAddr : 
		{
			required : i18n.prop('inputRequired'),
			isIp : i18n.prop('inputIsIp')
		},	
		mySqlPath : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength50')
		},
		mySqlUserName : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		},
		mySqlPwd : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('inputRangeLength30')
		}
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, messages, 'errorMsg', 'dhcpForm');

	// 校验页面输入框内容
	var checkDhcpFormInput = function()
	{
		return $('#dhcpForm').valid();
	};
	
	// 用户名由数字字母下划线组成	
    $.validator.addMethod("isCorrectString", function(value) {
        return checkCorrectStr(value);
    }, i18n.prop('syscfg_username_err'));
    // 检查 数字、字母、下划线
    function checkCorrectStr(str){
        var regu =/(^[a-zA-Z_0-9]*$)/; 
        var reg = new RegExp(regu);
        return reg.test(str); 
    }
    
    // 合法字符
    $.validator.addMethod("isValidStr", function(value) {
        return checkStr(value);
    }, i18n.prop('syscfg_character_err'));
    // 检查字符
    function checkStr(str){
        if (/[^\x00-\xff]/g.test(str)) {
            return false;
        }
        else {
            return true;
        }
    }
    
	
	//业务管理模板下载
	var downloadXML = function()
	{
		window.location.href = '/api/wm/v1/customerConfigure/downloadXml?&ss=' + Math.random();	
		/*$.ajax(
				{
					type : 'POST',
					url : "",
					data : ,
					contentType : 'application/json',
					success : function(msg)
					{
						// 查询到的JSON对象
						if ("0" == msg.result)
						{
							alertSuccess();
							
						}
						else
						{
							alertFail();
						}
					}
				});*/
	};
	//业务管理模板上传
	var uploadXML = function()
	{
	/*	if (validator.form())
		{*/
			/*var filePath = $("#file").val();
			 
			if ("" == filePath)
			{
				$.omMessageTip.show(
				{
					type : 'warning',
					title : i18n.prop('alert'),
					content : i18n.prop('selectFile'),
					timeout : 1500
				});
				
				return false;
			}
			*/
			
		   /*//向后台提交数据
			if(Sys.ie)   
			{   
				var fileobject = new ActiveXObject ("Scripting.FileSystemObject");//获取上传文件的对象   
				var file = fileobject.GetFile (document.getElementById("file").value);//获取上传的文件   
			}  
			else				
			{
				file = document.getElementById("file").files[0]; 
			}*/
			var formData = new FormData($("#fileFrom")[0]);
			$.ajax(
			{
				url : '/api/wm/v1/customerConfigure/uploadXml',
				type : 'POST',
				data : formData,
				async : false,
				cache : false,
				contentType : false,
				processData : false,
				//提交成功
				success: function (msg)
				{
					if('0' == msg.result)
					{
						alertUploadSuccess();
					}
					else if("1" == msg.result)
					{
					    	alertUploadIsExist();
					}
					else 
					{
						alerUploadFailed();
					}
				},
				error: function (data, status, e)
				{
					alerUploadFailed();
				}
			});
	/*	}		*/
	};

	
	//渲染上传按钮
	$("#upload").omButton(
	{
		label : i18n.prop('bussnissUpload'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			uploadXML();
		}
	});
	
	//渲染下载按钮
	$("#download").omButton(
	{
		label : i18n.prop('bussnissDownload'),
		icons : 
		{
			left : '../images/application_edit.png'
		},
		width : 80,
		onClick : function(event)
		{
			downloadXML();
		}
	});

	/*var  Sys = {};   
	if(navigator.userAgent.indexOf("MSIE")>0)   
	{   
		Sys.ie=true;   
	}   
	else  
	{   
		Sys.firefox=true;   
	}   */
    
});
