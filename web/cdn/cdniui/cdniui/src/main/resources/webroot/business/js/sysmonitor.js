$(document).ready(function()
{
	parent.setIframeHeight("sysmonitor", 550);
	// 条件查询的面板
//	var panel = $("#codition_div").omPanel(
//	{
//        iconCls : "panel_search",
//        header : true,
//        title : cdn_i18n.prop('cdn_search'),
//        collapsed : false,
//        collapsible : true
//    });
	
	
	// 把查询的表单加入到面板当中
	//$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	//$(".panel_search").removeClass("om-icon");
	//$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
//	$("#cod_btn_search").omButton(
//	{
//		label : cdn_i18n.prop('usmc_user_search'),
//		icons : 
//		{
//			left : '../../component/css/omui/default/images/search.png'
//		},
//		width : 80,
//		onClick : function(event)
//		{
//			//var username = encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val()));
//			//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
//			// 按钮失去焦点
//			//$(this).blur();
//		}
//	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			if(document.getElementsByName('cod_abyEnbName')[0] == document.activeElement)
			{
				//var username = encodeURIComponent(encodeURIComponent($.trim($('input[name="cod_abyEnbName"]').val())));
				//$('#usmc_userinfoGrid').omGrid("setData", '/api/usmc/v1/userbyname?username=' + username);
				
				return true;
			}
		}
	});
	
	
	
	// 创建表格
	$("#cdn_channleoGrid").omGrid(
	{
		dataSource : "/api/v1/resource/sysmonitor",
		title : '服务监控信息',
		limit : 5,
		height : 250,
		singleSelect : true,
		showIndex : true,
		colModel :
		[
			{
				header : cdn_i18n.prop("cdn_sys_serviceid"),
				name : 'serviceid',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_sys_servicename"),
				name : 'servicename',
				width : 100,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_sys_version"),
				name : 'version',
				width : 150,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_sys_sysmonitorurl"),
				name : 'sysmonitorurl',
				width : 220,
				align : 'left',
				renderer : function(colValue, rowData, rowIndex)
				{
					var data = $("#cdn_channleoGrid").omGrid("getData").rows[rowIndex];
					return '<span><a href="' + colValue	+ '"><u><b style="color:blue;">' +colValue + '</b></u></a></span>';
				}
			},
			{
				/*操作*/
				header : cdn_i18n.prop("cdn_sys_operation"),
				name : 'operation',
				width : 120,
				align : 'left',
				renderer : function(colValue, rowData, rowIndex)
				{
					var data = $("#cdn_channleoGrid").omGrid("getData").rows[rowIndex];
					var iscontrol = data.iscontrol;
					if('0' == iscontrol) // 按钮不可用
					{
						return '';
					}
					else if('1' == iscontrol)
					{
						return '<span>&nbsp;</span>' + '<button id="btnStart"  value="启动">启动</button>' + '<span>&nbsp;&nbsp;</span>'+ '<button id="btnStop" value="停止">停止</button>' + '<span>&nbsp;</span>';
					}
				}
			}
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			$("#divMoudle").hide();
		},
		onRowClick:function(rowIndex,rowData,event)
		{ // 表格行单击事件
			
			$("#divMoudle").show();
			var data = $("#cdn_channleoGrid").omGrid("getData").rows[rowIndex];
			
			var url = "/api/v1/resource/modules/" + data.serviceid;
			//url = url + "?serviceid=" + data.serviceid;
			
			// 创建表格
			$("#cdn_moudleGrid").omGrid(
			{
				dataSource : url,
				title : '模块监控信息',
				limit : 5,
				height : 250,
				singleSelect : true,
				showIndex : true,
				colModel :
				[
					{
						header : cdn_i18n.prop("cdn_sys_modulename"),
						name : 'modulename',
						width : 100,
						align : 'left'
					},
					
					{
						header : cdn_i18n.prop("cdn_sys_runstatus"),
						name : 'runstatus',
						width : 100,
						align : 'left',
						renderer : function(v, rowData , rowIndex)
						{
							if("0" == v)
							{
								return cdn_i18n.prop("cdn_stop");
							}
							else if("2" == v)
							{
								//return cdn_i18n.prop("cdn_alarm_info");
								return '<span style="color:green">' + cdn_i18n.prop("cdn_running") +' </span>';
							}
							else if("1" == v)
							{
								//return cdn_i18n.prop("cdn_alarm_warn");
								return '<span style="color:olive">' + cdn_i18n.prop("cdn_starting") +' </span>';
							}
						}
					},
					{
						header : cdn_i18n.prop("cdn_sys_laststarttime"),
						name : 'laststarttime',
						width : 180,
						align : 'left',
						renderer : function(v, rowData, rowIndex)
						{
							return formatUTC(v*1000);
						}
					},
					{
						/*操作*/
						header : cdn_i18n.prop("cdn_sys_operation"),
						name : 'operation',
						width : 100,
						align : 'left',
						renderer : function(colValue, rowData, rowIndex)
						{
							var data1 = $("#cdn_moudleGrid").omGrid("getData").rows[rowIndex];
							var iscontrol = data1.iscontrol;
							if('0' == iscontrol) // 按钮不可用
							{
								return '';
							}
							else if('1' == iscontrol)
							{
								return  '<span>&nbsp;</span>' + '<button id="btnStartMoudle" value="启动">启动</button>' + '<span>&nbsp;&nbsp;</span>'+ '<button id="btnStopMoudle" value="停止">停止</button> '+ '<span>&nbsp;</span>';
							}
						}
					}
				]
			});
	     }
	});
	
	
	 //再重新绑定新的事件
	 $("#btnStart").live('click', function(){
		 startService();
	});
	 
	 //再重新绑定新的事件
	 $("#btnStop").live('click', function(){
		 stopService();
	});
	 
	//再重新绑定新的事件
	 $("#btnStartMoudle").live('click', function(){
		 startMoudelService();
	});
	 
	 //再重新绑定新的事件
	 $("#btnStopMoudle").live('click', function(){
		 stopMoudelService();
	 });
	 
	// 启动服务
	var startMoudelService = function()
	{
		var selections = $('#cdn_moudleGrid').omGrid('getSelections', true);
		
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		
		var data = 
		{
			id : selections[0].id,
			serviceid : selections[0].serviceid,
			servicename : selections[0].servicename,
			version : selections[0].version,
			sysmonitorurl : selections[0].sysmonitorurl,
			modulename : selections[0].modulename,
			runstatus : selections[0].runstatus,
			laststarttime : selections[0].laststarttime,
			iscontrol : selections[0].iscontrol,
			control : 2
		}
		
		// 提交删除请求
		$.ajax(
		{
			type : 'PUT',
			url : '/api/v1/resource/sysmonitor',
			dataType : 'json',
			data : JSON.stringify(data),
			contentType : 'application/json',
			success : function(msg)
			{
				// 提示消息
				if (msg.result == "0")
				{
					// 更新表格
					$('#cdn_channleoGrid').omGrid('reload');
					
					// 删除成功提示
					alertSuccess();
				}
				else
				{
					$('#cdn_channleoGrid').omGrid('reload');
					// 删除失败提示
					alertFail();
				}
			}
		});
		
	};
	
	// 启动服务
	var stopMoudelService = function()
	{
		var selections = $('#cdn_moudleGrid').omGrid('getSelections', true);
		
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		
		var data = 
		{
			id : selections[0].id,
			serviceid : selections[0].serviceid,
			servicename : selections[0].servicename,
			version : selections[0].version,
			sysmonitorurl : selections[0].sysmonitorurl,
			modulename : selections[0].modulename,
			runstatus : selections[0].runstatus,
			laststarttime : selections[0].laststarttime,
			iscontrol : selections[0].iscontrol,
			control : 1
		}
		
		// 提交删除请求
		$.ajax(
		{
			type : 'PUT',
			url : '/api/v1/resource/sysmonitor',
			dataType : 'json',
			data : JSON.stringify(data),
			contentType : 'application/json',
			success : function(msg)
			{
				// 提示消息
				if (msg.result == "0")
				{
					// 更新表格
					$('#cdn_channleoGrid').omGrid('reload');
					
					// 删除成功提示
					alertSuccess();
				}
				else
				{
					$('#cdn_channleoGrid').omGrid('reload');
					// 删除失败提示
					alertFail();
				}
			}
		});
	};
	 
	 
	// 启动服务
	var startService = function()
	{
		var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
		
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		
		var data = 
		{
			serviceid : selections[0].serviceid,
			control : 2
		}
		
		// 提交删除请求
		$.ajax(
		{
			type : 'PUT',
			url : '/api/v1/resource/sysmonitor/runstatus',
			dataType : 'json',
			data : JSON.stringify(data),
			contentType : 'application/json',
			success : function(msg)
			{
				// 提示消息
				if (msg.result == "0")
				{
					// 更新表格
					$('#cdn_channleoGrid').omGrid('reload');
					
					// 删除成功提示
					alertSuccess();
				}
				else
				{
					$('#cdn_channleoGrid').omGrid('reload');
					// 删除失败提示
					alertFail();
				}
			}
		});
		
	};
	
	
	// 删除信息
	var stopService = function()
	{
		var selections = $('#cdn_channleoGrid').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
		}
		else
		{
			$.omMessageBox.confirm(
			{
				title : i18n.prop("alert"),
				content : cdn_i18n.prop("cdn_stopcontent"),
				onClose : function(value)
				{
					if (value)
					{
						var data = 
						{
							serviceid : selections[0].serviceid,
							control : 1
						}
						
						// 提交删除请求
						$.ajax(
						{
							type : 'PUT',
							url : '/api/v1/resource/sysmonitor/runstatus',
							dataType : 'json',
							data : JSON.stringify(data),
							contentType : 'application/json',
							success : function(msg)
							{
								// 提示消息
								if (msg.result == "0")
								{
									// 更新表格
									$('#cdn_channleoGrid').omGrid('reload');
									
									// 删除成功提示
									alertSuccess();
								}
								else
								{
									// 更新表格
									$('#cdn_channleoGrid').omGrid('reload');
									
									// 删除失败提示
									alertFail();
								}
							}
						});
					}
				}
			});
		}
	};
	
});
