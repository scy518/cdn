$(document).ready(function()
{
	parent.setIframeHeight("file", 650);
	$('#bmc_assetid').text('媒资ID');
	$('#bmc_providerid').text('提供商ID');
	
	// 条件查询的面板
	var panel = $("#codition_div").omPanel(
	{
        iconCls : "panel_search",
        header : true,
        title : '查询',
        collapsed : false,
        collapsible : true
    });
	
	
	// 把查询的表单加入到面板当中
	$("#condition_query").append($("form[name='codition_form']"));
	
	// 修改图标
	$(".panel_search").removeClass("om-icon");
	$(".panel_search").removeClass("om-panel-icon");
	
	// 渲染查询按钮
	$("#cod_btn_search").omButton(
	{
		label : '查询',
		icons : 
		{
			left : '../../component/css/omui/default/images/search.png'
		},
		width : 80,
		onClick : function(event)
		{
			var providerid = encodeURIComponent(encodeURIComponent($
											.trim($('#cod_providerid').val())));
			var assetid = encodeURIComponent(encodeURIComponent($
											.trim($('#cod_assetid').val())));
			$('#cdn_nodeGrid').omGrid("setData", '/api/v1/resource/filedistributeInfo?providerid=' + providerid + '&assetid=' + assetid);
			// 按钮失去焦点
			$(this).blur();
		}
	});
	
	// 回车事件
	// 点击“Enter”键，触发查询事件
	$(this).keydown(function(e)
	{
		if (e.which == "13")
		{
			 if ((document.getElementsByName('cod_providerid')[0] == document.activeElement)
				|| (document.getElementsByName('cod_assetid')[0] == document.activeElement))
		     {
				 var providerid = encodeURIComponent(encodeURIComponent($
												.trim($('#cod_providerid').val())));
				var assetid = encodeURIComponent(encodeURIComponent($
												.trim($('#cod_assetid').val())));
				$('#cdn_nodeGrid').omGrid("setData", '/api/v1/resource/filedistributeInfo?providerid=' + providerid + '&assetid=' + assetid);
				// 按钮失去焦点
				$(this).blur();
		     }
		 }
	});
	
	// 渲染清空按钮
	$("#cod_btn_clear").omButton(
	{
		label : cdn_i18n.prop('cdn_reset'),
		icons : 
		{
			left : '../../component/css/omui/default/images/clear.png'
		},
		width : 80,
		onClick : function(event)
		{
			$('input[name=cod_providerid]').val('');
			$('input[name=cod_assetid]').val('');
			$('input[name=cod_providerid]').focus();

			// 按钮失去焦点
			$(this).blur();
		}
	});
	

	// 创建表格
	$("#cdn_nodeGrid").omGrid(
	{
		dataSource : "/api/v1/resource/filedistributeInfo",
		limit : Omui.TABLE_ROWS,
		height : Omui.TABLE_HEIGHT,
		singleSelect : true,
		showIndex : true,
		colModel :
		[
			{
				header : cdn_i18n.prop("cdn_providerid"),
				name : 'providerid',
				width : 150,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_assetid"),
				name : 'assetid',
				width : 150,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_file_filename"),
				name : 'filename',
				width : 150,
				align : 'left'
			},
			{
				header : cdn_i18n.prop("cdn_citask_path"),
				name : 'path',
				width : 200,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_citask_nodeid"),
				name : 'nodeid',
				width : 150,
				align : 'left'
			},
			
			{
				header : cdn_i18n.prop("cdn_filedis_createtime"),
				name : 'createtime',
				width : 150,
				align : 'left',
				renderer : function(v, rowData, rowIndex)
				{
					return formatUTC(v*1000);
				}
			}
				
		],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		}
	});
});
