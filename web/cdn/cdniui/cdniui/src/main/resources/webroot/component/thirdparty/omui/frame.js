String.prototype.replaceAll = function(reallyDo, replaceWith, ignoreCase) 
{
	if (!RegExp.prototype.isPrototypeOf(reallyDo)) 
	{
		return this.replace(new RegExp(reallyDo, (ignoreCase ? "gi" : "g")),
				replaceWith);
	} 
	else 
	{
		return this.replace(reallyDo, replaceWith);
	}
}

var smallId = "";

/**
 * <一句话功能简述> 
 * <功能详细描述>
 * @author smart
 * @version [版本号, 2012-11-28]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
$(document).ready(function() 
{
	var validator = "";
	// 国际化
	document.title = i18n.prop('welcomeText');
	//$('.logo_text').html(i18n.prop('welcomeText'));
	$('#firstTab').text(i18n.prop('firstTab'));
//	$('#agwTab').text(i18n.prop('AGW'));
//	$('#segwTab').text(i18n.prop('SeGW'));
//	$('#orgTab').text(i18n.prop('composer'));
	$('#confTab').text(i18n.prop('OMC'));
	$('#fullTab').text('LTE');
	$('#oldPwdSpan').text(i18n.prop('oldPwdSpan') + i18n.prop('colon'));
	$('#newPwdSpan').text(i18n.prop('newPwdSpan') + i18n.prop('colon'));
	$('#affirmPwdSpan').text(i18n.prop('affirmPwdSpan') + i18n.prop('colon'));
	
	// 菜单国际化 AGW
//	$('#agwsmall').text(i18n.prop('agwsmall'));
//	$('#agwAgent').text(i18n.prop('agwAgent'));
//	$('#agw').text(i18n.prop('agw'));
//	$('#mmeConfig').text(i18n.prop('mmeConfig'));
//	$('#virgwstatus').text(i18n.prop('virgwstatus'));
//	$('#agweth').text(i18n.prop('agweth'));
//	$('#agwEnable').text(i18n.prop('agwEnable'));
//	$('#activeAlarm').text(i18n.prop('activeAlarm'));
//	$('#alarmAck').text(i18n.prop('alarmAck'));
//	$('#historys').text(i18n.prop('historys'));
//	$('#alarmFilter').text(i18n.prop('alarmFilter'));
//	$('#alarmSync').text(i18n.prop('alarmSync'));
//	$('#alarmAckFilter').text(i18n.prop('alarmAckFilter'));
//	$('#perTask').text(i18n.prop('perTask'));
//	$('#thre').text(i18n.prop('thre'));
//	$('#performanceModel').text(i18n.prop('performanceModel'));
//	$('#repTask').text(i18n.prop('repTask'));
//	$('#softupdate').text(i18n.prop('softupdate'));
//	$('#softupdatehis').text(i18n.prop('softupdatehis'));
//	$('#agwBackupRule').text(i18n.prop('agwBackupRule'));
//	$('#agwBackupData').text(i18n.prop('agwBackupData'));
//	$('#agwRunLogTime').text(i18n.prop('agwRunLogTime'));
//	$('#agwRunLog').text(i18n.prop('agwRunLog'));
//	$('#agwDeviceReset').text(i18n.prop('agwDeviceReset'));

	// 菜单国际化 SeGW
//	$('#segwAgent').text(i18n.prop('segwAgent'));
//	$('#ipsecipsec').text(i18n.prop('ipsecipsec'));
//	$('#ipsecPolicy').text(i18n.prop('ipsecPolicy'));
//	$('#radius').text(i18n.prop('radius'));
//	$('#segweth').text(i18n.prop('agweth'));
//	$('#segwsmall').text(i18n.prop('agwsmall'));
//	$('#segwactive').text(i18n.prop('activeAlarm'));
//	$('#segwAlarmAck').text(i18n.prop('alarmAck'));
//	$('#segwhistory').text(i18n.prop('historys'));
//	$('#segwAlarmFilter').text(i18n.prop('alarmFilter'));
//	$('#segwAlarmSync').text(i18n.prop('alarmSync'));
//	$('#segwalarmAckFilter').text(i18n.prop('alarmAckFilter'));
//	$('#eventInfo').text(i18n.prop('eventInfo'));
//	$('#segwperTask').text(i18n.prop('perTask'));
//	$('#segwthre').text(i18n.prop('thre'));
//	$('#segwperformanceModel').text(i18n.prop('performanceModel'));
//	$('#segwRepTask').text(i18n.prop('repTask'));
//	$('#segwsoftupdate').text(i18n.prop('softupdate'));
//	$('#softupdatehisegw').text(i18n.prop('softupdatehis'));
//	$('#segwBackupRule').text(i18n.prop('agwBackupRule'));
//	$('#segwBackupData').text(i18n.prop('agwBackupData'));
//	$('#segwRunLogTime').text(i18n.prop('agwRunLogTime'));
//	$('#segwRunLog').text(i18n.prop('agwRunLog'));
//	$('#signalingEnable').text(i18n.prop('signalingEnable'));
//	$('#segwDeviceReset').text(i18n.prop('agwDeviceReset'));
	
	// 菜单国际化 OMC
	$('#setPara').text(i18n.prop('setPara'));
	$('#sysConfig').text(i18n.prop('sysConfig'));
	$('#roleinfo').text(i18n.prop('roleinfo'));
	$('#userinfo').text(i18n.prop('userinfo'));
	$('#popedom').text(i18n.prop('popedom'));
	$('#menuinfo').text(i18n.prop('menuinfo'));
	$('#secmenuinfo').text(i18n.prop('secmenuinfo'));
	$('#inspection').text(i18n.prop('inspection'));
	$('#syslog').text(i18n.prop('syslog'));
	$('#pwdconfig').text(i18n.prop('pwdconfig'));
//	$('#tradeinfo').text(i18n.prop('tradeinfo'));
//	$('#placeinfo').text(i18n.prop('placeinfo'));
//	$('#smallGwIn').text(i18n.prop('smallGwIn'));
//	$('#alarmConfig').text(i18n.prop('alarmConfig'));
//	$('#eventConfig').text(i18n.prop('eventConfig'));
//	$('#topology').text(i18n.prop('topology'));
	$('#backRule').text(i18n.prop('backRule'));
	$('#backData').text(i18n.prop('backData'));
	$('#systemBackup').text(i18n.prop('systemBackup'));
	$('#coreHost').text(i18n.prop('coreHost'));
	$('#monitorConfig').text(i18n.prop('monitorCofig'));
	$('#monitorInfos').text(i18n.prop('monitorQuery'));
	$('#monitorInfo').text(i18n.prop('monitorHistory'));
	
	// 菜单国际化 LTE
	$('#hnbInfo').text(i18n.prop('hnbInfo'));
	$('#rpcController').text(i18n.prop('RPCInfo'));
	$('#ping').text(i18n.prop('ping'));
	$('#rebootController').text(i18n.prop('reboot'));
	$('#factoryReset').text(i18n.prop('factoryReset'));
	$('#hnbsoftware').text(i18n.prop('hnbsoftware'));
	$('#software').text(i18n.prop('software'));
	$('#upgradehistory').text(i18n.prop('upgradehistory'));
	$('#lteactive').text(i18n.prop('activeAlarm'));
	$('#confirmAlarm').text(i18n.prop('alarmAck'));
	$('#historyalarm').text(i18n.prop('historys'));
	$('#ltealarmFilter').text(i18n.prop('alarmFilter'));
	$('#synchronizealarm').text(i18n.prop('synchronizealarm'));
	$('#opLogInfo').text(i18n.prop('opLogInfo'));
	$('#safeLogInfo').text(i18n.prop('safeLogInfo'));
	$('#upLogInfo').text(i18n.prop('upLogInfo'));
	$('#tdLocked').text(i18n.prop('tdLocked'));
	
	// 设置右上角图片
	var home = path + '/images/home.png';
	var setting = path + '/images/setting.png';
	var refresh = path + '/images/refresh.png';
	var logout = path + '/images/exit.png';
	if(null != lang && undefined != lang && "" != lang && "zh_CN" != lang)
	{
		home = path + '/images/home_' + lang + '.png';
		setting = path + '/images/setting_' + lang + '.png';
		refresh = path + '/images/refresh_' + lang + '.png';
		logout = path + '/images/exit_' + lang + '.png';
	}
	$('#to_home').attr('src', home);
	$('#set_user').attr('src', setting);
	$('#refresh_page').attr('src', refresh);
	$('#logout').attr('src', logout);
	
	// 网管配置中的操作维护跳转至云主机
	$('#coreHost').click(function()
	{
		// 获取云主机的IP信息
		$.ajax(
		{
			type : 'POST',
			url : '../setPara/getParameterValue',
			dataType : 'json',
			success : function(msg)
			{
				// 查询到的JSON对象
				var json = msg[0];
				
				var netHostIP = json.netHostIP;
				
				// 打开云主机页面
				window.open("http://" + netHostIP + "/project/");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
			}
		});
	});
	
//	var getagwTree = function()
//	{
//	    // 接入网关树
//	    $("#agwTreeDiv").omTree(
//	    {
//		    dataSource : path + "/smallGwIn/getAllSmallGw?type=0",
//		    simpleDataModel: true,
//		    showCheckbox : false,
//		    showIcon : true,
//		    onSelect : function(nodeData)
//		    {
//			agwID = nodeData.id;
//			smallId = agwID;
//			agwStatus = nodeData.status;
//			goToHome();
//		    },
//		    onSuccess : function(data)
//		    {
//			if(data.length > 0)
//			{
//			    var children = data[0].children;
//			    if (children.length > 0)
//			    {
//				$("#agwTreeDiv").omTree('select', children[0]);
//			    }
//			}
//			smallId = "";
//		    }
//	    });
//	}
	
//	getagwTree();
	
//	var getsegwTree = function()
//	{
//	    // 安全网关树
//	    $("#segwTreeDiv").omTree(
//	    {
//			dataSource : path + "/smallGwIn/getAllSmallGw?type=1",
//			simpleDataModel: true,
//			showCheckbox : false,
//			showIcon : true,
//			onSelect : function(nodeData)
//			{
//				segwID = nodeData.id;
//				smallId = segwID;
//				segwStatus = nodeData.status;
//				goToHome();
//			},
//			onSuccess : function(data)
//			{
//			    if(data.length > 0)
//			    {
//				var children = data[0].children;
//				if (children.length > 0)
//				{
//					$("#segwTreeDiv").omTree('select', children[0]);
//				}
//			    }
//			    smallId = "";
//			}
//	     });
//	}
	
	/**
	 * 查询网元信息
	 * type 1接入网关 2安全网关 3编排器 4网管配置
	 */
//	var query_gw = function(type)
//	{
//		if (1 == type)
//		{
//			$("#agwTreeDiv").css('display', '');
//			$("#segwTreeDiv").css('display', 'none');
//			$('#menuTd').css('width', '60%');
//			if (undefined != $('#agwTreeDiv').omTree('getSelected'))
//			{
//				$("#agwTreeDiv").omTree('select', $('#agwTreeDiv').omTree('getSelected'));
//			}
//		}
//		else if (2 == type)
//		{
//			$("#agwTreeDiv").css('display', 'none');
//			$("#segwTreeDiv").css('display', '');
//			$('#menuTd').css('width', '60%');
//			if (undefined != $('#segwTreeDiv').omTree('getSelected'))
//			{
//				$("#segwTreeDiv").omTree('select', $('#segwTreeDiv').omTree('getSelected'));
//			}
//		}
//		else if (3 == type || 4 == type||5 == type)
//		{
//			$("#agwTreeDiv").css('display', 'none');
//			$("#segwTreeDiv").css('display', 'none');
//			$('#menuTd').css('width', '100%');
//		}
//	}
	
	// 默认显示接入网关
//	query_gw(4);
	
	// 总网元父级菜单页签对象
	var allNeNum = 
	[
	 	'lteMenuPanel','omcMenuPanel'
	];
	
	// 总网元子级菜单页签对象
	var menuspath =
	[
	 	'setPara', 'sysConfig', 'roleinfo', 'userinfo', 'popedom', 'syslog', 'pwdconfig', 'backRule', 'backData', 
	 	'systemBackup', 'monitorConfig', 'monitorInfos', 'monitorInfo', 'hnbInfo', 'rpcController','rebootController', 
	 	'logSynCon', 'hnbsoftware', 'software', 'upgradehistory', 'lteactive', 'confirmAlarm', 'historyalarm', 
	 	'synchronizealarm', 'ltealarmFilter', 'opLogInfo', 'safeLogInfo', 'upLogInfo', 'hnbLogInfo'
	];
	
	// 主界面布局
	var element = $('#page').omBorderLayout(
	{
		panels : 
		[
	        {
				id : "north-panel",
				header : false,
				region : "north",
				height : 38
			},
			{
				id : "center-panel",
				header : false,
				region : "center"
			}, 
			{
				id : "west-panel",
				resizable : true,
				collapsible : true,
				title : i18n.prop('systemCatalog'),
				region : "west",
				width : 280
			}
		]

	});
	
//	var agwMenuPanel = 
//	[
//	 	{
//			id : "agwConfPanel",
//			title : i18n.prop('confPanel')
//		},
//	
//		{
//			id : "agwAlarmPanel",
//			title : i18n.prop('alarmPanel')
//		},
//	
//		{
//			id : "agwPerformancePanel",
//			title : i18n.prop('performancePanel')
//		},
//	
//		{
//			id : "agwUpgradePanel",
//			title : i18n.prop('upgradePanel')
//		},
//		
//		{
//			id : "agwMaitenancePanel",
//			title : i18n.prop('maitenancePanel')
//		}
//	];

//	var segwMenuPanel = 
//	[
//	 	{
//			id : "segwConfPanel",
//			title : i18n.prop('confPanel')
//		},
//	
//		{
//			id : "segwAlarmPanel",
//			title : i18n.prop('alarmPanel')
//		},
//	
//		{
//			id : "segwPerformancePanel",
//			title : i18n.prop('performancePanel')
//		},
//	
//		{
//			id : "segwUpgradePanel",
//			title : i18n.prop('upgradePanel')
//		},
//		
//		{
//			id : "segwMaitenancePanel",
//			title : i18n.prop('maitenancePanel')
//		}
//	];

//	var orgMenuPanel = 
//	[
//	 	{
//			id : "orgPanel",
//			title : i18n.prop('composer')
//		}
//	];
	
	var omcMenuPanel = 
	[
	 	{
			id : "omcConfPanel",
			title : i18n.prop('omcConfPanel')
		},
		
		{
			id : "omcSafePanel",
			title : i18n.prop('omcSafePanel')
		},
		
		{
			id : "smallGwPanel",
			title : i18n.prop('smallGwPanel')
		},
		
		{
			id : "dataBackPanel",
			title : i18n.prop('dataBackPanel')
		},
		
		{
			id : "coreHostPanel",
			title : i18n.prop('maitenancePanel')
		}
	];
	
	var lteMenuPanel = 
		[
//		 	{
//				id : "ltePanel",
//				title : 'FullStack'
//			},
			{
				id : "hnbInfoPanel",
				title : i18n.prop('hnbInfoPanel')
			},
			{
				id : "upgradePanel",
				title :i18n.prop('upgradePanel')
			},
			{
				id : "alarmPanel",
				title : i18n.prop('alarmPanel')
			},
			{
				id : "logInfoPanel",
				title : i18n.prop('logInfoPanel')
			}
		];
	
	var id= "";

//	$(agwMenuPanel).each(function(index, panel) 
//	{
//		$("#" + panel.id).omPanel(
//		{
//			title : panel.title,
//			collapsed : false,
//			collapsible : true,
//
//			// 面板收缩和展开的时候重新计算自定义滚动条是否显示
//			onCollapse : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//			},
//			onExpand : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//			}
//			
//		});
//	});
	
//	$(segwMenuPanel).each(function(index, panel) 
//	{
//		$("#" + panel.id).omPanel(
//		{
//			title : panel.title,
//			collapsed : false,
//			collapsible : true,
//
//			// 面板收缩和展开的时候重新计算自定义滚动条是否显示
//			onCollapse : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//			},
//			onExpand : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//				id= panel.id;
//			}
//		});
//	});

//	$(orgMenuPanel).each(function(index, panel) 
//	{
//		$("#" + panel.id).omPanel(
//		{
//			title : panel.title,
//			collapsed : false,
//			collapsible : true,
//
//			// 面板收缩和展开的时候重新计算自定义滚动条是否显示
//			onCollapse : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//			},
//			onExpand : function() 
//			{
//				$("#west-panel").omScrollbar("refresh");
//			}
//		});
//	});
	
	$(omcMenuPanel).each(function(index, panel) 
	{
		$("#" + panel.id).omPanel(
		{
			title : panel.title,
			collapsed : false,
			collapsible : true,

			// 面板收缩和展开的时候重新计算自定义滚动条是否显示
			onCollapse : function() 
			{
				$("#west-panel").omScrollbar("refresh");
			},
			onExpand : function() 
			{
				$("#west-panel").omScrollbar("refresh");
			}
		});
	});
	
	$(lteMenuPanel).each(function(index, panel) 
	{
		$("#" + panel.id).omPanel(
		{
			title : panel.title,
			collapsed : false,
			collapsible : true,

			// 面板收缩和展开的时候重新计算自定义滚动条是否显示
			onCollapse : function() 
			{
				$("#west-panel").omScrollbar("refresh");
			},
			onExpand : function() 
			{
				$("#west-panel").omScrollbar("refresh");
			}
		});
	});
	
	// 定义全局变量
	var msgs = "";
	    
	var getRolePopedom = function()
	{
	    // 查询拥有权限的页面
	    $.ajax(
		{
			type : 'GET',
			url : path + '/popedom/getRolePopedom',
			success : function(msg)
			{
			    msgs = msg;
			    
			    // 默认安全网关菜单和编排器菜单隐藏
			    showOrHideMenu('lteMenuPanel');
			}
		});
	}
	
	getRolePopedom();
	
	// 显示和隐藏菜单
	var showOrHideMenu = function(curr)
	{
		var menupath = [];
		
		// 判断是否为空
		if(msgs.indexOf("/") != -1)
		{
			// 以"/"来分割获取的值
			menupath = msgs.split("/");
			
			var menupaths = [];
			
			var len = menupath.length;
			
			// 将数组中包含页面地址的值存放到另一个数组中
			for(var k = 1; k < len; k+=2)
			{
				menupaths.push(menupath[k]);
			}
			
			for (var i = 0; i <= allNeNum.length; i++)
			{
				if (allNeNum[i] == curr)
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('open');
					});
					
					for(var j = 0; j <= menuspath.length; j++)
					{
						if(menupaths.indexOf(menuspath[j]) != -1)
						{
						$(eval(menuspath[j])).each(function(index, panel)
						{
							$("#" + panel.id).show();
						});
						}
						else
						{
						$(eval(menuspath[j])).each(function(index, panel)
						{
							$("#" + panel.id).hide();
						});
						}
						
					}
				}
				else
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('close');
					});
				}
			}
		}
		else if(msgs.indexOf("root") != -1)
		{
			for (var i = 0; i <= allNeNum.length; i++)
			{
				if (allNeNum[i] == curr)
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('open');
					});
					
					for(var j = 0; j <= menuspath.length; j++)
					{
						$(eval(menuspath[j])).each(function(index, panel)
						{
							$("#" + panel.id).show();
						});
					}
				}
				else
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('close');
					});
				}
			}
		}
		else
		{
			for (var i = 0; i <= allNeNum.length; i++)
			{
				if (allNeNum[i] == curr)
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('open');
					});
					
					for(var j = 0; j <= menuspath.length; j++)
					{
						$(eval(menuspath[j])).each(function(index, panel)
						{
						$("#" + panel.id).hide();
						});
						
					}
				}
				else
				{
					$(eval(allNeNum[i])).each(function(index, panel)
					{
						$("#" + panel.id).omPanel('close');
					});
				}
			}
		}
	}
	
	// 父级页签菜单
	var headTabs = $("#headTabs").omTabs(
	{
		width : '100%',
		height : '100%',
		closeFirst: false,
		closable : true,
		tabWidth : 80
	});
	
	// 默认安全网关菜单和编排器菜单隐藏
	showOrHideMenu('lteMenuPanel');
	
	// 功能页签
	var tabs = $("#tabs").omTabs(
	{
		width : '100%',
		height : '100%',
		tabMenu : true,
		closeFirst: false,
		closable : true,
		tabWidth : 100
	});
	
	// 业务网关单击事件
//	$('#agwTab').click(function()
//	{
//		showOrHideMenu('agwMenuPanel');
//		getagwTree();
//		query_gw(1);
//	});

	// 安全网关单击事件
//	$('#segwTab').click(function()
//	{
//		showOrHideMenu('segwMenuPanel');
//		getsegwTree();
//		query_gw(2);
//	});

//	// 编排器单击事件
//	$('#orgTab').click(function()
//	{
//		showOrHideMenu('orgMenuPanel');
//		query_gw(3);
//	});

	// 网管设置单击事件
	$('#confTab').click(function()
	{
		showOrHideMenu('omcMenuPanel');
//		query_gw(4);
	});
	
	//LTE单击事件
	$('#fullTab').click(function()
	{
		showOrHideMenu('lteMenuPanel');
//		query_gw(5);
	});
	
	//LTE单击事件
	$('#hnbsoftware').click(function()
	{
	    	// 获取基站信息
		$.ajax(
		{
			type : 'POST',
			url : '../hnbInfo/getAllHnbInfo',
			dataType : 'json',
			data : 'type=0&start=0&limit=15'
		});
	});

	// 把首页的关闭图标去掉
	$('#headTabs').find('div ul li a').find('+ a.om-icon-close').remove();
    $("#firstTab").find("+ a.om-icon-close").remove();
    
    // 给logo右边的图片增加样式
	changeHeaderImg();
	
	// 回到主页
	$("#to_home").click(function()
	{
		$('#tabs').omTabs('activate', "firstTab");
	});
	
	// 修改密码窗体
	var dialog = $("#update_pwd_dialog-modal").omDialog(
	{
		autoOpen: false,
		height: 175,
		modal: true,
		width: 500,
		resizable : false,
		onClose : function(event)
		{
			// 清除红框样式
			$("input").removeClass("x-form-invalid");
		}
	});
	
	// 验证规则(数字和字母)
	var rules =
	{   
		oldPwd :	
		{
			required : true
		},
		newPwd : 
		{
			required : true,
			lengthrange : [6, 12],
			//isLetterAndNum : true
			checkPwdComplexity : true
		},
		affirmPwd : 
		{
            required : true,
            equalTo : '#newPwd'
        }
	};
	
	// 验证后的提示消息
	var messages =
	{
		oldPwd :	
		{
			required : i18n.prop('inputRequired')
		},
		newPwd : 
		{
			required : i18n.prop('inputRequired'),
			lengthrange : i18n.prop('newPwdRange')
		},
		affirmPwd : 
		{
            required : i18n.prop('inputRequired'),
            equalTo : i18n.prop('affirmPwdEqualTo')
        }
	};
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, messages, 'errorMsg', 'pwdForm');

	// 根据密码复杂度显示lable提示内容
	var getPwdFormat = function()
	{
		if (pwdComplexity == "1")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatOne'));
		}
		if (pwdComplexity == "2")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatTwo'));
		}
		if (pwdComplexity == "3")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatThree'));
		}
		if (pwdComplexity == "4")
		{
			$('#lbPwdFormat').text(i18n.prop('lbPwdFormatFour'));
		}
	}
	// 查询密码策略
	var getPwdConfig = function()
	{
		$.ajax(
		{
			type : 'POST',
			url : path + '/safeConfig/getSafeConfig',
			dataType : 'json',
			success : function(msg)
			{
				if (msg.length > 0)
				{
					// 查询到的JSON对象
					var json = msg[0];
					pwdComplexity = json.pwdComplexity;
					pwdLength = json.pwdLength;
					getPwdFormat();
				}
			},
			error : function(XMLHttpRequest, textStatus, errorThrown)
			{
				// 提示数据加载失败
				alertError();
			}
		});
	}
	// 修改密码按钮
	$("#set_user").click(function()
	{
		// 恢复Form到验证前的状态
		validator.resetForm();
		// 获取密码策略信息（复杂度和长度） 
		getPwdConfig();
		dialog.omDialog("option", "title", i18n.prop('updatePwdTitle'));
		dialog.omDialog('open');
	});
	
	// 设置密码按钮
	$("#update_pwd").omButton(
	{
		label : i18n.prop('btnEdit'),
		width : 60,
		onClick : function(event)
		{
			submitDialog();
			this.blur();
			return false;
		}
	});
	
	// dialog中点提交按钮时将数据提交到后台并执行相应的add或modify操作
	// 0--添加，1--修改
	var submitDialog = function()
	{
		if (validator.form())
		{
			$.omMessageBox.confirm(
			{
				title : i18n.prop('alert'),
				content : i18n.prop('modifyContent'),
				onClose : function(value)
				{
					if(value)
					{
						var submitData =
						{
							oldPwd : $("#oldPwd", dialog).val(),
							newPwd : $("#newPwd", dialog).val()
						};
						
						// 检查session是否过期
						// parent.checkSession();
						
						// 提交数据到后台
						$.post(path + "/index/modifyPwd", submitData, function(msg)
						{
							if(msg == '0')
							{
								 $.omMessageBox.alert(
								 {
									 content:i18n.prop('pwdSuccess'),
									 onClose:function(value)
									 {
					                    // 跳转到登录页面	
										currpage = self.location.href;
										currpage = currpage.replace("/login","");
										window.location.href = currpage;
					                }
								});
							}
							else if(msg == '2')
							{
								$.omMessageBox.alert(
								{
									type : 'warning',
									title : i18n.prop('alert'),
									content : i18n.prop('oldPwdError'),
									onClose : function(v)
									{
										$("#oldPwd").focus();
									}
								});
							}
							else if(msg == '3')
							{
								$.omMessageBox.alert(
								{
									type : 'warning',
									title : i18n.prop('alert'),
									content : i18n.prop('pwdEqual'),
									onClose : function(v)
									{
										$("#oldPwd").focus();
									}
								});
							}
							else
							{
								$.omMessageTip.show(
								{
									type : 'error',
									title : i18n.prop('alert'),
									content : i18n.prop('pwdFail'),
									timeout : 1500
								});
							}
							
							// 清除红框样式
							$("input").removeClass("x-form-invalid");
						}, "text");
					}
				}
			});
		}
	};
	
	// 取消修改密码按钮
	$("#cancel_pwd").omButton(
	{
		label : i18n.prop('btnDiaCancel'),
		width : 60,
		onClick : function(event)
		{
			// 清空输入框并关闭窗体
			$("#update_pwd_dialog-modal").omDialog('close');

			// 清除红框样式
			$("input").removeClass("x-form-invalid");
			this.blur();
			return false;
		}
	});
	
	// 刷新
	$("#refresh_page").click(function()
	{
		goToHome();
	});
	
	/**
	 * 回到首页
	 */
	var goToHome = function()
	{
		// 关闭其它页签，刷新首页页签
		var self = tabs, $headers = self.find('>div.om-tabs-headers');

		$headers.find('ul li').each(function(index, item) 
		{
			var itemId = $(item).find('a.om-tabs-inner').attr('tabid');
			if ("firstTab" === itemId)
			{
				return;
			}
			self.omTabs('close', (self.omTabs('getAlter', itemId)));
		});
	}
	
	// 导航退出
	$("#logout").click(function()
	{
		checkSession();
		var checkFlag = $.ajax(
		{
			url : path + '/index/logout',
			async : false
		});
		window.location = path + "/index";
	});

	// 修改个人资料
	$("#update_info").click(function()
	{
		$("#update_pwd_dialog-modal").omDialog('open');
	});
	
	//showLockDialog();
});

//刷新网元树
function refreshGwTree()
{
	$("#segwTreeDiv").omTree('refresh');
	$("#agwTreeDiv").omTree('refresh');
}
		
// 单击菜单项时，增加一个tabpanel
function addItemToTab(url, obj, title) 
{
	// 请求之前先判断一下session是否过期
	var flag = checkSession();
	
	var flags = checkValid();
	
	// 如果session超时，tab不增加内容
	if (!flag)
	{
		return ;
	}
	
	// 如果超过有效期，tab不增加内容
	if (!flags)
	{
		return ;
	}
	
	if(smallId != null && smallId != "")
	{
	    $.ajax(
	    {
		method : 'POST',
		url : path + '/smallGwIn/getAllSmallGwStatus?smallId=' + smallId,
		data : 'json',
		success : function(msg)
		{
		    if(msg.length > 0)
		    {
			var msgs = eval(msg);
			agwStatus = msgs[0].status;
			segwStatus = msgs[0].status;
		    }
		}
	    });
	}
	
	var urltemp = url;
	var tabid = urltemp.replaceAll("/", "");
	tabid = tabid.replace("\.", "");
	
	var count = $('#tabs').omTabs('getLength')
	if($("a[tabId='" + tabid + "']").length <= 0 && count > 20)
	{
		$.omMessageBox.alert(
		{
			type : 'warning',
			title : i18n.prop('alert'),
			content : i18n.prop('tabMaxConut')
		});
		return;
	}
	
	if ($("a[tabId='" + tabid + "']").length > 0) 
	{
		$('#tabs').omTabs('activate', tabid);
	} 
	else 
	{
		var titles = "";
		if(null != title && undefined != title && title.length > 0)
		{
			titles = title;
		}
		else
		{
			titles = obj.innerHTML;
		}
		
		url = path + url;
		
		urls = url;
		$('#tabs').omTabs('add', 
		{
			tabId : tabid,
			title : titles,
			closable : true,
			tabMenu : true,
			border : false,
			content : "<iframe src ='" + url + "' id='" + tabid + "' name='" + tabid
					+ "'  marginwidth=0 style='overflow:hidden;' marginheight=0 width=100% height='' " 
					+ " frameborder='no' border=0 ></iframe>"
		});
		
		contents = "<iframe src ='" + url + "' id='" + tabid + "' name='" + tabid
		+ "'  marginwidth=0 style='overflow:hidden;' marginheight=0 width=100% height='' " 
		+ " frameborder='no' border=0 ></iframe>"
		
	}
}

function lockdialog()
{
    	// 系统锁定窗体
	var lock_dialog = $("#system_lock_dialog").omDialog(
	{
		autoOpen: false,
		height: 170,
		modal: true,
		width: 380,
		resizable : false,
		closeOnEscape : false
	});
	
	// 打开系统锁定窗体
	var showLockDialog = function()
	{
	    	$("#lockPwd").val(""),
		lock_dialog.omDialog("option", "title", i18n.prop('systemLocked'));
		lock_dialog.omDialog('open');
		$(".om-dialog .om-dialog-titlebar-close span").css("display","none");
	}
	
	// 解除锁定按钮单击事件
	$("#release_lock").omButton(
	{
		label : i18n.prop('btnSubmit'),
		width : 60,
		onClick : function(event)
		{
			password = $("#lockPwd").val();
			
			if(password == null || password == "")
			{
				$.omMessageBox.alert(
				{
					type : 'alert',
					title : i18n.prop('alert'),
					content : i18n.prop('pwdEmpty')
				});
			}
			else
			{
				var submitData =
				{
					userName : loginName,
					password : $("#lockPwd").val(),
				};
					
				$.ajax(
				{
					method : 'POST',
					url : path + '/index/login',
					data : submitData,
					success : function(msg)
					{
						if(msg.indexOf("frame.js") != -1)
						{
							$("#system_lock_dialog").omDialog('close');
						}
						else
						{
							$.omMessageBox.alert(
							{
								content : i18n.prop('pwdError'),
								onClose : function(v) 
								{
									$("#lockPwd").val("");
									$("#lockPwd").focus();
								}
							});
						}
					}
				});
			}
			return false;
		}
	});
	showLockDialog();
}

/**
 * 检查session是否过期 
 * 如果过期，直接跳到登录页面
 * @param {} event
 * @return {Boolean}
 */
function checkSession() 
{
	var checkFlag = jQuery.ajax(
	{
		url : path + '/index/checkSession',
		async : false
	}).getResponseHeader("sessionStatus");

	if ("expired" == checkFlag || null == checkFlag) 
	{
	    lockdialog();
	    return false;
	}
	return true;
}

/**
 * 检查用户有效期是否过期 
 * 如果过期，直接跳到登录页面
 * @param {} event
 * @return {Boolean}
 */
function checkValid() 
{
	var checkFlag = jQuery.ajax(
	{
		url : path + '/index/checkValid',
		async : false
	}).getResponseHeader("valid");

	if ("OUT" == checkFlag) 
	{
		$.omMessageBox.alert(
		{
			content : i18n.prop('userisoutvalid'),
			onClose : function(v) 
			{
			    var checkFlag = $.ajax(
			    {
				url : path + '/index/logout',
				async : false
			    });
			    window.location = path + "/index";
			}
		});
		return false;
	}
	return true;
}

/**
 * 鼠标移入移出时改变图片的样式
 */
function changeHeaderImg() 
{
	var oLi = $(".right_imgs_css");
	for (var i = 0; i < oLi.length; i++) 
	{
		oLi[i].onmouseover = function() 
		{
			$(this).addClass("header_alpha");
		};
		oLi[i].onmouseout = function() 
		{
			$(this).removeClass("header_alpha");
		}
	}
}

/**
 * 设置iframe的高度（目前只考虑ff） 用name属性，可兼容所有浏览器
 */
function setIframeHeight(iframe_id, iframe_height) 
{
	document.getElementById(iframe_id).height = iframe_height;
}
