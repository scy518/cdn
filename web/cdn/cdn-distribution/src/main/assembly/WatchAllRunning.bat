@echo off

title WatchAllRunning

set currPath=
set _task=nginx.exe

for /f "tokens=1,2,* " %%i in ('REG QUERY "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" ^| find /i "WatchAllRunning.bat"') do set "installPath=%%k" 
echo installPath%installPath%

set currPath=%installPath:~0,-20%
echo %currPath%
cd /d %currPath%

:checkstart
echo %time%

echo Wscript.Sleep WScript.Arguments(0) >%tmp%/delay.vbs
cscript //b //nologo %tmp%/delay.vbs 30000

echo Watching httpfs...
set port=8214
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping httpfs
rem start /D httpfs stop.bat

echo ### Starting httpfs
start /D httpfs run.bat
)

echo Watching ITMSIUI...
set port=8202
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping ITMSIUI
rem start /D itmsiui stop.bat

echo ### Starting ITMSIUI
start /D itmsiui run.bat
)

echo Watching OpenResty...
set num=0
for /f "tokens=1" %%n in ('tasklist /nh /fi "imagename eq nginx.exe"') do (
if %%n==%_task% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping OpenResty
rem start /D  shutdown.bat

echo ### Starting OpenResty
start /D openresty run.bat
)

echo Watching Rc...
set port=8212
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Rc
rem start /D rc stop.bat

echo ### Starting Rc
start /D rc run.bat
)

echo Watching Sm...
set port=8208
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Sm
rem start /D sm stop.bat

echo ### Starting Sm
start /D sm run.bat
)

echo Watching Tr069...
set port=8210
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Tr069
rem start /D tr069 stop.bat

echo ### Starting Tr069
start /D tr069 run.bat
)

echo Watching Sm...
set port=8204
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Usmc
rem start /D usmc stop.bat

echo ### Starting Ussmc
start /D usmc run.bat
)

echo Watching Wm...
set port=8213
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Wm
rem start /D wm stop.bat

echo ### Starting Wm
start /D wm run.bat
)

echo Watching Stun...
set port=8215
set num=0
set protocol=TCP
for /f "tokens=1" %%n in ('netstat -ano ^| findstr %port% ^| findstr TCP') do (
if %%n==%protocol% (set /a num+=1)
)
if %num% EQU 0 (
echo ### Stopping Wm
rem start /D stun stop.bat

echo ### Starting Wm
start /D stun run.bat
)

goto checkstart
