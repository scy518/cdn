@echo off

set currPath=

for /f "tokens=1,2,* " %%i in ('REG QUERY "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run" ^| find /i "WatchAllRunning.bat"') do set "installPath=%%k" 
echo installPath%installPath%

set currPath=%installPath:~0,-20%
echo %currPath%
cd /d %currPath%

echo %time%

taskkill /f /fi "WINDOWTITLE eq WatchAllRunning"

echo Wscript.Sleep WScript.Arguments(0) >%tmp%/delay.vbs
cscript //b //nologo %tmp%/delay.vbs 3000

taskkill /f /fi "WINDOWTITLE eq WatchAllRunning"

