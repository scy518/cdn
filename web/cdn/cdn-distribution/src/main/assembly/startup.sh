#!/bin/bash
DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME

#ipAddrs=`ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $    2}'|tr -d "addr:"`
#echo "IP Addresses List , Please Choose one for FTP Server by number:"
#echo "Please input serial number to choose a IP address for FTP server:"
#j=0
#for i in $ipAddrs
#do
#    array_ip[$j]=$i
#    ((j=j+1))
#    echo "$j:$i"
#done
#
#read Arg
#flag=2
#for x in `seq $j`
#do
#    if [ $Arg -eq $x ];then
#        flag=1
#    fi
#done

#echo "\n\n### Starting ftpServer..."
#cd ./ftpserver
#./run.sh ${array_ip[$Arg-1]} $flag &
#cd $RUNHOME

echo "\n\n### Starting USMC..."
cd $RUNHOME
cd ./usmc
./run.sh &
cd $RUNHOME

echo "\n\n### Starting cdnIUI..."
cd ./cdniui
./run.sh &
cd $RUNHOME

echo "\n\n### Starting bmc..."
cd ./bmc
./run.sh &
cd $RUNHOME

echo "\n\n### Starting OpenResty..."
cd ./openresty
./run.sh &
cd $RUNHOME

# echo "\n\n### Starting Sm..."
# cd ./sm
# ./run.sh &
# cd $RUNHOME

echo "Startup will be finished in background...";
echo " + Wait a minute";
echo " + Open 'http://<HOST>' in your browser to access the ITMS
stem";
