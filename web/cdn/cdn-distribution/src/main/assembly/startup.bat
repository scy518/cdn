@echo off
title CDN Management system

set RUNHOME=%~dp0

rem echo ### Starting ftpServer
rem start /B/D ftpserver run.bat

echo ### Starting USMC
start /B/D usmc run.bat

echo ### Starting cdnIUI
start /B/D cdniui run.bat

echo ### Starting OpenResty
start /B/D openresty run.bat

rem echo ### Starting Sm
rem start /B/D sm run.bat

rem echo ### Starting Rc
rem start /B/D rc run.bat

rem echo ### Starting Wm
rem start /B/D wm run.bat

rem echo ### Starting Httpfs
rem start /B/D httpfs run.bat

rem echo ### Starting Stun server
rem start /B/D stun run.bat

echo ### Starting bmc
start /B/D bmc run.bat  

echo Startup will be finished in background...
echo  + Wait a minute
echo  + Open "http://<HOST>" in your browser to access the RTMS!
:finalend