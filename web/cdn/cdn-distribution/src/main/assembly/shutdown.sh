#!/bin/bash
DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME

echo "### Stopping RTMS...";

echo "\n\n### Stopping USMC..."
cd ./usmc
./stop.sh &
cd $RUNHOME

echo "\n\n### Stopping cdnIUI..."
cd ./cdniui
./stop.sh &
cd $RUNHOME

echo "\n\n### Stopping OpenResty..."
cd ./openresty
./stop.sh &
cd $RUNHOME

echo "\n\n### Stopping bmc..."
cd ./bmc
./stop.sh &
cd $RUNHOME


#echo "\n\n### Stopping ftpServer..."
#cd ./ftpServer
#./stop.sh &
#cd $RUNHOME

echo "Closing signal has been sent!";
echo "Stopping in background,wait for a moment";
sleep 3;