#!/bin/bash

stopNginx=/var/rtms/shutdown.sh
startNginx=/var/rtms/startup.sh

while true
do
    # echo " Watchdog Entering... "
    sleep 15
    nginxID=$(ps -ef |grep nginx | grep master | awk '{print $2}')
    if [ -z $nginxID ]; then
        # echo " nginx is stopping "
        $stopNginx
        sleep 5
        $startNginx
    else
        # echo " nginx is running... "
        
    fi
done
