@echo off
title stopping cdn cms...

set RUNHOME=%~dp0

echo ### Stopping USMC
start /B/D %RUNHOME%usmc stop.bat

echo ### Stopping cdnIUI
start /B/D %RUNHOME%cdniui stop.bat

echo ### Stopping OpenResty
start /B/D %RUNHOME%openresty stop.bat  

rem echo ### Stopping Sm
rem start /B/D %RUNHOME%sm stop.bat

rem echo ### Stopping Rc
rem start /B/D %RUNHOME%rc stop.bat

rem echo ### Stopping Wm
rem start /B/D %RUNHOME%wm stop.bat

rem echo ### Stopping Httpfs
rem start /B/D %RUNHOME%httpfs stop.bat

rem echo ### Stopping Stun Server
rem start /B/D %RUNHOME%stun stop.bat

echo ### Stopping bmc
start /B/D %RUNHOME%bmc stop.bat  

rem echo ### Stopping ftpServer
rem start /B/D %RUNHOME%ftpServer stop.bat

echo "Closing signal has been sent!";
echo "Stopping in background,wait for a moment";
:finalend