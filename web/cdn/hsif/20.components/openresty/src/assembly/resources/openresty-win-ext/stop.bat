@echo off
title close openresty-server
@if not "%ECHO%" == ""  echo %ECHO%
@if "%OS%" == "Windows_NT"  setlocal

set DIRNAME=.

if "%OS%" == "Windows_NT" set DIRNAME=%~dp0%

set ARGS=
:loop
if [%1] == [] goto endloop
        set ARGS=%ARGS% %1
        shift
        goto loop
:endloop

set HOME=%DIRNAME%nginx
set _NGINXCMD=%HOME%\nginx.exe


echo =========== openresty config info  ============================================
echo HOME=%HOME%
echo _NGINXCMD=%_NGINXCMD%
echo ===============================================================================


cd /d "%HOME%"
echo @WORK_DIR@%HOME%
echo @C_CMD@ "%_NGINXCMD% -s stop"

%_NGINXCMD% -s stop
echo closing signal has been sent,stopping in background,WAIT...
timeout /t 5 /nobreak > nul
exit
