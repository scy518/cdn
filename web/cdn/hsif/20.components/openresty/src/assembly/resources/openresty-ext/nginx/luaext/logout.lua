local cache = ngx.shared.ceryx
ngx.header["Set-Cookie"] = {"sessioncode="..ngx.var.cookie_sessioncode..";path=/; expires=31 Dec 2011 23:55:55 GMT",
			    "username="..ngx.var.cookie_username..";path=/; expires=31 Dec 2011 23:55:55 GMT"}
local succ, err, forcible = cache:delete(ngx.var.cookie_sessioncode)
if not succ then
	ngx.log(ngx.WARN, err)
end