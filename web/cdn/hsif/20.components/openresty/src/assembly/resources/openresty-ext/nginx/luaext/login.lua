local h = ngx.resp.get_headers()
if h["itmsauth"] and h["itmsauth"] == "true" then
	local cache = ngx.shared.ceryx
	local username = h["username"]
	local sessioncode = h["sessioncode"]
	local succ, err, forcible = cache:set(sessioncode, "place_holder", 0)
	if not succ then
		ngx.log(ngx.WARN, err)
	end	
	ngx.header["Set-Cookie"] = {"sessioncode="..sessioncode.."; expires=36000;path=/",
				    "username="..username.."; expires=36000;path=/"}
end	