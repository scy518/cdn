if ngx.req.get_method() == "GET" then
	local services = {
		{serviceName="umc",apiJson="/api/umcswagger/v1/swagger.json"},
		{serviceName="roc",apiJson="/api/roc/v1/swagger.json"},
		{serviceName="dac",apiJson="/api/dac/v1/swagger.json"}
	}
	local cjson = require "cjson"
	local jsonData = cjson.encode(services)
	jsonData = string.gsub(jsonData,"\\/","/")
	ngx.print(jsonData)
else
	ngx.log(ngx.WARN, "not a GET request.")
	ngx.exit(500)
end