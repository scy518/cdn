DIRNAME=`dirname $0`
RUNHOME=`cd $DIRNAME/; pwd`
echo @RUNHOME@ $RUNHOME

title UMC Database setup

echo ### Starting UMC Database setup

JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")
echo @JAVA_HOME@ $JAVA_HOME
JAVA="$JAVA_HOME/bin/java"
echo @JAVA@ $JAVA

class_path="$RUNHOME/:$RUNHOME/umc.jar"
echo @class_path@ $class_path

"$JAVA" -classpath "$class_path" org.openo.orchestrator.nfv.umc.UMCApp db migrate "$RUNHOME/conf/umc.yml"
