/**
 * <构造表单验证对象>
 * <功能详细描述>
 * @param rules 验证规则
 * @param messages 验证消息
 * @param className 显示消息的SPAN标记的class名称
 * @param formID 表单ID
 * @returns 返回验证对象
 * @see [类、类#方法、类#成员]
 */
var form_validator = function(rules, messages, className, formID)
{
	// 对表单进行校验
	var validator = $('#' + formID).validate(
	{
		rules : rules,
		messages : messages,
		errorPlacement : function(error, element)
		{
			if (error.html())
			{
				$(element).parents().map(function()
				{
					if (this.tagName.toLowerCase() == 'td')
					{
						var attentionElement = $(this).children().eq(1);
						attentionElement.html(error);
						attentionElement.css('display', 'none');
						attentionElement.prev().addClass("error-border");
						attentionElement.prev().children("input").addClass("x-form-invalid");
						if (attentionElement.prev().children().length <= 0)
						{
							attentionElement.prev().addClass("x-form-invalid");
						}
					}
					else if (this.tagName.toLowerCase() == 'span')
					{
						var attentionElement = $(this).next();
						attentionElement.html(error);
						attentionElement.css('display', 'none');
						$(this).addClass("x-form-invalid");
						$(this).removeClass('om-state-default');
					}
				});
			}
		},
		// 控制错误显示隐藏的方法，当自定义了显示方式之后一定要在这里做处理。
		showErrors : function(errorMap, errorList)
		{
			if (errorList && errorList.length > 0)
			{
				$.each(errorList, function(index, obj)
				{
					var msg = this.message;
					$(obj.element).parents().map(function()
					{
						if (this.tagName.toLowerCase() == 'td')
						{
							var attentionElement = $(this).children().eq(1);
							attentionElement.show();
							attentionElement.html(msg);
						}
						else if (this.tagName.toLowerCase() == 'span')
						{
							var attentionElement = $(this).next();
							attentionElement.show();
							attentionElement.html(msg);
						}
					});
				});
			}
			else
			{
				$(this.currentElements).parents().map(function()
				{
					if (this.tagName.toLowerCase() == 'td')
					{
						$(this).children().eq(1).hide();
						$(this).children().eq(0).removeClass("error-border");
					}
					else if (this.tagName.toLowerCase() == 'span')
					{
						$(this).next().hide();
						$(this).next().find("label").css('display', 'none');
						$(this).removeClass("x-form-invalid");
						$(this).addClass("om-state-default");
					}
					else
					{
						$(this).removeClass("error-border");
					}
					$(this).children().eq(0).removeClass("x-form-invalid");
				});
			}
			this.defaultShowErrors();
		}
	});
	
	// 获取字符串长度，其中数字、字符占一个字节，汉字占两个字节
	var getLength = function(str) 
	{
		var length = 0;
		for(var i = 0; i < str.length; i++) 
		{
			var c = str.charCodeAt(i);
			if((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f))
			{
				length++;
			} 
			else 
			{
				length += 2;
			}
		}
		return length;
	}
	
	// 添加IP验证函数
	$.validator.addMethod("isIp", function(value) 
	{
		if(null != value && undefined != value && "" != value)
		{
	        var regu = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/; 
	        var reg = new RegExp(regu);
	        return reg.test(value);
		}
		else
		{
			return true;
		}
	}, '不是有效的IP地址');
	
	// 自定义验证只能含有汉字、数字、字母格式方法
	$.validator.addMethod("isChineseOrNumberOrChar", function(value) {
		var flag = true;
		if(undefined != value && ""!=value)
		{
			// 包含汉字、数字、字母正则表达式
			var regu =/^[A-Za-z0-9\u4e00-\u9fa5]+$/; 
			var reg = new RegExp(regu);
			flag = reg.test(value);
		}
		return flag;
	}, '只能是字母或数字或汉字组成');

	// 添加IP验证函数
	$.validator.addMethod("lengthrange", function(value, obj, lengthRange) 
	{
		var length = getLength(value);
		if(length < lengthRange[0] || length > lengthRange[1])
		{
	        return false;
		}
		else
		{
			return true;
		}
	}, '不是有效的长度范围');
	
	$.validator.addMethod("isNum",function(value)
	{
		if(null != value && undefined != value && "" != value)
		{
			var regu =/^([-]?[0-9]){1,}$/; 
			var reg = new RegExp(regu);
			return reg.test(value);
		}
		else
		{
			return true;
		}
	}, '经度、维度必须为数字');
	
	// 给输入框绑定鼠标经过事件，鼠标移动过去才显示校验信息
	$('.' + className).prev().bind('mouseover', function(e)
	{
		// 要有错误才显示
		if ($(this).next().html().length > 0 && $(this).next().find("label").html().length > 0)
		{
			$(this).next().css('display', 'inline').css(
			{
				'top' : e.pageY + 10,
				'left' : e.pageX + 5
			});
			
			if ($(this).next().find("label").css('display') == 'none')
			{
				$(this).next().hide();
			}
		}
		
	}).bind('mouseout', function()
	{
		$(this).next().css('display', 'none');
	});
	
	return validator;
}
