(function($)
{
	/**
	 * 为表格增加分页下拉
	 */
	$.omWidget.addInitListener('om.omGrid', function()
	{
		var grid = this;
		var op = this.options;
		var limit = op.limit;
		var limits = op.limits ||
		[
			10, 15, 20, 50, 100
		];
		if ($.inArray(limit, limits) == -1)
		{
			limits.push(op.limit);
		}
		// .pLimit
		var html = "<div class='pGroup'>";
		html += "<div class='pLimit'>";
		html += i18n.prop('eachPage');
		html += "<select style='cursor:pointer'></select>";
		html += i18n.prop('article');
		html += "</div>";
		html += "</div>";
		html += "<div class='btnseparator'></div>";
		var limitDiv = $(html);
		var select = $("select", limitDiv);
		$.each(limits, function(i, v)
		{
			select.append($("<option></option>").attr("value", v).text(v));
		});
		select.val(limit);
		select.change(function()
		{
			grid.option("limit", parseInt(select.val()));
			// 需要跳转到第1页
			grid.reload(1);
		});
		
		// 插到分页条中
		$($(".pDiv2 .btnseparator", this.pDiv)[2]).after(limitDiv);
	});
})(jQuery);
