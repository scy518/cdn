说明：
此工程用于各服务模块UI开发，完成后统一打包入平台。

具体开发细节参考文档《ITMS UI集成方案.docx》
文档路径：https://192.168.11.90/svn/RAP/rtms_doc/ITMS UI集成方案.docx

js、css引用（已包含omui、echarts），国际化实现可参考已有工程
公共css路径：webroot/component/css
公共js路径：webroot/component/thirdparty

umc-monitor为参考工程

上库路径后期提供