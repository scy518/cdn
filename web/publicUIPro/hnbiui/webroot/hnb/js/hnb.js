$(document).ready(function()
{
	// 初始化设置界面语言
	loadLanguage();
	
	// 创建表格
	$("#com_raisecom_itms_hnb_data").omGrid(
	{
		dataSource : "http://127.0.0.1:8207/api/v1/hnbs/findByPage",// 调试写法，正式版本写成：/api/v1/hnbs/findByPage 即可
		limit : 10,
		height : 355,
		singleSelect : true,
		showIndex : false,
		colModel : 
		[
		    {
				header : hnb_i18n.prop("com_raisecom_itms_hnb_tdHnbName"),
				name : 'hnbName',
				width : 180,
				align : 'left'
			},
			
			{
				header : hnb_i18n.prop("com_raisecom_itms_hnb_tdHnbIpAddr"),
				name : 'hnbIpAddr',
				width : 180,
				align : 'left'
			},
			
			{
				header : hnb_i18n.prop("com_raisecom_itms_hnb_tdHnbState"),
				name : 'hnbState',
				width : 180,
				align : 'left',
				renderer : function(v, rowData , rowIndex)
				{
					if("1" == v)
					{
						return hnb_i18n.prop("com_raisecom_itms_hnb_tdHnbState1");
					}
					else
					{
						return hnb_i18n.prop("com_raisecom_itms_hnb_tdHnbState2");
					}
				}
			}
		 ],
		onRefresh : function(data, testStatus, XMLHttpRequest, event)
		{
			// 隐藏勾选框
			$(".checkboxCol").hide();
		}
	});
	
	// 控制表格字段的显示
	//controller_table_filed("../systemUsersServlet?method=gridLevel", "systemUsers", colModel, parent.loginName);
	
	
	// 修改信息
	var updateHnbInfo = function()
	{
		var selections = $('#com_raisecom_itms_hnb_data').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
			return false;
		}
		isAdd = false;
		
		showDialog(hnb_i18n.prop('com_raisecom_itms_hnb_mdy'), selections[0]);
	};
	
	// 删除信息
	var deleteHnbInfo = function()
	{
		var selections = $('#com_raisecom_itms_hnb_data').omGrid('getSelections', true);
		if (selections.length == 0)
		{
			alertUnCheckWarn();
		}
		else
		{
			$.omMessageBox.confirm(
			{
				title : hnb_i18n.prop('com_raisecom_itms_hnb_alert'),
				content : hnb_i18n.prop('com_raisecom_itms_hnb_deleteContent'),
				onClose : function(value)
				{
					if (value)
					{
						// hnb ID
						var request = {   
				            id: selections[0].id 
					    };
						
						// 提交删除请求
						$.ajax(
						{
							type : 'POST',
							url : 'http://127.0.0.1:8207/api/v1/hnbs/deleteHnbsById',// 调试写法，正式版本写成：/api/v1/hnbs/deleteHnbsById 即可
							dataType : 'json',
							data : JSON.stringify(request),
							success : function(msg)
							{
								// 提示消息
								if (msg.result == "0")
								{
									// 更新表格
									$('#com_raisecom_itms_hnb_data').omGrid('reload');
									
									// 删除成功提示
									alertSuccess();
								}
								else
								{
									// 删除失败提示
									alertFail();
								}
							}
						});
					}
				}
			});
		}
	};
	
	// 新增按钮
	$("#com_raisecom_itms_hnb_add").click(function()
	{
		isAdd = true;
		showDialog(hnb_i18n.prop('com_raisecom_itms_hnb_add'));
	});
	
	// 修改按钮
	$("#com_raisecom_itms_hnb_mdy").click(function()
	{
		updateHnbInfo();
	});
	
	// 删除按钮
	$("#com_raisecom_itms_hnb_del").click(function()
	{
		deleteHnbInfo();
	});
	
	// 定义修改或添加的弹出框
	var dialog = $("#dialog-form").omDialog(
	{
		width : '50%',
		autoOpen : false,
		modal : true,
		resizable : false,
		onClose : function(event)
		{
			// 清除红框样式
			$("input").removeClass("x-form-invalid");
		}
	});
	
	// 显示修改或增加的弹出框
	var showDialog = function(title, rowData)
	{
		validator.resetForm();
		
		// 设置表格提示信息
		var trs = $('table tr', dialog);
		$($('td', trs[0])[1]).find('label').text(hnb_i18n.prop('com_raisecom_itms_hnb_nameLen'));
		
		rowData = rowData || {};
		
		$('input[name=id]', dialog).val(rowData.id);
		$('input[name=hnbIpAddr]', dialog).val(rowData.hnbIpAddr);
		$('input[name=hnbName]', dialog).val(rowData.hnbName);
		$('input[name=hnbState]', dialog).val(rowData.hnbState);
		
		dialog.omDialog("option", "title", title);
		dialog.omDialog("open");
	};
	
	// 弹出框取消按钮
	$('#com_raisecom_itms_hnb_cancel').click(function()
	{
		$("#dialog-form").omDialog("close");
		
		// 清除红框样式
		$("input").removeClass("x-form-invalid");
		return false;
	});
	
	// 弹出框确定按钮：添加or修改
	$('#com_raisecom_itms_hnb_submit').click(function()
	{
		dataSubmitDialog();
		return false;
	});
	
	// 弹出框确定按钮的具体操作
	var dataSubmitDialog = function()
	{
		if (validator.form())
		{
			var selections = $('#com_raisecom_itms_hnb_data').omGrid('getSelections', true);
			var submitData =
			{
				hnbIpAddr : $("input[name=hnbIpAddr]", dialog).val(),
				hnbName : $("input[name=hnbName]", dialog).val(),
				hnbState : $("input[name=hnbState]", dialog).val()
			};
			
			var urls = "";
			if(isAdd)
			{
				urls = 'http://localhost:8207/api/v1/hnbs/insert';// 调试写法，正式版本写成：/api/v1/hnbs/insert 即可
			}
			else
			{
				submitData.push({id : selections[0].id});
				urls = 'http://localhost:8207/api/v1/hnbs/update';// 调试写法，正式版本写成：/api/v1/hnbs/update 即可
			}
			
			$.ajax(
			{
			    type : 'POST',
			    url : urls,
			    data : JSON.stringify(submitData),
			    contentType : 'application/json',
			    success: function (msg)
			    {
			    	// 查询到的JSON对象
					if (msg.result == "0")
					{
						alertSuccess();
						// 关闭dialog
						$("#dialog-form").omDialog("close");
						
						// 清除红框样式
						$("input").removeClass("x-form-invalid");
						
						// 更新表格
						$('#com_raisecom_itms_hnb_data').omGrid('reload');
					}
					else
					{
						alertFail();
					}
					
			    	
			    }
			});
		}
	};
	
	// 过滤字段
	var rules  = 
	{
		hnbName :
		{
			required : true,
			lengthrange : [1, 32]
		},
		hnbIpAddr :
		{
			required : true,
			isIp : true
		}
	};
	
	// 提示消息
	var errMsgs = 
	{
		hnbName :
		{
			required : hnb_i18n.prop('com_raisecom_itms_hnb_nameReq'),
			lengthrange : hnb_i18n.prop('com_raisecom_itms_hnb_nameTooLong')
		},
		hnbIpAddr :
		{
			required :  hnb_i18n.prop('com_raisecom_itms_hnb_ipReq'),
			isIp :  hnb_i18n.prop('com_raisecom_itms_hnb_ipNotRight')
		}
	};
	
	
	// 调用验证方法，创建验证对象
	var validator = new form_validator(rules, errMsgs, 'errorMsg', 'hnbGroupForm');
	
});

